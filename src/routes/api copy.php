<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/stores/upload-company-logo', 'StoreAPIController@uploadCompanyLogo')->name('stores.upload_company_logo');
    Route::post('/stores/upload-background', 'StoreAPIController@uploadRedirectPageBackground')->name('stores.upload_background');
    Route::post('/stores/download-qr', 'StoreAPIController@downloadQR')->name('stores.download_qr');
   // Route::get('/stores/download-qr/{url}', 'StoreAPIController@downloadQR')->name('stores.download_qr');
    Route::resource('stores', 'StoreAPIController');

    Route::get('stores/{id}/smsMessage', 'StoreAPIController@smsMessage');
    Route::get('stores/{id}/smsMessageOther', 'StoreAPIController@smsMessageOther');
    Route::get('stores/{id}/emailMessage', 'StoreAPIController@emailMessage');
    Route::get('stores/{id}/mmsMessage', 'StoreAPIController@mmsMessage');

    Route::resource('store_users', 'StoreUserAPIController');

    Route::get('/settings/by-user', 'SettingAPIController@getSettingByUser')->name('settings.by_user');
    Route::resource('settings', 'SettingAPIController');

    Route::get('/review_providers/by-user/{provider}', 'ReviewProviderAPIController@getProviderByUser')->name('review_providers.by_user.provider');
    Route::get('/review_providers/by-user', 'ReviewProviderAPIController@getListProviderByUser')->name('review_providers.by_user');
    Route::resource('review_providers', 'ReviewProviderAPIController');

    Route::get('/reviews/all', 'ReviewAPIController@all')->name('review.all');
    Route::get('/reviews/get', 'ReviewAPIController@get')->name('review.get_all_review');
    Route::resource('reviews', 'ReviewAPIController');

    Route::get('/bad-reviews/all', 'BadReviewAPIController@all')->name('bad_review.all');
    Route::get('/bad-reviews/get', 'BadReviewAPIController@get')->name('bad_review.get_all_review');
    Route::resource('bad-reviews', 'ReviewAPIController');

    Route::post('/invite_messages/upload-mms-picture', 'InviteMessageAPIController@uploadMmsPicture')->name('invite_messages.upload_mms_picture');
    Route::resource('invite_messages', 'InviteMessageAPIController');

    Route::get('/invites/pagination', 'InviteAPIController@pagination')->name('invite.pagination');
    Route::get('/invites/click_review_link/{id}', 'InviteAPIController@click_review_link')->name('invite.click_review_link');
    Route::post('/invites/import', 'InviteAPIController@import')->name('invite.import_customers');
    Route::post('/invites/send', 'InviteAPIController@send')->name('invite.send_list_invite');
    Route::post('/invites/send-all', 'InviteAPIController@sendAll')->name('invite.send_all_invite');
    Route::get('/invites/information/{startDate?}/{endDate?}', 'InviteAPIController@getCustomerInformation')->name('invite.get_customer_information');
    Route::get('/invites/information-invites/{startDate?}/{endDate?}', 'InviteAPIController@getCustomerInviteInformation')->name('invite.get_customer_invite_information');
    Route::resource('invites', 'InviteAPIController');

    Route::resource('notices', 'NoticeAPIController');

    Route::get('/users/stores', 'UserAPIController@stores');
    Route::post('/users/create-store', 'UserAPIController@createStore');
    Route::post('/users/update-store', 'UserAPIController@updateStore');
    Route::put('/users/update-password', 'UserAPIController@updatePassword');
    Route::put('/users/package/{id}', 'UserAPIController@createStorePackage');
    Route::put('/users/package-delete/{id}', 'UserAPIController@deleteStorePackage');
    Route::get('/users/list-package', 'UserAPIController@listUserPackage')->name('user.list_user_package');
    Route::get('/users/get-total-user/{idAgent}', 'UserAPIController@getUserByAgent')->name('user.get_user_by_agent');
    Route::resource('users', 'UserAPIController');

    Route::get('/statistic', 'StatisticAPIController@index');
    Route::get('/statistic/customer/{startDate}/{endDate?}', 'StatisticAPIController@customer');

    Route::get('/master_redirect_backgrounds/public', 'MasterRedirectBackgroundAPIController@index')->name('master_redirect_backgrounds.public');
    Route::get('/master_settings/key/{key}', 'MasterSettingAPIController@getByKey')->name('master_settings.get_key');
    Route::post('/common/support','CommonAPIController@supportAndRequest')->name('common.send_support');
    Route::get('/common/info-support','CommonAPIController@infoSupport')->name('common.info_support');
    Route::group(['middleware' => 'admin'], function () {
        Route::resource('activity_logs', 'ActivityLogAPIController');
        Route::get('/statistic/user', 'StatisticAPIController@user');
        Route::get('/statistic/reviews', 'StatisticAPIController@reviews');
        Route::post('/master_redirect_backgrounds/upload','MasterRedirectBackgroundAPIController@upload')->name('master_redirect_backgrounds.upload');
        Route::resource('master_redirect_backgrounds', 'MasterRedirectBackgroundAPIController');
        Route::resource('master_settings', 'MasterSettingAPIController');
        Route::get('/agent/statistic/user', 'StatisticAPIController@userAgent');
    });
    Route::get('/statistic/reviews-link', 'StatisticAPIController@reviewLink');
    Route::get('/feedback/pagination', 'FeedbackAPIController@pagination')->name('feedback.pagination');
    Route::get('/store-sent/pagination', 'StoreSentAPIController@pagination')->name('store_sent.pagination');

    Route::post('blast_templates/uploadPicture', 'BlastTemplateAPIController@uploadPicture')->name('blast_templates.upload_picture');
    Route::get('blast_templates/byUser', 'BlastTemplateAPIController@byUser')->name('blast_templates.by_user');
    Route::get('blast_templates/syncContact', 'BlastTemplateAPIController@syncContacts')->name('blast_templates.sync_contacts');
    Route::post('blast_templates/{id}/addContacts', 'BlastTemplateAPIController@addContacts')->name('blast_templates.add_contacts');
    Route::get('blast_templates/{id}/sendBlast', 'BlastTemplateAPIController@sendBlast')->name('blast_templates.send_blast');
    Route::get('blast_templates/{id}/sendAll', 'BlastTemplateAPIController@sendAll')->name('blast_templates.send_all');
    Route::resource('blast_templates', 'BlastTemplateAPIController');
    Route::resource('blast_contacts', 'BlastContactAPIController');


    Route::get('/customers/pagination', 'CustomerAPIController@pagination')->name('customer.pagination');
    Route::resource('customers', 'CustomerAPIController');

});
Route::get('/review-link', 'ReviewLinkAPIController@reviewLink')->name('reviewLink.reviewLink');
Route::resource('feedback', 'FeedbackAPIController');
Route::resource('review_providers/cronjob', 'ReviewProviderAPIController@cronjob');
