<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/stores/upload-company-logo', 'StoreAPIController@uploadCompanyLogo')->name('stores.upload_company_logo');
    Route::post('/stores/upload-background', 'StoreAPIController@uploadRedirectPageBackground')->name('stores.upload_background');
    Route::post('/stores/download-qr', 'StoreAPIController@downloadQR')->name('stores.download_qr');
   // Route::get('/stores/download-qr/{url}', 'StoreAPIController@downloadQR')->name('stores.download_qr');
    Route::resource('stores', 'StoreAPIController');

    Route::get('stores/{id}/smsMessage', 'StoreAPIController@smsMessage');
    Route::get('stores/{id}/smsMessageOther', 'StoreAPIController@smsMessageOther');
    Route::get('stores/{id}/emailMessage', 'StoreAPIController@emailMessage');
    Route::get('stores/{id}/mmsMessage', 'StoreAPIController@mmsMessage');
    Route::get('stores/{id}/emailTrackingMessage', 'StoreAPIController@emailTrackingMessage');
    Route::get('stores/{id}/emailRequestLabelMessage', 'StoreAPIController@emailRequestLabelMessage');
    Route::get('stores/{id}/emailRequestCustomerAlertMessage', 'StoreAPIController@emailRequestCustomerAlertMessage');
    Route::get('stores/{id}/emailRequestLabelAlertMessage', 'StoreAPIController@emailRequestLabelAlertMessage');

    Route::resource('store_users', 'StoreUserAPIController');

    Route::get('/settings/by-user', 'SettingAPIController@getSettingByUser')->name('settings.by_user');
    Route::resource('settings', 'SettingAPIController');

    Route::get('/review_providers/by-user/{provider}', 'ReviewProviderAPIController@getProviderByUser')->name('review_providers.by_user.provider');
    Route::get('/review_providers/by-user', 'ReviewProviderAPIController@getListProviderByUser')->name('review_providers.by_user');
    Route::resource('review_providers', 'ReviewProviderAPIController');

    Route::get('/reviews/all', 'ReviewAPIController@all')->name('review.all');
    Route::get('/reviews/get', 'ReviewAPIController@get')->name('review.get_all_review');
    Route::resource('reviews', 'ReviewAPIController');

    Route::get('/bad-reviews/all', 'BadReviewAPIController@all')->name('bad_review.all');
    Route::get('/bad-reviews/get', 'BadReviewAPIController@get')->name('bad_review.get_all_review');
    Route::resource('bad-reviews', 'ReviewAPIController');

    Route::post('/invite_messages/upload-mms-picture', 'InviteMessageAPIController@uploadMmsPicture')->name('invite_messages.upload_mms_picture');
    Route::resource('invite_messages', 'InviteMessageAPIController');

    Route::get('/invites/pagination', 'InviteAPIController@pagination')->name('invite.pagination');
    Route::get('/invites/click_review_link/{id}', 'InviteAPIController@click_review_link')->name('invite.click_review_link');
    Route::post('/invites/import', 'InviteAPIController@import')->name('invite.import_customers');
    Route::post('/invites/send', 'InviteAPIController@send')->name('invite.send_list_invite');
    Route::post('/invites/send-all', 'InviteAPIController@sendAll')->name('invite.send_all_invite');
    Route::get('/invites/information/{startDate?}/{endDate?}', 'InviteAPIController@getCustomerInformation')->name('invite.get_customer_information');
    Route::get('/invites/information-invites/{startDate?}/{endDate?}', 'InviteAPIController@getCustomerInviteInformation')->name('invite.get_customer_invite_information');
    Route::resource('invites', 'InviteAPIController');

    Route::resource('notices', 'NoticeAPIController');

    Route::get('/users/statistic', 'StatisticAPIController@user_statistic');
    Route::get('/users/stores', 'UserAPIController@stores');
    Route::post('/users/create-store', 'UserAPIController@createStore');
    Route::post('/users/update-store', 'UserAPIController@updateStore');
    Route::put('/users/update-password', 'UserAPIController@updatePassword');
    Route::put('/users/package/{id}', 'UserAPIController@createStorePackage');
    Route::put('/users/package-delete/{id}', 'UserAPIController@deleteStorePackage');
    Route::get('/users/list-package', 'UserAPIController@listUserPackage')->name('user.list_user_package');
    Route::get('/users/get-total-user/{idAgent}', 'UserAPIController@getUserByAgent')->name('user.get_user_by_agent');
    Route::resource('users', 'UserAPIController');

    Route::get('/statistic', 'StatisticAPIController@index');
    Route::get('/statistic/customer/{startDate}/{endDate?}', 'StatisticAPIController@customer');

    Route::get('/master_redirect_backgrounds/public', 'MasterRedirectBackgroundAPIController@index')->name('master_redirect_backgrounds.public');
    Route::get('/master_settings/key/{key}', 'MasterSettingAPIController@getByKey')->name('master_settings.get_key');
    Route::post('/common/support','CommonAPIController@supportAndRequest')->name('common.send_support');
    Route::get('/common/info-support','CommonAPIController@infoSupport')->name('common.info_support');
    Route::group(['middleware' => 'admin'], function () {
        Route::resource('activity_logs', 'ActivityLogAPIController');
        Route::get('/statistic/user', 'StatisticAPIController@user');
        Route::get('/statistic/reviews', 'StatisticAPIController@reviews');
        Route::post('/master_redirect_backgrounds/upload','MasterRedirectBackgroundAPIController@upload')->name('master_redirect_backgrounds.upload');
        Route::resource('master_redirect_backgrounds', 'MasterRedirectBackgroundAPIController');
        Route::resource('master_settings', 'MasterSettingAPIController');
        Route::get('/agent/statistic/user', 'StatisticAPIController@userAgent');
        Route::get('/sta/user-statistic', 'StatisticAPIController@user_statistic');

        Route::get('/shipmentstatus/pagination','ShipmentStatusAPIController@pagination')->name('shipment_status.pagination');
        Route::resource('shipmentstatus','ShipmentStatusAPIController');

        Route::get('service_type_zone/pagination','ServiceTypeZoneAPIController@pagination');
        Route::get('service_type_zone/list_service_type_zone/{id}','ServiceTypeZoneAPIController@getListServiceTypeZone');
        Route::get('service_type_zone/list_service','ServiceTypeZoneAPIController@getListService');
        Route::get('service_type_zone/list_zone','ServiceTypeZoneAPIController@getListZone');
        Route::post('service_type/create_type','ServiceTypeZoneAPIController@createTypeZone');
        Route::put('service_type/delete/{id}','ServiceTypeZoneAPIController@deleteTypeZone');
        Route::resource('/service_type_zone','ServiceTypeZoneAPIController');

        Route::get('city/pagination','CityAPIController@pagination')->name('city.pagination');
        Route::resource('city','CityAPIController');

        Route::get('district/district_list_by_id/{id}','DistrictAPIController@getDistrictListById');
        Route::resource('district','DistrictAPIController');

        Route::resource('country','CountryAPIController');

        Route::get('awb-air/pagination','AwbAirAPIController@pagination')->name('awb_air.pagination');
        Route::post('awb-air/edit/{id}','AwbAirAPIController@edit');
        Route::resource('awb-air','AwbAirAPIController');
    });

    Route::get('/statistic/reviews-link', 'StatisticAPIController@reviewLink');
    Route::get('/feedback/pagination', 'FeedbackAPIController@pagination')->name('feedback.pagination');
    Route::get('/store-sent/pagination', 'StoreSentAPIController@pagination')->name('store_sent.pagination');

    Route::post('blast_templates/uploadPicture', 'BlastTemplateAPIController@uploadPicture')->name('blast_templates.upload_picture');
    Route::get('blast_templates/byUser', 'BlastTemplateAPIController@byUser')->name('blast_templates.by_user');
    Route::get('blast_templates/syncContact', 'BlastTemplateAPIController@syncContacts')->name('blast_templates.sync_contacts');
    Route::post('blast_templates/{id}/addContacts', 'BlastTemplateAPIController@addContacts')->name('blast_templates.add_contacts');
    Route::get('blast_templates/{id}/sendBlast', 'BlastTemplateAPIController@sendBlast')->name('blast_templates.send_blast');
    Route::get('blast_templates/{id}/sendAll', 'BlastTemplateAPIController@sendAll')->name('blast_templates.send_all');
    Route::resource('blast_templates', 'BlastTemplateAPIController');
    Route::resource('blast_contacts', 'BlastContactAPIController');

    Route::post('/customers/check-customer-code/{code}','CustomerAPIController@check_customer_code');
    Route::get('/customers/pagination', 'CustomerAPIController@pagination')->name('customer.pagination');
    Route::get('/customers/load-customer-code/{type_customer}', 'CustomerAPIController@showCustomerCode')->name('customer.showCustomerCode');
    Route::get('/customers/load-cities-zipcode/{zipcode}', 'CustomerAPIController@showCitiesZipcode')->name('customer.showCitiesZipcode');
    Route::post('/customers/upload-id', 'CustomerAPIController@customerUploadId')->name('customer.customerUploadId');
    Route::resource('customers', 'CustomerAPIController');

    Route::get('/sender/pagination', 'SenderAPIController@pagination')->name('sender.pagination');
    Route::get('/sender/load-customer', 'SenderAPIController@load_customer')->name('sender.load_customer_sender');
    Route::get('/sender/load-country', 'SenderAPIController@load_country')->name('sender.load_country');
    Route::get('/sender/load-city/{id}', 'SenderAPIController@load_city')->name('sender.load_city');
    Route::get('/sender/load-district/{id}', 'SenderAPIController@load_district')->name('sender.load_district');
    Route::get('/sender/load-city-detail/{id}', 'SenderAPIController@load_city_detail')->name('sender.load_city_detail');
    Route::resource('sender', 'SenderAPIController');

    Route::get('/recipient/pagination', 'RecipientAPIController@pagination')->name('recipient.pagination');
    Route::get('/recipient/load-customer', 'RecipientAPIController@load_customer')->name('recipient.load_customer_recipient');
    Route::get('/recipient/load-country', 'RecipientAPIController@load_country')->name('recipient.load_country');
    Route::get('/recipient/load-city/{id}', 'RecipientAPIController@load_city')->name('recipient.load_city');
    Route::get('/recipient/load-city', 'RecipientAPIController@load_city_')->name('recipient.load_city_');
    Route::resource('recipient', 'RecipientAPIController');

    Route::get('/shipments/get-package-by-consolidate/{id}', 'ShipmentsAPIController@getPackageByConsolidate')->name('shipments.getPackageByConsolidate');
    Route::get('/shipments/shipmentStatus', 'ShipmentsAPIController@shipmentStatus')->name('shipments.shipment_status');
    Route::get('/shipments/shipmentByMonth', 'ShipmentsAPIController@ShipmentByMonth')->name('shipments.shipment_status_by_month');
    Route::get('/shipments/pagination', 'ShipmentsAPIController@pagination')->name('shipments.pagination');
    Route::get('/shipments/load-customer', 'ShipmentsAPIController@load_customer')->name('shipments.load_customer_shipment');
    Route::get('/shipments/load-customer-detail/{id}', 'ShipmentsAPIController@load_customer_detail')->name('shipments.load_customer_detail');
    Route::get('/shipments/load-customer/{id}', 'ShipmentsAPIController@load_customer_by_type')->name('shipments.load_customer_by_type');
    Route::get('/shipments/load-country', 'ShipmentsAPIController@load_country')->name('shipments.load_country');
    Route::get('/shipments/load-city/{id}', 'ShipmentsAPIController@load_city')->name('shipments.load_city');
    Route::get('/shipments/load-city', 'ShipmentsAPIController@load_city_')->name('shipments.load_city_');
    Route::get('/shipments/load-commo', 'ShipmentsAPIController@load_commo')->name('shipments.load_commo');
    Route::get('/shipments/load-commo-cate/{id}', 'ShipmentsAPIController@load_commo_cate')->name('shipments.load_commo_cate');
    Route::get('/shipments/create', 'ShipmentsAPIController@create')->name('shipments.create');
    Route::get('/shipments/edit/{id}', 'ShipmentsAPIController@edit')->name('shipments.edit');
    Route::get('/shipments/load-sender', 'ShipmentsAPIController@load_sender')->name('shipments.load_sender');
    Route::get('/shipments/load-rate/{id}', 'ShipmentsAPIController@shipment_rate')->name('shipments.shipment_rate');
    Route::get('/shipments/load-service-type', 'ShipmentsAPIController@load_service_type')->name('shipments.load_service_type');
    Route::get('/shipments/load-service-type-items/{id}', 'ShipmentsAPIController@load_service_type_items')->name('shipments.load_service_type_items');
    Route::get('/shipments/load-sender/{id}', 'ShipmentsAPIController@load_sender_customer')->name('shipments.load_sender_id');
    Route::get('/shipments/load-sender-detail/{id}', 'ShipmentsAPIController@load_sender_detail')->name('shipments.load_sender_detail');
    Route::get('/shipments/load-recipient/{id}', 'ShipmentsAPIController@load_recipient')->name('shipments.load_recipient');
    Route::get('/shipments/load-recipient-by-sender/{id}', 'ShipmentsAPIController@load_recipient_by_sender')->name('shipments.load_recipient_by_sender');
    Route::get('/shipments/load-recipient-detail/{id}', 'ShipmentsAPIController@load_recipient_detail')->name('shipments.load_recipient_detail');
    Route::get('/shipments/load-service-price/{zoneId}/{type}/{customer_id}', 'ShipmentsAPIController@load_service_price')->name('shipments.load_service_price');
    Route::post('/shipments/package-price', 'ShipmentsAPIController@package_price')->name('shipments.package_price');
    Route::post('/shipments/commo-price', 'ShipmentsAPIController@commo_price')->name('shipments.commo_price');
    Route::get('/shipments/detail/{id}', 'ShipmentsAPIController@shipment_detail')->name('shipments.shipment_detail');
    Route::post('/shipments/change-status', 'ShipmentsAPIController@shipment_change_status')->name('shipments.shipment_change_status');
    Route::get('/shipments/load-packages/{id}', 'ShipmentsAPIController@shipment_packages')->name('shipments.shipment_packages');
    Route::post('/shipments/commo-price-item', 'ShipmentsAPIController@commo_price_item')->name('shipments.commo_price_item');
    Route::resource('shipments', 'ShipmentsAPIController');

    Route::get('/commodities/pagination', 'CommoditiesAPIController@pagination')->name('commodities.pagination');
    Route::get('/commodities/load-cate', 'CommoditiesAPIController@load_cate')->name('commodities.load_cate');
    Route::post('/commodities/add-from-shipment', 'CommoditiesAPIController@add_from_shipment')->name('commodities.add_from_shipment');
    Route::get('/commodities/load-brand/{id}', 'CommoditiesAPIController@load_brand')->name('commodities.load_brand');
    Route::get('/commodities/load-charges/{id}', 'CommoditiesAPIController@load_charges')->name('commodities.load_charges');
    Route::resource('commodities', 'CommoditiesAPIController');

    Route::get('/commodities/categories/pagination', 'CommoditiesCateAPIController@pagination')->name('commodities_cate.pagination');
    Route::resource('commodities/categories', 'CommoditiesCateAPIController');

    Route::get('/commodities/brand/pagination', 'CommoditiesBrandsAPIController@pagination')->name('commodities_brand.pagination');
    Route::get('/commodities/brand/categories', 'CommoditiesBrandsAPIController@list_categories')->name('commodities_brand.list_categories');
    Route::resource('commodities/brand', 'CommoditiesBrandsAPIController');

    Route::get('/commodities/charges/pagination', 'CommoditiesChargesAPIController@pagination')->name('commodities_charges.pagination');
    Route::get('/commodities/charges/categories', 'CommoditiesChargesAPIController@list_categories')->name('commodities_charges.list_categories');
    Route::get('/commodities/charges/load-cate', 'CommoditiesChargesAPIController@load_cate')->name('commodities_charges.load_cate');
    Route::resource('commodities/charges', 'CommoditiesChargesAPIController');

    Route::put('/export-list/change-status/{id}', 'ExportListAPIController@changeStatus')->name('export_list.change_status');
    Route::get('/export-list/load-edit/{id}', 'ExportListAPIController@getExportEdit')->name('export_list.get_export_edit');
    Route::get('/export-list/import', 'ExportListAPIController@import')->name('export_list.import');
    Route::post('/export-list/import-add', 'ExportListAPIController@import_add')->name('export_list.import_add');
    Route::post('/export-list/import-upload', 'ExportListAPIController@import_upload')->name('export_list.import_upload');
    Route::get('/export-list/load-shipments', 'ExportListAPIController@load_shipments')->name('export_list.load_shipments');
    Route::get('/export-list/load-air', 'ExportListAPIController@load_air')->name('export_list.load_air');
    Route::get('/export-list/load-export-list/{id}', 'ExportListAPIController@load_exportlist_edit')->name('export_list.load_edit');
    Route::get('/export-list/shipment-detail/{id}', 'ExportListAPIController@load_shipment_detail')->name('export_list.load_shipment_detail');
    Route::get('/export-list/shipment-detail-code/{code}', 'ExportListAPIController@load_shipment_detail_code')->name('export_list.load_shipment_detail_code');
    Route::get('/export-list/shipment-detail-code-scan/{code}/{exportlist_id}', 'ExportListAPIController@load_shipment_detail_code_scan')->name('export_list.load_shipment_detail_code_scan');
    Route::get('/export-list/pagination', 'ExportListAPIController@pagination')->name('export_list.pagination');
    Route::get('/export-list/pagination-list', 'ExportListAPIController@pagination_list')->name('export_list.pagination_list');
    Route::post('/export-list/update-export-list-status', 'ExportListAPIController@import_edit')->name('export_list.import_edit');
    Route::delete('/export-list/import-delete/{id}', 'ExportListAPIController@import_delete')->name('export_list.import_delete');
    Route::resource('export-list', 'ExportListAPIController');
    
    Route::post('/request-label/rate-ups/{id}', 'RequestLabelAPIController@rate_ups')->name('request_label.rate_ups');
    Route::post('/request-label/rate-fedex/{id}', 'RequestLabelAPIController@rate_fedex')->name('request_label.rate_fedex');
    Route::get('/request-label/list-package/{id}', 'RequestLabelAPIController@listPackageById')->name('request_label.list_package');
    Route::get('/request-label-list/pagination', 'RequestLabelAPIController@pagination')->name('request_label.pagination');
    Route::post('/confirm-request-label/{id}', 'RequestLabelAPIController@confirm_request_label')->name('request_label.confirm_request_label');
    Route::post('/request-label-send-email/{id}', 'RequestLabelAPIController@request_label_send_email')->name('request_label.request_label_send_email');
    Route::resource('/request-label', 'RequestLabelAPIController');
    
    Route::resource('/request-label-package-list', 'RequestLabelPackageAPIController');

    Route::get('/shipments/shipmentStatusById/{id}', 'ShipmentStatusAPIController@LoadShipmentStatusById')->name('shipments.shipment_status_log_by_id');
    Route::get('/shipmentstatuslogs/{id}', 'ShipmentStatusAPIController@getShipmentStatusLogById')->name('shipment_status_log.load_status_log_by_id');;
    Route::get('/shipmentstatuslog/loadstatus', 'ShipmentStatusAPIController@LoadShipmentStatus')->name('shipment_status.load_status');

    Route::get('/tracking/get-carrier/{tracking_number}','TrackingAPIController@getCarrier')->name('tracking.get_carrier');
    Route::get('/tracking/getListTrackingByCustomer','TrackingAPIController@getListTrackingByCustomer')->name('tracking.get_tracking_list_by_customer');
    Route::get('/tracking/getConsolidatedByCustomer/{customer_code}','TrackingAPIController@getConsolidatedByCustomer')->name('tracking.get_consolidated_by_customer');
    Route::get('/tracking/checkTrackingCode/{tracking_code}/{customer_code}','TrackingAPIController@checkTrackingCode')->name('tracking.check_tracking_code');
    Route::put('/tracking/updateConsolidatedTracking','TrackingAPIController@updateConsolidatedTracking')->name('tracking.update_consolidated_tracking');
    Route::post('/tracking/createConsolidatedTracking','TrackingAPIController@createConsolidatedTracking')->name('tracking.create_consolidated_tracking');
    Route::get('/tracking/getlistconsolidated','TrackingAPIController@getListConsolidated')->name('tracking.get_list_consolidated');
    Route::get('/tracking/getlistconsolidatedById/{id}','TrackingAPIController@getListConsolidatedById')->name('tracking.get_list_consolidated_id');
    Route::get('/tracking/pagination','TrackingAPIController@pagination')->name('tracking.pagination');
    Route::resource('tracking','TrackingAPIController');

    Route::get('request-customer/pagination','RequestCustomerAPIController@pagination');
    Route::post('request-customer/approve-request-customer/{id}','RequestCustomerAPIController@approve');
    Route::post('/request-customer/edit/{id}','RequestCustomerAPIController@edit');
    Route::post('/request-customer-change-status/{id}','RequestCustomerAPIController@change_status');
    Route::resource('request-customer','RequestCustomerAPIController');
});
/* Route::get('/review-link', 'ReviewLinkAPIController@reviewLink')->name('reviewLink.reviewLink');
Route::resource('feedback', 'FeedbackAPIController');
Route::resource('review_providers/cronjob', 'ReviewProviderAPIController@cronjob');
*/
Route::get('/check-shipments/pagination', 'CheckShipmentsAPIController@pagination')->name('checkShipments.pagination');
Route::get('/check-export-list/pagination', 'CheckShipmentsAPIController@check_export_list_pagination')->name('checkShipments.export_list_pagination');
Route::get('/check-export-list/pagination-new', 'CheckShipmentsAPIController@check_export_list_pagination_new')->name('checkShipments.export_list_pagination_new');
Route::get('/check-export-list/pagination-filter', 'CheckShipmentsAPIController@check_export_list_filter')->name('checkShipments.pagination_filter');

Route::post('/request-label-client/submit-rate/{id}', 'RequestLabelAPIController@submitRate')->name('request_label.submit_rate');
Route::resource('/request-label-package', 'RequestLabelPackageAPIController');
Route::post('/request-label-package/rate-ups', 'RequestLabelPackageAPIController@rateUPS');
Route::get('/list-customer', 'CustomerAPIController@getListCustomer');
Route::get('/get-customer/{code}', 'CustomerAPIController@getCustomerByCode');
Route::get('/get-customer-by-phone/{phone}', 'CustomerAPIController@getCustomerByPhone');
Route::get('/test-fedex', 'TestAPIController@test_shipment');
Route::get('/test-pdf', 'TestAPIController@form_pdf');
Route::get('/list-recipient/{id}', 'RecipientAPIController@loadRecipientByCustomer')->name('recipient.list_recipient');

Route::post('/customer-login', 'CustomerAPIController@customerLogin');

Route::post('/request-customer/upload-id', 'RequestCustomerAPIController@uploadIdNumber')->name('request_customer.uploadIdNumber');
Route::post('/request-customer/store','RequestCustomerAPIController@store');
//Route::group(['middleware' => 'web'], function () {
    Route::post('/request-customer/test-csrf','RequestCustomerAPIController@test_csrf');
//});
Route::get('/request-customer/get-service-type','RequestCustomerAPIController@getServiceType');
Route::resource('request-customer', 'RequestCustomerAPIController');

Route::get('/requestcustomer/load-country', 'SenderAPIController@load_country')->name('sender.load_country');
Route::get('/requestcustomer/load-city/{id}', 'SenderAPIController@load_city')->name('sender.load_city');
Route::get('/requestcustomer/load-district/{id}', 'SenderAPIController@load_district')->name('sender.load_district');

Route::post('/request-label/create-by-customer', 'RecipientAPIController@createByCustomer')->name('requestLabel.create_by_customer');
Route::post('/request-label/update-by-customer/{id}', 'RecipientAPIController@updateByCustomer')->name('requestLabel.update_by_customer');
Route::post('/request-label/create', 'RequestLabelAPIController@store');