<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
if (env('APP_ENV') === 'production') {
    URL::forceSchema('https');
}
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('dashboard');
    })->name('root');
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/settings', function () {
        return view('setting');
    });
    Route::get('/reviews', function () {
        return view('review');
    });
    Route::get('/bad-reviews', function () {
        return view('bad_review');
    });
    Route::get('profile', function () {
        return view('profile');
    })->name('profile');

    Route::get('customers', function () {
        return view('customer');
    })->name('customers');

    Route::get('templates', function () {
        return view('template');
    })->name('templates');

    Route::get('feedback',function(){
        return view('feedback');
    })->name('feedback');

    Route::get('sender',function(){
        return view('sender');
    })->name('sender');

    Route::get('recipient',function(){
        return view('recipient');
    })->name('recipient');

    Route::get('shipments',function(){
        return view('shipments');
    })->name('shipments');

    Route::get('shipments/create',function(){
        return view('shipments-create');
    })->name('shipments.create');

    Route::get('shipments/edit',function(){
        return view('shipments-edit');
    })->name('shipments.edit');

    Route::get('commodities',function(){
        return view('commodities');
    })->name('commodities');

    Route::get('commodities/categories',function(){
        return view('commodities-categories');
    })->name('commodities.categories');

    Route::get('commodities/brand',function(){
        return view('commodities-brand');
    })->name('commodities.brand');

    Route::get('commodities/charges',function(){
        return view('commodities-charges');
    })->name('commodities.charges');

    Route::get('export-list',function(){
        return view('export-list');
    })->name('export-list');

    Route::get('export-list/create',function(){
        return view('export-list-create');
    })->name('export-list.create');

    Route::get('export-list/scan',function(){
        return view('export-list-scan');
    })->name('export-list.scan');

    Route::get('export-list/export-awb',function(){
        return view('export-awb');
    })->name('export-list.export_awb');

    Route::get('export-list/edit',function(){
        return view('export-list-edit');
    })->name('export-list.edit');

    Route::get('export-list/import',function(){
        return view('import-export-list');
    })->name('export-list.import');

    Route::get('export-list/import-list',function(){
        return view('import-list');
    })->name('export-list.import-list');

    Route::get('tracking-list',function(){
        return view('tracking');
    })->name('tracking.tracking-list');
    
    Route::get('consolidated-tracking-list',function(){
        return view('consolidated_tracking_list');
    })->name('tracking.consolidated-tracking-list');

    Route::get('tracking/scan',function(){
        return view('scan_tracking');
    })->name('tracking.scan-tracking');

    Route::get('tracking/consolidated-tracking',function(){
        return view('consolidated_tracking');
    })->name('tracking.consolidated-tracking');
});

    Route::get('request-customer-list',function(){
        return view('request-customer-list');
    })->name('request-customer-list');
    
    Route::get('check-shipment',function(){
        return view('check_shipments');
    })->name('check-shipment');

    Route::get('request-label-list',function(){
        return view('request-label-list');
    })->name('request-label-list');

    Route::get('request-label-package-list',function(){
        return view('request-label-package');
    })->name('request-label-package-list');

//Route::get('/check-shipments/pagination', 'CheckShipmentsAPIController@pagination')->name('checkShipments.pagination');
/*
 /--------------------------------------------------------------------
 / Route admin
 /--------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    Route::get('/', function () {
        return view('admin.dashboard');
    })->name('root');

    Route::get('/dashboard-agent', function () {
        return view('admin.dashboard_agent');
    })->name('root_agent');

    Route::get('dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::get('access-user/{userId}/{redirectTo?}', 'DashboardController@accessUser');

    Route::get('users', function () {
        return view('admin.users');
    })->name('users');

    Route::get('notices', function () {
        return view('admin.notices');
    })->name('notices');

    Route::get('manage-background', function () {
        return view('admin.manage_background');
    })->name('manage_background');

    Route::get('settings',function(){
        return view('admin.settings');
    })->name('settings');

    Route::get('shipment-status',function(){
        return view('admin.shipment_status');
    })->name('shipment_status');

    Route::get('service-type-zone',function(){
        return view('admin.service_type_zone');
    })->name('service_type_zone');

    Route::get('city',function(){
        return view('admin.city');
    })->name('city');

    Route::get('awb-air',function(){
        return view('admin.awb_air');
    })->name('awb-air');

    Route::get('request-label-list',function(){
        return view('admin.request_label_list');
    })->name('request-label-list');

    Route::get('setting-general',function(){
        return view('admin.setting_general');
    })->name('setting_general');

});
Route::get('request-label',function(){
    return view('request-label');
})->name('request-label');

Route::get('request-customer',function(){
    return view('request-customer');
})->name('request-customer');

Route::get('check-export-list',function(){
    return view('check-export-list');
})->name('check-export-list');

Route::get('check-exportlist',function(){
    return view('check-export-list-2');
})->name('check-export-list-2');

Route::get('customer-login',function(){
    return view('customer-login');
})->name('customer-login');

Route::get('callback/facebook/{store}/{provider}', 'CallbackController@facebookCallback')->name('callback.facebook');
Route::post(
    'stripe/webhook',
    '\App\Http\Controllers\StripeWebhookController@handleWebhook'
);
Route::get('/review/{shortId}/{step?}', 'ReviewRedirectController@index')->name('review.redirect');
Route::post('/review/{shortId}/{step?}', 'ReviewRedirectController@submit')->name('review.redirect.submit');
Route::get('/register-thanks', function () {
    return view('auth.register_thanks');
});
Route::get('/get-reviews-api', 'ApiGetReviews@get')->name('data.reviews');
Route::get('/get-reviews-detail-api', 'ApiGetReviews@detail')->name('data.detail.reviews');
Route::get('/{slug}', 'ReviewRedirectController@redirectByStoreSlug')->name('review.redirect.slug');
Route::get('/{slug}/{id}', 'ReviewRedirectController@redirectByStoreSlug')->name('review.redirect.slug.id');
Route::get('/storage-link', function() {
	echo Artisan::call("storage:link");
});

