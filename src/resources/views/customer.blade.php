@extends('layouts.app')
@section('header-title')
  Customers
@endsection
@section('page-title')
  <h1>Customers
  </h1>
@endsection

@section('css')
  <link href="../assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <list-customer></list-customer>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="../assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/vue-simple-search-dropdown/vue-simple-search-dropdown.min.js" type="text/javascript"></script>
  <script src="https://unpkg.com/vue-select@2.4.0/dist/vue-select.js"></script>
  <script src="https://unpkg.com/vue-html-to-paper/build/vue-html-to-paper.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlsINbj5NMB83kNq5PzN5gitTb3ibYMTY&libraries=places"></script>
@endsection
