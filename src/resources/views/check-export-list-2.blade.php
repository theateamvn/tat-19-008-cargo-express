@extends('layouts.basic')

@section('page-title')
  <h1>Check Shipments
  </h1>
@endsection

@section('css')
  <link href="../assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <list-check-export-list-2></list-check-export-list-2>
    </div>
  </div>
@endsection

@section('scripts')
@endsection
