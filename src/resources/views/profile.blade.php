@extends('layouts.app')

@section('page-title')
  <h1>Users
    <small>profile</small>
  </h1>
@endsection

@section('content')
  <profile-show :user_id="{!! Auth::user()->id !!}"></profile-show>
@endsection

@section('css')
  <link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
@endsection
