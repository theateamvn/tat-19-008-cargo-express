@extends('layouts.auth')

@section('title')
    Register
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
            <div class="row center-wrap">
                <div class="col-md-7">
                    <form id="register-form" class="form-horizontal" method="post" action="{!! url('/register') !!}">
                        {!! csrf_field() !!}
                        <div class="form-header">
                            <span class="form-title">Step 1: Account information</span>
                        </div>
                        <div class="panel-body">
                            <div class="form-group has-feedback {!! $errors->has('username') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="username"
                                           value="{!! old('username') !!}"
                                           placeholder="Username">
                                    @if ($errors->has('username'))
                                        <span class="help-block"><strong>{!! $errors->first('username') !!}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback {!! $errors->has('password') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="password" class="form-control" name="password"
                                           placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block"><strong>{!! $errors->first('password') !!}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback {!! $errors->has('password_confirmation') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="password" name="password_confirmation" class="form-control"
                                           placeholder="Confirmed password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block"><strong>{!! $errors->first('password_confirmation') !!}</strong></span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group has-feedback {!! $errors->has('full_name') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="full_name"
                                           value="{!! old('full_name') !!}"
                                           placeholder="Full Name">
                                    @if ($errors->has('full_name'))
                                        <span class="help-block"><strong>{!! $errors->first('full_name') !!}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback {!! $errors->has('email') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="email" class="form-control" name="email"
                                           value="{!! old('email') !!}"
                                           placeholder="Email">
                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{!! $errors->first('email') !!}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback {!! $errors->has('phone') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="phone"
                                           value="{!! old('phone') !!}"
                                           placeholder="Phone Number">
                                    @if ($errors->has('phone'))
                                        <span class="help-block"><strong>{!! $errors->first('phone') !!}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback {!! $errors->has('website') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="website"
                                           value="{!! old('website') !!}"
                                           placeholder="Your website">
                                    @if ($errors->has('website'))
                                        <span class="help-block"><strong>{!! $errors->first('website') !!}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback {!! $errors->has('name') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="name" value="{!! old('name') !!}"
                                           placeholder="Ex: Hotel name, Spa name,...">
                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{!! $errors->first('name') !!}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback {!! $errors->has('address') ? ' has-error' : '' !!}">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="address"
                                           value="{!! old('address') !!}"
                                           placeholder="Ex: Hotel address, Spa address,...">
                                    @if ($errors->has('address'))
                                        <span class="help-block"><strong>{!! $errors->first('address') !!}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-header">
                            <span class="form-title">Step 2: Payment information</span>
                        </div>
                        <div class="panel-body">
                            <p class="payment-errors text-danger"></p>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Card Number" size="20"
                                           data-stripe="number"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="text" class="form-control" placeholder="MM" size="2"
                                                   data-stripe="exp_month"/>
                                        </div>
                                        <div class="col-xs-6">
                                            <input type="text" class="form-control" placeholder="YY" size="2"
                                                   data-stripe="exp_year"/></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="password" class="form-control" placeholder="CVC" size="4"
                                           data-stripe="cvc"/>
                                </div>
                            </div>

                            <div class="radio">
                           <?php
                            $type =  isset($_GET['type']) ? $_GET['type'] : 0;
                            if($type != 0)
                            {
                                $type = $type - 1;
                            }
                           ?>
                                @foreach($plans as $index => $plan)
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <input type="radio" value="{{$plan['id']}}"
                                                   name="package" data-id="{{$plan['id']}}"
                                                   onclick="handlePackageSelect(this)"
                                                   data-amount="{{number_format($plan['amount']/100,2)}}" {{$index==$type?'checked':''}}>
                                            {{$plan['name']}}
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <span>$ {{number_format($plan['amount']/100,2)}}</span>
                                        </div>
                                    </div>
                                @endforeach
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span>Total due today:</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        $
                                        <span id="total">
                                            @if(count($plans)>0)
                                                {{number_format($plans[0]['amount']/100,2)}}
                                            @else
                                                0.0
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button style="height: 50px;width:200px;margin-top: 20px" type="submit"
                                        class="btn btn-success btn-flat submit">Submit Order
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <img style="width: 130px;margin: 10px auto;display: block"
                                 src="/assets/layouts/layout4/img/guarantee-seal.png" alt="">
                        </div>
                    </div>
                    <br>
                    <h4>Privacy</h4>

                    <p>We will not share or trade online information that you provider us (include email
                        addresses).</p>
                    <br>
                    <h4>Security</h4>

                    <p>All personal information you submit is encrypted and secure</p>


                    <span><i class="glyphicon glyphicon-ok text-success"></i> Instant access to your account immmediatly</span>
                    <p></p><br>
                    <h4>Customer Support</h4>
                    <p>Phone: +1 (469)535-6789</p>
                    <p></p>
                    <p>{!! 'Email:info@PushMeUp.net' !!}</p>

                    <br>

                    <p><i>100% Satisfaction Guaranteed</i></p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>
        function handlePackageSelect (e) {
            $('#total').text($(e).data('amount'));
        }
    </script>
    <script type="text/javascript">
        Stripe.setPublishableKey('{{config('services.stripe.key')}}');

        $(function () {
            var form = $('#register-form');
            form.submit(function (event) {
                // Disable the submit button to prevent repeated clicks:
                form.find('.submit').prop('disabled', true);

                // Request a token from Stripe:
                Stripe.card.createToken(form, stripeResponseHandler);

                // Prevent the form from being submitted:
                return false;
            });
        });

        function stripeResponseHandler(status, response) {
            // test without input payment // theateam edited
            /*var $form = $('#register-form');
            var token = 'TEST12345';
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            $form.get(0).submit();*/
            // Grab the form:
            var $form = $('#register-form');
            if (response.error) { // Problem!

                // Show the errors on the form:
                $form.find('.payment-errors').text(response.error.message);
                $form.find('.submit').prop('disabled', false); // Re-enable submission

            } else { // Token was created!

                // Get the token ID:
                var token = response.id;
                console.log('Token' + token)
                // Insert the token ID into the form so it gets submitted to the server:
                $form.append($('<input type="hidden" name="stripeToken">').val(token));

                // Submit the form:
                $form.get(0).submit();
            }
        }

    </script>
    
@endsection
