@extends('layouts.auth')

@section('title')
    Login
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="login-form" action="{!! url('/login') !!}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="logo-login">
                            <img src="{!! asset('/assets/layouts/layout4/img/logo-blue1.png') !!}" alt="Xpress Cargo">
                        </div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group">
                            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                            <label class="control-label visible-ie8 visible-ie9">Username</label>
                            <input class="form-control form-control-solid placeholder-no-fix" type="text"
                                   autocomplete="off"
                                   placeholder="Username" name="email"/></div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Password</label>
                            <input class="form-control form-control-solid placeholder-no-fix" type="password"
                                   autocomplete="off"
                                   placeholder="Password" name="password"/></div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection