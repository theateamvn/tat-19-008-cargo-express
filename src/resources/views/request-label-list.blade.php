@extends('layouts.app')
@section('header-title')
  Request Label
@endsection

@section('page-title')
  <h1>Request Label List
  </h1>
@endsection

@section('css')
  <link href="../assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <request-label-list></request-label-list>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="../assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/vue-simple-search-dropdown/vue-simple-search-dropdown.min.js" type="text/javascript"></script>
@endsection
