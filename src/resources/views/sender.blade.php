@extends('layouts.app')
@section('header-title')
  Sender
@endsection

@section('page-title')
  <h1>Sender
  </h1>
@endsection

@section('css')
  <link href="../assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <list-sender></list-sender>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="../assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/vue-simple-search-dropdown/vue-simple-search-dropdown.min.js" type="text/javascript"></script>
@endsection
