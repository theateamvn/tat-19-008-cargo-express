@extends('layouts.app')
@section('header-title')
  Export List
@endsection

@section('page-title')
  <h1>Scan Export List
  </h1>
@endsection

@section('css')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <scan-export-list></scan-export-list>
    </div>
  </div>
@endsection

@section('scripts')
@endsection
