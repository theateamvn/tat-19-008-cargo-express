@extends('layouts.admin.app')

@section('page-title')
@endsection

@section('content')
  <request-label-list-admin></request-label-list-admin>
@endsection

@section('scripts')
  <script src="../assets/global/plugins/vue-simple-search-dropdown/vue-simple-search-dropdown.min.js" type="text/javascript"></script>
@endsection
@section('css')
    <link href="../node_modules/vue-multiselect/dist/vue-multiselect.min.css" />
@stop