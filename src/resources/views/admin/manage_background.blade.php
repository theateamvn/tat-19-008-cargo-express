@extends('layouts.admin.app')

@section('page-title')
    <h1>Background
        <small>management</small>
    </h1>
@endsection

@section('content')
    <admin-background-main></admin-background-main>
@endsection
