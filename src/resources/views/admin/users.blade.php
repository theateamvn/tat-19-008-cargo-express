@extends('layouts.admin.app')

@section('page-title')
  <h1>Users
    <small>management</small>
  </h1>
@endsection

@section('content')
  <user-list></user-list>
@endsection

@section('scripts')
  <script src="../assets/global/plugins/vue-simple-search-dropdown/vue-simple-search-dropdown.min.js" type="text/javascript"></script>
@endsection