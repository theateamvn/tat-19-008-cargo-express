@extends('layouts.admin.app')

@section('page-title')
    <h1>Categories
        <small>management</small>
    </h1>
@endsection

@section('content')
    <admin-categories-main></admin-categories-main>
@endsection
