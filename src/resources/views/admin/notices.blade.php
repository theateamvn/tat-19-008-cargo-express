@extends('layouts.admin.app')

@section('page-title')
  <h1>Notices
    <small>management</small>
  </h1>
@endsection

@section('content')
  <notice-list></notice-list>
@endsection

@section('scripts')
  <script src="../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
  <script src="../../assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
  <script src="../../assets/pages/scripts/modernizr.custom.js" type="text/javascript"></script>
@endsection

@section('css')
  <link href="../../assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
@endsection