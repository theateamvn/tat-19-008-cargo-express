@extends('layouts.admin.app')
@section('header-title')
Commodities
@endsection

@section('page-title')
  <h1>Commodity Categories
  </h1>
@endsection

@section('css')
  <link href="../assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <list-commodities-categories></list-commodities-categories>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="../assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/vue-simple-search-dropdown/vue-simple-search-dropdown.min.js" type="text/javascript"></script>
  <script src="https://unpkg.com/vue-select@2.4.0/dist/vue-select.js"></script>
  <script src="https://unpkg.com/vue-html-to-paper/build/vue-html-to-paper.js"></script>
@endsection
