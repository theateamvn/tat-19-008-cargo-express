@extends('layouts.app')

@section('page-title')
    <h1>Settings
    </h1>
@endsection

@section('content')
    @if(empty(Auth::user()->store()))
        <div class="note note-danger">
            <p> @lang('messages.note_store_must_config') </p>
        </div>
    @endif
    <store-setting-form></store-setting-form>
    <review-provider-setting></review-provider-setting>
    <sms-setting></sms-setting>
    <review-flow-setting></review-flow-setting>
@endsection

@section('scripts')
    <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
@endsection
