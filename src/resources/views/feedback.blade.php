@extends('layouts.app')

@section('page-title')
    <h1>Feedback
    </h1>
@endsection

@section('content')
    <div class="row">
        <feedback-panel></feedback-panel>
    </div>
@endsection