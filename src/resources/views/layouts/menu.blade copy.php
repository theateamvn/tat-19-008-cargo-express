<li class="{!! Request::is('dashboard*') || Request::is('/') ? 'active' : '' !!} sidebar-customer" id="menu-1">
    <a href="{!! url('/dashboard') !!}" class="nav-link nav-toggle">
        <i class="icon-diamond"></i>
        <span class="title">Dashboard</span>

    </a>
</li>
<li class="heading">
    <h3 class="uppercase">Information Data</h3>
</li>
<li data-target="#request-label" class="{!! (Request::is('customers*') || Request::is('customers*')) ? 'active' : '' !!} sidebar-customer" id="menu-2">
    <a href="{!! url('/customers') !!}" class="nav-link nav-toggle">
        <i class="icon-users"></i>
        <span class="title">Customers</span>
    </a>
</li>
<li data-toggle="collapse" data-target="#sender_recipient" class="nav-item {!! (Request::is('sender*') || Request::is('recipient*')) ? 'open' : '' !!} sidebar-customer" id="menu-1">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-paper-plane"></i>
        <span class="title">Sender/Recipient</span>
        <span class="arrow {!! (Request::is('sender*') || Request::is('recipient*')) ? 'open' : '' !!}"></span>
    </a>

    <ul class="sub-menu"   style="display: {!! (Request::is('sender*') || Request::is('recipient*')) ? 'block' : '' !!}" id="sender_recipient">
        <li class="{!! Request::is('sender*') ? 'active' : '' !!} sidebar-customer" id="menu-1-1">
            <a href="{!! url('/sender') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">Sender</span>
            </a>
        </li>
        <li class="{!! Request::is('recipient*') ? 'active' : '' !!} sidebar-customer" id="menu-1-2">
            <a href="{!! url('/recipient') !!}" class="nav-link  sub-active-custom">
                <i class="fa fa-registered"></i>
                <span class="title">Recipient</span>
            </a>
        </li>
    </ul>
</li>
<li class="heading">
    <h3 class="uppercase">Shipment Function</h3>
</li>
<li data-toggle="collapse" data-target="#shipments" class="nav-item {!! (Request::is('shipments*') || Request::is('recipients*')) ? 'active' : '' !!} sidebar-customer" id="menu-3">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-bag"></i>
        <span class="title">Shipments</span>
        <span class="arrow {!! Request::is('shipments*') ? 'open' : '' !!}"></span>
    </a>
    <ul class="sub-menu" style="display:{!! (Request::is('shipments*') || Request::is('shipments*')) ? 'block' : 'none' !!}" id="shipments">
        <li class="{!! Request::is('shipments') ? 'active' : '' !!} sidebar-customer" id="menu-3-1">
            <a href="{!! url('/shipments') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">List Shipment</span>
            </a>
        </li>
        <li class="{!! Request::is('shipments/create*') ? 'active' : '' !!} sidebar-customer" id="menu-3-2">
            <a href="{!! url('/shipments/create') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">Create Shipment</span>
            </a>
        </li>
    </ul>
</li>
<li data-toggle="collapse" data-target="#export_list" class="{!! (Request::is('export-list*') || Request::is('export-list*')) ? 'active' : '' !!} sidebar-customer nav-item" id="menu-4">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-bag"></i>
        <span class="title">Export List</span>
        <span class="arrow {!! Request::is('export-list*') ? 'open' : '' !!}"></span>
    </a>

    <ul class="sub-menu"  style="display:{!! (Request::is('export-list*') || Request::is('export-list*')) ? 'block' : 'none' !!}" id="export_list">
        <li class="{!! Request::is('export-list') ? 'active' : '' !!} sidebar-customer" id="menu-4-1">
            <a href="{!! url('/export-list') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">List</span>
            </a>
        </li>
        <li class="{!! Request::is('export-list/create*') ? 'active' : '' !!} sidebar-customer" id="menu-4-2">
            <a href="{!! url('/export-list/create') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">Create</span>
            </a>
        </li>
        <li class="{!! Request::is('export-list/import') ? 'active' : '' !!} sidebar-customer" id="menu-4-2">
            <a href="{!! url('/export-list/import') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">Import</span>
            </a>
        </li>
        <li class="{!! Request::is('export-list/import-list*') ? 'active' : '' !!} sidebar-customer" id="menu-4-2">
            <a href="{!! url('/export-list/import-list') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">Import List</span>
            </a>
        </li>
    </ul>
</li>

<li data-toggle="collapse" data-target="#commodities" class="nav-item  {!! (Request::is('commodities*') || Request::is('commodities*')) ? 'open' : '' !!} sidebar-customer" id="menu-5">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-layers"></i>
        <span class="title">Commodities</span>
        <span class="arrow {!! Request::is('commodities*') ? 'open' : '' !!}"></span>
    </a>

    <ul class="sub-menu {!! (Request::is('commodities*') || Request::is('commodities*')) ? 'in' : '' !!}" id="commodities">
        <li class="{!! Request::is('commodities') ? 'active' : '' !!} sidebar-customer" id="menu-5-1">
            <a href="{!! url('/commodities') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">List</span>
            </a>
        </li>
        <li class="{!! Request::is('commodities/charges*') ? 'active' : '' !!} sidebar-customer" id="menu-5-2">
            <a href="{!! url('/commodities/charges') !!}" class="nav-link sub-active-custom">
                <i class="fa fa-bolt"></i>
                <span class="title">Estimates Charges</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item {!! (Request::is('tracking*') || Request::is('tracking*') || Request::is('consolidated-tracking-list*')) ? 'open' : '' !!}">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-docs"></i>
        <span class="title">Tracking</span>
        <span class="arrow {!! (Request::is('tracking*') || Request::is('tracking*') || Request::is('consolidated-tracking-list*')) ? 'open' : '' !!}"></span>
    </a>
    <ul class="sub-menu" style="display: {!! (Request::is('tracking*') || Request::is('tracking*') || Request::is('consolidated-tracking-list*')) ? 'block' : 'none' !!};">
        <li class="{!! Request::is('tracking-list') ? 'active' : '' !!} sidebar-customer" id="menu-5-1">
            <a href="{!! url('/tracking-list')  !!}" class="nav-link">
                <i class="fa fa-bolt"></i>
                <span class="title">List</span>
            </a>
        </li>
        <li class="{!! Request::is('consolidated-tracking-list') ? 'active' : '' !!} sidebar-customer" id="menu-5-2">
        <a href="{!! url('/consolidated-tracking-list') !!}" class="nav-link sub-active-custom">
            <i class="fa fa-bolt"></i>
            <span class="title">List Consolidated</span>
        </a>
    </li>
    <li class="{!! Request::is('tracking/scan') ? 'active' : '' !!} sidebar-customer" id="menu-5-2">
        <a href="{!! url('/tracking/scan') !!}" class="nav-link sub-active-custom">
            <i class="fa fa-bolt"></i>
            <span class="title">Scan Tracking</span>
        </a>
    </li>
    <li class="{!! Request::is('tracking/consolidated-tracking') ? 'active' : '' !!} sidebar-customer" id="menu-5-2">
        <a href="{!! url('/tracking/consolidated-tracking') !!}" class="nav-link sub-active-custom">
            <i class="fa fa-bolt"></i>
            <span class="title">Consolidated Tracking</span>
        </a>
    </li>
    </ul>
</li>
<li class="heading">
    <h3 class="uppercase">Request Functions</h3>
</li>
<li data-target="#request-label" class="{!! (Request::is('request-label-list*') || Request::is('request-label-list*')) ? 'active' : '' !!} sidebar-customer" id="menu-5">
    <!-- <a href="{!! url('/commodities') !!}" class="nav-link nav-toggle"> -->
    <a href="{!! url('/request-label-list') !!}" class="nav-link nav-toggle">
        <i class="fab fa-accusoft"></i>
        <span class="title">Request Label</span>
    </a>
</li>

<li data-target="#request-customer" class="{!! (Request::is('request-customer-list*') || Request::is('request-customer-list*')) ? 'active' : '' !!} sidebar-customer" id="menu-5">
    <!-- <a href="{!! url('/commodities') !!}" class="nav-link nav-toggle"> -->
    <a href="{!! url('/request-customer-list') !!}" class="nav-link nav-toggle">
        <i class="fab fa-accusoft"></i>
        <span class="title">Request Customer</span>
    </a>
</li>