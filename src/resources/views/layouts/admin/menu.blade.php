<?php if(Auth::user()->role == 1) { ?>


<li class="{!! Request::is('admin/users') ? 'active' : '' !!} sidebar-customer">
    <a href="{!! url('admin/users') !!}" class="nav-link nav-toggle">
        <i class="icon-users"></i>
        <span class="title">Users Account</span>
    </a>
</li>

<li class="{!! Request::is('admin/service-type-zone') ? 'active' : '' !!} sidebar-customer">
    <a href="{!! url('admin/service-type-zone') !!}" class="nav-link nav-toggle">
        <i class="icon-list"></i>
        <span class="title">Service Type Zone</span>
    </a>
</li>
<li class="{!! Request::is('admin/city') ? 'active' : '' !!} sidebar-customer">
    <a href="{!! url('admin/city') !!}" class="nav-link nav-toggle">
        <i class="icon-list" ></i>
        <span class="title">City</span>
    </a>
</li>
<li class="{!! Request::is('admin/awb-air') ? 'active' : '' !!} sidebar-customer">
    <a href="{!! url('admin/awb-air') !!}" class="nav-link nav-toggle">
        <i class="icon-plane" ></i>
        <span class="title">Awb Air</span>
    </a>
</li>

<li data-toggle="collapse" data-target="#shipments" class="{!! (Request::is('shipments*') || Request::is('recipients*')) ? 'active' : '' !!} sidebar-customer" id="menu-3">
    <a href="#" class="nav-link nav-toggle">
        <i class="icon-bag"></i>
        <span class="title">Shipments</span>
        <span class="fa {!! Request::is('shipments*') ? 'fa-angle-down' : 'fa-angle-left' !!}"></span>
    </a>
</li>
<ul class="collapse sub-menu {!! (Request::is('shipments*') || Request::is('shipments*')) ? 'in' : '' !!}" id="shipments">
    <li class="{!! Request::is('admin/shipment-status') ? 'active' : '' !!} sidebar-customer">
        <a href="{!! url('admin/shipment-status') !!}" class="nav-link nav-toggle">
            <i class="icon-users"></i>
            <span class="title">Shipment Status</span>
        </a>
    </li>
</ul>
<li data-toggle="collapse" data-target="#commodities" class="{!! (Request::is('commodities*') || Request::is('commodities*')) ? 'active' : '' !!} sidebar-customer" id="menu-5">
    <!-- <a href="{!! url('/commodities') !!}" class="nav-link nav-toggle"> -->
    <a href="#" class="nav-link nav-toggle">
        <i class="icon-layers"></i>
        <span class="title">Commodities</span>
        <span class="fa {!! Request::is('commodities*') ? 'fa-angle-down' : 'fa-angle-left' !!}"></span>
    </a>
</li>
<ul class="collapse sub-menu {!! (Request::is('commodities*') || Request::is('commodities*')) ? 'in' : '' !!}" id="commodities">
    <li class="{!! Request::is('commodities/brand*') ? 'active' : '' !!} sidebar-customer" id="menu-5-3">
        <a href="{!! url('/commodities/brand') !!}" class="nav-link nav-toggle sub-active-custom">
            <i class="fa fa-bolt"></i>
            <span class="title">Brand</span>
        </a>
    </li>
    <li class="{!! Request::is('commodities/categories*') ? 'active' : '' !!} sidebar-customer" id="menu-5-4">
        <a href="{!! url('/commodities/categories') !!}" class="nav-link nav-toggle sub-active-custom">
            <i class="fa fa-bolt"></i>
            <span class="title">Categories</span>
        </a>
    </li>
</ul>
<li class="{!! Request::is('admin/request-label-list') ? 'active' : '' !!} sidebar-customer">
    <a href="{!! url('admin/request-label-list') !!}" class="nav-link nav-toggle">
        <i class="icon-list" ></i>
        <span class="title">Request Label</span>
    </a>
</li>
<li data-toggle="collapse" data-target="#setting_config" class="{!! (Request::is('setting-general*') || Request::is('setting-general*')) ? 'active' : '' !!} sidebar-customer" id="menu-3">
    <a href="{!! url('admin/setting-general') !!}" class="nav-link nav-toggle">
        <i class="icon-settings"></i>
        <span class="title">Setting General</span>
    </a>
</li>

<?php } ?>


