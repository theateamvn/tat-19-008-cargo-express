window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

//window.$ = window.jQuery = require('jquery');
//require('bootstrap-sass');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');
require('vue-resource');

// This is the event hub we'll use in every
// component to communicate between them.
window.eventHub = new Vue();

Vue.http.interceptors.push((request, next) => {
  request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

  Pace.start();

  next((response)=>{
    Pace.stop();
    if(response.status == 401){
        window.location.replace('/login');
    }
    if(response.status < 200 || response.status >299){
      if(response.data && response.data.message){
        window.eventHub.$emit('EVENT_SHOW_ALERT',{
          message:response.data.message,
          type:'danger'
        });
      }
      if(response.data && response.data.error){
        window.eventHub.$emit('EVENT_SHOW_ALERT',{
          message:response.data.error,
          type:'danger'
        });
      }
    }
  });
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
