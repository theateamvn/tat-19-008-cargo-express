/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue')
);

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue')
);

require('./components/setting/components');
require('./components/common/components');
require('./components/review/components');
require('./components/bad-review/components');
require('./components/customer/components');
require('./components/dashboard/components');
require('./components/template/components');
require('./components/admin/dashboard/components');
//require('./components/admin/dashboard-agent/components');

require('./components/admin/background/components');
require('./components/admin/settings/components');
require('./components/admin/setting_general/components');
require('./components/admin/categories/components');
require('./components/review_redirect/components');
require('./components/feedback/components');
require('./components/shipments/components');
require('./components/export_list/components');
require('./components/commodities/components');
// new
Vue.component('list-sender', require('./components/sender/ListSender.vue'));
Vue.component('list-recipient', require('./components/recipient/ListRecipient.vue'));
Vue.component('create-shipment', require('./components/shipments/create/CreateShipment.vue'));

Vue.component('list-commodities-categories', require('./components/commodities/categories/ListCommoditiesCategories.vue'));
Vue.component('list-commodities-brand', require('./components/commodities/brand/ListCommoditiesBrand.vue'));
Vue.component('list-commodities-charges', require('./components/commodities/estimates_charges/ListCommoditiesCharges.vue'));
Vue.component('list-check-shipments', require('./components/check_shipments/ListCheckShipments.vue'));
Vue.component('list-check-export-list', require('./components/check_shipments/ListCheckExportList.vue'));
Vue.component('list-check-export-list-2', require('./components/check_shipments/ListCheckExportList2.vue'));
Vue.component('list-export-list', require('./components/export_list/ListExportList.vue'));
Vue.component('import-list', require('./components/export_list/ListExportListImport.vue'));
Vue.component('import-export-list', require('./components/export_list/ImportExportList.vue'));
Vue.component('create-export-list', require('./components/export_list/CreateExportList.vue'));
Vue.component('scan-export-list', require('./components/export_list/ScanExportList.vue'));
Vue.component('export-awb', require('./components/export_list/ExportAwb.vue'));
Vue.component('request-label', require('./components/request_label/RequestLabel.vue'));
Vue.component('request-label-list', require('./components/request_label/RequestLabelList.vue'));
Vue.component('request-label-package-list', require('./components/request_label_package/RequestLabelPackageList.vue'));
Vue.component('awb-air-list', require('./components/admin/awb_air/List.vue'));
Vue.component('request-label-list-admin', require('./components/admin/request_label/RequestLabelList.vue'));

Vue.component('request-customer', require('./components/request_customer/RequestCustomer.vue'));
Vue.component('request-customer-list', require('./components/request_customer/RequestCustomerList.vue'));
Vue.component('consolidated-tracking-list', require('./components/tracking/ListConsolidatedTracking.vue'));
Vue.component('consolidated-tracking', require('./components/tracking/ConsolidatedTracking.vue'));
Vue.component('scan-tracking', require('./components/tracking/ScanTracking.vue'));
Vue.component('tracking-list', require('./components/tracking/ListTracking.vue'));
Vue.component('city-list', require('./components/admin/city/List.vue'));
Vue.component('service-type-zone-list', require('./components/admin/service_type_zone/List.vue'));
Vue.component('shipment-status-list', require('./components/admin/shipment_status/List.vue'));
Vue.component('user-list', require('./components/admin/user/List.vue'));
Vue.component('profile-show', require('./components/profile/Show.vue'));
Vue.component('notice-list', require('./components/admin/notice/List.vue'));
Vue.component('notices', require('./components/common/Notices.vue'));
//Vue.component('categories-list', require('./components/admin/categories/List.vue'));

//Login
Vue.component('customer-login', require('./components/login/customerLogin.vue'));

//test
Vue.component('test-api', require('./components/test/test.vue'));
console.log('%c WELCOME TO ALIOSTEK TEAM! ', 'background: #9CB8B3; color: #ffffff', 'Developer area!');

const app = new Vue({
  el: '#app'
});

window.showToast = function (title, message, type) {
  toastr[type](message, title);
}

window.showToastFromResponse = function (response) {
  var message = "";
  var type = "success";
  if (response) {
    if (response.status < 200 || response.status > 299) {
      if (response.data && response.data.message) {
        message = response.data.message
      }
      if (response.data && response.data.error) {
        message = response.data.error
      }
      type = 'error';
    } else {
      if (response.data && response.data.message) {
        message = response.data.message;
        type = 'success'
      }
    }
    window.showToast('',message,type);
  }
};
