/**
 * Created by acesi on 17/11/2016.
 */
Vue.component('store-setting-form',require('./StoreSettingForm.vue'));
Vue.component('review-provider-setting',require('./ReviewProviderSetting.vue'));
Vue.component('sms-setting',require('./SmsProviderSetting.vue'));
Vue.component('review-flow-setting',require('./ReviewFlowSetting.vue'));
