<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 2/20/2017
 * Time: 9:50 AM
 */

namespace App\Notifications\Channels;


use App\Notifications\Messages\SendInviteMessage;
use App\Notifications\SendReviewInvitation;
use Catapult\Client;
use Catapult\Credentials;
use Catapult\Message;
use Catapult\PhoneNumber;
use Catapult\TextMessage;
use Illuminate\Notifications\Notification;
use App\Repositories\InviteLogsRepository;

class SmsBandwidthChannel
{
    /**
     * @var Client
     */
    private $client;
    private $inviteLogsRepository;

    /**
     * TwilioChannel constructor.
     */
    public function __construct(InviteLogsRepository $inviteLogsRepo)
    {
         $this->inviteLogsRepository = $inviteLogsRepo;
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        $this->saveInviteLogs($notifiable);
        /** @var SendInviteMessage $message */
        $message = $notification->toSmsBandwidth($notifiable);
        $cred = new Credentials($message->sid, $message->token, $message->secret);
        $this->client = new Client($cred);
        // Send notification to the $notifiable instance...
        $this->client = new Message([
            "from" => new PhoneNumber($message->from), // Replace with a Bandwidth Number
            "to" => new PhoneNumber($message->to),
            "text" => new TextMessage($message->body)
        ]);

    }
    private function saveInviteLogs($notifiable) {
        $input['invite_id'] = $notifiable->id;
        $input['store_id'] = $notifiable->store_id;
        $input['type_detail_sent'] = 'smsBandwidth';
        $input['type_sent'] = 'phone_sent';
        $this->inviteLogsRepository->create($input);
        return true;
    }
}