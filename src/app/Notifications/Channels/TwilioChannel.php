<?php
namespace App\Notifications\Channels;

use App\Notifications\SendReviewInvitation;
use Illuminate\Notifications\Notification;
use Twilio\Rest\Client;
use App\Repositories\InviteLogsRepository;

/**
 * Created by PhpStorm.
 * User: Lai Vu
 * Date: 11/30/2016
 * Time: 10:09 AM
 */
class TwilioChannel
{

    /**
     * @var Client
     */
    private $client;
    private $inviteLogsRepository;

    /**
     * TwilioChannel constructor.
     */
    public function __construct(InviteLogsRepository $inviteLogsRepo)
    {
         $this->inviteLogsRepository = $inviteLogsRepo;
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        $this->saveInviteLogs($notifiable);
        /** var SendReviewInvitation $message */
        $message = $notification->toTwilio($notifiable);

        // Send notification to the $notifiable instance...
        $this->client = new Client($message->sid,$message->token);

        $this->client->messages->create($message->to,[
            'from'=>$message->from,
            'body'=>$message->body
        ]);

    }
    private function saveInviteLogs($notifiable) {
        $input['invite_id'] = $notifiable->id;
        $input['store_id'] = $notifiable->store_id;
        $input['type_detail_sent'] = 'twilio';
        $input['type_sent'] = 'phone_sent';
        $this->inviteLogsRepository->create($input);
        return true;
    }
}