<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 11/30/2016
 * Time: 11:22 AM
 */

namespace App\Notifications\Channels;


use Illuminate\Notifications\Notification;
use App\Repositories\InviteLogsRepository;

class MailChannel
{
    private $inviteLogsRepository;

    public function __construct(InviteLogsRepository $inviteLogsRepo)
    {
         $this->inviteLogsRepository = $inviteLogsRepo;
    }
    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification $notification
     */
    public function send($notifiable, Notification $notification)
    {
        $this->saveInviteLogs($notifiable);
        /** var SendReviewInvitation $message */
        $message = $notification->toMail($notifiable);

        \Mail::send([],[],function ($mail) use ($message){
            $mail->to($message->to)
                ->from($message->from,$message->name)
                ->subject($message->title)
                ->setBody($message->body,'text/html');
        });
    }
    private function saveInviteLogs($notifiable) {
        $input['invite_id'] = $notifiable->id;
        $input['store_id'] = $notifiable->store_id;
        $input['type_detail_sent'] = 'mail';
        $input['type_sent'] = 'email_sent';
        $this->inviteLogsRepository->create($input);
        return true;
    }
}