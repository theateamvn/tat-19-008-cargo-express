<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/14/2016
 * Time: 2:43 PM
 */

namespace App\Notifications\Channels;


use App\Notifications\SendReviewInvitation;
use Illuminate\Notifications\Notification;
use Twilio\Rest\Client;
use App\Repositories\InviteLogsRepository;

class MmsTwilioChannel
{
    /**
     * @var Client
     */
    private $client;
    private $inviteLogsRepository;

    /**
     * TwilioChannel constructor.
     */
    public function __construct(InviteLogsRepository $inviteLogsRepo)
    {
         $this->inviteLogsRepository = $inviteLogsRepo;
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        $this->saveInviteLogs($notifiable);
        /** var SendReviewInvitation $message */
        $message = $notification->toMmsTwilio($notifiable);

        // Send notification to the $notifiable instance...
        $this->client = new Client($message->sid,$message->token);

        if(is_array($message->to)){
            foreach ($message->to as $to){
                $this->client->messages->create($to,[
                    'from'=>$message->from,
                    'body'=>$message->body,
                    'mediaUrl'=>$message->mediaUrl
                ]);
            }
        }else{
            $this->client->messages->create($message->to,[
                'from'=>$message->from,
                'body'=>$message->body,
                'mediaUrl'=>$message->mediaUrl
            ]);
        }

    }
    private function saveInviteLogs($notifiable) {
        $input['invite_id'] = $notifiable->id;
        $input['store_id'] = $notifiable->store_id;
        $input['type_detail_sent'] = 'mmsTwilio';
        $input['type_sent'] = 'mms_sent';
        $this->inviteLogsRepository->create($input);
        return true;
    }






    public function send_bk($notifiable, Notification $notification)
    {
        /** var SendReviewInvitation $message */
        $message = $notification->toMmsTwilio($notifiable);

        // Send notification to the $notifiable instance...
        $this->client = new Client($message->sid,$message->token);

        $this->client->messages->create($message->to,[
            'from'=>$message->from,
            'body'=>$message->body,
            'mediaUrl'=>$message->mediaUrl
        ]);

    }
}