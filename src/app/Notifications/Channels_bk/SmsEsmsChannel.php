<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 2/20/2017
 * Time: 10:35 AM
 */

namespace App\Notifications\Channels;


use App\Notifications\Messages\SendInviteMessage;
use App\Notifications\SendReviewInvitation;
use Illuminate\Notifications\Notification;
use App\Services\Sms\Esms\Client;
use App\Repositories\InviteLogsRepository;

class SmsEsmsChannel
{
    /**
     * @var Client
     */
    private $client;
    private $inviteLogsRepository;
    /**
     * TwilioChannel constructor.
     */
    public function __construct(InviteLogsRepository $inviteLogsRepo)
    {
         $this->inviteLogsRepository = $inviteLogsRepo;
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        $this->saveInviteLogs($notifiable);
        /** @var SendInviteMessage $message */
        $message = $notification->toSmsEsms($notifiable);
        $this->client = new Client($message->token, $message->secret);
        $this->client->create($message->to, [
            "body" => $message->body,
            "smsType" => $message->from,
            "brandName"=>$message->brandName
        ]);

    }
    private function saveInviteLogs($notifiable) {
        $input['invite_id'] = $notifiable->id;
        $input['store_id'] = $notifiable->store_id;
        $input['type_sent'] = 'smsEsms';
        $this->inviteLogsRepository->create($input);
        return true;
    }
}