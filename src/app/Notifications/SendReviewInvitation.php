<?php

namespace App\Notifications;

use App\Exceptions\ConfigurationRequiredException;
use App\Models\Invite;
use App\Models\InviteMessage;
use App\Models\Setting;
use App\Notifications\Channels\MailChannel;
use App\Notifications\Channels\MmsTwilioChannel;
use App\Notifications\Channels\TwilioChannel;
use App\Notifications\Messages\SendInviteMessage;
use App\Services\InviteMessageService;
use App\Services\SettingService;
use App\Services\Utils\StringUtils;
use App\Services\Bijective;
use App\Exceptions\ServiceNotSupportException;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use App\Notifications\Channels\MmsBandwidthChannel;
use App\Notifications\Channels\SmsBandwidthChannel;
use App\Notifications\Channels\SmsEsmsChannel;


class SendReviewInvitation extends Notification
{
    use Queueable;

    const MATCH_BRACKET_PATTERN = '/\{{2}(([^{}]|(?R))*)\}{2}/';

    /**
     * @var Setting
     */
    private $setting;
    /**
     * @var InviteMessageService
     */
    private $inviteMessageService;
    /**
     * @var Invite
     */
    protected $invite;

    private $channels;

    private $bijective;

    /**
     * Create a new notification instance.
     * @param array $channels
     * @throws ConfigurationRequiredException
     */
    public function __construct(array $channels = ['sms', 'mms', 'email', 'email-request-label','email-request-customer-alert'])
    {
        $this->channels = $channels;
        $settingService = resolve(SettingService::class);
        $this->bijective = resolve(Bijective::class);
        if($this->channels[0] == "email"){
            $this->setting = $settingService->getSettingByUser();
        }else{
            $this->setting = $settingService->getSetting();
        }
        if (!$this->setting) {
            throw new ConfigurationRequiredException('Setting');
        }
        $this->inviteMessageService = resolve(InviteMessageService::class);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  Invite $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->invite = $notifiable;
        $channels = $this->getChannels();
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toMail($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->email_title)
            || empty($this->setting->sender_email
                || empty($this->setting->sender_name))
        ) {
            throw new ConfigurationRequiredException("Mail Provider");
        }
        // check type email 
        $type_email = $this->invite->type_email;//"email_tracking"; 
        if($this->invite->type_email == 'email_request'){
            $type_email = 'email_request';
        }
        if($type_email == 'email_request') {
            $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_MAIL_REQUEST_LABEL);
        } else if($type_email == 'email_request_customer_alert'){
            $inviteMessage = $this->inviteMessageService->getMessage(InviteMessage::TYPE_MAIL_REQUEST_CUSTOMER_ALERT);
        } else if($type_email == 'email_request_label_alert'){
            $inviteMessage = $this->inviteMessageService->getMessage(InviteMessage::TYPE_MAIL_REQUEST_LABEL_ALERT);
        } else { 
            $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_MAIL);
        }
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("Mail Content");
        }
        if($type_email == 'email_request') {
            return $this->getSendInviteMessageForMailRequestLabel($inviteMessage);
        } else if($type_email == 'email_request_customer_alert'){
            return $this->getSendInviteMessageForMailRequestCustomerAlert($inviteMessage);
        } else if($type_email == 'email_request_label_alert'){
            return $this->getSendInviteMessageForMailRequestLabelAlert($inviteMessage);
        } else {
            return $this->getSendInviteMessageForMail($inviteMessage);
        }
    }

    /**
     * @param Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toTwilio($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_username)
            || empty($this->setting->sms_password
                || empty($this->setting->sms_user))
        ) {
            throw new ConfigurationRequiredException("SMS Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_SMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("SMS Content");
        }

        return $this->getSendInviteMessageForSMS($inviteMessage);
    }

    public function toMmsTwilio($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_username)
            || empty($this->setting->sms_password
                || empty($this->setting->sms_user))
        ) {
            throw new ConfigurationRequiredException("SMS Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_MMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("MMS Content");
        }

        return $this->getSendInviteMessageForMMS($inviteMessage);
    }

    /**
     * @param Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toMmsBandwidth($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_bandwidth_phone)
            || empty($this->setting->sms_bandwidth_user_id)
            || empty($this->setting->sms_bandwidth_api_token)
            || empty($this->setting->sms_bandwidth_api_secret)
        ) {
            throw new ConfigurationRequiredException("Bandwidth Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_MMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("MMS Content");
        }

        return $this->getSendInviteMessageForMMSBandwidth($inviteMessage);
    }

    /**
     * @param Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toSmsBandwidth($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_bandwidth_phone)
            || empty($this->setting->sms_bandwidth_user_id)
            || empty($this->setting->sms_bandwidth_api_token)
            || empty($this->setting->sms_bandwidth_api_secret)
        ) {
            throw new ConfigurationRequiredException("Bandwidth Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_SMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("Sms Content");
        }

        return $this->getSendInviteMessageForSMSBandwidth($inviteMessage);
    }

    /**
     * @param $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toSmsEsms($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_esms_sms_type)
            || empty($this->setting->sms_esms_api_token)
            || empty($this->setting->sms_esms_api_secret)
        ) {
            throw new ConfigurationRequiredException("Esms Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_SMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("SMS Content");
        }

        return $this->getSendInviteMessageForSMSEsms($inviteMessage);
    }

    /**
     * @return array
     */
    protected function getChannels()
    {
        $channels = [];
        if (!empty($this->invite->email) && $this->hasChannel('email')) {
            $channels[] = MailChannel::class;
        }
        if (!empty($this->invite->email) && $this->hasChannel('email-request-label')) {
            $channels[] = MailChannel::class;
        }
        if (!empty($this->invite->email) && $this->hasChannel('email-request-customer-alert')) {
            $channels[] = MailChannel::class;
        }
        if (!empty($this->invite->email) && $this->hasChannel('email-request-label-alert')) {
            $channels[] = MailChannel::class;
        }
        if (!empty($this->invite->phone) && $this->hasChannel('sms')) {
            $channels[] = $this->getSmsChannelByGatewaySetting();
        }
        if (!empty($this->invite->phone) && $this->hasChannel('mms')) {
            $channels[] = $this->getMmsChannelByGatewaySetting();
            return $channels;
        }
        return $channels;
    }
    protected function getChannelsBK()
    {
        $channels = [];
        if (!$this->invite->email_sent && !empty($this->invite->email) && $this->hasChannel('email')) {
            $channels[] = MailChannel::class;
        }
        if (!$this->invite->phone_sent && !empty($this->invite->phone) && $this->hasChannel('sms')) {
            $channels[] = $this->getSmsChannelByGatewaySetting();
        }
        if (!$this->invite->mms_sent && !empty($this->invite->phone) && $this->hasChannel('mms')) {
            $channels[] = $this->getMmsChannelByGatewaySetting();
            return $channels;
        }
        return $channels;
    }

    /**
     * @return mixed
     */
    protected function getSmsChannelByGatewaySetting()
    {
        switch ($this->setting->sms_gateway) {
            case Setting::SMS_GATEWAY_TWILIO:
                return TwilioChannel::class;
            case Setting::SMS_GATEWAY_BANDWIDTH:
                return SmsBandwidthChannel::class;
            case Setting::SMS_GATEWAY_ESMS:
                return SmsEsmsChannel::class;
            default:
                return TwilioChannel::class;
        }
    }

    /**
     * @return mixed
     * @throws ServiceNotSupportException
     */
    protected function getMmsChannelByGatewaySetting()
    {
        /*if ($this->setting->sms_gateway == Setting::SMS_GATEWAY_ESMS) {
            throw new ServiceNotSupportException("{$this->setting->sms_gateway} is not support for mms");
        }*/
        switch ($this->setting->sms_gateway) {
            case Setting::SMS_GATEWAY_TWILIO:
                return MmsTwilioChannel::class;
            case Setting::SMS_GATEWAY_BANDWIDTH:
                return MmsBandwidthChannel::class;
            default:
                return MmsTwilioChannel::class;
        }
    }

    private function isMail($destination)
    {
        if (!$destination) return false;
        return preg_match('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', $destination);
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return string
     */
    private function getBodyContent($inviteMessage)
    {
        if ($inviteMessage->content) {
            return $this->replacePlaceholder($inviteMessage->content);
        }
        return $inviteMessage->fallback_content;
    }
    private function getBodyContentEmailRequestLabel($inviteMessage)
    {
        if ($inviteMessage->content) {
            return $this->replacePlaceholderRequestLabel($inviteMessage->content);
        }
        return $inviteMessage->fallback_content;
    }

    private function getBodyContentEmailRequestCustomerAlert($inviteMessage)
    {
        if ($inviteMessage->content) {
            return $this->replacePlaceholderRequestCustomerAlert($inviteMessage->content);
        }
        return $inviteMessage->fallback_content;
    }

    private function getBodyContentEmailRequestLabelAlert($inviteMessage)
    {
        if ($inviteMessage->content) {
            return $this->replacePlaceholderRequestLabelAlert($inviteMessage->content);
        }
        return $inviteMessage->fallback_content;
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMS(InviteMessage $inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_user;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->sid = $this->setting->sms_username;
        $sendInviteMessage->token = $this->setting->sms_password;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }

    private function getSendInviteMessageForMail(InviteMessage $inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sender_email;
        $sendInviteMessage->to = $this->invite->email;
        $sendInviteMessage->name = $this->setting->sender_name;
        $sendInviteMessage->title = $this->setting->email_title;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }

    private function getSendInviteMessageForMailRequestLabel(InviteMessage $inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sender_email;
        $sendInviteMessage->to = $this->invite->email;
        $sendInviteMessage->name = $this->setting->sender_name;
        $sendInviteMessage->title = $this->setting->email_title;
        $sendInviteMessage->body = $this->getBodyContentEmailRequestLabel($inviteMessage);
        return $sendInviteMessage;
    }

    private function getSendInviteMessageForMailRequestCustomerAlert(InviteMessage $inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sender_email;
        $sendInviteMessage->to = $this->invite->email;
        $sendInviteMessage->name = $this->setting->sender_name;
        $sendInviteMessage->title = $inviteMessage->title;//$this->setting->email_title;
        $sendInviteMessage->body = $this->getBodyContentEmailRequestCustomerAlert($inviteMessage);
        return $sendInviteMessage;
    }

    private function getSendInviteMessageForMailRequestLabelAlert(InviteMessage $inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sender_email;
        $sendInviteMessage->to = $this->invite->email;
        $sendInviteMessage->name = $this->setting->sender_name;
        $sendInviteMessage->title = $inviteMessage->title;
        $sendInviteMessage->body = $this->getBodyContentEmailRequestLabelAlert($inviteMessage);
        return $sendInviteMessage;
    }

    private function replacePlaceholderRequestLabel($input)
    {
        return StringUtils::replacePlaceholder(static::MATCH_BRACKET_PATTERN, $input, [
            'full_name'             => $this->invite->name,
            'email'                 => $this->invite->email,
            'tracking_label'        => $this->invite->tracking_label,
            'tracking_label_info'   => $this->invite->tracking_label_info
        ]);
    }
    private function replacePlaceholder($input)
    {
        return StringUtils::replacePlaceholder(static::MATCH_BRACKET_PATTERN, $input, [
            'full_name'             => $this->invite->name,
            'email'                 => $this->invite->email,
            'phone_number'          => $this->invite->phone,
            'customer_code'         => $this->invite->customer_code,
            'total_tracking'         => $this->invite->total_tracking,
            'tracking_number_list'   => $this->invite->tracking_number_list
        ]);
    }
    private function replacePlaceholderRequestLabelAlert($input)
    {
        return StringUtils::replacePlaceholder(static::MATCH_BRACKET_PATTERN, $input, [
            'customer_code'         => $this->invite->customer_code,
            'full_name'             => $this->invite->full_name,
            'email'                 => $this->invite->email,
            'send_date'             => $this->invite->send_date,
            'total_package'         => $this->invite->total_package,
            'delivery_company'      => $this->invite->delivery_company
        ]);
    }
    private function replacePlaceholderRequestCustomerAlert($input)
    {
        return StringUtils::replacePlaceholder(static::MATCH_BRACKET_PATTERN, $input, [
            'customer_name'         => $this->invite->customer_name,
            'email'                 => $this->invite->email,
            'customer_email'        => $this->invite->customer_email,
            'customer_phone'        => $this->invite->customer_phone,
            'payment'               => $this->invite->payment,
        ]);
    }
    private function replacePlaceholder_bk($input)
    {
        $cusId = 0;
        if($this->invite)
        {
            $cusId = $this->bijective->encode($this->invite->id);
        }
        return StringUtils::replacePlaceholder(static::MATCH_BRACKET_PATTERN, $input, [
            'full_name' => $this->invite->name,
            'email' => $this->invite->email,
            'phone_number' => $this->invite->phone,
            'invite_link' => $this->invite->getInviteLink(),
            'review_link' => $this->invite->store->getReviewRedirectLink($cusId),
            'business_name' => $this->invite->store->name,
            'business_address' => $this->invite->store->address
        ]);
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForMMS($inviteMessage)
    {
        $sendInviteMessage = $this->getSendInviteMessageForSMS($inviteMessage);
        $sendInviteMessage->mediaUrl = url($inviteMessage->getMmsMediaPath());
        return $sendInviteMessage;
    }

    private function hasChannel($string)
    {
        return in_array(strtolower($string), $this->channels);
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForMMSBandwidth($inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_bandwidth_phone;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->sid = $this->setting->sms_bandwidth_user_id;
        $sendInviteMessage->token = $this->setting->sms_bandwidth_api_token;
        $sendInviteMessage->secret = $this->setting->sms_bandwidth_api_secret;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        $sendInviteMessage->mediaUrl = url($inviteMessage->getMmsMediaPath());
        return $sendInviteMessage;
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMSBandwidth($inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_bandwidth_phone;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->sid = $this->setting->sms_bandwidth_user_id;
        $sendInviteMessage->token = $this->setting->sms_bandwidth_api_token;
        $sendInviteMessage->secret = $this->setting->sms_bandwidth_api_secret;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMSEsms($inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_esms_sms_type;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->token = $this->setting->sms_esms_api_token;
        $sendInviteMessage->secret = $this->setting->sms_esms_api_secret;
        $sendInviteMessage->brandName = $this->setting->sms_esms_brand_name;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }
}
