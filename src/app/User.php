<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
  use Notifiable, HasApiTokens;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'username',
    'full_name',
    'email',
    'password',
    'phone',
    'role',
    'note'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   **/
  public function storeUsers()
  {
    return $this->hasMany(\App\Models\StoreUser::class);
  }

  /**
   * Get first user's store
   * @return null|\App\Models\Store
   */
  public function store()
  {
    $stores = $this->storeUsers;
    if (count($stores) > 0) {
      return $stores[0];
    }
    return null;
  }
}
