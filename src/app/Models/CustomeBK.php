<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class Customer extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_customers';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* TYPE DEFINITIONS */
  const TYPE_WI   = 0;
  const TYPE_R    = 1;
  const TYPE_A    = 2;
  
  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;
  const STATUS_BANNED = 2;

  public $fillable = [
    'firstname',
    'lastname',
    'address',
    'email',
    'phone',
    'status',
    'country',
    'city',
    'customer_code',
    'member_level',
    'pay_to',
    'type_customer',
    'password',
    'created_by',
    'updated_by',
    'ip'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'firstname'   => 'string',
    'lastname'    => 'string',
    'address'     => 'string',
    'email'       => 'string',
    'phone'       => 'string',
    'status'      => 'integer',
    'country'     => 'integer',
    'city'     => 'integer',
    'customer_code'     => 'string',
    'member_level'      => 'integer',
    'pay_to'            => 'integer',
    'type_customer'     => 'integer',
    'password'          => 'string',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      'email' => 'required|email|max:500',
      'phone' => 'required|max:20',
      'firstname' => 'required|max:255',
      'lastname' => 'required|max:255',
      'address' => 'required|max:500',
      'country' => 'required',
      'city' => 'required'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
     'phone' => 'required|max:20',
     'firstname' => 'required|max:255',
     'lastname' => 'required|max:255',
     'address' => 'required|max:500',
     'country' => 'required',
     'city' => 'required'
  ];

  public static function rulesUpdate(){
      return array(
        'email' => 'required|email|max:255|unique:ec_customers,email',
        'phone' => 'required|max:20'
      );
  }

  public function isWI(){
    return $this->type_customer == static::TYPE_WI;
  }

  public function isCustomerRemote(){
    return $this->type_customer == static::TYPE_R;
  }

  public function isCustomerAgent(){
    return $this->type_customer == static::TYPE_A;
  }

  protected static function boot()
  {
      static::creating(function (Customer $customer) {
        // create customer code
        /*$number = mt_rand(10000, 99999);
        if($customer->type_customer != static::TYPE_WI){
          $type = "W";
          $code = static::generateCustomerCodeNumber($type);
          $customer->customer_code = $code;
        }
        if($customer->type_customer != static::TYPE_R){
          $type = "R";
          $code = static::generateCustomerCodeNumber($type);
          $customer->customer_code = $code;
          $customer->password = $code.'P';
        }
        if($customer->type_customer != static::TYPE_A){
          $type = "A";
          $code = static::generateCustomerCodeNumber($type);
          $customer->customer_code = $code;
          $customer->password = $code.'P';
        }*/
      });
      parent::boot();
  }
  private static function customerCodeExists($code) {
      return Customer::whereCustomerCode($code)->exists();
  }
  private static function generateCustomerCodeNumber($type) {
    $code = $type.mt_rand(10000, 99999);
    if (static::customerCodeExists($code)) {
        return static::generateCustomerCodeNumber($type);
    }
    return $code;
  }
}
