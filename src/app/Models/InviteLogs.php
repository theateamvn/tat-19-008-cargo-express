<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Feedback
 * @package App\Models
 * @version February 23, 2017, 2:42 pm UTC
 */
class InviteLogs extends Model
{

    public $table = 'invite_logs';

    const CREATED_AT = 'updated_at';



    public $fillable = [
        'invite_id',
        'store_id',
        'type_sent',
        'type_detail_sent',
        'ip'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'invite_id' => 'integer',
        'store_id' => 'integer',
        'type_sent' => 'string',
        'type_detail_sent' => 'string',
        'ip' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}
