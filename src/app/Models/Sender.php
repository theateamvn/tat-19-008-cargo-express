<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class Sender extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_sender';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;

  public $fillable = [
    'customer_id',
    'contact_code',
    'contact_firstname',
    'contact_lastname',
    'contact_address',
    'contact_country',
    'contact_city',
    'contact_zipcode',
    'contact_zipcode_name',
    'contact_email',
    'contact_phone',
    'status',
    'created_by',
    'updated_by',
    'ip'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'customer_id'       => 'integer',
    'contact_code'      => 'string',
    'contact_firstname' => 'string',
    'contact_lastname'  => 'string',
    'contact_address'   => 'string',
    'contact_country'   => 'integer',
    'contact_city'      => 'integer',
    'contact_zipcode'   => 'string',
    'contact_zipcode_name'   => 'string',
    'contact_email'     => 'string',
    'contact_phone'     => 'string',
    'status'            => 'integer',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'contact_email'         => 'email|max:500|unique:ec_sender,contact_email',
      'contact_email'         => 'sometimes|nullable|email|max:500',
      // 'contact_phone'         => 'required|max:20|unique:ec_sender,contact_phone',
      'contact_firstname'     => 'max:500',
      'contact_lastname'      => 'required|max:500',
      'contact_address'       => 'required|max:500',
      'contact_country'       => 'required|not_in:0',
      //'contact_city'          => 'required|not_in:0'
  ];

  public static function rulesUpdate($id){
    return array(
      //'contact_email'     => 'email|max:500|unique:ec_sender,contact_email,'.$id,
      'contact_email'     => 'sometimes|nullable|email|max:500'.$id,
      // 'contact_phone'     => 'required|max:20|unique:ec_sender,contact_phone,'.$id,
      'contact_firstname' => 'required|max:500',
      'contact_lastname'  => 'required|max:500',
      'contact_address'   => 'required|max:500',
      'contact_country'   => 'required|not_in:0',
      //'contact_city'      => 'required|not_in:0'
    );
  }
}
