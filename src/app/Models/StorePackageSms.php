<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Feedback
 * @package App\Models
 * @version February 23, 2017, 2:42 pm UTC
 */
class StorePackageSms extends Model
{

    public $table = 'store_package_sms';

    const CREATED_AT = 'updated_at';



    public $fillable = [
        'store_id',
        'user_id',
        'package',
        'total_allow_sent'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'user_id' => 'integer',
        'package' => 'string',
        'total_allow_sent' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}
