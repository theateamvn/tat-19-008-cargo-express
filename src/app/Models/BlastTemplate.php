<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class BlastTemplate
 *
 * @package App\Models
 * @version March 1, 2017, 1:14 pm UTC
 * @property int $id
 * @property string $language
 * @property string $content
 * @property array $extra_data
 * @property int $store_id
 * @property int $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $last_sent_at
 * @property-read \App\Models\Store $store
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientContact[] $contacts
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereExtraData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastTemplate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BlastTemplate extends Model
{

    public $table = 'blast_templates';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const TYPE_SMS = 0;
    const TYPE_MAIL = 1;
    const TYPE_MMS = 2;

    public $fillable = [
        'language',
        'content',
        'extra_data',
        'store_id',
        'type'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'last_sent_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'language' => 'string',
        'content' => 'string',
        'extra_data' => 'array',
        'store_id' => 'integer',
        'type' => 'integer',
        'last_sent_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'content' => 'required',
        'store_id' => 'required',
        'type' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function contacts()
    {
        return $this->belongsToMany(\App\Models\ClientContact::class, 'blast_contact');
    }

    public function getMmsMediaPath()
    {
        return array_get($this->extra_data, 'mediaUrl');
    }
}
