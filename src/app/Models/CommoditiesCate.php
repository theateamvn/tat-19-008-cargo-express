<?php

namespace App\Models;

use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;


class CommoditiesCate extends Authenticatable
{

  public $table = 'ec_commodities_cate';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;

  public $fillable = [
    'commodity_cate_name',
    'status',
    'created_by',
    'updated_by',
    'ip'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'commodity_cate_name'      => 'string',
    'status'            => 'integer',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string'
  ];

  public static $rules = [
      //'email' => 'required|email|max:255',
      'commodity_cate_name' => 'required|max:500'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      'commodity_cate_name' => 'required|max:500'
  ];
}
