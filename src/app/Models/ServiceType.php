<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class ServiceType extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_service_type';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;

  public $fillable = [
    'id',
    'service_type',
    'service_type_id',
    'service_type_item',
    'delivery_option',
    'rate',
    'discount_rate',
    'status',
    'created_by',
    'updated_by',
    'ip'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'                => 'integer',
    'service_type'      => 'string',
    'service_type_id'   => 'integer',
    'service_type_item' => 'string',
    'delivery_option'   => 'string',
    'rate'              => 'string',
    'discount_rate'     => 'string',
    'status'            => 'integer',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'email' => 'required|email|max:255',
     // 'customer_id' => 'required|max:20'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      //'customer_id' => 'required|max:20'
  ];
}
