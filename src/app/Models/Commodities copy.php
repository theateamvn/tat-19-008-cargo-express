<?php

namespace App\Models;

use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Commodities extends Authenticatable
{

  public $table = 'ec_commodities';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;

  public $fillable = [
    'commodity_name',
    'commodity_cate',
    'commodity_brand',
    'unit',
    'status',
    'created_by',
    'updated_by',
    'ip',
    'based_on',
    'max_item',
    'min_charge',
    'based_on_type',
    'condition_q',
    'condition_q_type',
    'condition_v',
    'condition_v_type',
    'condition_w',
    'condition_w_type',
    'value_correct',
    'value_correct_type',
    'value_uncorrect_type',
    'value_uncorrect',
    'value',
    'value_type',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'commodity_name'      => 'string',
    'commodity_cate'      => 'integer',
    'commodity_brand'  => 'integer',
    'unit'  => 'string',
    'status'            => 'integer',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string',
    'based_on' => 'string',
    'max_item'  => 'integer',
    'min_charge' => 'string',
    'based_on_type' => 'string',
    'condition_q' => 'string',
    'condition_q_type' => 'string',
    'condition_v' => 'string',
    'condition_v_type' => 'string',
    'condition_w' => 'string',
    'condition_w_type' => 'string',
    'value_correct' => 'string',
    'value_correct_type' => 'string',
    'value_uncorrect_type' => 'string',
    'value_uncorrect' => 'string',
    'value' => 'string',
    'value_type' => 'string',
  ];

  public static $rules = [
      //'email' => 'required|email|max:255',
      'commodity_name' => 'required|max:500',
      'commodity_cate' => 'required',
      'commodity_brand' => 'required',
      'unit' => 'required',
      'based_on' => 'required'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      'commodity_name' => 'required|max:500',
      'commodity_cate' => 'required',
      'commodity_brand' => 'required',
      'unit' => 'required',
      'based_on' => 'required'
  ];
}
