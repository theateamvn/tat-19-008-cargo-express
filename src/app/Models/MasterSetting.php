<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class MasterSetting
 * @package App\Models
 * @version February 8, 2017, 2:41 pm UTC
 */
class MasterSetting extends Model
{

    public $table = 'master_settings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'key',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'key' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    
}
