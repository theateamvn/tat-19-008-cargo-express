<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @package App\Models
 * @version November 17, 2016, 1:22 pm UTC
 * @property integer $id
 * @property string $username
 * @property string $full_name
 * @property string $website
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property integer $role
 * @property integer $status
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Store[] $storeUsers
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $readNotifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereFullName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRole($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereWebsite($value)
 * @mixin \Eloquent
 * @property string $stripe_id
 * @property string $card_brand
 * @property string $card_last_four
 * @property string $trial_ends_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\Subscription[] $subscriptions
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereStripeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCardBrand($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCardLastFour($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereTrialEndsAt($value)
 * @property string $note
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereNote($value)
 */
class User extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'users';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* ROLE DEFINITIONS */
  const ROLE_USER = 0;
  const ROLE_ADMIN = 1;
  const ROLE_AGENT = 2;

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;
  const STATUS_BANNED = 2;

  public $fillable = [
    'username',
    'full_name',
    'email',
    'phone',
    'website',
    'role',
    'status',
    'note',
    'password',
    'created_by',
    'updated_by',
    'package'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'username' => 'string',
    'full_name' => 'string',
    'email' => 'string',
    'password' => 'string',
    'phone' => 'string',
    'website' => 'string',
    'role' => 'integer',
    'status' => 'integer',
    'note' => 'string',
    'remember_token' => 'string',
    'created_by' => 'integer',
    'updated_by' => 'integer',
    'package' => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      'username' => 'required|min:6|max:255|without_spaces|unique:users',
      'email' => 'required|email|max:255|unique:users',
      'password' => 'required|min:6|confirmed',
      'full_name' => 'required|max:255',
      'phone' => 'required|max:20'
  ];

  public static $updateRules = [
      'username' => 'required|min:6|max:255|without_spaces|unique:users',
      'email' => 'required|email|max:255|unique:users',
      'full_name' => 'required|max:255',
      'phone' => 'required|max:20'
  ];

  public static function rulesUpdate($id){
      return array(
        'username' => 'required|min:6|max:255|without_spaces|unique:users,username,'.$id,
        'email' => 'required|email|max:255|unique:users,email,'.$id,
        'full_name' => 'required|max:255',
        'phone' => 'required|max:20'
      );
  }

  public static function rulesAdminUpdate($id){
      return array(
        'username' => 'min:6|max:255|without_spaces|unique:users,username,'.$id,
        'email' => 'email|max:255|unique:users,email,'.$id,
        'full_name' => 'max:255',
        'phone' => 'max:30',
        'password' => 'min:6|confirmed'
      );
  }

  public static $passwordRules = [
      'current_password' => 'required|min:6',
      'password' => 'required|min:6|confirmed'
  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   **/
  public function storeUsers()
  {
    return $this->belongsToMany(\App\Models\Store::class, 'store_users');
  }

  /**
   * Get first user's store
   * @return null|Store
   */
  public function store()
  {
    $stores = $this->storeUsers;
    if (count($stores) > 0) {
      return $stores[0];
    }
    return null;
  }

  public function isAdmin(){
    return $this->role == static::ROLE_ADMIN;
  }

  public function isAgent(){
    return $this->role == static::ROLE_AGENT;
  }
}
