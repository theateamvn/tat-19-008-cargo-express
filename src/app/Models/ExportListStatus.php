<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class ExportListStatus extends Authenticatable
{

  public $table = 'ec_export_list_status';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
    'customer_code',
    'customer_awb',
    'shipment_status'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'customer_code'      => 'string',
    'customer_awb'       => 'string',
    'shipment_status'    => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'email' => 'required|email|max:255',
     // 'customer_id' => 'required|max:20'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      //'customer_id' => 'required|max:20'
  ];
}
