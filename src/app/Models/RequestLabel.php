<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class RequestLabel extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_request_label';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  //const STATUS_PENDING = 0;
  //const STATUS_ACTIVATED = 1;

  public $fillable = [
    'id',
    'sender_name',
    'customer_code',
    'phone',
    'send_date',
    'email',
    'address',
    'zipcode',
    'city',
    'state',
    'service_option',
    'receiver_id',
    'receiver_name',
    'receiver_address',
    'delivery_company',
    'tracking_number',
    'status',
    'created_by',
    'updated_by',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'                => 'integer',
    'sender_name'       => 'string',
    'send_date'         => 'string',
    'customer_code'     => 'string',
    'address'           => 'string',
    'zipcode'           => 'string',
    'city'              => 'string',
    'state'             => 'string',
    'service_option'    => 'string',
    'receiver_id'       => 'integer',
    'receiver_name'     => 'string',
    'receiver_address'  => 'string',
    'delivery_company'  => 'integer',
    'tracking_number'   => 'string',
    'status'            => 'integer',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
    
  ];

  public static $updateRules = [
    
  ];
}
