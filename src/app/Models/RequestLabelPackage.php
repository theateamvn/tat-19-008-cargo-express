<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class RequestLabelPackage extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_request_label_package';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  //const STATUS_PENDING = 0;
  //const STATUS_ACTIVATED = 1;

  public $fillable = [
    'id',
    'request_label_id',
    'weight',
    'weight_unit',
    'height',
    'width',
    'length',
    'unit',
    'shippingRate',
    'status',
    'description'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'                => 'integer',
    'request_label_id'  => 'integer',
    'weight'            => 'string',
    'weight_unit'       => 'string',
    'height'            => 'string',
    'width'             => 'string',
    'length'            => 'string',
    'unit'              => 'string',
    'shippingRate'      => 'string',
    'status'            => 'integer',
    'description'       => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
    
  ];

  public static $updateRules = [
    
  ];
}
