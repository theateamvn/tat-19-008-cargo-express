<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class ShipmentStatusLogs extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_shipments_status_log';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;

  public $fillable = [
    'id',
    'shipment_id',
    'status_id',
    'log_note',
    'date_log',
    'created_by',
    'updated_by',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'                => 'integer',
    'shipment_id'       => 'integer',
    'status_id'         => 'integer',
    'log_note'          => 'string',
    'date_log'          => 'string',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'email' => 'required|email|max:255',
     // 'customer_id' => 'required|max:20'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      //'customer_id' => 'required|max:20'
  ];
}
