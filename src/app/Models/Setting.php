<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Setting
 *
 * @package App\Models
 * @version November 15, 2016, 2:18 pm UTC
 * @property integer $id
 * @property integer $store_id
 * @property boolean $image_message_enabled
 * @property integer $image_message_id
 * @property integer $review_goal
 * @property string $sender_email
 * @property string $sender_name
 * @property string $email_title
 * @property string $sms_username
 * @property string $sms_password
 * @property string $sms_user
 * @property string $sms_gateway
 * @property string $sms_bandwidth_phone
 * @property string $sms_bandwidth_user_id
 * @property string $sms_bandwidth_api_token
 * @property string $sms_bandwidth_api_secret
 * @property string $sms_esms_api_token
 * @property string $sms_esms_api_secret
 * @property string $sms_esms_sms_type
 * @property string $review_page_mode
 * @property string $email_nofify_feedback
 * @property string $sms_esms_brand_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Store $store
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereImageMessageEnabled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereImageMessageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereReviewGoal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereSenderEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereSenderName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereEmailTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereSmsUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereSmsPassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereSmsUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{

    public $table = 'settings';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const SMS_GATEWAY_TWILIO = "twilio";
    const SMS_GATEWAY_BANDWIDTH = "bandwidth";
    const SMS_GATEWAY_ESMS = "esms";

    const REVIEW_PAGE_REVIEW_ONLY = "review_only";
    const REVIEW_PAGE_SURVEY_REVIEW = "survey_review";

    public $fillable = [
        'store_id',
        'image_message_enabled',
        'image_message_id',
        'review_goal',
        'sender_email',
        'sender_name',
        'email_title',
        'email_request_label_title',
        'sms_username',
        'sms_password',
        'sms_user',
        'sms_gateway',
        'sms_bandwidth_phone',
        'sms_bandwidth_user_id',
        'sms_bandwidth_api_token',
        "sms_bandwidth_api_secret",
        "sms_esms_api_token",
        "sms_esms_api_secret",
        "sms_esms_sms_type",
        "review_page_mode",
        'email_nofify_feedback',
        'feedback_message',
        'sms_esms_brand_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'image_message_id' => 'integer',
        'review_goal' => 'integer',
        'sender_email' => 'string',
        'sender_name' => 'string',
        'email_title' => 'string',
        'email_request_label_title' => 'string',
        'sms_username' => 'string',
        'sms_password' => 'string',
        'sms_user' => 'string',
        'sms_gateway' => 'string',
        'sms_bandwidth_phone' => 'string',
        'sms_bandwidth_user_id' => 'string',
        'sms_bandwidth_api_token' => 'string',
        "sms_bandwidth_api_secret" => 'string',
        "sms_esms_api_token" => 'string',
        "sms_esms_api_secret" => 'string',
        "sms_esms_sms_type" => 'string',
        "review_page_mode" => "string",
        'email_nofify_feedback' => 'string',
        'feedback_message' => 'string',
        'sms_esms_brand_name'=>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    public static function createDefaultSetting($storeId, $sms_api_token, $sms_api_secret, $sms_brand_name)
    { // jim edited
        return static::create([
            'store_id' => $storeId,
            'sender_email' => 'info@companyname.com',
            'sender_name' => 'Marketing team',
            'email_title' => 'Thank you for review us',
            'sms_esms_api_token' => $sms_api_token,
            'sms_esms_api_secret' => $sms_api_secret,
            'sms_esms_brand_name' => $sms_brand_name,
            'sms_esms_sms_type' => 2,
            'sms_gateway' => 'esms'
        ]);
    }
}
