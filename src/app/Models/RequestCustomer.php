<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class RequestCustomer extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_request_customer';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  //const STATUS_PENDING = 0;
  //const STATUS_ACTIVATED = 1;

  public $fillable = [
    'id',
    'customer_name',
    'customer_address',
    'customer_zipcode',
    'customer_phone',
    'customer_email',
    'customer_id_number',
    'id_number_url',
    'consignee_name',
    'consignee_address',
    'consignee_phone',
    'consignee_country',
    'consignee_city',
    'consignee_zipcode',
    'consignee_district',
    'consignee_ward',
    'service_type_id',
    'payment',
    'status'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'                  => 'integer',
    'customer_name'       => 'string',
    'customer_address'    => 'string',
    'customer_zipcode'    => 'string',
    'customer_phone'      => 'string',
    'customer_email'      => 'string',
    'customer_id_number'  => 'string',
    'id_number_url'       => 'string',
    'consignee_name'      => 'string',
    'consignee_address'   => 'string',
    'consignee_phone'     => 'string',
    'consignee_country'   => 'integer',
    'consignee_city'      => 'integer',
    'consignee_zipcode'   => 'string',
    'consignee_district'  => 'integer',
    'consignee_ward'      => 'string',
    'service_type_id'     => 'integer',
    'payment'             => 'string',
    'status'              => 'integer'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
    
  ];

  public static $updateRules = [

  ];

}
