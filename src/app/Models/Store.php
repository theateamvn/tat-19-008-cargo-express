<?php

namespace App\Models;

use App\Observers\StoreObserver;
use Eloquent as Model;
use App\Models\Traits\UniqueSluggable;
use \Shorty;

/**
 * Class Store
 *
 * @package App\Models
 * @version December 14, 2016, 7:27 am UTC
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $address
 * @property string $company_logo
 * @property string $invitation_introducing
 * @property string $survey_message_1
 * @property string $survey_message_2
 * @property string $slug
 * @property string $review_redirect_background
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InviteMessage[] $inviteMessages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invite[] $invites
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReviewProvider[] $reviewProviders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Setting[] $settings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StoreUser[] $storeUsers
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereCompanyLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereInvitationIntroducing($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Store whereReviewRedirectBackground($value)
 */
class Store extends Model
{
    use UniqueSluggable;

    public $table = 'stores';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'name',
        'url',
        'address',
        'phone',
        'company_logo',
        'invitation_introducing',
        'review_redirect_background',
        'survey_message_1',
        'survey_message_2',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'url' => 'string',
        'phone' => 'string',
        
        'address' => 'string',
        'company_logo' => 'string',
        'invitation_introducing' => 'string',
        'slug' => 'string',
        'review_redirect_background' => 'string',
        'survey_message_1'=>'string',
        'survey_message_2'=>'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
        'slug' => 'slug|exists:stores|max:255',
        'address'=>'max:255',
        
        'phone'=>'max:50',
        'url'=>'max:255',
        'invitation_introducing'=>'max:255'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function inviteMessages()
    {
        return $this->hasMany(\App\Models\InviteMessage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function invites()
    {
        return $this->hasMany(\App\Models\Invite::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function reviewProviders()
    {
        return $this->hasMany(\App\Models\ReviewProvider::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function reviews()
    {
        return $this->hasMany(\App\Models\Review::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function settings()
    {
        return $this->hasMany(\App\Models\Setting::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function storeUsers()
    {
        return $this->hasMany(\App\Models\StoreUser::class);
    }

    public function sluggable()
    {
        return [
            'source' => 'name',
            'target' => 'slug'
        ];
    }

    public function getReviewRedirectLink($cusId = null){
       if($cusId){
            $longUrl = route('review.redirect.slug.id', ['slug' => $this->slug, 'id' => $cusId]);
        }else {
            $longUrl = route('review.redirect.slug', ['slug' => $this->slug]);
        }
        return Shorty::shorten($longUrl);
    }

    /**
     * @return User
     */
    public function user(){
        return $this->storeUsers->first();
    }
}
