<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class BlastContact
 *
 * @package App\Models
 * @version March 1, 2017, 2:34 pm UTC
 * @property int $id
 * @property int $blast_template_id
 * @property int $client_contact_id
 * @property-read \App\Models\BlastTemplate $blastTemplate
 * @property-read \App\Models\ClientContact $clientContact
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastContact whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastContact whereBlastTemplateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlastContact whereClientContactId($value)
 * @mixin \Eloquent
 */
class BlastContact extends Model
{

    public $table = 'blast_contact';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'blast_template_id',
        'client_contact_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'blast_template_id' => 'integer',
        'client_contact_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blastTemplate()
    {
        return $this->belongsTo(\App\Models\BlastTemplate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function clientContact()
    {
        return $this->belongsTo(\App\Models\ClientContact::class);
    }
}
