<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ActivityLog
 *
 * @package App\Models
 * @version December 5, 2016, 8:10 am UTC
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property string $ip_address
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityLog whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityLog whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityLog whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityLog whereIpAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ActivityLog extends Model
{

    public $table = 'activity_log';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'user_id',
        'text',
        'ip_address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'text' => 'string',
        'ip_address' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
