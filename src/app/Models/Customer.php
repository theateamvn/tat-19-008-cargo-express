<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class Customer extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_customers';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* TYPE DEFINITIONS */
  const TYPE_WI   = 0;
  const TYPE_R    = 1;
  const TYPE_A    = 2;

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;
  const STATUS_BANNED = 2;

  public $fillable = [
    'firstname',
    'lastname',
    'address',
    'email',
    'phone',
    'status',
    'country',
    'city',
    'city_name_full',
    'service_type_id',
    'customer_code',
    'member_level',
    'discount_rate',
    'pay_to',
    'type_customer',
    'customer_upload_id',
    'customer_id_number',
    'password',
    'created_by',
    'updated_by',
    'zipcode',
    'ip'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'          => 'integer',
    'firstname'   => 'string',
    'lastname'    => 'string',
    'address'     => 'string',
    'email'       => 'string',
    'phone'       => 'string',
    'discount_rate'       => 'string',
    'status'      => 'integer',
    'country'     => 'integer',
    'city'        => 'integer',
    'city_name_full'     => 'string',
    'customer_code'     => 'string',
    'member_level'      => 'integer',
    'customer_id_number'      => 'string',
    'customer_upload_id'      => 'string',
    'service_type_id'         => 'integer',
    'pay_to'            => 'integer',
    'type_customer'     => 'integer',
    'password'          => 'string',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string',
    'zipcode'                => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'email' => 'required|email|max:500|unique:ec_customers,email',
      'email' => 'sometimes|nullable|email|max:500',
      //'phone' => 'required|max:20|unique:ec_customers,phone',
      'phone' => 'required|max:20',
      'customer_code' => 'required|max:255',
      'firstname' => 'max:255',
      'lastname' => 'required|max:255',
      'address' => 'required|max:500',
      'country' => 'required|not_in:0',
      'type_customer' => 'required',
      'zipcode' => 'required',
      //'city' => 'required|not_in:0'
  ];

  public static function rulesUpdate($id){
    return array(
      //'email' => 'required|email|max:500|unique:ec_customers,email,'.$id,
      'email' => 'sometimes|nullable|email|max:500'.$id,
      //'phone' => 'required|max:20|unique:ec_customers,phone,'.$id,
      'phone' => 'required|max:20',
      'customer_code' => 'required|max:255',
      'firstname' => 'max:255',
      'lastname' => 'required|max:255',
      'address' => 'required|max:500',
      'country' => 'required|not_in:0',
      'type_customer' => 'required',
      'zipcode' => 'required',
      //'city' => 'required|not_in:0'
    );
  }

  public function isWI(){
    return $this->type_customer == static::TYPE_WI;
  }

  public function isCustomerRemote(){
    return $this->type_customer == static::TYPE_R;
  }

  public function isCustomerAgent(){
    return $this->type_customer == static::TYPE_A;
  }
}
