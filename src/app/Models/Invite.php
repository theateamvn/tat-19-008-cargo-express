<?php

namespace App\Models;

use App\Services\Bijective;
use Eloquent as Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\LogsActivity;

/**
 * Class Invite
 *
 * @package App\Models
 * @version December 10, 2016, 4:14 pm UTC
 * @property int $id
 * @property int $store_id
 * @property string $phone
 * @property string $email
 * @property string $name
 * @property string $created_by
 * @property string $updated_by
 * @property bool $followed_link
 * @property bool $recommended
 * @property \Carbon\Carbon $email_sent
 * @property \Carbon\Carbon $phone_sent
 * @property \Carbon\Carbon $mms_sent
 * @property bool $review
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Store $store
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereFollowedLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereRecommended($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereEmailSent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite wherePhoneSent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereMmsSent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereReview($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User $owner
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $readNotifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 */
class Invite extends RecordFingerPrintModel
{

    use Notifiable;

    public $table = 'invites';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = [
        'created_at',
        'updated_at',
        'email_sent',
        'phone_sent',
        'mms_sent',
    ];

    public $fillable = [
        'store_id',
        'phone',
        'email',
        'name',
        'created_by',
        'updated_by',
        'followed_link',
        'recommended',
        'email_sent',
        'phone_sent',
        'mms_sent',
        'review',
        'type',
        'birthday',
        'gender',
        'note'        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'phone' => 'string',
        'email' => 'string',
        'name' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'type' => 'integer',
        'followed_link' => 'boolean',
        'recommended' => 'boolean',
        'email_sent' => 'datetime',
        'phone_sent' => 'datetime',
        'mms_sent' => 'datetime',
        'review' => 'boolean',
        'birthday' => 'string',
        'gender' => 'string',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
        'phone' => 'numeric|phone|required_without_all:email',
        'email' => 'email|required_without_all:phone',
        'name' => 'required|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    public function getInviteLink()
    {
        $bijective = resolve(Bijective::class);
        return route('review.redirect', ['shortId' => $bijective->encode($this->id)]);
    }
    public function reviewLink($inviteId)
    {
        
    }
}
