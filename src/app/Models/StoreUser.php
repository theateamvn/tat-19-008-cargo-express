<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StoreUser
 *
 * @package App\Models
 * @version November 15, 2016, 2:18 pm UTC
 * @property integer $id
 * @property integer $store_id
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Store $store
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StoreUser whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StoreUser whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StoreUser whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StoreUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StoreUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StoreUser extends Model
{

    public $table = 'store_users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'store_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
