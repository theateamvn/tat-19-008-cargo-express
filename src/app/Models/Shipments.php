<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class Shipments extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_shipments';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;

  public $fillable = [
    'customer_id',
    'sender_id',
    'shipment_code',
    'recipient_id',
    'info_service_type',
    'info_service_item',
    'payment_in',
    'payment_in_type',
    'payment_type',
    'payment_type_item',
    'payment_type_paid_type',
    'status',
    'status_note',
    'created_by',
    'updated_by',
    'ip'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'customer_id'         => 'integer',
    'sender_id'           => 'integer',
    'shipment_code'       => 'string',
    'recipient_id'        => 'integer',
    'info_service_type'   => 'integer',
    'info_service_item'   => 'integer',
    'payment_in'          => 'integer',
    'payment_in_type'     => 'integer',
    'payment_type'        => 'integer',
    'payment_type_item'       => 'integer',
    'payment_type_paid_type'  => 'integer',
    'status'            => 'integer',
    'status_note'       => 'string',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'email' => 'required|email|max:255',
     // 'customer_id' => 'required|max:20'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      //'customer_id' => 'required|max:20'
  ];
}
