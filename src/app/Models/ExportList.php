<?php

namespace App\Models;

use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;


class ExportList extends Authenticatable
{

  public $table = 'ec_export_list';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  const STATUS_PENDING = 0;
  const STATUS_ACTIVATED = 1;

  public $fillable = [
    'export_list_name',
    'maw',
    'code_T_Express',
    'total_weight',
    'total_packages',
    'total_box',
    'status',
    'created_by',
    'updated_by',
    'ip',
    'shipment_barcode_airport',
    'shipment_barcode_awb',
    'shipment_barcode_departure',
    'shipment_barcode_destination',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'                => 'integer',
    'export_list_name'  => 'string',
    'maw'               => 'string',
    'code_T_Express'    => 'string',
    'total_weight'      => 'string',
    'total_packages'    => 'string',
    'total_box'         => 'string',
    'status'            => 'integer',
    'created_by'        => 'integer',
    'updated_by'        => 'integer',
    'ip'                => 'string',
    'shipment_barcode_airport'         => 'string',
    'shipment_barcode_awb'             => 'string',
    'shipment_barcode_departure'       => 'string',
    'shipment_barcode_destination'     => 'string',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'email' => 'required|email|max:255',
     // 'customer_id' => 'required|max:20'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      //'customer_id' => 'required|max:20'
  ];
}
