<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;


class Country extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  public $table = 'ec_city';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  /* STATUS DEFINATIONS */
  //const STATUS_PENDING = 0;
  //const STATUS_ACTIVATED = 1;

  public $fillable = [
    'id',
    'country_name',
    'status'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id'                => 'integer',
    'country_name'      => 'string',
    'status'            => 'integer',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
      //'email' => 'required|email|max:255',
     // 'customer_id' => 'required|max:20'
  ];

  public static $updateRules = [
     // 'email' => 'required|email|max:255|unique:customers',
      //'customer_id' => 'required|max:20'
  ];
}
