<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/2/2016
 * Time: 10:28 AM
 */

namespace App\DTOs;


class OverviewInformation
{
    public $shipmentInDate;
    public $totalShipments;
    public $totalCustomers;
    public $totalSender;

    public $overallRating;
    public $totalReview;
    public $reviewByGoogle;
    public $reviewByFacebook;
    public $reviewByYelp;
    public $reviewByTripadvisor;
    public $ratingByGoogle;
    public $ratingByFacebook;
    public $ratingByYelp;
    public $ratingByTripadvisor;
    public $inviteSentThisMonth;
    public $inviteSentLastMonth;

    /**
     * @var CustomerInformation
     */
    public $customerInformation;
}