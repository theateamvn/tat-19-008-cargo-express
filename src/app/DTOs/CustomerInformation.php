<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 18/12/2016
 * Time: 11:22 AM
 */

namespace App\DTOs;


class CustomerInformation
{
    public $all;
    public $smsSentCount;
    public $emailSentCount;
    public $mmsSentCount;
    public $newCreatedCount;
}