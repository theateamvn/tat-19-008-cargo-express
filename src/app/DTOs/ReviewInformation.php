<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 24/11/2016
 * Time: 3:39 PM
 */

namespace App\DTOs;


use App\Models\Review;

class ReviewInformation
{
  /**
   * @var string
   */
  public $rating;
  /**
   * @var Review[]
   */
  public $reviews = [];

  /**
   * @var string
   */
  public $provider;

  public function merge(ReviewInformation $reviewInformation){
      $this->reviews = array_merge($this->reviews, $reviewInformation->reviews);
  }
}