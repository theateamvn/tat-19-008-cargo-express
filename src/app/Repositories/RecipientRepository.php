<?php

namespace App\Repositories;

use App\Models\Recipient;
use InfyOm\Generator\Common\BaseRepository;

class RecipientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'contact_code' => 'like',
        'contact_name' => 'like',
        'contact_phone' => 'like',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Recipient::class;
    }
}
