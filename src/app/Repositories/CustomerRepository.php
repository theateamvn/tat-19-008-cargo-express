<?php

namespace App\Repositories;

use App\Models\Customer;
use InfyOm\Generator\Common\BaseRepository;

class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'firstname'  => 'like',
        'lastname' => 'like',
        'email' => 'like',
        'phone' => 'like',
        'type',
        'status',
        'customer_code'  => 'like',
        'type_customer'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
