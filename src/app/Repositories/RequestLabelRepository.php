<?php

namespace App\Repositories;

use App\Models\RequestLabel;
use InfyOm\Generator\Common\BaseRepository;

class RequestLabelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'sender_name',
        'customer_code',
        'send_date',
        'address',
        'zipcode',
        'service_option',
        'receiver_id',
        'receiver_name',
        'receiver_address',
        'delivery_company'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestLabel::class;
    }
}
