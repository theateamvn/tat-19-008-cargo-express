<?php

namespace App\Repositories;

use App\Models\ShipmentStatus;
use InfyOm\Generator\Common\BaseRepository;

class ShipmentStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'vietnam_name',
        'english_name',
        'note'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ShipmentStatus::class;
    }
}
