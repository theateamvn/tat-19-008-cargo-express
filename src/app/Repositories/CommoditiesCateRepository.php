<?php

namespace App\Repositories;

use App\Models\CommoditiesCate;
use InfyOm\Generator\Common\BaseRepository;

class CommoditiesCateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'commodity_cate_name' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommoditiesCate::class;
    }
}
