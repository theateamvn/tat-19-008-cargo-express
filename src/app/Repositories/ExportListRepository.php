<?php

namespace App\Repositories;

use App\Models\ExportList;
use InfyOm\Generator\Common\BaseRepository;

class ExportListRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ExportList::class;
    }
}
