<?php

namespace App\Repositories;

use App\Models\ServiceType;
use InfyOm\Generator\Common\BaseRepository;

class ServiceTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
    'id',
    'service_type',
    'service_type_id',
    'service_type_item',
    'delivery_option',
    'rate',
    'discount_rate',
    'status',
    'created_by',
    'updated_by',
    'ip'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceType::class;
    }
}
