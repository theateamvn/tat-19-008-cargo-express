<?php

namespace App\Repositories;

use App\Models\InviteMessage;
use InfyOm\Generator\Common\BaseRepository;

class InviteMessageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'language',
        'content',
        'fallback_content',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InviteMessage::class;
    }
}
