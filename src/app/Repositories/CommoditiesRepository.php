<?php

namespace App\Repositories;

use App\Models\Commodities;
use InfyOm\Generator\Common\BaseRepository;

class CommoditiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'commodity_name',
        'commodity_cate',
        'commodity_brand',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Commodities::class;
    }
}
