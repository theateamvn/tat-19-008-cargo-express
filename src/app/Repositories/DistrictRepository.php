<?php

namespace App\Repositories;

use App\Models\District;
use InfyOm\Generator\Common\BaseRepository;

class DistrictRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'district_name',
        'city_id',
        'zipcode',
        'zone_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return District::class;
    }
}
