<?php

namespace App\Repositories;

use App\Models\City;
use InfyOm\Generator\Common\BaseRepository;

class CityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'city_name',
        'country_id',
        'zone_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return City::class;
    }
}
