<?php

namespace App\Repositories;

use App\Models\Invite;
use InfyOm\Generator\Common\BaseRepository;

class InviteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'phone' => 'like',
        'email' => 'like',
        'name' => 'like',
        'owner.name' => 'like',
        'email_sent',
        'phone_sent',
        'mms_sent'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Invite::class;
    }
}
