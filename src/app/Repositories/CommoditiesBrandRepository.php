<?php

namespace App\Repositories;

use App\Models\CommoditiesBrands;
use InfyOm\Generator\Common\BaseRepository;

class CommoditiesBrandRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'commodity_brand_name',
        'commodity_cate'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommoditiesBrands::class;
    }
}
