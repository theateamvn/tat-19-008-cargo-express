<?php

namespace App\Repositories;

use App\Models\Shipments;
use InfyOm\Generator\Common\BaseRepository;

class ShipmentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'contact_code',
        'contact_name',
        'contact_phone',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Shipments::class;
    }
}
