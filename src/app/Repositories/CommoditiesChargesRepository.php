<?php

namespace App\Repositories;

use App\Models\CommoditiesCharges;
use InfyOm\Generator\Common\BaseRepository;

class CommoditiesChargesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'commodity_charges_name',
        'commodity_cate',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommoditiesCharges::class;
    }
}
