<?php

namespace App\Repositories;

use App\Models\TrackingConsolidated;
use InfyOm\Generator\Common\BaseRepository;

class TrackingConsolidatedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'customer_code',
        'group_tracking',
        'weight',
        'depth',
        'width',
        'height',
        'note',
        'status',
        'created_at',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TrackingConsolidated::class;
    }
}
