<?php

namespace App\Repositories;

use App\Models\ReviewProvider;
use InfyOm\Generator\Common\BaseRepository;

class ReviewProviderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'name',
        'reviewed_key',
        'reviewed_url',
        'extra_data'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReviewProvider::class;
    }
}
