<?php

namespace App\Repositories;

use App\Models\ActivityLog;
use InfyOm\Generator\Common\BaseRepository;

class ActivityLogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'text',
        'ip_address'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ActivityLog::class;
    }
}
