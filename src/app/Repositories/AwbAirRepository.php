<?php

namespace App\Repositories;

use App\Models\AwbAir;
use InfyOm\Generator\Common\BaseRepository;

class AwbAirRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'air_name',
        'air_pre_code',
        'status',
        'ip'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AwbAir::class;
    }
}
