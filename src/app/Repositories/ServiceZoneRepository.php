<?php

namespace App\Repositories;

use App\Models\ServiceZone;
use InfyOm\Generator\Common\BaseRepository;

class ServiceZoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'zone_name',
        'status',
        'created_by',
        'updated_by',
        'ip'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceZone::class;
    }
}
