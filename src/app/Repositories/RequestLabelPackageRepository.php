<?php

namespace App\Repositories;

use App\Models\RequestLabelPackage;
use InfyOm\Generator\Common\BaseRepository;

class RequestLabelPackageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'request_label_id',
        'weight',
        'weight_unit',
        'height',
        'width',
        'length',
        'unit',
        'shippingRate',
        'status',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestLabelPackage::class;
    }
}
