<?php

namespace App\Repositories;

use App\Models\ExportListStatus;
use InfyOm\Generator\Common\BaseRepository;

class ExportListImportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_code',
        'customer_awb',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ExportListStatus::class;
    }
}
