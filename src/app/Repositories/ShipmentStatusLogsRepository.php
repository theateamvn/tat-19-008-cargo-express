<?php

namespace App\Repositories;

use App\Models\ShipmentStatusLogs;
use InfyOm\Generator\Common\BaseRepository;

class ShipmentStatusLogsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'shipment_id',
        'status_id',
        'log_note',
        'date_log'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ShipmentStatusLogs::class;
    }
}
