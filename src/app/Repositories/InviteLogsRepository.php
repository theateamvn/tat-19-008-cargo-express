<?php

namespace App\Repositories;

use App\Models\InviteLogs;
use InfyOm\Generator\Common\BaseRepository;

class InviteLogsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'invite_id',
        'type_sent',
        'ip'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InviteLogs::class;
    }
}