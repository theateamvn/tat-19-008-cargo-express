<?php

namespace App\Repositories;

use App\Models\Customer;
use InfyOm\Generator\Common\BaseRepository;

class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'firstname',
        'lastname',
        'address',
        'email',
        'phone',
        'type',
        'status',
        'country',
        'customer_code',
        'member_level',
        'pay_to',
        'type_customer',
        'password',
        'created_by',
        'updated_by',
        'ip'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
