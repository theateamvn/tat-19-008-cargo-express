<?php

namespace App\Repositories;

use App\Models\RequestCustomer;
use InfyOm\Generator\Common\BaseRepository;

class RequestCustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'customer_name',
        'customer_address',
        'customer_zipcode',
        'customer_phone',
        'customer_email',
        'customer_id_number',
        'id_number_url',
        'consignee_name',
        'consignee_address',
        'consignee_phone',
        'consignee_country',
        'consignee_city',
        'consignee_zipcode',
        'consignee_district',
        'consignee_ward',
        'service_type_id',
        'payment',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestCustomer::class;
    }
}
