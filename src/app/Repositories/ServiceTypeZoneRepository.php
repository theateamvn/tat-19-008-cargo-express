<?php

namespace App\Repositories;

use App\Models\ServiceTypeZone;
use InfyOm\Generator\Common\BaseRepository;

class ServiceTypeZoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
    'id',
    'zone_to',
    'service_type',
    'service_type_item',
    'rate',
    'discount_rate',
    'status',
    'created_by',
    'updated_by',
    'ip'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceTypeZone::class;
    }
}
