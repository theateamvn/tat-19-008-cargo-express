<?php

namespace App\Repositories;

use App\Models\StorePackageSms;
use InfyOm\Generator\Common\BaseRepository;

class UserPackageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'store_id',
        'user_id',
        'package',
        'total_allow_sent',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StorePackageSms::class;
    }
}
