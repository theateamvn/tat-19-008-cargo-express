<?php

namespace App\Repositories;

use App\Models\ExportListStatus;
use InfyOm\Generator\Common\BaseRepository;

class CheckExportListRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_code',
        'contact_awb',
        'shipment_status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ExportListStatus::class;
    }
}
