<?php

namespace App\Repositories;

use App\Models\Setting;
use InfyOm\Generator\Common\BaseRepository;

class SettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'image_message_enabled',
        'image_message_id',
        'review_goal',
        'sender_email',
        'sender_name',
        'email_title',
        'sms_username',
        'sms_password',
        'sms_user'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Setting::class;
    }
}
