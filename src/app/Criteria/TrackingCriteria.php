<?php

namespace App\Criteria;

use Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


class TrackingCriteria implements CriteriaInterface
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request['customer_code']){
            $model->where('customer_code','LIKE', "%{$this->request['customer_code']}%");
        }
        if($this->request['tracking_code']){
            $model->where('tracking_code','LIKE', "%{$this->request['tracking_code']}%");
        }
        if(isset($this->request['from'], $this->request['to'])){
            $model->whereBetween('created_at',[$this->request['from'], $this->request['to']]);
        }
        return $model;
    }
}
