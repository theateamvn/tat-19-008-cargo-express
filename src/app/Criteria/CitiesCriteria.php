<?php

namespace App\Criteria;

use Auth;
use App\Models\City;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


class CitiesCriteria implements CriteriaInterface
{


    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('status', '<>', '-1');
        return $model;
    }
}
