<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class RequestCustomerCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request['customer_phone']){
            $model->where('ec_request_customer.customer_phone','LIKE', "%{$this->request['customer_phone']}%");
        }
        if($this->request['customer_email']){
            $model->where('ec_request_customer.customer_email','LIKE', "%{$this->request['customer_email']}%");
        }
        if($this->request['customer_name']){
            $model->where('ec_request_customer.customer_name','LIKE', "%{$this->request['customer_name']}%");
        }
        if($this->request['customer_zipcode']){
            $model->where('ec_request_customer.customer_zipcode', $this->request['customer_zipcode']);
        }
        if($this->request['status']){
            $model->where('ec_request_customer.status', $this->request['status']);
        }
        return $model;
    }
}