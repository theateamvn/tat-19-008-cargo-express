<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListCommoditiesChargesCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_commodities_cate.commodity_cate_name'
                                ,'ec_commodities_charges.*'
                                )
                    ->from('ec_commodities_charges')
                    ->leftJoin("ec_commodities_cate","ec_commodities_charges.commodity_cate","=","ec_commodities_cate.id")
                    ->where('ec_commodities_charges.status', '<>', -1)
        ;
        if($this->request['commodities_cate']){
            $model->where('ec_commodities_charges.commodity_cate','=', $this->request['commodities_cate']);
        }
        if($this->request['commodities_charge_name']){
            $model->where('ec_commodities_charges.commodity_charges_name','LIKE', "%{$this->request['commodities_charge_name']}%");
        }
        return $model;
    }
}
