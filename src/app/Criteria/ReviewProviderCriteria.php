<?php

namespace App\Criteria;

use App\Models\ReviewProvider;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ReviewProviderCriteria
 * @package namespace App\Criteria;
 */
class ReviewProviderCriteria implements CriteriaInterface
{

    const REVIEW_REQUEST_FILTER_KEY_NAME = 'review_provider';

    private $request;

    /**
     * ReviewProviderCriteria constructor.
     * @param array $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }


    /**
     * Apply criteria in query repository
     *
     * @param ReviewProvider      $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(isset($this->request[static::REVIEW_REQUEST_FILTER_KEY_NAME])){
            $model = $model->where('name',$this->request[static::REVIEW_REQUEST_FILTER_KEY_NAME]);
        }
        return $model;
    }
}
