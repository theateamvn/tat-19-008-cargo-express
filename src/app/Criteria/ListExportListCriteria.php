<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListExportListCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_export_list.*'
                                )
                    ->from('ec_export_list')
                    ->where('ec_export_list.status','<>','-1')
                    ->orderby('ec_export_list.id', 'DESC')
        ;
        if($this->request['status']){
            $model->where('ec_export_list.status','=', $this->request['status']);
        } else if($this->request['export_list_name']) {
            $model->where('ec_export_list.export_list_name', 'like', '%'.$this->request['export_list_name'].'%');
        } else if($this->request['code_T_Express']) {
            $model->where('ec_export_list.code_T_Express', 'like', '%'.$this->request['code_T_Express'].'%');
        } else if($this->request['shipment_barcode_awb']) {
            $model->where('ec_export_list.shipment_barcode_awb', 'like', '%'.$this->request['shipment_barcode_awb'].'%');
        }
        return $model;
    }
}