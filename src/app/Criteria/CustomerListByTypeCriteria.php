<?php

namespace App\Criteria;

use Auth;
use App\Models\Customer;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class CustomerListByTypeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $modelSS
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $userId = Auth::user()->id;
        $model = $model->select('ec_customers.*','ec_sender.status as existed ')
        ->from('ec_customers')
        ->leftJoin("ec_sender","ec_sender.customer_id","=","ec_customers.id")
        ->orderby('ec_customers.id', 'DESC');
        if(isset($userId)){
           // $model = $model->where('created_by', $userId)->orderBy('id', 'DESC');
        }
        return $model;
    }
}
