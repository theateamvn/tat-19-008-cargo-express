<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListCheckExportListByCodeTExpressCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_export_list_items.export_list_id','ec_export_list.id','ec_export_list.code_T_Express','ec_export_list.created_at','ec_export_list.status As shipment_status')
        ->from('ec_export_list_items')
        ->leftJoin('ec_export_list', "ec_export_list_items.export_list_id","=","ec_export_list.id")
        ->orderBy('ec_export_list.id', 'DESC');
        if($this->request['code_T_Express']){
            $model->where('ec_export_list.code_T_Express','LIKE', "%{$this->request['code_T_Express']}%")->distinct();
        }
        return $model;
    }
}