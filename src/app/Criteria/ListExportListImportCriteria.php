<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListExportListImportCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_export_list_status.*')
                    ->from('ec_export_list_status')
                    ->orderby('ec_export_list_status.id', 'DESC')
        ;
        if($this->request['customer_code']){
            $model->where('ec_export_list_status.customer_code','LIKE',  "%{$this->request['customer_code']}%");
        }
        if($this->request['customer_awb']){
            $model->where('ec_export_list_status.customer_awb','LIKE',  "%{$this->request['customer_awb']}%");
        }
        return $model;
    }
}