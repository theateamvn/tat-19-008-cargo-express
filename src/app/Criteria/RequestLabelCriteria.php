<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class RequestLabelCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_request_label.*', DB::raw('COUNT(ec_request_label_package.request_label_id) as volumn'))
                    ->from('ec_request_label')
                    ->leftJoin('ec_request_label_package','ec_request_label.id','=','ec_request_label_package.request_label_id')
                    ->orderBy('ec_request_label.id', 'DESC')
                    ->groupBy('ec_request_label.id');
        if($this->request['customer_code']){
            $model->where('ec_request_label.customer_code','LIKE', "%{$this->request['customer_code']}%");
        }
        if($this->request['customer_name']){
            $model->where('ec_request_label.sender_name','LIKE', "%{$this->request['customer_name']}%");
        }
        if($this->request['delivery_company']){
            $model->where('ec_request_label.delivery_company', $this->request['delivery_company']);
        }
        if($this->request['status']){
            $model->where('ec_request_label.status', $this->request['status']);
        }
        if($this->request['tracking_number']){
            $model->where('ec_request_label.tracking_number', 'LIKE' , "%{$this->request['tracking_number']}%");
        }
        return $model;
    }
}