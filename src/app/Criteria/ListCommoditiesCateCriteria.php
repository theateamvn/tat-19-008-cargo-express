<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListCommoditiesCateCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_commodities_cate.*')
        ->from('ec_commodities_cate')
        ->orderby('ec_commodities_cate.id', 'DESC');
        if($this->request['commodity_cate_name']){
            $model->where('ec_commodities_cate.commodity_cate_name','LIKE',  "%{$this->request['commodity_cate_name']}%");
        }
        return $model;
    }
}
