<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListRecipientCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
       // echo $this->request['contact_name'];die;
        $model = $model->select('ec_customers.firstname as customer_firstname', 'ec_customers.lastname as customer_lastname', 'ec_customers.customer_code'
                                ,'ec_recipient.*'
                                , 'ec_country.country_name', 'ec_city.city_name'
                                )
                    ->from('ec_recipient')
                    ->leftJoin("ec_customers","ec_recipient.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_country","ec_recipient.contact_country","=","ec_country.id")
                    ->leftJoin("ec_city","ec_recipient.contact_city","=","ec_city.id")
        ;
        if($this->request['customer_code']){
           $model->where('ec_customers.customer_code','LIKE', "%{$this->request['customer_code']}%");
        }
        if($this->request['contact_name']){
            $model->where('ec_recipient.contact_name','LIKE', "%{$this->request['contact_name']}%");
        }
        if($this->request['contact_code']){
            $model->where('ec_recipient.contact_code','LIKE', "%{$this->request['contact_code']}%");
        }
        if($this->request['contact_phone']){
            $model->where('ec_recipient.contact_phone','LIKE', "%{$this->request['contact_phone']}%");
        }
        return $model;
    }
}
