<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class RequestLabelPackageCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        // $model = $model->select('ec_request_label.*'
        //                         )
        //             ->from('ec_request_label')
        //             ->orderby('ec_request_label.id', 'DESC')

        $model = $model->select('ec_request_label_package.*')
                    ->from('ec_request_label_package')
                    ->addSelect('ec_request_label.sender_name')
                    ->leftJoin('ec_request_label', 'ec_request_label_package.request_label_id', '=', 'ec_request_label.id')
                    ->orderBy('ec_request_label_package.id', 'DESC');
        // if($this->request['customer_code']){
        //     $model->where('ec_request_label.customer_code','LIKE',  "%{$this->request['customer_code']}%");
        // }
        // if($this->request['customer_name']){
        //     $model->where('ec_request_label.sender_name','LIKE',  "%{$this->request['customer_name']}%");
        // }
        return $model;
    }
}