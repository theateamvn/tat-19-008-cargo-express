<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListCommoditiesCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_commodities_brand.commodity_brand_name', 'ec_commodities_cate.commodity_cate_name', 'ec_commodities_charges.commodity_charges_name'
                                ,'ec_commodities.*'
                                )
                    ->from('ec_commodities')
                    ->leftJoin("ec_commodities_brand","ec_commodities.commodity_brand","=","ec_commodities_brand.id")
                    ->leftJoin("ec_commodities_cate","ec_commodities.commodity_cate","=","ec_commodities_cate.id")
                    ->leftJoin("ec_commodities_charges","ec_commodities.commodity_charges","=","ec_commodities_charges.id")
                    ->where('ec_commodities.status', '<>', -1)
        ;
        if($this->request['commodities_brand']){
           $model->where('ec_commodities.commodity_brand','=', $this->request['commodities_brand']);
        }
        if($this->request['commodities_cate']){
            $model->where('ec_commodities.commodity_cate','=', $this->request['commodities_cate']);
        }
        if($this->request['commodities_name']){
            $model->where('ec_commodities.commodity_name','LIKE', "%{$this->request['commodities_name']}%");
        }
        return $model;
    }
}
