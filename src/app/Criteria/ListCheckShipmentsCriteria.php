<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListCheckShipmentsCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_customers.customer_code','ec_customers.firstname','ec_customers.lastname', 'ec_customers.phone'
                                ,'ec_shipments.*','ec_shipments.status', 'ec_shipments.id AS shipment_id'
                                , 'ec_recipient.contact_firstname AS recipient_firstname', 'ec_recipient.contact_lastname AS recipient_lastname', 'ec_recipient.contact_code AS recipient_code', 'ec_recipient.contact_phone AS recipient_phone'
                                , 'ec_sender.contact_firstname AS sender_firstname', 'ec_sender.contact_lastname AS sender_lastname', 'ec_sender.contact_code AS sender_code', 'ec_sender.contact_phone AS sender_phone'
                                ,'ec_service_type.service_type_item'
                                ,'ec_shipments_fee.packagePrice','ec_shipments_fee.shippingRate','ec_shipments_fee.otherCharge','ec_shipments_fee.packageSurchargePrice','ec_shipments_fee.totalRate','ec_shipments_fee.insurancePrice', 'ec_shipments_fee.upsFee', 'ec_shipments_fee.totalRate'
                                )
                    ->from('ec_shipments')
                    ->leftJoin("ec_customers","ec_shipments.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_recipient","ec_shipments.recipient_id","=","ec_recipient.id")
                    ->leftJoin("ec_sender","ec_shipments.sender_id","=","ec_sender.id")
                    ->leftJoin("ec_service_type","ec_shipments.info_service_item","=","ec_service_type.id")
                    ->leftJoin("ec_shipments_fee","ec_shipments.id","=","ec_shipments_fee.shipment_id")
                    //->where('ec_shipments.status', '<>', '-1')
                    ->orderby('ec_shipments.id', 'DESC')
        ;
        if(!$this->request['customer_code'] && !$this->request['customer_phone'] && !$this->request['shipment_code']){
            $model->where('ec_shipments.status', '-100');
        }
        if($this->request['customer_code']){
            $model->where('ec_customers.customer_code','LIKE', $this->request['customer_code']);
        }
        if($this->request['customer_phone']){
            $model->where('ec_customers.phone','LIKE', $this->request['customer_phone']);
        }
        if($this->request['shipment_code']){
            $model->where('ec_shipments.shipment_code','LIKE',$this->request['shipment_code']);
        }
        /*
        if($this->request['customer_code']){
            $model->where('ec_customers.customer_code','LIKE', "%{$this->request['customer_code']}%");
        } else {}
        if($this->request['customer_phone']){
            $model->where('ec_customers.phone','LIKE', "%{$this->request['customer_phone']}%");
        }
        if($this->request['shipment_code']){
            $model->where('ec_shipments.shipment_code','LIKE', "%{$this->request['shipment_code']}%");
        }*/
        if($this->request['status']){
            $model->where('ec_shipments.status','=', $this->request['status']);
        } else {
            $model->where('ec_shipments.status', '<>', '-1');
        }
        return $model;
    }
}