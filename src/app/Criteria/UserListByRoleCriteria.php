<?php

namespace App\Criteria;

use Auth;
use App\Models\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class UserListByRoleCriteria implements CriteriaInterface
{

    /**
     * Apply criteria in query repository
     *
     * @param                     $modelSS
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $userId = Auth::user()->id;
        if(isset($userId)){
            $model = $model->where('created_by', $userId)->orderBy('id', 'DESC');
        }
        return $model;
    }
}
