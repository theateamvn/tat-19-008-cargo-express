<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ListCommoditiesBrandCriteria implements CriteriaInterface
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('ec_commodities_brand.*'
        , 'ec_commodities_cate.commodity_cate_name'
        )
        ->from('ec_commodities_brand')
        ->leftJoin("ec_commodities_cate","ec_commodities_brand.commodity_cate","=","ec_commodities_cate.id")
        ->orderby('ec_commodities_brand.id', 'DESC');
        if($this->request['commodity_cate']){
            $model->where('ec_commodities_brand.commodity_cate','=', $this->request['commodity_cate']);
        }
        if($this->request['commodity_brand_name']){
            $model->where('ec_commodities_brand.commodity_brand_name','LIKE',  "%{$this->request['commodity_brand_name']}%");
        }
        return $model;
    }
}
