<?php

namespace App\Criteria;

use Auth;
use App\Models\StorePackageSms;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class UserPackageListByUserIdCriteria implements CriteriaInterface
{
    const REVIEW_REQUEST_FILTER_KEY_NAME = 'id';
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $modelSS
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $userId = $this->request['userid'];
        if(isset($userId)){
            $model = $model->where('user_id', $userId)->orderBy('id', 'DESC');
        }
        return $model;
    }
}
