<?php

namespace App\Criteria;

use Auth;
use App\Models\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


class CategoriesCriteria implements CriteriaInterface
{


    public function apply($model, RepositoryInterface $repository)
    {
        $userId = Auth::user()->id;
        if(isset($userId)){
            $model = $model->orderBy('id', 'DESC');
        }
        return $model;
    }
}
