<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ReviewProvider;

class ReviewProviderPolicy extends BaseAdminPolicy
{

    /**
     * Determine whether the user can view the reviewProvider.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ReviewProvider  $reviewProvider
     * @return mixed
     */
    public function view(User $user, ReviewProvider $reviewProvider)
    {
        return true;
    }

    /**
     * Determine whether the user can create reviewProviders.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the reviewProvider.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ReviewProvider  $reviewProvider
     * @return mixed
     */
    public function update(User $user, ReviewProvider $reviewProvider)
    {
        $store = $user->store();
        if(isset($store)){
            return $store->id == $reviewProvider->store_id;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the reviewProvider.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ReviewProvider  $reviewProvider
     * @return mixed
     */
    public function delete(User $user, ReviewProvider $reviewProvider)
    {
        return false;
    }
}
