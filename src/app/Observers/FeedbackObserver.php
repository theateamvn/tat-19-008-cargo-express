<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 23/02/2017
 * Time: 9:43 PM
 */

namespace App\Observers;


use App\Mail\SendCustomerComplaint;
use App\Models\Feedback;
use App\Models\Setting;
use App\Models\Store;
use Mail;

class FeedbackObserver
{
    /**
     * @param Feedback $feedback
     */
    public function created(Feedback $feedback)
    {
        /** @var Store $store
         * @var Setting $setting
         */
        $store = $feedback->store;
        $setting = $store->settings->first();
        if($setting->email_nofify_feedback){
            $emails = explode(',',$setting->email_nofify_feedback);
            Mail::to($emails)->send(new SendCustomerComplaint($store, $feedback->content));
        }
    }
}