<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/21/2016
 * Time: 3:38 PM
 */

namespace App\Observers;


use App\Models\InviteMessage;
use App\Models\Setting;
use App\Models\Store;
use DB;
class StoreObserver
{
    public function deleting(Store $store)
    {
        foreach ($store->reviewProviders as $provider) {
            $provider->delete();
        }
        foreach ($store->reviews as $review) {
            $review->delete();
        }
        foreach ($store->inviteMessages as $inviteMessage){
            $inviteMessage->delete();
        }
        foreach ($store->settings as $setting){
            $setting->delete();
        }
    }

    public function creating(Store $store){
        // jim edited
        //$store->invitation_introducing = trans('messages.we_appreciate_you_so_much_for_leaving_your_review_and_rating_us');
        //$store->survey_message_1 = trans('messages.survey_message_1');
        //$store->survey_message_2 = trans('messages.survey_message_2');
    }

    public function created(Store $store){
        // jim edited
        $sms_api_token = $this->getConfigMasterSetting('sms_api_token')[0]->value;
        $sms_api_secret = $this->getConfigMasterSetting('sms_api_secret')[0]->value;
        $sms_brand_name = $this->getConfigMasterSetting('sms_brand_name')[0]->value;
        InviteMessage::createDefaultContent($store->id);
        Setting::createDefaultSetting($store->id, $sms_api_token, $sms_api_secret, $sms_brand_name);
    }
    public function getConfigMasterSetting($key) {
        return DB::table('master_settings')->where('key', $key)->get();
    }
}