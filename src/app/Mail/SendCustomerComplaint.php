<?php

namespace App\Mail;

use App\Models\Store;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCustomerComplaint extends Mailable
{
    use Queueable, SerializesModels;

    public $store;
    public $content;

    /**
     * Create a new message instance.
     * @param Store $store
     * @param $content
     */
    public function __construct(Store $store,$content)
    {
        $this->store = $store;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.review.complaint')
            ->subject('Customer Complaint');
    }
}
