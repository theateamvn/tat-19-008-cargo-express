<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Sender;
use App\Repositories\SenderRepository;
use Faker\Factory;
use Request;
use DB;
class SenderService
{
    /**
     * @var SenderRepository
     */
    private $senderRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * UserService constructor.
     * @param $userRepository
     */
    public function __construct(SenderRepository $senderRepository)
    {
        $this->senderRepository = $senderRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $number = $this->getTotalSenderByCustomer($input['customer_id']);
        $number = $number +1;
        if($number < 10){
            $number = sprintf("%02s", $number);
        }
        $input['contact_code']     = $input['customer_code'].'-'.$number;
        $sender = Sender::create($input);
        $sender->save();
        // update
        /*Sender::update($sender['id'], array(
            'legal_entity' => array(
                'contact_code' => $input['customer_code'].'-'.$sender['id']
            )
        ));*/
        return $sender;
    }
    public function getCustomerList()
    {
        $result = array();
        $customers = DB::table('ec_customers')
                    ->where('status', 1)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['customer_code'].' - '.$customer['firstname'].' '.$customer['lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getCountryList()
    {
        $result = DB::table('ec_country')
                    ->where('status', 1)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $result   =  json_decode(json_encode($result), True);
        return $result;
    }
    public function getCityList($id)
    {
        if(!$id){
            $cities = DB::table('ec_city')
                    ->where('status', 1)
                    ->orderBy('city_name', 'ASC')
                    ->get()
                    ->toArray();
        } else {
            $cities = DB::table('ec_city')
                        ->where('status', 1)
                        ->where('country_id', $id)
                        ->orderBy('id', 'ASC')
                        ->get()
                        ->toArray();
        }
        $result = array();
        foreach($cities as $key => $city){
            $result[$key]['value'] = $city->id;
            $result[$key]['label'] = $city->city_name;
        }
        $result   =  json_decode(json_encode($result), True);
        return $result;
    }
    public function getCityDetail($id)
    {
        $cities = DB::table('ec_city')
                        ->where('status', 1)
                        ->where('id', $id)
                        ->get()
                        ->toArray();
        $result = array();
        foreach($cities as $key => $city){
            $result[$key]['value'] = $city->id;
            $result[$key]['label'] = $city->city_name;
        }
        $result   =  json_decode(json_encode($result), True);
        return $result;
    }
    public function getDistrictList($id){
        $result = array();
        $districts = DB::table('ec_district')
                    ->where('status', 1)
                    ->where('city_id', $id)
                    ->orderBy('district_name', 'DESC')
                    ->get()
                    ->toArray();
        foreach ($districts as $key => $district) {
            $result[$key]['value']  = $district->id;
            $result[$key]['label']  = $district->district_name;
        }
        return $result;
    }
    public function getTotalSenderByCustomer($customer_id)
    {
        return DB::table('ec_sender')->where([
            ['customer_id', '=', $customer_id]
        ])->count();
    }

}