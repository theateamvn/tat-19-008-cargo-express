<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/12/2017
 * Time: 12:02 PM
 */

namespace App\Services;


use App\Services\Utils\NetworkUtils;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class ProxyService
{
    private $client;

    private $proxyEnabled;
    private $autoProxy;

    /**
     * ProxyService constructor.
     */
    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'http://www.us-proxy.org/']);
        $this->proxyEnabled = config('pushmeup.setting.proxy_enabled');
        $this->autoProxy = config('pushmeup.setting.auto_proxy');
    }

    public function getAll()
    {
        $response = $this->client->get('')->getBody()->getContents();
        $crawler = new Crawler($response);
        $items = $crawler->filterXPath('//*[@id="proxylisttable"]/tbody/tr')->each(function (Crawler $tr, $trIndex) {
            $cols = $tr->filter('td')->each(function (Crawler $td, $tdIndex) {
                return $td->text();
            });
            return [
                'ip' => $cols[0],
                'port' => $cols[1],
                'country_code' => $cols[2],
                'country' => $cols[3],
                'anonymity' => $cols[4],
                'google' => $cols[5],
                'https' => $cols[6],
                'last_check' => $cols[7]
            ];
        });
        return collect($items);
    }

    public function getAvailable($offset = 0,$pageSize = 200){
        $allProxy = $this->getAll();
        $proxies = collect($allProxy->chunk($pageSize)->get($offset));
        $proxies= $proxies->filter(function($proxy){
            return NetworkUtils::checkProxy($this->getProxyText($proxy));
        });
        return $proxies;
    }

    public function getFirstAvailable($offset = 0,$pageSize = 200){
        $availableProxies = $this->getAvailable($offset,$pageSize);
        return $availableProxies->first();
    }

    public function getFirstAvailableText($offset = 0,$pageSize = 200){
        $availableProxy = $this->getFirstAvailable($offset,$pageSize);
        return 'tcp://'.$this->getProxyText($availableProxy);
    }

    private function getProxyText($proxy){
        return $proxy['ip'].':'.$proxy['port'];
    }

    public function getProxy(){
        if($this->proxyEnabled){
            $proxy = $this->autoProxy ? $this->getFirstAvailableText(0, 5) : config('pushmeup.setting.proxy');
            \Log::info("Using proxy: $proxy");
            return $proxy;
        }
        return '';
    }

}