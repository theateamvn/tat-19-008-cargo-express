<?php

namespace App\Services;


use App\Models\Categories;
use Faker\Factory;
use Request;

class CategoriesService
{

    public function createCategories($input = null){
        if($input){
            $categories = Categories::create($input);
        }else{
            $categories = Categories::create([
                'name' => $this->faker->name,
                'slug' => $this->faker->slug,
                'parent' => $this->faker->parent
            ]);
        }
        $categories->save();
        return $categories;
    }
}
