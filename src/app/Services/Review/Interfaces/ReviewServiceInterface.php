<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 2:49 PM
 */

namespace App\Services\Review\Interfaces;


interface ReviewServiceInterface
{
    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId);

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId);

   

    /**
     * @param $reviewId
     * @return string
     */
    public function getReviewLink($reviewId);
     public function getAllReviewsWithOtherId($reviewId, $storeId, $hotelId);
}