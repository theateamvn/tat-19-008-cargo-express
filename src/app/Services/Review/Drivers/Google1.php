<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 3:24 PM
 */

namespace App\Services\Review\Drivers;

use App\Exceptions\DataRequiredException;
use App\Exceptions\ExternalAPIException;
use App\Models\Review;
use App\Models\ReviewProvider;
use App\Services\Review\Interfaces\ReviewServiceInterface;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Google implements ReviewServiceInterface
{
    const GOOGLE_CID_REGEX = '/.*cid=(\d+)/';

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    private $key;
    private $format;

    /**
     * Google constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/place/details/',
        ]);
        $this->key = env('GOOGLE_API_KEY');
        $this->format = "json";
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        $response = $this->makeRequest($reviewId);
        $reviews = $response['result']['reviews'];
        $results = [];
        foreach ($reviews as $review) {
            $results[] = $this->transformResultToReview($review, $storeId);
        }
        return collect($results);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
        $reviews = [];
        $start_at = 0;
        $start_at_step = 10;
        $failedCount = 0;
        $star_division = 13;
        $lrd = $this->getLRD($reviewId);
        $base_uri_format = 'https://www.google.com/async/reviewSort?async=feature_id:%s,start_index:%d';
        while ($failedCount == 0) {
            $client = new Client(['base_uri' => sprintf($base_uri_format, $lrd, $start_at)]);
            $contents = $client->get('', $this->buildRequestHeader())->getBody()->getContents();
            $contents = utf8_decode($contents);
            $response = json_decode($contents, JSON_UNESCAPED_UNICODE);
            $html = $response[1][1];
            $crawler = new Crawler($html);
            $filters = $crawler->filter('._D7k');

            if ($filters->count() == 0) {
                $failedCount++;
                continue;
            }

            $failedCount = 0;

            foreach ($filters as $filter) {
                $review_html = $filter->ownerDocument->saveHTML($filter);
                $review_crawler = new Crawler($review_html);

                $result_name = $review_crawler->filter('._e8k');
                $result_content = $review_crawler->filter('._ucl');
                $rating_content = $review_crawler->filter('._Q7k');

                $star_content = $review_crawler->filter('._WQf.star div');
                // var_dump($rating_num);
                if ($result_name->count() && $result_content->count() && $rating_content->count() && $star_content->count()) {
                    $content = iconv(mb_detect_encoding($result_content->text(), mb_detect_order(), true), "ISO-8859-1", $result_content->text());
                    $author = iconv(mb_detect_encoding($result_name->text(), mb_detect_order(), true), "ISO-8859-1", $result_name->text());
                    $published_at = trim(str_replace('-', '', $rating_content->text()));
                    preg_match('/\d+/', $star_content->attr('style'), $rating_match);
                    $rating_num = $rating_match[0] / $star_division;
                    $reviewer_id = $result_name->attr('href');
                }
                $result_name = $review_crawler->filter('_G7k');
                if ($result_name->count() && $rating_content->count() && $star_content->count()) {
                    $author = $result_name->text();
                    $published_at = trim(str_replace('-', '', $rating_content->text()));
                    preg_match('/\d+/', $star_content->attr('style'), $rating_match);
                    $rating_num = $rating_match[0] / $star_division;
                }
                $review = new Review();
                $review->store_id = $storeId;
                $review->reviewer_id = $reviewer_id;
                $review->content = $content;
                $review->rating = $rating_num;
                $review->real_rating = $rating_num;
                $review->author = $author;
                $review->published_at = date('Y-m-d H:i:00', strtotime($published_at));
                $review->provider_name = ReviewProvider::PROVIDER_GOOGLE;
                $review->url = $this->getReviewLink($reviewId);
                $reviews[] = $review;
            }
            $start_at += $start_at_step;
        }

        return collect($reviews);
    }

    /**
     * @param $reviewId
     * @return string
     * @throws DataRequiredException
     */
    public function getReviewLink($reviewId)
    {
        if ($reviewId == null || $reviewId == '') {
            throw new DataRequiredException('Place ID');
        }
        return "http://search.google.com/local/writereview?placeid=" . $reviewId;
    }

    private function transformResultToReview($result, $storeId)
    {
        $review = new Review();
        $review->store_id = $storeId;
        $review->reviewer_id = $result['author_url'];
        $review->content = $result['text'];
        $review->rating = $result['rating'];
        $review->real_rating = $result['rating'];
        $review->author = $result['author_name'];
        $review->published_at = $result['time'];
        $review->provider_name = ReviewProvider::PROVIDER_GOOGLE;
        $review->url = "";
        return $review;
    }

    /**
     * @param $placeId
     * @return mixed
     * @throws DataRequiredException
     * @throws \Exception
     */
    private function makeRequest($placeId)
    {

        if ($placeId == null || $placeId == '') {
            throw new DataRequiredException('Place ID');
        }
        $options = [
            'query' => [
                'key' => $this->key,
                'placeid' => $placeId
            ],
        ];

        $contents = $this->client->get($this->format, $options)->getBody()->getContents();
        $response = json_decode($contents, true);
        $this->errorHandling($response);
        return $response;
    }

    /**
     * @param $response
     * @throws ExternalAPIException
     */
    private function errorHandling($response)
    {
        if ($response['status'] !== 'OK') {
            switch ($response['status']) {
                case "ZERO_RESULTS":
                    $errorMessage = "Result is empty";
                    break;
                case "NOT_FOUND":
                case "INVALID_REQUEST":
                    $errorMessage = "Place id not found";
                    break;
                default:
                    $errorMessage = "Technical error";
                    break;
            }
            throw new ExternalAPIException($response['status'], $errorMessage, ReviewProvider::PROVIDER_GOOGLE);
        }
    }

    /**
     * @param string $placeId . Ex: ChIJVa8k6hYh3YAR_K_rCWRiSCk
     * @return mixed
     * @throws DataRequiredException
     */
    public function getCID($placeId)
    {
        $this->client = new Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/place/details/',
        ]);

        if ($placeId == null || $placeId == '') {
            throw new DataRequiredException('Place ID');
        }

        $response = $this->makeRequest($placeId);
        $url = $response['result']['url'];
        return preg_replace(static::GOOGLE_CID_REGEX, "$1", $url);
    }

    /**
     * @param $placeId . Ex: ChIJVa8k6hYh3YAR_K_rCWRiSCk
     * return Ex: 0x0:0x2948626409ebaffc,1
     * @return string
     */
    public function getLRD($placeId)
    {
        $cid = $this->getCID($placeId);
        return '0x0%3a0x' . $this->dec2hex($cid);
    }

    private function dec2hex($str)
    {
        $dec = str_split($str);
        $sum = [];
        $hex = [];
        $i = 0;
        $s = 0;
        while (count($dec)) {
            $s = 1 * array_shift($dec);
            for ($i = 0; $s || $i < count($sum); $i++) {
                $val = isset($sum[$i]) ? $sum[$i] : 0;
                $s += $val * 10;
                $sum[$i] = $s % 16;
                $s = ($s - $sum[$i]) / 16;
            }
        }
        while (count($sum)) {
            $val = dechex(array_pop($sum));
            array_push($hex, $val);
        }
        return implode('', $hex);
    }

    protected function buildRequestHeader()
    {
        return [
            'delay' => config('pushmeup.setting.delay_time_each_request')
        ];
    }
}
