<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 4:57 PM
 */

namespace App\Services\Review\Drivers;


use App\Exceptions\DataRequiredException;
use App\Exceptions\ExternalAPIException;
use App\Models\MasterSetting;
use App\Models\Review;
use App\Models\ReviewProvider;
use App\Services\Review\Interfaces\ReviewServiceInterface;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Log;

class Yelp implements ReviewServiceInterface
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;
    private $auth;
    private $app_id;
    private $app_secret;
    private $format;
    private $proxy;

    /**
     * Yelp constructor.
     */
    public function __construct()
    {
        $this->app_id = env('YELP_APP_ID');
        $this->app_secret = env('YELP_APP_SECRET');
        $this->format = "json";
    }


    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        $response = $this->makeRequest($reviewId);
        if($response) {
            //Log::info(json_encode($response));
            $reviews = $response['reviews'];
            //Log::info(json_encode($reviews));
            $result = [];
            foreach ($reviews as $review) {
                $result[] = $this->transformResultToReview($review, $storeId);
            }
        }
        return collect($result);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
        $setting = MasterSetting::where('key', 'proxy_url')->first();
        if($setting){
            $this->proxy = $setting->value;
        }
        $reviews = [];
        $start_at = 0;
        $start_at_step = 20;
        $isFinished = false;
        $failedCount = 0;
        $base_uri_format = 'https://www.yelp.com/biz/%s/review_feed/?start=%d';
        $review_url_format = 'https://www.yelp.com/biz/%s?hrid=%s';
        
      //  while ($failedCount < 2) {
            $client = new Client(['base_uri' => sprintf($base_uri_format, $reviewId, $start_at)]);
            $contents = $client->get('', $this->buildRequestHeader())->getBody()->getContents();
            $response = json_decode($contents, true);
            $crawler = new Crawler($response['review_list']);
            $filters = $crawler->filter('div.review--with-sidebar');
            foreach ($filters as $filter) {
                $review_html = $filter->ownerDocument->saveHTML($filter);
//var_dump($review_html );
                $review_crawler = new Crawler($review_html);
                /*$is_review = $review_crawler->filter('.review-sidebar-content')->filter('div')->count();
               // echo $is_review;die;
                if ($is_review == 0) {
                    $failedCount++;
                    continue;
                }*/
                if($review_crawler->filter('.review-sidebar-content')->filter('.user-name')->count())
                {
                    //var_dump($review_crawler->filter(".rating-qualifierwwwww")->count());;
                    //$failedCount = 0;
                    $result_name = $review_crawler->filter(".user-display-name");
                    $result_content = $review_crawler->filter('.review-content p');
                    $result_rating = $review_crawler->filter('.i-stars');
                    $result_date = $review_crawler->filter('.rating-qualifier');
               //      echo $result_name->text();
                    $review = new Review();
                    $review->store_id = $storeId;
                    $review->reviewer_id = $result_name->attr('data-hovercard-id');
                //    echo 5;
                    $review->content = utf8_decode($result_content->text());
                    $rating_num = intval($result_rating->attr('title'));
                    
                    $review->rating = $rating_num;
                    $review->real_rating = $rating_num;
                    $review->author = utf8_decode($result_name->text());
                    $review->published_at = date('Y-m-d H:i:00', strtotime($result_date->text()));
                    $review->published_at_text = $result_date->text();
                    $review->provider_name = ReviewProvider::PROVIDER_YELP;
                    $review->url = sprintf($review_url_format, $reviewId, $review->reviewer_id);
                     
                    $reviews[] = $review;
                }
        //    }
            $start_at += $start_at_step;
        }
        return collect($reviews);
    }
    public function getAllReviewsWithOtherId($reviewId, $storeId, $hotelId)
    {
        return;
    }

    /**
     * @param $reviewId
     * @return string
     * @throws DataRequiredException
     */
    public function getReviewLink($reviewId)
    {
        if ($reviewId == null || $reviewId == '') {
            throw new DataRequiredException('Place ID');
        }
        return "https://www.yelp.com/biz/" . $reviewId;
    }

    /**
     * @param $placeid
     * @return mixed
     * @throws DataRequiredException
     * @throws \Exception
     */
    private function makeRequest($placeid)
    {
        if ($placeid == null || $placeid == '') {
            throw new DataRequiredException('Place ID');
        }
        /*
        $this->auth = new Client([
            'base_uri' => 'https://api.yelp.com/oauth2/token',
        ]);
        $bodyAuth = [
            'form_params' => [
                'client_id' => $this->app_id,
                'client_secret' => $this->app_secret,
                'grant_type' => 'client_credentials'
            ]
        ];
        $auth_response = $this->auth->request('POST', '', $bodyAuth)->getBody()->getContents();
        $auth_response = json_decode($auth_response, true);
        
        $access_token = $auth_response['token_type'] . ' ' . $auth_response['access_token'];
        */
        $access_token = "Bearer ".$this->app_secret;
        
        $base_uri = 'https://api.yelp.com/v3/businesses/' . $placeid . '/reviews';
        
        $this->client = new Client([
            'base_uri' => $base_uri,
        ]);
        $contents = $this->client->request('GET', '', [
            'headers' => [
                'Authorization' => $access_token
            ]
        ])->getBody()->getContents();
        $response = json_decode($contents, true);
        $this->responseErrorHandle($response);
        return $response;
    }

    private function responseErrorHandle($response)
    {
        if (isset($response['error'])) {
            $responseJson = \GuzzleHttp\json_encode($response);
            $errorMessage = "";
            $code = $response['error']['code'];
            switch ($code) {
                case "BUSINESS_NOT_FOUND":
                    $errorMessage = "Place id not found";
                    break;
                default:
                    $errorMessage = "Technical error";
                    break;
            }
            throw new ExternalAPIException($code, $errorMessage, ReviewProvider::PROVIDER_YELP);
        }
    }

    private function transformResultToReview($result, $storeId)
    {
        $review = new Review();
        $review->store_id = $storeId;
        $review->reviewer_id = isset($result['user']) ? $result['user']['image_url'] : '';
        $review->content = $result['text'];
        $review->rating = $result['rating'];
        $review->real_rating = $result['rating'];
        $review->author = isset($result['user']) ? $result['user']['name'] : '';
        $review->published_at = $result['time_created'];
        $review->provider_name = ReviewProvider::PROVIDER_YELP;
        $review->url = $result['url'];
        return $review;
    }

    protected function buildRequestHeader()
    {
        return [
            'delay' => config('pushmeup.setting.delay_time_each_request')
            //'proxy' => $this->proxy
        ];
    }
}
