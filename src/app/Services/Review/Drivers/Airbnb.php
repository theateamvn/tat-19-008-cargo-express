<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 18/01/2017
 * Time: 7:52 PM
 */

namespace App\Services\Review\Drivers;


use App\Models\MasterSetting;
use App\Models\Review;
use App\Models\ReviewProvider;
use App\Services\Review\Interfaces\ReviewServiceInterface;
use App\Services\Utils\StringUtils;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Airbnb implements ReviewServiceInterface
{
    const AIRBNB_BASE_URL = "https://www.airbnb.com";
    const AIRBNB_GET_REVIEW_URL = "https://www.airbnb.com/api/v2/reviews";

    private $proxy;

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        return true;
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
       return true;
    }
    public function getAllReviewsWithOtherId($reviewId, $storeId)
    {
        return true;
    }

    /**
     * @param $reviewId
     * @return string
     */
    public function getReviewLink($reviewId)
    {
        return static::AIRBNB_GET_REVIEW_URL;
    }

    private function buildRequestHeader($options = [])
    {
        return array_merge([
            'delay' => config('pushmeup.setting.delay_time_each_request')
           // 'proxy' => $this->proxy
        ], $options);
    }

    public function getHotelId($reviewId)
    {
        return true;
    }

    public function getReview($storeId, $page = 1, $pageSize = 10)
    {
       return true;
    }
}
