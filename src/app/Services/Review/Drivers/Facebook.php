<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 4:07 PM
 */

namespace App\Services\Review\Drivers;


use App\Models\Review;
use App\Models\ReviewProvider;
use App\Services\Review\Interfaces\ReviewServiceInterface;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use DB;
class Facebook implements ReviewServiceInterface
{

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://graph.facebook.com/v2.8/'
        ]);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        return $this->getAllReviews($reviewId,$storeId,10);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
        return $this->fetchReview($reviewId,$storeId,100);
    }

    private function fetchReview($reviewId, $storeId,$limit){
        $reviews = [];
        $failedCount = 0;
        // $placeid = '1444711412459962';
        $facebook_uri_format = 'https://www.facebook.com%s';
        $base_uri_format = 'https://www.facebook.com/ajax/pages/review/spotlight_reviews_tab_pager/?max_fetch_count=%d&page_id=%s&sort_order=most_recent&__user=0&__a=1';
        $client = new Client(['base_uri' => sprintf($base_uri_format, $limit, $reviewId)]);
        $contents = $client->get('', $this->buildRequestHeader())->getBody()->getContents();
        $fuck_string = 'for (;;);';
        $contents = str_replace($fuck_string, '', $contents);
        $contents = utf8_decode($contents);
        $response = \GuzzleHttp\json_decode($contents, true);
        $html = $response['domops'][0][3]['__html'];
        $crawler = new Crawler($html);
        $filters = $crawler->filter('._1dwg._1w_m');

        foreach ($filters as $filter) {
            $review_html = $filter->ownerDocument->saveHTML($filter);
            $review_crawler = new Crawler($review_html);
            $result_name = $review_crawler->filter('span .profileLink');
            $result_content = $review_crawler->filter('.userContent');
            $published_at = $review_crawler->filter('abbr._5ptz');
            $star_content = $review_crawler->filter('._51mq.img u');
            preg_match('/\d+/', $star_content->text(), $rating_match);
            $rating_num = $rating_match[0];
            $result_url = $review_crawler->filter('._5pcq');
            $review_url = explode('/', $result_url->attr('href'));

            $review = new Review();
            $review->store_id = $storeId;
            $review->reviewer_id = sprintf($facebook_uri_format, '/' . $review_url[1]);
            // $review->content = utf8_decode($result_content->text());
            $review->content = iconv("UTF-8", "ISO-8859-1", utf8_decode($result_content->text()));
            $review->rating = $rating_num;
            $review->real_rating = $rating_num;
            $review->author =  iconv("UTF-8", "ISO-8859-1", utf8_decode(($result_name->text())));
            $review->published_at = date('Y-m-d H:i:00', $published_at->attr('data-utime'));
            $review->provider_name = ReviewProvider::PROVIDER_FACEBOOK;
            $review->url = sprintf($facebook_uri_format, $result_url->attr('href'));
            $checkExistData = $this->checkExistData($review);
            if($checkExistData){
                continue;
            }
            $reviews[] = $review;
        }

        return collect($reviews);
    }
    public function getAllReviewsWithOtherId($reviewId, $storeId, $hotelId)
    {
        return;
    }
    /**
     * @param $reviewId
     * @return string
     */
    public function getReviewLink($reviewId)
    {
        return "https://www.facebook.com/$reviewId/reviews/";
    }

    private function buildRequestHeader()
    {
        return [
            'delay' => config('pushmeup.setting.delay_time_each_request')
        ];
    }
    /**
    * check exist data
    **/
    private function checkExistData($review)
    {
        $key = implode('-', [
                $review->store_id,
                $review->content,
                $review->author,
                $review->rating,
                $review->real_rating,
                $review->published_at
            ]);
        $key_ = hash('md5', $key);
        return DB::table('reviews')->where('review_unique_id', $key_)->count();
    }
}
