<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 3:19 PM
 */

namespace App\Services\Review\Exceptions;


class ServiceNotImplementException extends \Exception
{
    private $serviceName;

    /**
     * ServiceNotImplementException constructor.
     * @param $serviceName
     */
    public function __construct($serviceName)
    {
        $this->serviceName = $serviceName;
        $this->message = "Service {$this->serviceName} have not implemented";
    }


}