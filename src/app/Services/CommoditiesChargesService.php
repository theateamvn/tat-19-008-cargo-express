<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\CommoditiesCharges;
use App\Repositories\CommoditiesChargesRepository;
use Faker\Factory;
use Request;
use DB;
class CommoditiesChargesService
{
    /**
     * @var CommoditiesRepository
     */
    private $commoditiesChargesRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(CommoditiesChargesRepository $commoditiesChargesRepository)
    {
        $this->commoditiesChargesRepository = $commoditiesChargesRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $commodities = CommoditiesCharges::create($input);
        $commodities->save();
        return $commodities;
    }
    public function getCateList()
    {
        $result_cate = DB::table('ec_commodities_cate')
                    ->select('ec_commodities_cate.*')
                    ->where('ec_commodities_cate.status', 1)
                    ->orderBy('ec_commodities_cate.id', 'DESC')
                    ->get()
                    ->toArray();
        $result_cate   =  json_decode(json_encode($result_cate), True);
        $cus = array();
        foreach ($result_cate as $key => $value) {
            $cus[$key]['label'] = $value['commodity_cate_name'];
            $cus[$key]['value'] = $value['id'];
        }
        return $cus;
    }

}