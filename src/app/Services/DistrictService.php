<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\District;
use App\Repositories\DistrictRepository;
use Faker\Factory;
use Request;
use DB;
class DistrictService
{
    /**
     * @var ServiceTypeZoneRepository
     */
    private $districtRepository;

    public function __construct(DistrictRepository $districtRepository)
    {
        $this->districtRepository = $districtRepository;
    }

    public function getDistrictListById($id)
    {
        $district = DB::table('ec_district')->where('city_id', '=', $id)->get()->toArray();
        return $district;
    }

    public function create($input)
    {
        $district = District::create($input);
        $district->save();
        return $district;
    }

}