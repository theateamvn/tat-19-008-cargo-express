<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 29/11/2016
 * Time: 11:31 PM
 */

namespace App\Services;


use App\Criteria\ByUserStoreCriteria;
use App\Exceptions\ConfigurationRequiredException;
use App\Repositories\SettingRepository;
use DB;
class SettingService
{
    /**
     * @var SettingRepository
     */
    private $repository;

    /**
     * SettingService constructor.
     * @param SettingRepository $repository
     */
    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getSettingByUser()
    {
        $this->repository->pushCriteria(new ByUserStoreCriteria());
        $setting = $this->repository->first();
        // set default for SMS Gateway
        $smsDefault = $this->getConfigSmsGateway()[0];
        if($setting->sms_username == '' || $setting->sms_password == '' || $setting->sms_user == '')
        {
            $setting->sms_username = $smsDefault->sms_username;
            $setting->sms_password = $smsDefault->sms_password;
            $setting->sms_user = $smsDefault->sms_user;
        }
        if($setting->sms_bandwidth_phone == '' || $setting->sms_bandwidth_user_id == '' || $setting->sms_bandwidth_api_token == '' || $setting->sms_bandwidth_api_secret == '')
        {
            $setting->sms_bandwidth_phone       = $smsDefault->sms_bandwidth_phone;
            $setting->sms_bandwidth_user_id     = $smsDefault->sms_bandwidth_user_id;
            $setting->sms_bandwidth_api_token   = $smsDefault->sms_bandwidth_api_token;
            $setting->sms_bandwidth_api_secret  = $smsDefault->sms_bandwidth_api_secret;
        }
        if($setting->sms_esms_sms_type == '' || $setting->sms_esms_api_token == '' || $setting->sms_esms_api_secret == '')
        {
            $setting->sms_esms_sms_type = $smsDefault->sms_esms_sms_type;
            $setting->sms_esms_api_token = $smsDefault->sms_esms_api_token;
            $setting->sms_esms_api_secret = $smsDefault->sms_esms_api_secret;
        }
        return $setting;
    }

    public function getSetting()
    {
        $setting = $this->repository->first();
        // set default for SMS Gateway
        $smsDefault = $this->getConfigSmsGateway()[0];
        if($setting->sms_username == '' || $setting->sms_password == '' || $setting->sms_user == '')
        {
            $setting->sms_username = $smsDefault->sms_username;
            $setting->sms_password = $smsDefault->sms_password;
            $setting->sms_user = $smsDefault->sms_user;
        }
        if($setting->sms_bandwidth_phone == '' || $setting->sms_bandwidth_user_id == '' || $setting->sms_bandwidth_api_token == '' || $setting->sms_bandwidth_api_secret == '')
        {
            $setting->sms_bandwidth_phone       = $smsDefault->sms_bandwidth_phone;
            $setting->sms_bandwidth_user_id     = $smsDefault->sms_bandwidth_user_id;
            $setting->sms_bandwidth_api_token   = $smsDefault->sms_bandwidth_api_token;
            $setting->sms_bandwidth_api_secret  = $smsDefault->sms_bandwidth_api_secret;
        }
        if($setting->sms_esms_sms_type == '' || $setting->sms_esms_api_token == '' || $setting->sms_esms_api_secret == '')
        {
            $setting->sms_esms_sms_type = $smsDefault->sms_esms_sms_type;
            $setting->sms_esms_api_token = $smsDefault->sms_esms_api_token;
            $setting->sms_esms_api_secret = $smsDefault->sms_esms_api_secret;
        }
        return $setting;
    }

    public function getSettingByUserBK()
    {
        $this->repository->pushCriteria(new ByUserStoreCriteria());
        $setting = $this->repository->first();
        return $setting;
    }
    public function getConfigSmsGateway() {
        return DB::table('config_smsgateway')->where('status', 1)->get();
    }


}