<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 22/11/2016
 * Time: 8:15 PM
 */

namespace App\Services;


use App\Criteria\ByUserStoreCriteria;
use App\Criteria\ReviewProviderByStoreCriteria;
use App\Criteria\ReviewProviderCriteria;
use App\Exceptions\DataRequiredException;
use App\Models\ReviewProvider;
use App\Repositories\ReviewProviderRepository;
use App\Services\Review\Drivers\Factory;
class ReviewProviderService
{

    /**
     * @var ReviewProviderRepository
     */
    private $reviewProviderRepository;

    /**
     * @var Factory
     */
    private $reviewFactory;

    /**
     * ReviewProviderService constructor.
     * @param ReviewProviderRepository $reviewProviderRepository
     * @param Factory $reviewFactory
     */
    public function __construct(
        ReviewProviderRepository $reviewProviderRepository,
        Factory $reviewFactory)
    {
        $this->reviewProviderRepository = $reviewProviderRepository;
        $this->reviewFactory = $reviewFactory;
    }


    /**
     * @param array $model
     * @return bool
     * @throws DataRequiredException
     */
    public function save($model)
    {
        if (!isset($model['name'])) {
            throw new DataRequiredException('Review provider name');
        }
        $this->deleteAllReviewProviderByNameAndCurrentUser($model['name']);

        if (!isset($model->reviewed_url)) {
            $model = $this->getReviewedUrl($model);
        }
        $reviewProvider = $this->reviewProviderRepository->create($model);

        return $reviewProvider;
    }

    /**
     * @param array $model
     * @param string $id
     * @return mixed
     */
    public function update($model, $id)
    {
        if (!isset($model->reviewed_url)) {
            $model = $this->getReviewedUrl($model);
        }
        return $this->reviewProviderRepository->update($model, $id);
    }

    /**
     * @param $model
     * @return mixed
     * @throws DataRequiredException
     */
    private function getReviewedUrl($model)
    {
        if (!isset($model['name'])) {
            throw new DataRequiredException('Review provider name');
        }

        if (!isset($model['reviewed_key'])) {
            throw new DataRequiredException('Review key');
        }
        $model['reviewed_url'] =  $this->reviewFactory->make($model['name'])->getReviewLink($model['reviewed_key']);
        return $model;
    }

    /**
     * @param string $providerName
     * @return mixed
     */
    public function findAllProviderByCurrentUser($providerName)
    {
        $params = [
            ReviewProviderCriteria::REVIEW_REQUEST_FILTER_KEY_NAME => $providerName
        ];
        $this->reviewProviderRepository->resetCriteria();
        $this->reviewProviderRepository->pushCriteria(new ByUserStoreCriteria());
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderCriteria($params));

        return $this->reviewProviderRepository->all();
    }

    /**
     * @param $provider
     * @return ReviewProvider
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findProviderByCurrentUser($provider)
    {
        $params = [
            ReviewProviderCriteria::REVIEW_REQUEST_FILTER_KEY_NAME => $provider
        ];
        $this->reviewProviderRepository->resetCriteria();
        $this->reviewProviderRepository->pushCriteria(new ByUserStoreCriteria());
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderCriteria($params));
        return $this->reviewProviderRepository->first();
    }

    public function findProviderByStore($provider, $storeId)
    {
        $params = [
            ReviewProviderCriteria::REVIEW_REQUEST_FILTER_KEY_NAME => $provider,
            ReviewProviderByStoreCriteria::REVIEW_REQUEST_FILTER_KEY_NAME => $storeId
        ];
        $this->reviewProviderRepository->resetCriteria();
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderCriteria($params));
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderByStoreCriteria($params));
        return $this->reviewProviderRepository->first();
    }

    public function findAllProviderByStore($provider, $storeId)
    {
        $params = [
            ReviewProviderCriteria::REVIEW_REQUEST_FILTER_KEY_NAME => $provider,
            ReviewProviderByStoreCriteria::REVIEW_REQUEST_FILTER_KEY_NAME => $storeId
        ];
        $this->reviewProviderRepository->resetCriteria();
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderCriteria($params));
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderByStoreCriteria($params));
        return $this->reviewProviderRepository->all();
    }

    /**
     * @param $storeId
     * @return ReviewProvider[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findProvidersByStore($storeId)
    {
        $params = [
            ReviewProviderByStoreCriteria::REVIEW_REQUEST_FILTER_KEY_NAME => $storeId
        ];
        $this->reviewProviderRepository->resetCriteria();
        return $this->reviewProviderRepository->getByCriteria(new ReviewProviderByStoreCriteria($params));
    }

    /**
     * @param $name
     */
    public function deleteAllReviewProviderByNameAndCurrentUser($name)
    {
        $reviewProviders = $this->findAllProviderByCurrentUser($name);
        foreach ($reviewProviders as $reviewProvider) {
            /** @var ReviewProvider $reviewProvider */
            $reviewProvider->delete();
        }
    }

}