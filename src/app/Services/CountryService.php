<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Country;
use App\Repositories\CountryRepository;
use Faker\Factory;
use Request;
use DB;
class CountryService
{
    /**
     * @var CountryRepository
     */
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function getListCountry()
    {
        $country = DB::table('ec_country')->where('status', '<>', -1)->get()->toArray();
        return $country;
    }

    public function create($input)
    {
        $country = City::create($input);
        $country->save();
        return $country;
    }
    
}