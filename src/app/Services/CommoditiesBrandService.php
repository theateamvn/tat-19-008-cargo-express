<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\CommoditiesBrands;
use App\Repositories\CommoditiesBrandRepository;
use Faker\Factory;
use Request;
use DB;
class CommoditiesBrandService
{
    /**
     * @var CommoditiesRepository
     */
    private $commoditiesBrandRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(CommoditiesBrandRepository $commoditiesBrandRepository)
    {
        $this->commoditiesBrandRepository = $commoditiesBrandRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $commoditiesBrand = CommoditiesBrands::create($input);
        $commoditiesBrand->save();
        return $commoditiesBrand;
    }

    public function getCategoriesList()
    {
        $result = array();
        $list = DB::table('ec_commodities_cate')
                    ->select('ec_commodities_cate.*')
                    ->get()
                    ->toArray();
        if($list){
            $list   =  json_decode(json_encode($list), True);
        }
        return $list;
    }

}