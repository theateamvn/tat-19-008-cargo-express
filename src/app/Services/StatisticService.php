<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/2/2016
 * Time: 8:45 AM
 */

namespace App\Services;


use App\DTOs\OverviewInformation;
use App\Models\ReviewProvider;
use Stores;
use DB;
use Auth;
class StatisticService
{

    /**
     * @var ReviewService
     */
    private $reviewService;
    /**
     * @var InviteService
     */
    private $inviteService;

    /**
     * StatisticService constructor.
     * @param ReviewService $reviewService
     * @param InviteService $inviteService
     */
    public function __construct(ReviewService $reviewService, InviteService $inviteService)
    {
        $this->reviewService = $reviewService;
        $this->inviteService = $inviteService;
    }
    public function getOverviewApp($startDate = "1900-01-01", $endDate = 'now')
    {
        $overviewInformation = new OverviewInformation();
        $overviewInformation->shipmentInDate    = $this->getShipmentInDate();
        $overviewInformation->totalShipments    = $this->getShipmentTotal();
        $overviewInformation->totalCustomers    = $this->getTotalCustomers();
        $overviewInformation->totalSender       = $this->getTotalSender();
        return $overviewInformation;
    }
    public function getShipmentInDate($startDate = 'now',$endDate = 'now')
    {
        $startDate = date('Y-m-d', strtotime($startDate.'-1 day'));
        $endDate = date('Y-m-d', strtotime($endDate));
        $newCustomer = DB::table('ec_shipments')
            ->selectRaw('count(*) as count_new,DATE(`created_at`) as date')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->groupBy(DB::raw('DATE(`created_at`)'))
            ->get()
            ->pluck('count_new', 'date')
            ->toArray();

        $date = $startDate;
        $result = [];
        while ($date != $endDate) {
            if(isset($result[$date])) $result[$date] = [];
            if(isset($newCustomer[$date])) $result[$date]['new_count'] = $newCustomer[$date];
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $result = collect($result);
        $result = $result->mapWithKeys(function($item,$key){
            if(isset($item['count_new'])) $item['count_new'] = 0;
            return [array_merge($item,['date'=>$key])];
        });
        return count($result->toArray());
    }
    public function getShipmentTotal($startDate = '1900-01-01',$endDate = 'now')
    {
        return DB::table('ec_shipments')->where([
            ['status', '<>', -1]
            ])
            ->count();
    }
    public function getTotalCustomers()
    {
        return DB::table('ec_customers')->where([
            ['status', '=', 1]
            ])
            ->count();
    }
    public function getTotalSender()
    {
        $sender = DB::table('ec_sender')->where([
            ['status', '=', 1]
            ])
            ->count();
        $recipient = DB::table('ec_recipient')->where([
            ['status', '=', 1]
            ])
            ->count();
        return $sender + $recipient;
    }









    /**
     * @param $storeId
     * @return OverviewInformation
     */
    public function getOverviewInfo($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        $overviewInformation = new OverviewInformation();
        $overviewInformation->overallRating = $this->reviewService->getOverallRating($storeId);
        $overviewInformation->totalReview = $this->reviewService->getReviewCount($storeId);
        $overviewInformation->reviewByGoogle = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_GOOGLE);
        $overviewInformation->reviewByFacebook = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_FACEBOOK);
        $overviewInformation->reviewByYelp = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_YELP);
        $overviewInformation->reviewByTripadvisor = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_TRIPADVISOR);
        $overviewInformation->reviewByAgoda = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_AGODA);
        $overviewInformation->reviewByBooking = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_BOOKING);
        $overviewInformation->reviewByAirbnb = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_AIRBNB);
        $overviewInformation->ratingByGoogle = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_GOOGLE);
        $overviewInformation->ratingByFacebook = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_FACEBOOK);
        $overviewInformation->ratingByYelp = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_YELP);
        $overviewInformation->ratingByTripadvisor = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_TRIPADVISOR);
        $overviewInformation->ratingByAgoda = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_AGODA);
        $overviewInformation->ratingByBooking = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_BOOKING);
        $overviewInformation->ratingByAirbnb = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_AIRBNB);
        $overviewInformation->inviteClickedThroughRate = $this->inviteService->getInviteClickedThroughRate($storeId);
        $overviewInformation->inviteSentThisMonth = $this->inviteService->getCustomerCreatedCountInThisMonth($storeId);
        $overviewInformation->inviteSentLastMonth = $this->inviteService->getCustomerCreatedCountInLastMonth($storeId);
        $overviewInformation->customerInformation = $this->inviteService->getCustomerInformation($startDate, $endDate);
        // get new invites - theateam edited
        $overviewInformation->invSentThisMonth = $this->inviteService->getInvitesCreatedCountInThisMonth($storeId);
        $overviewInformation->invSentLastMonth = $this->inviteService->getInvitesCreatedCountInLastMonth($storeId);
        $smsSentCount                           = $this->inviteService->countSmsInviteSent($storeId);
        $mmsSentCount                           = $this->inviteService->countMmsInviteSent($storeId);
        $overviewInformation->smsSentTotal      = $smsSentCount + $mmsSentCount;
        $overviewInformation->emailSentTotal = $this->inviteService->countEmailInviteSent($storeId);

        return $overviewInformation;
    }

    public function getOverviewInfoByCurrentUser()
    {
        $store = Stores::getCurrentStore();
        return $this->getOverviewInfo($store->id);
    }

    public function getUsersInformation()
    {
        $total = \DB::table('users')->count();
        $activatedUser = \DB::table('users')->where('status', 1)->count();
        $managers = \DB::table('users')->where('role', 1)->count();
        $agent = \DB::table('users')->where('role', 2)->count();

        return [
            'total' => $total,
            'activated' => $activatedUser,
            'pending' => $total - $activatedUser,
            'managers' => $managers,
            'agent' => $agent
        ];
    }


    public function getCustomerStatistic($startDate,$endDate = 'now'){
        $store = Stores::getCurrentStore();
        return $this->inviteService->statisticInviteByDateRange($store->id,$startDate,$endDate);
    }


    /* by agent role*/
    public function getAgentUsersInformation()
    {
        $userId  = Auth::user()->id;
       // $store = Stores::getCurrentStore();
        $total = \DB::table('users')->where('created_by', $userId)->count();
        $activatedUser = \DB::table('users')->where([['status', '=', 1],
                                                    ['created_by', '=', $userId]])->count();

        return [
            'total' => $total,
            'activated' => $activatedUser,
            'pending' => $total - $activatedUser
        ];
    }
}
