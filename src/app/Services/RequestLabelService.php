<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\RequestLabel;
use App\Repositories\RequestLabelRepository;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SendReviewInvitation;
use Notification;
use Faker\Factory;
use Request;
use DB;
class RequestLabelService
{
    /**
     * @var RequestLabelRepository
     */
    private $requestlabelRepository;

    public function __construct(RequestLabelRepository $requestlabelRepository)
    {
        $this->requestlabelRepository = $requestlabelRepository;
    }

    public function create($input)
    {
        $requestlabel = RequestLabel::create($input);
        $requestlabel->save();
        $input['id'] = $requestlabel->id;
        // $this->sendMail((object)$input);
        return $requestlabel;
    }

    public function list_request_label()
    {
        $request_label = DB::table('ec_request_label')
        ->selectRaw('ec_request_label.*, count(ec_request_label_package.request_label_id) AS volumn,SUM(ec_request_label_package.shippingRate) AS rate')
        ->leftJoin('ec_request_label_package','ec_request_label.id','=','ec_request_label_package.request_label_id')
        ->orderBy('ec_request_label.id', 'DESC')
        ->groupBy('ec_request_label.id')
        ->get()
        ->toArray();
        return $request_label;
    }

    public function listPackageById($id)
    {
        $request_label_package = DB::table('ec_request_label_package')->select(array('ec_request_label_package.*','ec_request_label.sender_name','ec_request_label.delivery_company'))
        ->leftJoin('ec_request_label','ec_request_label.id','ec_request_label_package.request_label_id')->where('request_label_id',$id)->get()->toArray();
        return $request_label_package;
    }
    // submit to delivery to company
    public function checkRequestLabel($id)
    {
        return DB::table('ec_request_label')->where([
            ['id', '=', $id],
            ['status', '=', 1]
        ])->count();
    }
    public function getRequestLabelInfor($id, $status = 1)
    {
        $result = array();
        $result = DB::table('ec_request_label')
                    ->select('ec_request_label.*')
                    ->where('ec_request_label.id', $id)
                    ->where('ec_request_label.status', $status)
                    ->get()
                    ->toArray();
        return $result[0];
    }
    public function getRequestLabelPackageInfor($id, $status = 1)
    {
        $result = array();
        $result = DB::table('ec_request_label_package')
                    ->select('ec_request_label_package.*')
                    ->where('ec_request_label_package.request_label_id', $id)
                    ->where('ec_request_label_package.status', $status)
                    ->get()
                    ->toArray();
        return $result;
    }
    public function updateRequestLabel($id, $tracking_number) {
        $input = array( 'tracking_number' => $tracking_number);
        $this->requestlabelRepository->update($input, $id);
        return true;
    }
    public function updateRequestLabelAccept($id, $tracking_number) {
        $userId = $this->getCurrentUser()->id;
        $input = array( 'status' => 2, 'tracking_number' => $tracking_number, 'updated_by' => $userId);
        $this->requestlabelRepository->update($input, $id);
        return true;
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
    // send email
    public function checkRequestLabelSendEmail($id)
    {
        return DB::table('ec_request_label')->where([
            ['id', '=', $id],
            ['status', '=', 2]
        ])->count();
    }

    public function sendMail($requestLabel)
    {
        $via_email = ['email-request-label-alert'];
        $emailDetail = [
            'id'                => $requestLabel->id,
            'type_email'        => 'email_request_label_alert',
            'store_id'          => 1,
            'customer_code'     => $requestLabel->customer_code,
            'full_name'         => $requestLabel->sender_name,
            'sender_name'       => 'T-Express',
            'email_title'       => 'Notice of creating new request label',
            'email'             => 'lamdongb@gmail.com',
            'send_date'         => $requestLabel->send_date,
            'total_package'     => $requestLabel->total_package,
            'delivery_company'  => $requestLabel->delivery_company == 1 ? 'UPS' : 'FEDEX'
        ];
        //$emailDetail = [0=>$emailDetail];
        Notification::send((object)$emailDetail, new SendReviewInvitation($via_email));
    }
    
}