<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Http\Requests\API\CreateUserAPIRequest;
use App\Models\Store;
use App\Models\User;
use App\Models\StorePackageSms;
use App\Repositories\UserRepository;
use Faker\Factory;
use Request;
use DB;
class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * UserService constructor.
     * @param $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->faker = Factory::create();
    }

    public function createNewUserWithoutPayment($input){

        $user = User::create($input);
        $user->password = bcrypt($input['password']);
        $user->save();

        $store = $this->createStoreBasic($input);
        $user->storeUsers()->attach($store);
        // create package sms
        $package_basic = [
            'store_id' => $store['id'],
            'user_id' => $user['id'],
            'package' => 'Basic message',
            'total_allow_sent' => 100
        ];
        $this->createStorePackageBasic($package_basic);

        return $user;
    }

    public function createStore($input = null){
        if($input){
            $store = Store::create($input);
        }else{
            // get information config // jim edited
            $invitation_introducing = $this->getConfigMasterSetting('introduction_text')[0]->value;
            $survey_message = $this->getConfigMasterSetting('survey_message')[0]->value;
            $apologize_message = $this->getConfigMasterSetting('apologize_message')[0]->value;
            $store = Store::create([
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'url' => $this->faker->url,
                'survey_message_1' => $survey_message,
                'survey_message_2' => $apologize_message,
                'invitation_introducing' => $invitation_introducing
            ]);
        }
        $store->save();
        return $store;
    }
    public function createStoreBasic($input = null){
        //var_dump($this->getConfigMasterSetting('introduction_text')[0]);die;
        // get information config
        $invitation_introducing = $this->getConfigMasterSetting('introduction_text')[0]->value;
        $survey_message = $this->getConfigMasterSetting('survey_message')[0]->value;
        $apologize_message = $this->getConfigMasterSetting('apologize_message')[0]->value;

        $store = Store::create([
            'phone' => $input['phone'],
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'url' => $this->faker->url,
            'survey_message_1' => $survey_message,
            'survey_message_2' => $apologize_message,
            'invitation_introducing' => $invitation_introducing
        ]);
        $store->save();
        return $store;
    }
    public function getConfigMasterSetting($key) {
        return DB::table('master_settings')->where('key', $key)->get();
    }
    public function createStorePackageBasic($input = null){
        $store_package = StorePackageSms::create([
            'store_id' => $input['store_id'],
            'user_id' => $input['user_id'],
            'package' => $input['package'],
            'total_allow_sent' => $input['total_allow_sent']
        ]);
        $store_package->save();
        return $store_package;
    }
    public function createStorePackage($input = null){
        $store_package = StorePackageSms::create([
            'store_id' => $input['store_users'][0]['id'],
            'user_id' => $input['id'],
            'package' => $input['package'],
            'total_allow_sent' => $input['total_allow_sent']
        ]);
        $store_package->save();
        return $store_package;
    }
    public function getUserTotalByAgent($idAgent)
    {
        $total = \DB::table('users')->where('created_by', $idAgent)->count();
        return [
            'idAgent' => $idAgent,
            'total' => $total
        ];
    }

}