<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Commodities;
use App\Repositories\CommoditiesRepository;
use Faker\Factory;
use Request;
use DB;
class CommoditiesService
{
    /**
     * @var CommoditiesRepository
     */
    private $commoditiesRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(CommoditiesRepository $commoditiesRepository)
    {
        $this->commoditiesRepository = $commoditiesRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $commodities = Commodities::create($input);
        $commodities->save();
        return $commodities;
    }
    public function getCustomerList()
    {
        $result = array();
        $customers = DB::table('ec_customers')
                    ->where('status', 1)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['customer_code'].' - '.$customer['firstname'].' '.$customer['lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getCateList()
    {
        $result_cate = DB::table('ec_commodities_cate')
                    ->select('ec_commodities_cate.*')
                    ->where('ec_commodities_cate.status', 1)
                    ->orderBy('ec_commodities_cate.id', 'DESC')
                    ->get()
                    ->toArray();
        $result_cate   =  json_decode(json_encode($result_cate), True);
        $cus = array();
        foreach ($result_cate as $key => $value) {
            $cus[$key]['label'] = $value['commodity_cate_name'];
            $cus[$key]['value'] = $value['id'];
        }
        return $cus;
    }
    public function getBrandList($id)
    {
        $result = DB::table('ec_commodities_brand')
                    ->where('status', 1)
                    ->where('commodity_cate', $id)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $result   =  json_decode(json_encode($result), True);
        $cus = array();
        foreach ($result as $key => $value) {
            $cus[$key]['label'] = $value['commodity_brand_name'];
            $cus[$key]['value'] = $value['id'];
        }
        return $cus;
    }
    public function getChargesList($id)
    {
        $result = DB::table('ec_commodities_charges')
                    ->where('status', 1)
                    ->where('commodity_cate', $id)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $result   =  json_decode(json_encode($result), True);
        $cus = array();
        foreach ($result as $key => $value) {
            $cus[$key]['label'] = $value['commodity_charges_name'];
            $cus[$key]['value'] = $value['id'];
        }
        return $cus;
    }


}