<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\ServiceTypeZone;
use App\Models\ServiceZone;
use App\Models\ServiceType;
use App\Repositories\ServiceTypeZoneRepository;
use Faker\Factory;
use Request;
use DB;
class ServiceTypeZoneService
{
    /**
     * @var ServiceTypeZoneRepository
     */
    private $serviceTypeZoneRepository;

    public function __construct(ServiceTypeZoneRepository $serviceTypeZoneRepository)
    {
        $this->serviceTypeZoneRepository = $serviceTypeZoneRepository;
    }

    public function getListServiceTypeZone($id)
    {
        $result=array();
        $serviceTypeZone = DB::table('ec_service_type_zone')->where('service_type_item', '=', $id)->get()->toArray();
        $serviceZone = DB::table('ec_service_zone')->get()->toArray();
        $serviceTypeZone   =  json_decode(json_encode($serviceTypeZone), True);
        foreach ($serviceTypeZone as $key => $value) {
            $list_zone = explode(",",$value['zone_to']);
            $serviceTypeZone[$key]['list_zone']=$list_zone;
        }
        $result['service_type_zone']= $serviceTypeZone;
        $result['service_zone']= $serviceZone;
        return $result;
    }

    public function create($input)
    {
        $serviceTypeZone = ServiceTypeZone::create($input);
        $serviceTypeZone->save();
        return $serviceTypeZone;
    }

    public function createServiceZone($input)
    {
        $serviceType = ServiceType::create($input);
        $arr= array(
            'zone_to'=>'1,2,3,4,5,6',
            'service_type' => $serviceType->service_type_id,
            'service_type_item' => $serviceType->id,
            'rate' => $serviceType->rate,
            'discount_rate' => $serviceType->discount_rate,
            'status' => 1,
            'created_by' =>$serviceType->created_by,
            'updated_by' =>$serviceType->updated_by,
            'ip'=> ''
        );
        $serviceTypeZone = ServiceTypeZone::create($arr);
        $serviceTypeZone->save();
        $serviceType->save();
        return $serviceType;
    }

    public function getListService()
    {
        $serviceTypeZone = DB::table('ec_service_type_zone')->get()->toArray();
        $serviceType = DB::table('ec_service_type')->get()->toArray();
        $serviceZone = DB::table('ec_service_zone')->get()->toArray();
        $serviceTypeZone   =  json_decode(json_encode($serviceTypeZone), True);
        $serviceType   =  json_decode(json_encode($serviceType), True);
        $arr =array();
        foreach ($serviceTypeZone as $key => $value) {
            $list_zone = explode(",",$value['zone_to']);
            $serviceTypeZone[$key]['list_zone']=$list_zone;
            
            foreach ($serviceType as $k => $v) {
                if($value['service_type_item'] == $v['id']){
                   $serviceType[$k]['type_zone'] = $serviceTypeZone[$key];
                }
            }  
        }
        return $serviceType;
    }
    
    public function getListZone()
    {
        $result = DB::table('ec_service_zone')->get()->toArray();
        $result   =  json_decode(json_encode($result), True);
        return $result;
    }
}