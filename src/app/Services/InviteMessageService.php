<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 11/30/2016
 * Time: 10:45 AM
 */

namespace App\Services;


use App\Criteria\ByUserStoreCriteria;
use App\Criteria\InviteMessageByTypeCriteria;
use App\Models\InviteMessage;
use App\Repositories\InviteMessageRepository;

class InviteMessageService
{
    private $inviteMessageRepo;

    /**
     * InviteMessageService constructor.
     * @param $inviteRepo
     */
    public function __construct(InviteMessageRepository $inviteRepo)
    {
        $this->inviteMessageRepo = $inviteRepo;
    }

    /**
     * @param $type
     * @return InviteMessage
     */
    public function getMessageByUser($type){
        $this->inviteMessageRepo->resetCriteria();
        $this->inviteMessageRepo->pushCriteria(new ByUserStoreCriteria());
        $this->inviteMessageRepo->pushCriteria(new InviteMessageByTypeCriteria($type));
        return $this->inviteMessageRepo->first();
    }

    public function getMessage($type){
        $this->inviteMessageRepo->resetCriteria();
        $this->inviteMessageRepo->pushCriteria(new InviteMessageByTypeCriteria($type));
        return $this->inviteMessageRepo->first();
    }

}