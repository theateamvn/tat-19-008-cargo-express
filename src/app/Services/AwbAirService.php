<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\AwbAir;
use App\Repositories\AwbAirRepository;
use Faker\Factory;
use Request;
use DB;
class AwbAirService
{
    /**
     * @var AwbAirRepository
     */
    private $awbAirRepository;

    public function __construct(AwbAirRepository $awbAirRepository)
    {
        $this->awbAirRepository = $awbAirRepository;
    }

    public function getListAwbAir()
    {
        $awb_air = DB::table('ec_awb_air')->where('status', '<>', -1)->get()->toArray();
        return $awb_air;
    }

    public function create($input)
    {
        $awbAir = AwbAir::create($input);
        $awbAir->save();
        return $awbAir;
    }

    public function checkCode($code)
    {
        $result = DB::table('ec_awb_air')->where('air_pre_code','=',$code)->count();
        return $result;
    }
    
}