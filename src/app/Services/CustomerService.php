<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Models\Customer;
use App\Models\StorePackageSms;
use App\Repositories\CustomerRepository;
use Faker\Factory;
use Request;
use DB;
class CustomerService
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * UserService constructor.
     * @param $userRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->faker = Factory::create();
    }
    public function create($input){

        $customer = Customer::create($input);
        $customer->save();

        return $customer;
    }

    public function customerCreatedSender($customer_id, $input)
    {
        $sender         =  DB::table('ec_sender')->where('customer_id', $customer_id)->count();
        $typeCustomer   =  DB::table('ec_customers')->where('id', $customer_id)->where('type_customer', $input['type_customer'])->count();
        if($sender && !$typeCustomer){
            return true;
        } else {
            return false;
        }
    }
    
    public function getCityByZipcode($zipcode)
    {
        $cities = DB::table('ec_city')
                        ->where('status', 1)
                        ->where('zipcode', $zipcode)
                        ->get()
                        ->toArray();
        $result   =  json_decode(json_encode($cities[0]), True);
        return $result;
    }

    public function getTotalCustomerByType($type)
    {
        return DB::table('ec_customers')->where([
            ['type_customer', '=', $type]
        ])->count();
    }

    public function getCustomerByCode($customer_code)
    {
        $customers= DB::table('ec_customers')->where('customer_code', $customer_code)->get()->toArray();
        //var_dump($customers);
        if(count($customers)){
            $customers   =  json_decode(json_encode($customers[0]), True);
        } else {
            $customers   = [];
        }
        return $customers;
    }
    public function getCustomerByPhone_bk($customer_phone)
    {
        $customers= DB::table('ec_customers')->where('phone', $customer_phone)->get()->toArray();
        $customers   =  json_decode(json_encode($customers[0]), True);
        return $customers;
    }
    public function getCustomerByPhone($customer_phone)
    {
        $customers= DB::table('ec_customers')->where('phone','LIKE', "%{$customer_phone}%")->get()->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        return $customers;
    }
    public function customerLogin($user)
    {
        $customers = DB::table('ec_customers')->where('email','=',$user)->orWhere('phone','=',$user)->get()->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        $result = array();
        foreach ($customers as $key=>$customer) {
            $result[$key]['customer_code'] = $customer['customer_code'];
        }
        return $result;
    }

}