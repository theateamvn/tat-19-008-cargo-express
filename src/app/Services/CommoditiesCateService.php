<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\CommoditiesCate;
use App\Repositories\CommoditiesCateRepository;
use Faker\Factory;
use Request;
use DB;
class CommoditiesCateService
{
    /**
     * @var CommoditiesRepository
     */
    private $commoditiesCateRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(CommoditiesCateRepository $commoditiesCateRepository)
    {
        $this->commoditiesCateRepository = $commoditiesCateRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $commoditiesCate = CommoditiesCate::create($input);
        $commoditiesCate->save();
        return $commoditiesCate;
    }

}