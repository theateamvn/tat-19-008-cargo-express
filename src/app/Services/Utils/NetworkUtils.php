<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/12/2017
 * Time: 11:51 AM
 */

namespace App\Services\Utils;


class NetworkUtils
{
    public static function checkProxy($proxy=null)
    {
        $proxy=  explode(':', $proxy);
        $host = $proxy[0];
        $port = $proxy[1];
        $waitTimeoutInSeconds = 3;
        if($fp = @fsockopen($host,$port,$errCode,$errStr,$waitTimeoutInSeconds)){
            fclose($fp);
            return true;
        } else {
            return false;
        }
    }
}