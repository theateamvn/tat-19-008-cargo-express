<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\City;
use App\Repositories\CityRepository;
use Faker\Factory;
use Request;
use DB;
class CityService
{
    /**
     * @var CityRepository
     */
    private $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function getListCities()
    {
        $city = DB::table('ec_city')->where('status', '<>', -1)->get()->toArray();
        return $city;
    }

    public function create($input)
    {
        $city = City::create($input);
        $city->save();
        return $city;
    }
    
}