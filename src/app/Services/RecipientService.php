<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Recipient;
use App\Repositories\RecipientRepository;
use Faker\Factory;
use Request;
use DB;
class RecipientService
{
    /**
     * @var RecipientRepository
     */
    private $recipientRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * UserService constructor.
     * @param $userRepository
     */
    public function __construct(RecipientRepository $recipientRepository)
    {
        $this->RecipientRepository = $recipientRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $number = $this->getTotalSenderByCustomer($input['customer_id']);
        $number = $number +1;
        if($number < 10){
            $number = sprintf("%02s", $number);
        }
        $input['contact_code']     = $input['customer_code'].'-'.$number;
        $recipient = Recipient::create($input);
        $recipient->save();
        return $recipient;
    }
    public function getCustomerList()
    {
        $result = array();
        $customers = DB::table('ec_customers')
                    ->where('status', 1)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['customer_code'].' - '.$customer['firstname'].' '.$customer['lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getTotalSenderByCustomer($customer_id)
    {
        return DB::table('ec_recipient')->where([
            ['customer_id', '=', $customer_id]
        ])->count();
    }

    public function loadRecipientByCustomer($customer_id)
    {
        $result = DB::table('ec_recipient')->where('customer_id','=',$customer_id)->get()->toArray();
        $result   =  json_decode(json_encode($result), True);
        return $result;
    }

}