<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Shipments;
use App\Models\ExportList;
use App\Repositories\ExportListRepository;
use Faker\Factory;
use Request;
use DB;
use Exception;
use Log;
class ExportListService
{

    /**
     * @var \Faker\Generator
     */
    private $faker;
    private $exportListRepository;

    /**
     * UserService constructor.
     * @param $userRepository
     */
    public function __construct(ExportListRepository $exportListRepository)
    {
        $this->exportListRepository = $exportListRepository;
        $this->faker = Factory::create();
    }

    /**
     * export list
     */
    public function getShipmentList()
    {
        $result = array();
        $shipments = DB::table('ec_shipments')
                    ->select('ec_shipments.*')
                    ->whereNotExists(function($query)
                    {
                        $query->select(DB::raw(1))
                            ->from('ec_export_list_items')
                            ->leftJoin("ec_export_list","ec_export_list.id","=","ec_export_list_items.export_list_id")
                            ->where('ec_export_list.status','<>', -1)
                            ->whereRaw('ec_export_list_items.shipment_id = ec_shipments.id');
                    })
                    ->where('ec_shipments.status', 1)
                    ->where('ec_shipments.shipment_code','<>', NULL)
                    ->orderBy('ec_shipments.id', 'DESC')
                    ->get()
                    ->toArray();
        $shipments   =  json_decode(json_encode($shipments), True);
        foreach ($shipments as $key => $shipment) {
            $result[$key]['name'] = $shipment['shipment_code'];
            $result[$key]['id']   = $shipment['id'];
            $result[$key]['dataCode']   = $shipment['shipment_code'];
        }
        return $result;
    }
    public function getAirList()
    {
        $result = array();
        $airs = DB::table('ec_awb_air')
                    ->select('ec_awb_air.*')
                    ->where('ec_awb_air.status', 1)
                    ->orderBy('ec_awb_air.id', 'DESC')
                    ->get()
                    ->toArray();
        $airs   =  json_decode(json_encode($airs), True);
        foreach ($airs as $key => $air) {
            $result[$key]['name']       = $air['air_name'];
            $result[$key]['id']         = $air['id'];
            $result[$key]['dataCode']   = $air['air_pre_code'];
        }
        return $result;
    }
    public function getShipmentDetail($id)
    {
        $result = array();
        $result = DB::table('ec_shipments')
                    ->select('ec_shipments.*'
                        ,'ec_st.service_type_name'
                        ,'ec_service_type.service_type_item'
                        ,'ec_shipments_packages.package_number','ec_shipments_packages.package_weight','ec_shipments_packages.package_d','ec_shipments_packages.package_w','ec_shipments_packages.package_h'
                    )
                    ->leftJoin("ec_st","ec_st.id","=","ec_shipments.info_service_type")
                    ->leftJoin("ec_service_type","ec_service_type.id","=","ec_shipments.info_service_item")
                    ->leftJoin("ec_shipments_packages","ec_shipments_packages.shipment_id","=","ec_shipments.id")
                    ->where('ec_shipments.id', $id)
                    ->get()
                    ->toArray();
        return $result[0];
    }
    public function getShipmentDetailCode($code)
    {
        $result = array();
        $result = DB::table('ec_shipments')
                    ->select('ec_shipments.*'
                        ,'ec_st.service_type_name'
                        ,'ec_service_type.service_type_item'
                        ,'ec_shipments_packages.package_number','ec_shipments_packages.package_weight','ec_shipments_packages.package_d','ec_shipments_packages.package_w','ec_shipments_packages.package_h'
                    )
                    ->leftJoin("ec_st","ec_st.id","=","ec_shipments.info_service_type")
                    ->leftJoin("ec_service_type","ec_service_type.id","=","ec_shipments.info_service_item")
                    ->leftJoin("ec_shipments_packages","ec_shipments_packages.shipment_id","=","ec_shipments.id")
                    ->where('ec_shipments.shipment_code', $code)
                    ->get()
                    ->toArray();
        
        if(isset($result[0])){
            $detail = $result[0];
            // check scan exist
            $checkExportList = DB::table('ec_export_list_items')
                                ->where('shipment_id', $detail->id)
                                ->where('status','<>', -1)
                                ->get()
                                ->toArray();
            $finalDetail = array();
            if(isset($checkExportList[0])){
                $finalDetail = [
                    'success' => false,
                    'data' => $checkExportList
                ];
            } else {
                $finalDetail = [
                    'success' => true,
                    'data' => $detail
                ];
            }
            return $finalDetail;
        } else {
            return false;
        }
    }
    public function getShipmentDetailCodeScan($code, $exportlist_id = 0)
    {
        // check scan exist
        $result = array();
        if($exportlist_id) {
            $check = DB::table('ec_export_list_items')
                ->leftJoin("ec_export_list","ec_export_list.id","=","ec_export_list_items.export_list_id")
                ->where([
                ['ec_export_list_items.shipment_barcode', '=', strtolower($code)],
                ['ec_export_list_items.export_list_id', '<>', $exportlist_id],
                ['ec_export_list.status', '1']
            ])->count();
        } else {
            $check = DB::table('ec_export_list_items')
                ->leftJoin("ec_export_list","ec_export_list.id","=","ec_export_list_items.export_list_id")
                ->where([
                ['ec_export_list_items.shipment_barcode', '=', strtolower($code)],
                ['ec_export_list.status', '1']
            ])->count();
        }
        if(!$check) {
            $result = [
                'success' => false,
                'code' => $code
            ];
        } else {
            $result = [
                'success' => true,
                'code' => $code
            ];
        }
        return $result;
    }
    public function create($input){
        $exportList = ExportList::create($input);
        $exportList->save();
        return $exportList;
    }
    public function create_awb($awb, $id) {
            DB::table('ec_awb')->insert(
                [
                    'export_list_id'        => $id,
                    'numberMasterAwwb'      => $awb['numberMasterAwwb'],
                    'dateAwb'               => $awb['dateAwb'],
                    'airportAwb'            => $awb['airportAwb'],
                    'airCarrierAwb'         => $awb['airCarrierAwb']['id']
                ]
            );
        return true;
    }
    public function saveExportListItem($id, $listBox, $listItemBox, $export_list_id = 0) {
        if($export_list_id) { 
            // deleted by shiment
            DB::table('ec_export_list_items')->where('export_list_id', $export_list_id)->delete();
        }
            // insert new
            $result = array();
            foreach ($listItemBox as $key => $itemBox) {
                DB::table('ec_export_list_items')->insert(
                    [
                        'export_list_id'        => $id,
                        'box_id'                => $itemBox['box_id'],
                        'shipment_id'           => '-1',
                        'shipment_barcode'          => $itemBox['id'],
                        'shipment_package_num_separate' => isset($itemBox['shipment_package_num_separate']) ? $itemBox['shipment_package_num_separate'] : '',
                        'shipment_barcode_customer_code'    => $itemBox['cus_code'],
                        'shipment_barcode_num'              => $itemBox['shipment_num'],
                        'shipment_barcode_weight'           => $itemBox['shipment_weight'],
                        'shipment_barcode_package_num'      => $itemBox['shipment_package_num'],
                    ]
                );
            }
        return true;
    }
    public function getExportEdit($id){
        $result = array();
        // get export detail
        $detail_exportlist = DB::table('ec_export_list')
                    ->select('ec_export_list.*'
                    )
                    ->where('ec_export_list.id', $id)
                    ->where('ec_export_list.status', 1)
                    ->get()
                    ->toArray();
        if(isset($detail_exportlist[0])){
            // get list 
            $box        = array();
            $box_list   = array();
            $export_packages = DB::table('ec_export_list_items')
                                    ->select(
                                        //'ec_export_list_items.export_list_id', 'ec_export_list_items.box_id', 'ec_export_list_items.shipment_id','ec_export_list_items.id'
                                        'box_id', DB::raw('count(*) as total')
                                        )
                                    ->where('ec_export_list_items.export_list_id', $id)
                                    ->groupBy('box_id')
                                    ->get()
                                    ->toArray();
            $export_packages   =  json_decode(json_encode($export_packages), True);
            //var_dump($export_packages);die;
            $box[0]['box_id'] = 0;
            foreach ($export_packages as $key => $item) {
                // box list
                //$box_list[$key]['box_id']   = $item['box_id'];
                //$box_list[$key]['id']       = $item['id'];
                // box 
                $box[$item['box_id']]['box_id'] = $item['box_id'];
                /*if ( count($box) > 1 && !in_array($item['box_id'], $box)) { 
                    $box[$key]['box_id'] = $item['box_id'];
                }else{
                    $box[$key]['box_id'] = $item['box_id'];
                }*/
            }

            $box_list = DB::table('ec_export_list_items')
            ->select('ec_export_list_items.box_id', 'ec_export_list_items.shipment_barcode AS id', 'shipment_barcode_customer_code AS cus_code', 
            'shipment_barcode_num AS shipment_num', 'shipment_barcode_weight AS shipment_weight', 'shipment_barcode_package_num AS shipment_package_num',
            'ec_export_list_items.shipment_package_num_separate')
            ->from('ec_export_list_items')
            ->where('export_list_id','=', $id)->get()->toArray();
            $box_list   =  json_decode(json_encode($box_list), True);
            
            $list_master = DB::table('ec_export_list_items')
            ->select('ec_export_list_items.shipment_barcode AS id', 'shipment_barcode_customer_code AS cus_code', 
            'shipment_barcode_num AS shipment_num', 'shipment_barcode_weight AS shipment_weight', 'shipment_barcode_package_num AS shipment_package_num',
            'ec_export_list_items.shipment_package_num_separate')
            ->from('ec_export_list_items')
            ->where('export_list_id','=', $id)->get()->toArray();
            $list_master   =  json_decode(json_encode($list_master), True);
            $box = array_unique($box, SORT_REGULAR);
            
            $result = [
                'detail_exportlist' => $detail_exportlist,
                'box'               => $box,
                'box_list'          => $box_list,
                'list_master'       => $list_master
            ];
            return $result;
            
        } else {
            return false;
        }
        
    }
    public function getExportListDetail($id){
        $result = array();
        // get export detail
        $detail_exportlist = DB::table('ec_export_list')
                    ->select('ec_export_list.*'
                    )
                    ->where('ec_export_list.id', $id)
                    ->where('ec_export_list.status', 1)
                    ->get()
                    ->toArray();
        $detail_exportlist   =  json_decode(json_encode($detail_exportlist), True);
        return $detail_exportlist[0];
        
    }

    // import and add 
    public function create_import($input) {
        $insert = DB::table('ec_export_list_status')->insert(
            [
                'customer_code'        => $input['customer_code'],
                'shipment_status'      => $input['shipment_status'],
                'customer_awb'         => $input['customer_awb']
            ]
        );
        return $insert;
    }
    public function edit_import($input) {
        $insert = DB::table('ec_export_list_status')
                        ->where('id', $input['id'])
                        ->update(
                            [
                                'customer_code'        => $input['customer_code'],
                                'shipment_status'      => $input['shipment_status'],
                                'customer_awb'         => $input['customer_awb']
                            ]
                        );
        if($input['check']){
            DB::table('ec_export_list_status')
                    ->where('customer_awb','=', $input['customer_awb'])
                    ->update(
                        [
                            'shipment_status'      => $input['shipment_status']
                        ]
                    );
        }
        return $insert;
    }
    public function create_import_upload(array $exportlist)
    {
        try {
            foreach ($exportlist as $export) {
                if($export['customer_code'] != "" && $export['customer_code'] != " " && $export['customer_code'] != NULL) {
                    DB::table('ec_export_list_status')->insert(
                        [
                            'customer_code'        => $export['customer_code'],
                            'shipment_status'      => $export['shipment_status'],
                            'customer_awb'         => $export['customer_awb']
                        ]
                    );
                }
            }
            return true;
        } catch (\Exception $ex) {
            Log::error($ex);
            return false;
        }
    }
    // END import and add

    //Delete export list item 
    public function delExportListItem($id)
    {
        DB::table('ec_export_list_items')->where('export_list_id', $id)->delete();
    }
}