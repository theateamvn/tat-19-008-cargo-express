<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\RequestLabelPackage;
use App\Repositories\RequestLabelPackageRepository;
use Faker\Factory;
use Request;
use DB;
class RequestLabelPackageService
{
    /**
     * @var RequestLabelRepository
     */
    private $requestlabelpackageRepository;

    public function __construct(RequestLabelPackageRepository $requestlabelpackageRepository)
    {
        $this->requestlabelpackageRepository = $requestlabelpackageRepository;
    }

    public function create($input)
    {
        $requestlabelpackage = RequestLabelPackage::create($input);
        $requestlabelpackage->save();
        return $requestlabelpackage;
    }
    
}