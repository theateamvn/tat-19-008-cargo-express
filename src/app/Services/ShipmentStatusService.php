<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\ShipmentStatus;
use App\Models\ShipmentStatusLogs;
use App\Repositories\ShipmentStatusRepository;
use App\Repositories\ShipmentStatusLogsRepository;
use Faker\Factory;
use Request;
use DB;
class ShipmentStatusService
{
    /**
     * @var ShipmentStatusRepository
     */
    private $shipmentStatusRepository;
    private $shipmentStatusLogsRepository;

    public function __construct(ShipmentStatusRepository $shipmentStatusRepository, ShipmentStatusLogsRepository $shipmentStatusLogsRepository)
    {
        $this->shipmentStatusRepository = $shipmentStatusRepository;
        $this->shipmentStatusLogsRepository = $shipmentStatusLogsRepository;
    }

    public function create($input){
        $shipmentStatus = ShipmentStatus::create($input);
        $shipmentStatus->save();
        return $shipmentStatus;
    }

    public function createShipmentStatuslog($input)
    {
        $shipmentStatusLogs=ShipmentStatusLogs::create($input);
        $shipmentStatusLogs->save();
        return $shipmentStatusLogs; 
    }

    public function getShipmentStatusLog($id)
    {
        $result=array();
        $shipments = DB::select('SELECT ec_shipments_status.*,ec_shipments_status_log.log_note FROM ec_shipments_status 
        LEFT JOIN ec_shipments_status_log 
        on ec_shipments_status.id=ec_shipments_status_log.status_id AND ec_shipments_status_log.shipment_id = '.$id);
        $shipments   =  json_decode(json_encode($shipments), True);
        foreach ($shipments as $value) {
            $result[$value['id']]=$value;
        }
        return (array)$result;
    }

    public function changeShipmentStatusLogs($id, $status_id, $status_note, $user_id, $date_log)
    {
            $shipmentStatusLog=array(
                'shipment_id'=> $id,
                'status_id'  => $status_id,
                'log_note'   => $status_note,
                'date_log'   => $date_log,
                'updated_by' => $user_id,
                'created_by' => $user_id,
            );
            $this->shipmentStatusLogsRepository->create($shipmentStatusLog);
    }

    public function LoadShipmentStatus()
    {
        $shipmentStatus= DB::table('ec_shipments_status')->get()->toArray();
        $shipmentStatus   =  json_decode(json_encode($shipmentStatus), True);
        $result=array();
        foreach ($shipmentStatus as $key => $value) {
           $result[$key]['id']= $value['id'];
           $result[$key]['english_name']= $value['english_name'];
           $result[$key]['vietnam_name']= $value['vietnam_name'];
        }
        return $result;
    }

    public function shipmentStatus()
    {
        $result = DB::table('ec_shipments')->selectRaw('ec_shipments.status,ec_shipments_status.english_name,count(*) as volumn')
        ->leftJoin('ec_shipments_status','ec_shipments_status.id','=','ec_shipments.status')
        ->where('status', '<>', -1)->groupBy('status')->get()->toArray();
        return $result;
    }

    public function ShipmentByMonth()
    {
        $shipmentByMonth = DB::table('ec_shipments_status_log')
        ->selectRaw('DISTINCT ec_shipments_status_log.status_id,ec_shipments_status.english_name, ec_shipments_status_log.date_log,COUNT(*) as volumn')
        ->leftJoin('ec_shipments_status','ec_shipments_status.id','=','ec_shipments_status_log.status_id')
        ->groupBy('ec_shipments_status_log.date_log')->groupBy('ec_shipments_status_log.status_id')->get()->toArray();
        $shipmentByMonth = json_decode(json_encode($shipmentByMonth), True);
        for ($i = 0; $i < count($shipmentByMonth);$i++) {
            $result[$i]['date'] = $shipmentByMonth[$i]['date_log'];
            $result[$i][$shipmentByMonth[$i]['status_id']] = $shipmentByMonth[$i]['volumn'];
        }
        return $result;
    }

    public function LoadShipmentStatusById($shipment_id)
    {
        $result = DB::table('ec_shipments_status_log')
        ->leftJoin('ec_shipments_status','ec_shipments_status_log.status_id','=','ec_shipments_status.id')
        ->where('shipment_id',$shipment_id)
        ->orderBy('ec_shipments_status_log.id', 'desc')
        ->get()->toArray();
        return $result;
    }

}