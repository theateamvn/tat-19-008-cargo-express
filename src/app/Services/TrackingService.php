<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Tracking;
use App\Models\TrackingConsolidated;
use App\Repositories\TrackingRepository;
use App\Repositories\TrackingConsolidatedRepository;
use App\Notifications\SendReviewInvitation;
use Faker\Factory;
use Request;
use DB;
use Notification;
class TrackingService
{
    /**
     * @var TrackingRepository
     */
    private $trackingRepository;

    /**
     * @var TrackingConsolidatedRepository
     */
    private $trackingConsolidatedRepository;

    public function __construct(TrackingRepository $trackingRepository, TrackingConsolidatedRepository $trackingConsolidatedRepository)
    {
        $this->trackingRepository = $trackingRepository;
        $this->trackingConsolidatedRepository = $trackingConsolidatedRepository;
    }

    public function getListTracking($customer_code)
    {
        $tracking = DB::table('ec_tracking')->where('customer_code','=',$customer_code)->where('status','=','0')->get()->toArray();
        return $tracking;
    }

    public function create($listItem)
    {
        $tracking_number_list = "<br/>";
        foreach ($listItem as $item) {
            $tracking = Tracking::create($item);
            $tracking->save();
            $tracking_number_list .= $item['tracking_code']." <br/>";
        }
        if(count($listItem)) {
            // send email 
            $customer_id = $listItem[0]['customer_id'];
            $customer_detail = $this->getDetailCustomer($customer_id);
            
            $via_email = ['email'];
            $emailDetail = [
                'id' =>$customer_detail->id,
                'type_email' => 'tracking_email',
                'store_id' => 1,
                'name' => $customer_detail->customer_code,
                'email' => $customer_detail->email,
                'phone' => $customer_detail->phone,
                'phone_number' => $customer_detail->phone,
                'customer_code' => $customer_detail->customer_code,
                'total_tracking' => count($listItem),
                'tracking_number_list' => substr_replace($tracking_number_list, "", -1)
            ];
            //$emailDetail = [0=>$emailDetail];
            Notification::send((object)$emailDetail, new SendReviewInvitation($via_email));
        }
       // var_dump($emailDetail);die;
        return $listItem;
    }
    
    public function getListConsolidated_bk($input)
    {
        $tracking = DB::table('ec_tracking_consolidated')
        ->select(DB::raw('DISTINCT customer_code,count(customer_code) AS consolidated'))
        ->where('customer_code','like', isset($input['customer_code']) ? '%'.$input['customer_code'].'%': '%%')
        ->where('status','=',1)
        ->groupBy('customer_code')
        ->paginate();
        return $tracking;
    }
    public function getListConsolidated($input)
    {
        $tracking = DB::table('ec_tracking_consolidated')
        ->select('ec_tracking_consolidated.*')
        //->where('customer_code','like', isset($input['customer_code']) ? '%'.$input['customer_code'].'%': '%%')
        ->where('status','=',1)
        ->paginate();
        return $tracking;
    }

    public function UpdateConsolidatedTracking($input)
    {
        $tracking = $this->trackingRepository->update($input, $input['id']);
        return $tracking;
    }

    public function createConsolidated($listItem)
    {
        foreach ($listItem as $item) {
            $tracking = TrackingConsolidated::create($item);
            $tracking->save();
        }
    }

    public function checkTrackingcode($tracking_code, $customer_code)
    {
        $result = DB::table('ec_tracking')->where('tracking_code',$tracking_code)->where('customer_code',$customer_code)->get()->toArray();
        return count($result) > 0 ? false : true;
    }

    public function getConsolidatedByCustomer($customer_code)
    {
        $result = DB::table('ec_tracking_consolidated')->where('customer_code',$customer_code)->get()->toArray();
        return $result;
    }

    public function getListTrackingByCustomer($customer_code, $list_tracking_id)
    {
        $result = DB::table('ec_tracking')
        ->select(DB::raw("id,tracking_code,customer_code,status, false as 'change'"))
        ->where(function ($query) use ($list_tracking_id,$customer_code)
        {
            $query->where('customer_code','=', $customer_code);
            $query->where('status','=', 0);
            for ($i=0; $i < count($list_tracking_id) - 1; $i++) { 
                $query->orWhere('id','=', $list_tracking_id[$i]);
            }
        })
        ->get()->toArray();
        return $result;
    }
    // get detail customer detail
    public function getDetailCustomer($id){
        $customer_detail = DB::table('ec_customers')->where('id', $id)->get()->toArray();
        return $customer_detail[0];
    }

    public function getListConsolidatedById($id)
    {
        $tracking = DB::table('ec_tracking_consolidated')
        ->select('ec_tracking_consolidated.*', 'ec_customers.id AS customer_id')
        ->leftJoin("ec_customers","ec_tracking_consolidated.customer_code","=","ec_customers.customer_code")
        ->where('ec_tracking_consolidated.id','=',$id)
        ->where('ec_tracking_consolidated.status','=',1)
        ->get()->toArray();
        return $tracking;
    }
}