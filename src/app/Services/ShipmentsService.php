<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Shipments;
use App\Repositories\ShipmentsRepository;
use Faker\Factory;
use Request;
use DB;
class ShipmentsService
{
    /**
     * @var ShipmentsRepository
     */
    private $recipientRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * UserService constructor.
     * @param $userRepository
     */
    public function __construct(ShipmentsRepository $recipientRepository)
    {
        $this->ShipmentsRepository = $recipientRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $shipment = Shipments::create($input);
        $shipment->save();
        return $shipment;
    }
    public function getCustomerList()
    {
        $result = array();
        $customers = DB::table('ec_customers')
                    ->where('status', 1)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['customer_code'].' - '.$customer['firstname'].' '.$customer['lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getCustomerByTypeList($id)
    {
        $result = array();
        if($id == -1){
            $customers = DB::table('ec_customers')
                        ->where('status', 1)
                        ->orderBy('id', 'DESC')
                        ->get()
                        ->toArray();
        } else {
            $customers = DB::table('ec_customers')
                        ->where('status', 1)
                        ->where('type_customer', $id)
                        ->orderBy('id', 'DESC')
                        ->get()
                        ->toArray();
        }
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['customer_code'].' - '.$customer['firstname'].' '.$customer['lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getCountryList()
    {
        $result = DB::table('ec_country')
                    ->where('status', 1)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $result   =  json_decode(json_encode($result), True);
        return $result;
    }
    public function getCityList($id)
    {
        $result = DB::table('ec_city')
                    ->where('status', 1)
                    ->where('country_id', $id)
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
        $result   =  json_decode(json_encode($result), True);
        return $result;
    }

    // function shipments
    public function getSenderList()
    {
        $result = array();
        $customers = DB::table('ec_sender')
                    ->select('ec_customers.firstname as customer_firstname', 'ec_customers.lastname as customer_lastname', 'ec_customers.customer_code'
                                ,'ec_sender.*'
                                , 'ec_country.country_name', 'ec_city.city_name')
                    ->leftJoin("ec_customers","ec_sender.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_country","ec_sender.contact_country","=","ec_country.id")
                    ->leftJoin("ec_city","ec_sender.contact_city","=","ec_city.id")
                    ->where('ec_sender.status', 1)
                    ->orderBy('ec_sender.id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['customer_code'].' - '.$customer['customer_firstname'].' '.$customer['customer_lastname'];
            $result[$key]['id']   = $customer['customer_id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getSenderCustomerList($id)
    {
        $result = array();
        $customers = DB::table('ec_sender')
                    ->select('ec_customers.firstname as customer_firstname', 'ec_customers.lastname as customer_lastname', 'ec_customers.customer_code'
                                ,'ec_sender.*'
                                , 'ec_country.country_name', 'ec_city.city_name')
                    ->leftJoin("ec_customers","ec_sender.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_country","ec_sender.contact_country","=","ec_country.id")
                    ->leftJoin("ec_city","ec_sender.contact_city","=","ec_city.id")
                    ->where('ec_sender.status', 1)
                    ->where('ec_sender.customer_id', $id)
                    ->orderBy('ec_sender.id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['contact_code'].' - '.$customer['contact_firstname'].' '.$customer['contact_lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['contact_code'];
        }
        return $result;
    }
    public function getRecipientList($id)
    {
        $result = array();
        $customers = DB::table('ec_recipient')
                    ->select('ec_customers.firstname as customer_firstname', 'ec_customers.lastname as customer_lastname', 'ec_customers.customer_code'
                                ,'ec_recipient.*'
                                , 'ec_country.country_name', 'ec_city.city_name','ec_district.district_name')
                    ->leftJoin("ec_customers","ec_recipient.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_country","ec_recipient.contact_country","=","ec_country.id")
                    ->leftJoin("ec_city","ec_recipient.contact_city","=","ec_city.id")
                    ->leftJoin("ec_district","ec_recipient.contact_district","=","ec_district.id")
                    ->where('ec_recipient.status', 1)
                    ->where('ec_recipient.customer_id', $id)
                    ->orderBy('ec_recipient.id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['contact_code'].' - '.$customer['contact_firstname'].' '.$customer['contact_lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getRecipientBySenderList($id)
    {
        $result = array();
        $customers = DB::table('ec_recipient')
                    ->select('ec_customers.firstname as customer_firstname', 'ec_customers.lastname as customer_lastname', 'ec_customers.customer_code'
                                ,'ec_recipient.*'
                                , 'ec_country.country_name','ec_city.city_name')
                    ->leftJoin("ec_customers","ec_recipient.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_country","ec_recipient.contact_country","=","ec_country.id")
                    ->leftJoin("ec_city","ec_recipient.contact_city","=","ec_city.id")
                    ->where('ec_recipient.status', 1)
                    ->where('ec_recipient.sender_id', $id)
                    ->orderBy('ec_recipient.id', 'DESC')
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        foreach ($customers as $key => $customer) {
            $result[$key]['name'] = $customer['contact_code'].' - '.$customer['contact_firstname'].' '.$customer['contact_lastname'];
            $result[$key]['id']   = $customer['id'];
            $result[$key]['dataCode']   = $customer['customer_code'];
        }
        return $result;
    }
    public function getSenderDetail($id)
    {
        $result = array();
        $customers = DB::table('ec_sender')
                    ->select('ec_customers.firstname as customer_firstname', 'ec_customers.lastname as customer_lastname', 'ec_customers.customer_code'
                                ,'ec_sender.*'
                                , 'ec_country.country_name', 'ec_city.city_name')
                    ->leftJoin("ec_customers","ec_sender.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_country","ec_sender.contact_country","=","ec_country.id")
                    ->leftJoin("ec_city","ec_sender.contact_city","=","ec_city.id")
                    ->where('ec_sender.status', 1)
                    ->where('ec_sender.id', $id)
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        //var_dump($customers);die;
        return $customers[0];
    }
    public function getRecipientDetail($id)
    {
        $result = array();
        $customers = DB::table('ec_recipient')
                    ->select('ec_customers.firstname as customer_firstname', 'ec_customers.lastname as customer_lastname', 'ec_customers.customer_code'
                                ,'ec_recipient.*'
                                , 'ec_country.country_name', 'ec_city.city_name', 'ec_city.zone_id as city_zone_id', 'ec_district.district_name', 'ec_district.zone_id as district_zonde_id')
                    ->leftJoin("ec_customers","ec_recipient.customer_id","=","ec_customers.id")
                    ->leftJoin("ec_country","ec_recipient.contact_country","=","ec_country.id")
                    ->leftJoin("ec_city","ec_recipient.contact_city","=","ec_city.id")
                    ->leftJoin("ec_district","ec_recipient.contact_district","=","ec_district.id")
                    ->where('ec_recipient.status', 1)
                    ->where('ec_recipient.id', $id)
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        //var_dump($customers);die;
        return isset($customers[0]) ? $customers[0] : '';
    }
    public function getServicePrice($zoneId, $type, $customer_id)
    {
        if($zoneId == 'null'){
            $zoneId = 1;
        }
        $result = array();
        $customer_detail = $this->getCustomerDetail($customer_id);
        if($customer_detail['type_customer'] == 0) {
            $price = DB::table('ec_service_type_zone')
                        ->select('ec_service_type_zone.*')
                        ->where('ec_service_type_zone.service_type_item', 1)
                    ->where('ec_service_type_zone.zone_to','LIKE', '%'.$zoneId.'%')
                    //    ->where('ec_service_type_zone.zone_to','LIKE', "%{$zoneId}%")
                        ->get()
                        ->toArray();
        } else { 
            $price = DB::table('ec_service_type_zone')
                        ->select('ec_service_type_zone.*')
                        //->where('ec_service_type_zone.service_type_item', $type)
                        ->where('ec_service_type_zone.service_type_item', 2)
                    ->where('ec_service_type_zone.zone_to','LIKE', '%'.$zoneId.'%')
                    //    ->where('ec_service_type_zone.zone_to','LIKE', "%{$zoneId}%")
                        ->get()
                        ->toArray();
        }
        if($price[0]) {
            $price[0]->customer_discount_rate = $customer_detail['discount_rate'];
            if($price){ 
                $result   =  json_decode(json_encode($price[0]), True);
            }
        }
        return $result;
    }
    public function getListCommo(){
        $result = array();
        $list = DB::table('ec_commodities')
                    ->select('ec_commodities.*', 'ec_commodities_brand.commodity_brand_name', 'ec_commodities_cate.commodity_cate_name','ec_commodities_charges.unit')
                    ->leftJoin("ec_commodities_brand","ec_commodities.commodity_brand","=","ec_commodities_brand.id")
                    ->leftJoin("ec_commodities_cate","ec_commodities.commodity_cate","=","ec_commodities_cate.id")
                    ->leftJoin("ec_commodities_charges","ec_commodities.commodity_charges","=","ec_commodities_charges.id")
                    ->where('ec_commodities.status', 1)
                    ->get()
                    ->toArray();
        $list   =  json_decode(json_encode($list), True);
        foreach ($list as $key => $customer) {
            $result[$key]['name'] = $customer['commodity_name'].' - '.$customer['commodity_brand_name'].' - '.$customer['commodity_cate_name'];
            $result[$key]['id']   = $customer['id'];
        }
        return $result;
    }
    public function getListCommoCate($id){
        $result = array();
        $list = DB::table('ec_commodities')
                    ->select('ec_commodities.*', 'ec_commodities_brand.commodity_brand_name', 'ec_commodities_cate.commodity_cate_name','ec_commodities_charges.unit')
                    ->leftJoin("ec_commodities_brand","ec_commodities.commodity_brand","=","ec_commodities_brand.id")
                    ->leftJoin("ec_commodities_cate","ec_commodities.commodity_cate","=","ec_commodities_cate.id")
                    ->leftJoin("ec_commodities_charges","ec_commodities.commodity_charges","=","ec_commodities_charges.id")
                    ->where('ec_commodities.status', 1)
                    ->where('ec_commodities.commodity_cate', $id)
                    ->get()
                    ->toArray();
        $list   =  json_decode(json_encode($list), True);
        foreach ($list as $key => $customer) {
            $result[$key]['name'] = $customer['commodity_cate_name'].' - '.$customer['commodity_brand_name'].' - '.$customer['commodity_name'].' ('.$customer['unit'].')';
            $result[$key]['id']   = $customer['id'];
        }
        return $result;
    }
    public function getCommoDetail($id)
    {
        $result = array();
        $result = DB::table('ec_commodities')
                    ->select('ec_commodities.*')
                    ->where('ec_commodities.id', $id)
                    ->get()
                    ->toArray();
        return $result[0];
    }
    public function getCommoChargerDetail($id)
    {
        $result = array();
        $result = DB::table('ec_commodities')
                    ->select('ec_commodities_charges.*')
                    ->leftJoin("ec_commodities_charges","ec_commodities.commodity_charges","=","ec_commodities_charges.id")
                    ->where('ec_commodities.id', $id)
                    ->get()
                    ->toArray();
        return $result[0];
    }

    public function saveShipmentFee($id, $shipmentFee){
        // deleted by shiment
        DB::table('ec_shipments_fee')->where('shipment_id', $id)->delete();
        // insert new
        $result = DB::table('ec_shipments_fee')->insert(
            [
            'shipment_id'           => $id,
            'packagePrice'          => $shipmentFee['packagePrice'],
            'shippingRate'          => $shipmentFee['shippingRate'],
            'packageSurchargePrice' => $shipmentFee['packageSurchargePrice'],
            'insurancePrice'        => $shipmentFee['insurancePrice'],
            'upsFee'                => $shipmentFee['upsFee'],
            'customer_discount_rate'                => $shipmentFee['customer_discount_rate'],
            'totalRate'             => round($shipmentFee['totalRate'], 2)
            ]
            );
        return $result;
    }
    public function savePackages($id, $packages) {
        // deleted by shiment
        DB::table('ec_shipments_packages')->where('shipment_id', $id)->delete();
        // insert new
        $result = array();
        foreach ($packages as $key => $package) {
            DB::table('ec_shipments_packages')->insert(
                [
                'shipment_id'           => $id,
                'commodityName'         => $package['commodityName'],
                'package_number'        => ($key+1),
                'package_weight'        => $package['w'],
                'package_d'             => $package['dD'],
                'package_w'             => $package['dW'],
                'package_h'             => $package['dH']
                ]
                );
        }
        return true;
    }
    public function saveCommos($id, $commos) {
        // deleted by shiment
        DB::table('ec_shipments_packages_commo')->where('shipment_id', $id)->delete();
        // insert new
        $result = array();
        foreach ($commos as $key => $commo) {
            DB::table('ec_shipments_packages_commo')->insert(
                [
                'shipment_id'           => $id,
                'commo_name'            => $commo['commo_name'],
                'package_id'            => $commo['package_id'],
                'commo_amount'          => $commo['commo_amount'],
                'commo_total_value'     => '0',
                'surchage'              => '0'
                ]
            );
        }
        return true;    
    }
    public function saveCommos_BK($id, $commos) {
        // deleted by shiment
        DB::table('ec_shipments_packages_commo')->where('shipment_id', $id)->delete();
        // insert new
        $result = array();
        foreach ($commos as $key => $commo) {
            DB::table('ec_shipments_packages_commo')->insert(
                [
                'shipment_id'           => $id,
                'commo_id'              => $commo['id'],
                'commo_name'            => $commo['commo_name'],
                'package_id'            => $commo['package_id'],
                'commo_quanlity'        => $commo['quanlity'],
                'commo_total_value'     => $commo['total_value'],
                'commo_weight'          => $commo['weight'],
                'surchage'              => $commo['surchage']
                ]
            );
        }
        return true;    
    }
    public function getCustomerDetail($id)
    {
        $result = array();
        $customers = DB::table('ec_customers')
                    ->select('ec_customers.*')
                    ->where('ec_customers.status', 1)
                    ->where('ec_customers.id', $id)
                    ->get()
                    ->toArray();
        $customers   =  json_decode(json_encode($customers), True);
        //var_dump($customers);die;
        return isset($customers[0]) ? $customers[0] : '';
    }
    public function getShipmentDetail($id)
    {
        $result = array();
        $result = DB::table('ec_shipments')
                    ->select('ec_shipments.*'
                        ,'ec_st.service_type_name'
                        ,'ec_service_type.service_type_item'
                    )
                    ->leftJoin("ec_st","ec_st.id","=","ec_shipments.info_service_type")
                    ->leftJoin("ec_service_type","ec_service_type.id","=","ec_shipments.info_service_item")
                    ->where('ec_shipments.id', $id)
                    ->get()
                    ->toArray();
        return $result[0];
    }
    public function getShipmentPackages($id){
        $result = array();
        $list = DB::table('ec_shipments_packages')
                    ->select('ec_shipments_packages.*')
                    ->where('ec_shipments_packages.shipment_id', $id)
                    ->where('ec_shipments_packages.status', 1)
                    ->get()
                    ->toArray();
        $list   =  json_decode(json_encode($list), True);
        foreach ($list as $key => $package) {
            $result[$key]['w']      =    $package['package_weight'];
            $result[$key]['dD']     =   $package['package_d'];
            $result[$key]['dW']     =   $package['package_w'];
            $result[$key]['dH']     =   $package['package_h'];
        }
        return $result;
    }
    public function getShipmentCommos($id){
        $result = array();
        $list = DB::table('ec_shipments_packages_commo')
                    ->select('ec_shipments_packages_commo.*','ec_commodities.commodity_name')
                    ->leftJoin("ec_commodities","ec_shipments_packages_commo.commo_id","=","ec_commodities.id")
                    ->where('ec_shipments_packages_commo.shipment_id', $id)
                    ->where('ec_shipments_packages_commo.status', 1)
                    ->get()
                    ->toArray();
        $list   =  json_decode(json_encode($list), True);
        foreach ($list as $key => $package) {
            $result[$key]['id']                   =   $package['commo_id'];
            $result[$key]['name']                 =   $package['commodity_name'];
            $result[$key]['package_id']           =   $package['package_id'];
            $result[$key]['quanlity']             =   $package['commo_quanlity'];
            $result[$key]['total_value']          =   $package['commo_total_value'];
            $result[$key]['surchage']             =   $package['surchage'];
            $result[$key]['weight']               =   $package['commo_weight'];
        }
        return $result;
    }
    public function getServiceTypes(){
        $result = array();
        $list = DB::table('ec_st')
                    ->where('ec_st.status', 1)
                    ->get()
                    ->toArray();
        $lists   =  json_decode(json_encode($list), True);
        foreach ($lists as $key => $list) {
            $result[$key]['name']      =    $list['service_type_name'];
            $result[$key]['id']     =   $list['id'];
        }
        return $result;
    }
    public function getServiceTypeItems($id){
        $result = array();
        $list = DB::table('ec_service_type')
                    ->select('ec_service_type.*')
                    ->where('ec_service_type.status', 1)
                    ->where('ec_service_type.service_type_id', $id)
                    ->get()
                    ->toArray();
        $lists   =  json_decode(json_encode($list), True);
        foreach ($lists as $key => $list) {
            $result[$key]['name']      =    $list['service_type_item'];
            $result[$key]['id']     =   $list['id'];
        }
        return $result;
    }
    public function getShipmentRates($id){
        $result = array();
        $list = DB::table('ec_shipments_fee')
                    ->select('ec_shipments_fee.*')
                    ->where('ec_shipments_fee.shipment_id', $id)
                    ->where('ec_shipments_fee.status', 1)
                    ->get()
                    ->toArray();
        $list   =  json_decode(json_encode($list), True);
        foreach ($list as $key => $package) {
            $result[$key]['old_packagePrice']                   =    $package['packagePrice'];
            $result[$key]['old_shippingRate']                   =   $package['shippingRate'];
            $result[$key]['old_otherCharge']                    =   $package['otherCharge'];
            $result[$key]['old_packageSurchargePrice']          =   $package['packageSurchargePrice'];
            $result[$key]['old_insurancePrice']                 =   $package['insurancePrice'];
            $result[$key]['old_upsFee']                         =   $package['upsFee'];
            $result[$key]['old_customer_discount_rate']         =   $package['customer_discount_rate'];
            $result[$key]['old_totalRate']                      =   $package['totalRate'];
        }
        return $result;
    }
    public function getTotalShipmentByCustomer($customer_id)
    {
        return DB::table('ec_shipments')->where([
            ['customer_id', '=', $customer_id]
        ])->count();
    }
}