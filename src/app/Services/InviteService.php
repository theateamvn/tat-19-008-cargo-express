<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 29/11/2016
 * Time: 11:21 PM
 */

namespace App\Services;


use Activity;
use App\Criteria\ByUserStoreCriteria;
use App\DTOs\CustomerInformation;
use App\Models\Invite;
use App\Models\InviteLogs;
use App\Models\StorePackageSms;
use App\Notifications\SendReviewInvitation;
use App\Repositories\InviteRepository;
use App\Models\ReviewProvider;
use Carbon\Carbon;
use DB;
use Log;
use Notification;
use Stores;

class InviteService
{
    private $inviteRepository;

    public function __construct(InviteRepository $inviteRepository)
    {
        $this->inviteRepository = $inviteRepository;
    }

    /**
     * @param $storeId
     * @return int
     */
    public function getInviteClickedCount($storeId)
    {
        return DB::table('invites')->where([
            ['store_id', '=', $storeId],
            ['followed_link', '=', true]
        ])->count();
    }

    /**
     * @return int
     */
    public function getInviteClickedCountByCurrentUser()
    {
        $store = Stores::getCurrentStore();
        return $this->getInviteClickedCount($store->id);
    }

    /**
     * @param $storeId
     * @return double
     */
    public function getInviteClickedThroughRate($storeId)
    {
        $inviteClicked = $this->getInviteClickedCount($storeId);
        $totalInvite = DB::table('invites')->where('store_id', $storeId)->count();
        if ($totalInvite != 0) {
            return round($inviteClicked * 100.0 / $totalInvite, 1);
        }
        return 0;
    }

    /**
     * @return double
     */
    public function getInviteClickedThroughRateByCurrentUser()
    {
        $store = Stores::getCurrentStore();
        return $this->getInviteClickedThroughRate($store->id);
    }

    /**
     * @param $storeId
     * @param $startDate
     * @param string $endDate
     * @return int
     */
    public function getCustomerCreatedCountByTimeRange($storeId, $startDate, $endDate = 'now')
    {
        return Invite::where('store_id', $storeId)->whereBetween('created_at', [new Carbon($startDate), new Carbon($endDate)])->count();
    }

    public function getCustomerLogCreatedCountByTimeRange($storeId, $startDate, $endDate = 'now')
    {
        return InviteLogs::where('store_id', $storeId)->whereBetween('created_at', [new Carbon($startDate), new Carbon($endDate)])->count();
    }

    /**
     * @param $storeId
     * @return int
     */
    public function getCustomerCreatedCountInThisMonth($storeId)
    {
        return $this->getCustomerCreatedCountByTimeRange($storeId, 'first day of this month');
    }

    /**
     * @param $storeId
     * @return int
     */
    public function getCustomerCreatedCountInLastMonth($storeId)
    {
        return $this->getCustomerCreatedCountByTimeRange($storeId, 'first day of last month', 'last day of last month');
    }
    public function getCustomerLogCreatedCountInLastMonth($storeId)
    {
        return $this->getCustomerLogCreatedCountByTimeRange($storeId, 'first day of last month', 'last day of last month');
    }

    public function importInvites(array $invites)
    {
        try {
            foreach ($invites as $invite) {
                $invite['store_id'] = Stores::getCurrentStore()->id;
                $this->inviteRepository->create($invite);
            }
            return true;
        } catch (\Exception $ex) {
            Log::error($ex);
            return false;
        }
    }

    public function sendListInvites(array $inviteIds, array $via = ['sms', 'mms', 'email'])
    {
        $invites = $this->inviteRepository->findWhereIn('id', $inviteIds);
        Notification::send($invites, new SendReviewInvitation($via));
        Activity::log('Send invites');
    }

    public function sendAll(array $via = ['sms', 'mms', 'email'])
    {
        $this->inviteRepository->pushCriteria(new ByUserStoreCriteria());
        $invites = $this->inviteRepository->all();
        Notification::send($invites, new SendReviewInvitation($via));
        Activity::log('Send invites');
    }

    private function countSentByField($field, $storeId, $startDate, $endDate)
    {
        return Invite::whereNotNull($field)
            ->whereBetween($field, [new Carbon($startDate), new Carbon($endDate)])
            ->where('store_id', $storeId)
            ->count();
    }

    private function countInviteSentByField($field, $storeId, $startDate, $endDate)
    {
        //return InviteLogs::whereBetween('updated_at', [new Carbon($startDate), new Carbon($endDate)])
        return InviteLogs::where('store_id', $storeId)
            ->where('type_sent', $field)
            ->count();
    }

    public function countSmsSent($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        return $this->countSentByField('phone_sent', $storeId, $startDate, $endDate);
    }

    public function countEmailSent($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        return $this->countSentByField('email_sent', $storeId, $startDate, $endDate);
    }

    public function countMmsSent($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        return $this->countSentByField('mms_sent', $storeId, $startDate, $endDate);
    }

    public function countSmsInviteSent($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        return $this->countInviteSentByField('phone_sent', $storeId, $startDate, $endDate);
    }

    public function countEmailInviteSent($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        return $this->countInviteSentByField('email_sent', $storeId, $startDate, $endDate);
    }

    public function countMmsInviteSent($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        return $this->countInviteSentByField('mms_sent', $storeId, $startDate, $endDate);
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return CustomerInformation
     */
    public function getCustomerInformation($startDate = "1900-01-01", $endDate = 'now')
    {
        $storeId = Stores::getCurrentStore()->id;
        $customerInformation = new CustomerInformation();
        $customerInformation->all = Invite::where('store_id', $storeId)->count();
        $customerInformation->smsSentCount = $this->countSmsSent($storeId, $startDate, $endDate);
        $customerInformation->emailSentCount = $this->countEmailSent($storeId, $startDate, $endDate);
        $customerInformation->mmsSentCount = $this->countMmsSent($storeId, $startDate, $endDate);
        $customerInformation->newCreatedCount = $this->getCustomerCreatedCountByTimeRange($storeId, $startDate, $endDate);
        return $customerInformation;
    }

    public function getCustomerInviteInformation($startDate = "1900-01-01", $endDate = 'now')
    {
        $storeId = Stores::getCurrentStore()->id;
        $totalPackage = $this->getTotalPackages($storeId);
        $customerInformation = new CustomerInformation();
        $customerInformation->all = Invite::where('store_id', $storeId)->count();
        $customerInformation->totalSentCount = InviteLogs::where([['store_id', $storeId], ['type_sent', 'phone_sent']])->count();
        $customerInformation->allSentAllow  = $totalPackage[0]->count_phone;
        $customerInformation->smsSentCount = $this->countSmsInviteSent($storeId, $startDate, $endDate);
        $customerInformation->emailSentCount = $this->countEmailInviteSent($storeId, $startDate, $endDate);
        $customerInformation->mmsSentCount = $this->countMmsInviteSent($storeId, $startDate, $endDate);
        return $customerInformation;
    }

    public function statisticNewCustomerByDateRange($storeId, $startDate, $endDate = 'now')
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        return DB::table('invites')
            ->selectRaw('count(*) as count_new,DATE(`created_at`) as date')
            ->where('store_id', $storeId)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->groupBy(DB::raw('DATE(`created_at`)'))
            ->get()
            ->pluck('count_new', 'date')
            ->toArray();
    }

    public function statisticSentEmailByDateRange($storeId, $startDate, $endDate = 'now')
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        return DB::table('invites')
            ->selectRaw('count(*) as count_email,DATE(`email_sent`) as date')
            ->where('store_id', $storeId)
            ->whereNotNull('email_sent')
            ->whereBetween('email_sent', [$startDate, $endDate])
            ->groupBy(DB::raw('DATE(`email_sent`)'))
            ->get()
            ->pluck('count_email', 'date')
            ->toArray();
    }

    public function statisticSentSMSByDateRange($storeId, $startDate, $endDate = 'now')
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        return DB::table('invites')
            ->selectRaw('count(*) as count_phone,DATE(`phone_sent`) as date')
            ->where('store_id', $storeId)
            ->whereNotNull('phone_sent')
            ->whereBetween('phone_sent', [$startDate, $endDate])
            ->groupBy(DB::raw('DATE(`phone_sent`)'))
            ->get()
            ->pluck('count_phone', 'date')
            ->toArray();
    }

    public function statisticSentMMSByDateRange($storeId, $startDate, $endDate = 'now')
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        return DB::table('invites')
            ->selectRaw('count(*) as count_mms,DATE(`mms_sent`) as date')
            ->where('store_id', $storeId)
            ->whereNotNull('mms_sent')
            ->whereBetween('mms_sent', [$startDate, $endDate])
            ->groupBy(DB::raw('DATE(`mms_sent`)'))
            ->get()
            ->pluck('count_mms', 'date')
            ->toArray();
    }

    public function statisticInviteByDateRange($storeId, $startDate, $endDate = 'now')
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $newCustomer = $this->statisticNewCustomerByDateRange($storeId, $startDate, $endDate);
        $sentEmail = $this->statisticSentEmailByDateRange($storeId, $startDate, $endDate);
        $sentSms = $this->statisticSentSMSByDateRange($storeId, $startDate, $endDate);
        $sentMms = $this->statisticSentMMSByDateRange($storeId, $startDate, $endDate);

        $date = $startDate;
        $result = [];
        while ($date != $endDate) {
            if(isset($result[$date])) $result[$date] = [];
            if(isset($newCustomer[$date])) $result[$date]['new_count'] = $newCustomer[$date];
            if(isset($sentEmail[$date])) $result[$date]['count_email'] = $sentEmail[$date];
            if(isset($sentSms[$date])) $result[$date]['count_phone'] = $sentSms[$date];
            if(isset($sentMms[$date])) $result[$date]['count_mms'] = $sentMms[$date];
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $result = collect($result);
        $result = $result->mapWithKeys(function($item,$key){
            if(isset($item['count_new'])) $item['count_new'] = 0;
            if(isset($item['count_email'])) $item['count_email'] = 0;
            if(isset($item['count_phone'])) $item['count_phone'] = 0;
            if(isset($item['count_mms'])) $item['count_mms'] = 0;
            return [array_merge($item,['date'=>$key])];
        });
        return $result->toArray();
    }
    public function checkExistInvite($storedId, $phone, $email) {
        $totalPhone = 0;
        $totalEmail = 0;
        $total =0;
        if($phone){
            $totalPhone = DB::table('invites')->where([
                                                ['store_id', '=', $storedId],
                                                ['phone', '=', $phone]
                                                ])
                                                ->count();
        }
        if($email) {
            $totalEmail = DB::table('invites')->where([
                                                ['store_id', '=', $storedId],
                                                ['email', '=', $email]
                                                ])
                                                ->count();
        }
        if($phone && $email ) {
            $total = DB::table('invites')->where([
                                                ['store_id', '=', $storedId],
                                                ['phone', '=', $phone] ,
                                                ['email', '=', $email]
                                                ])
                                                ->count();
        }
        if ($totalPhone != 0) {
            return 1;
        } else if($totalEmail != 0) {
            return 2;
        } else if($total != 0) {
            return 3;
        } else {
            return 0;
        }
    }
    public function getDetailByStoreIdAndPhone($storedId, $phone) {
        // check phone or email exist
        return DB::table('invites')->where([
                                            ['store_id', '=', $storedId],
                                            ['phone', '=', $phone]
                                            ])
                                            ->get()
                                            ->toArray();
    }
    public function getDetailByStoreIdAndEmail($storedId, $email) {
        // check phone or email exist
        return DB::table('invites')->where([
                                            ['store_id', '=', $storedId],
                                            ['email', '=', $email]
                                            ])
                                            ->get()
                                            ->toArray();
    }
    public function getReviewLinkByCustomer($id)
    {
        $store = Stores::getCurrentStore();
        $total = \DB::table('review_links')->where('storeId', $store->id)->where('inviteId', $id)->count();
        $reviewByGoogle = $this->getReviewLinkCustomerCountByProvider($id, ReviewProvider::PROVIDER_GOOGLE);
        $reviewByFacebook = $this->getReviewLinkCustomerCountByProvider($id, ReviewProvider::PROVIDER_FACEBOOK);
        $reviewByYelp = $this->getReviewLinkCustomerCountByProvider($id, ReviewProvider::PROVIDER_YELP);
        $reviewByTripadvisor = $this->getReviewLinkCustomerCountByProvider($id, ReviewProvider::PROVIDER_TRIPADVISOR);
        $reviewByAgoda = $this->getReviewLinkCustomerCountByProvider($id, ReviewProvider::PROVIDER_AGODA);
        $reviewByBooking = $this->getReviewLinkCustomerCountByProvider($id, ReviewProvider::PROVIDER_BOOKING);
        $reviewByAirbnb = $this->getReviewLinkCustomerCountByProvider($id, ReviewProvider::PROVIDER_AIRBNB);

        return [
            'id' => $id,
            'total' => $total,
            'google' => $reviewByGoogle,
            'facebook' => $reviewByFacebook,
            'tripadvisor' => $reviewByTripadvisor,
            'yepl' => $reviewByYelp,
            'agoda' => $reviewByAgoda,
            'booking' => $reviewByBooking,
            'airbnb' => $reviewByAirbnb
        ];
    }
    public function getReviewLinkCustomerCountByProvider($id, $providerName)
    {
        $store = Stores::getCurrentStore();
        return DB::table('review_links')->where([
            ['providerName', '=', $providerName],
            ['storeId', '=', $store->id],
            ['inviteId', '=', $id]
        ])->count();
    }
    public function getTotalSentTypeCount($storeId, $via)
    {
        return DB::table('invite_logs')->where([
            ['store_id', '=', $storeId],
            ['type_sent', '=', $via]
        ])->count();
    }
    public function getTotalPackages($storeId)
    {
        return DB::table('store_package_sms')
        ->selectRaw('SUM(total_allow_sent) as count_phone')
        ->where([
            ['store_id', '=', $storeId]
        ])->get()->toArray();
        // orderBy('id', 'DESC')
    }
    public function getStoreIdByCustomerId($customerId) {
        return DB::table('invites')
        ->selectRaw('store_id')
        ->where([
            ['id', '=', $customerId]
        ])->get()->toArray();
    }

    /*
    * invites number
    */
    public function getInvitesCreatedCountInLastMonth($storeId)
    {
        return $this->getInvitesCreatedCountByTimeRange($storeId, 'first day of last month', 'last day of last month');
    }
    public function getInvitesCreatedCountInThisMonth($storeId)
    {
        return $this->getInvitesCreatedCountByTimeRange($storeId, 'first day of this month');
    }
    public function getInvitesCreatedCountByTimeRange($storeId, $startDate, $endDate = 'now')
    {
        return InviteLogs::where('store_id', $storeId)->whereBetween('updated_at', [new Carbon($startDate), new Carbon($endDate)])->count();
    }
}