<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 2/17/2017
 * Time: 1:14 PM
 */

namespace App\Services\Sms\Esms;

use App\Services\Sms\Esms\Exceptions\EsmsException;
use GuzzleHttp\Client as GuzzleClient;

class Client
{

    private $client;
    private $apiKey;
    private $secretKey;

    /**
     * Client constructor.
     * @param $apiKey
     * @param $secretKey
     */
    public function __construct($apiKey, $secretKey)
    {
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey;
        $this->client = new GuzzleClient(['base_uri' => 'http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get']);
    }


    public function create($to, $options)
    {
        $body = $options['body'];
        $smsType = urlencode($options['smsType']);
        $brandName = $options['brandName'];
        $response = $this->client->get("SendMultipleMessage_V4_get?Phone=$to&ApiKey={$this->apiKey}&SecretKey={$this->secretKey}&Content=$body&SmsType=$smsType&Brandname=$brandName");
        $jsonRespose = json_decode($response->getBody(), true);
        $this->handleException($jsonRespose);
    }

    private function handleException($jsonRespose)
    {
        if (!isset($jsonRespose['CodeResult']) || $jsonRespose['CodeResult'] != 100) {
            throw new EsmsException($jsonRespose['CodeResult'], $jsonRespose['ErrorMessage']);
        }
    }
}