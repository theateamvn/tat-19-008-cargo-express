<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\RequestCustomer;
use App\Repositories\RequestCustomerRepository;
use App\Notifications\SendReviewInvitation;
use Faker\Factory;
use Request;
use Notification;
use DB;
class RequestCustomerService
{
    /**
     * @var RequestCustomerRepository
     */
    private $requestCustomer;

    public function __construct(RequestCustomerRepository $requestCustomerRepository)
    {
        $this->requestCustomer = $requestCustomerRepository;
    }

    public function getListRequestCustomer()
    {
        $requestCustomer = DB::table('ec_request_customer')->where('status', '<>', -1)->get()->toArray();
        return $requestCustomer;
    }

    public function create($input)
    {
        $requestCustomer = RequestCustomer::create($input);
        $requestCustomer->save();
        $this->sendMail($requestCustomer);
        return $requestCustomer;
    }

    public function getServiceType()
    {
        $st = DB::table('ec_st')->get()->toArray();
        return $st;
    }
    
    public function sendMail($requestCustomer)
    {
        $via_email = ['email-request-customer-alert'];
        $emailDetail = [
            'id'            => $requestCustomer->id,
            'type_email'    => 'email_request_customer_alert',
            'store_id'      => 1,
            'customer_name' => $requestCustomer->customer_name,
            'sender_name'   => 'T-Express',
            'email_title'   => 'Notice of creating new request customer',
            'email'         => 'support@texpresscargo.net',
            'customer_email'=> $requestCustomer->customer_email,
            'customer_phone'=> $requestCustomer->customer_phone,
            'payment'       => $requestCustomer->payment == 1 ? 'Pay In US' : 'Pay In VN',
        ];
        //$emailDetail = [0=>$emailDetail];
        Notification::send((object)$emailDetail, new SendReviewInvitation($via_email));
    }

}