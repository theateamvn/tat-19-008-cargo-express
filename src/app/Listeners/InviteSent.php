<?php

namespace App\Listeners;

use App\Models\Invite;
use App\Notifications\Channels\MailChannel;
use App\Notifications\Channels\MmsTwilioChannel;
use App\Notifications\Channels\TwilioChannel;
use App\Notifications\SendReviewInvitation;
use Carbon\Carbon;
use Illuminate\Notifications\Events\NotificationSent;
use App\Notifications\Channels\MmsBandwidthChannel;
use App\Notifications\Channels\SmsBandwidthChannel;
use App\Notifications\Channels\SmsEsmsChannel;

class InviteSent
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationSent $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
        $this->handleSendInvitationSuccess($event);
    }

    /**
     * @param NotificationSent $event
     */
    protected function handleSendInvitationSuccess(NotificationSent $event)
    {
        if ($event->notification instanceof SendReviewInvitation
            && ($event->channel == MailChannel::class
                || $event->channel == TwilioChannel::class
                || $event->channel == MmsTwilioChannel::class
                || $event->channel == SmsBandwidthChannel::class
                || $event->channel == MmsBandwidthChannel::class
                || $event->channel == SmsEsmsChannel::class)
        ) {
            /** @var Invite $invite */
            $invite = $event->notifiable;
            if ($event->channel == MailChannel::class) {
                //$invite->email_sent = Carbon::now();
            }
            if ($event->channel == TwilioChannel::class
                || $event->channel == SmsEsmsChannel::class
                || $event->channel == SmsBandwidthChannel::class
            ) {
                $invite->phone_sent = Carbon::now();
            }
            if ($event->channel == MmsTwilioChannel::class
                || $event->channel == MmsBandwidthChannel::class
            ) {
                $invite->mms_sent = Carbon::now();
            }
            //$invite->save(); // TAT edit
        }
    }
}
