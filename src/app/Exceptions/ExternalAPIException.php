<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 11/29/2016
 * Time: 11:04 AM
 */

namespace App\Exceptions;


class ExternalAPIException extends \Exception
{
    private $provider;
    private $errorCode;

    /**
     * @param string $errorCode
     * @param string $errorMessage
     * @param string $provider
     */
    public function __construct($errorCode, $errorMessage,$provider = '')
    {
        $this->errorCode = $errorCode;
        $this->message = $errorMessage;
        $this->provider = $provider;
    }


}