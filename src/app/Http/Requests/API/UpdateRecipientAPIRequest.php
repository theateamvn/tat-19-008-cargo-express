<?php

namespace App\Http\Requests\API;

use App\Models\Recipient;
use App\Http\Requests\APIRequest;
use Auth;

class UpdateRecipientAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        return Recipient::rulesUpdate($id);
    }
}
