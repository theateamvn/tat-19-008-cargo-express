<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 11/12/2016
 * Time: 11:09 AM
 */

namespace App\Http\Requests\API;


use App\Http\Requests\APIRequest;

class SendInviteRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'list' => 'array',
            'via' => 'array'
        ];
    }
}