<?php

namespace App\Http\Requests\API;

use App\Models\User;
use App\Http\Requests\APIRequest;
use Auth;

class UpdateUserAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->role === 1 || Auth::user()->role === 2){
          $user_id = $this->has('user_id') ? $this->all()['user_id'] : $this->all()['id'];
          return User::rulesAdminUpdate($user_id);
        }
        return User::rulesUpdate($user_id);
    }
}
