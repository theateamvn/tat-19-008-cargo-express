<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateCommoditiesBrandAPIRequest;
use App\Http\Requests\API\UpdateCommoditiesBrandAPIRequest;
use App\Criteria\ListCommoditiesBrandCriteria;
use App\Repositories\CommoditiesBrandRepository;
use App\Services\CommoditiesBrandService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\CommoditiesBrands;
use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CommoditiesBrandsAPIController extends AppBaseController
{
    private $commoditiesBrandRepository;

    private $recipientService;

    public function __construct(CommoditiesBrandRepository $commoditiesBrandRepo, CommoditiesBrandService $commoditiesBrandService)
    {
        $this->commoditiesBrandRepository = $commoditiesBrandRepo;
        $this->commoditiesBrandService = $commoditiesBrandService;
    }

    public function pagination(Request $request)
    {
        $this->commoditiesBrandRepository->pushCriteria(new ListCommoditiesBrandCriteria($request));
        $this->commoditiesBrandRepository->pushCriteria(new RequestCriteria($request));
        $this->commoditiesBrandRepository->pushCriteria(new LimitOffsetCriteria($request));
        $brands = $this->commoditiesBrandRepository->paginate();
        return $this->sendResponse($brands->toArray(), 'CommoditiesBrand retrieved successfully');
    }
    public function store(CreateCommoditiesBrandAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['ip'] = $request->ip();
        $input['updated_by'] = $userId;
        $brand = $this->commoditiesBrandService->create($input);
        return $this->sendResponse($brand->toArray(), 'CommoditiesBrand saved successfully');
    }
    // function update
    public function update($id, UpdateCommoditiesBrandAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $brand = $this->commoditiesBrandRepository->findWithoutFail($id);

        if (empty($brand)) {
            return $this->sendError('customer not found');
        }

        $input['updated_by'] = $userId;
        $brand = $this->commoditiesBrandRepository->update($input, $id);

        return $this->sendResponse($brand->toArray(), 'User updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $brand = $this->commoditiesBrandRepository->findWithoutFail($id);

        if (empty($brand)) {
            return $this->sendResponse('CommoditiesBrand not found');
        }
        $brand->delete();
        return $this->sendResponse($id, 'CommoditiesBrand deleted successfully');
    }
    public function list_categories()
    {
        $list_categories = $this->commoditiesBrandService->getCategoriesList();
        return $this->sendResponse($list_categories, 'Customer list get successfully');
    }



    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

}
