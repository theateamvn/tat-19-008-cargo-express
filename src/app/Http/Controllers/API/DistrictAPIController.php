<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDistrictAPIRequest;
use App\Http\Requests\API\UpdateDistrictAPIRequest;
use App\Models\District;
use App\Repositories\DistrictRepository;
use App\Services\DistrictService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class DistrictAPIController extends AppBaseController
{
    /** @var  DistrictRepository */
    private $districtRepository;

    /**
     * @var DistrictService
     */
    private $districtService;

    public function __construct(DistrictRepository $districtRepo, DistrictService $districtService)
    {
        $this->districtRepository = $districtRepo;
        $this->districtService = $districtService;
    }

    public function store(CreateDistrictAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->districtService->create($input);
        return $this->sendResponse($result, 'District created successfully');
    }

    public function update($id, UpdateDistrictAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->districtRepository->update($input, $id);
        return $this->sendResponse($result, 'District updated successfully');
    }

    public function destroy($id)
    {
        $city = $this->districtRepository->findWithoutFail($id);

        if (empty($city)) {
            return $this->sendResponse('District not found');
        }
       $result = $this->districtRepository->delete($id);
       return $this->sendResponse($result, 'District deleted successfully');
    }

    public function getDistrictListById($id)
    {
        $result = $this->districtService->getDistrictListById($id);
        return $this->sendResponse($result, 'District retrieved successfully'); 
    }

}
