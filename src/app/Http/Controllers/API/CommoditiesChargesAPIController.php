<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateCommoditiesChargesAPIRequest;
use App\Http\Requests\API\UpdateCommoditiesChargesAPIRequest;
use App\Criteria\ListCommoditiesChargesCriteria;
use App\Models\CommoditiesCharges;
use App\Repositories\CommoditiesChargesRepository;
use App\Services\CommoditiesChargesService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CommoditiesChargesAPIController extends AppBaseController
{
    private $commoditiesChargesRepository;

    private $commoditiesChargesService;

    public function __construct(CommoditiesChargesRepository $commoditiesChargesRepo, CommoditiesChargesService $commoditiesChargesService)
    {
        $this->commoditiesChargesRepository = $commoditiesChargesRepo;
        $this->commoditiesChargesService = $commoditiesChargesService;
    }

    public function pagination(Request $request)
    {
        $this->commoditiesChargesRepository->pushCriteria(new ListCommoditiesChargesCriteria($request));
        $this->commoditiesChargesRepository->pushCriteria(new RequestCriteria($request));
        $this->commoditiesChargesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commoditiesCharges = $this->commoditiesChargesRepository->paginate();
        return $this->sendResponse($commoditiesCharges->toArray(), 'Commodities Charge retrieved successfully');
    }
    public function store(CreateCommoditiesChargesAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['ip'] = $request->ip();
        $input['updated_by'] = $userId;
        $commoditiesCharges = $this->commoditiesChargesService->create($input);
        return $this->sendResponse($commoditiesCharges->toArray(), 'Commodities Charges saved successfully');
    }
    // function update
    public function update($id, UpdateCommoditiesChargesAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $commoditiesCharges = $this->commoditiesChargesRepository->findWithoutFail($id);

        if (empty($commoditiesCharges)) {
            return $this->sendError('Commodities not found');
        }

        $input['updated_by'] = $userId;
        $commoditiesCharges = $this->commoditiesChargesRepository->update($input, $id);

        return $this->sendResponse($commoditiesCharges->toArray(), 'Commodities Charges updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $commoCharges = $this->commoditiesChargesRepository->findWithoutFail($id);

        if (empty($commoCharges)) {
            return $this->sendResponse('Commodities Charges not found');
        }
        //update shipment code
        $input_ = array( 'status' => -1);
        $this->commoditiesChargesRepository->update($input_, $id);
        return $this->sendResponse($id, 'Commodities Charges deleted successfully');
    }
    public function load_cate()
    {
        $list = $this->commoditiesChargesService->getCateList();
        return $this->sendResponse($list, 'Cate list list get successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

}
