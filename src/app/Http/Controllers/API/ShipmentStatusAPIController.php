<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateShipmentStatusAPIRequest;
use App\Http\Requests\API\UpdateShipmentStatusAPIRequest;
use App\Http\Requests\API\CreateShipmentStatusLogsAPIRequest;
use App\Http\Requests\API\UpdateShipmentStatusLogsAPIRequest;
use App\Criteria\ListShipmentsCriteria;
use App\Models\ShipmentStatus;
use App\Repositories\ShipmentStatusRepository;
use App\Repositories\ShipmentStatusLogsRepository;
use App\Services\ShipmentStatusService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class ShipmentStatusAPIController extends AppBaseController
{
    private $shipmentStatusRepository;
    private $shipmentStatusLogsRepository;

    /**
     * @var UserService
     */
    private $shipmentStatusService;

    public function __construct(ShipmentStatusRepository $shipmentsRepo, ShipmentStatusService $shipmentsService, ShipmentStatusLogsRepository $shipmentStatusLogsRepository)
    {
        $this->shipmentStatusRepository = $shipmentsRepo;
        $this->shipmentStatusService = $shipmentsService;
        $this->shipmentStatusLogsRepository = $shipmentStatusLogsRepository;
    }

    public function pagination(Request $request)
    {
        $this->shipmentStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->shipmentStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shipmnets = $this->shipmentStatusRepository->paginate();
        return $this->sendResponse($shipmnets->toArray(), 'Shipment status retrieved successfully');
    }

    public function store(CreateShipmentStatusAPIRequest $request)
    {
        $input = $request->all();
        $input['created_by']= $this->getCurrentUser()->id;
        $input['updated_by']= $this->getCurrentUser()->id;
        $result = $this->shipmentStatusService->create($input);
        return $this->sendResponse($result, 'Shipment status saved successfully');
    }

    // function update
    public function update($id, UpdateShipmentStatusAPIRequest $request)
    {
        $input = $request->all();
        $input['updated_by']= $this->getCurrentUser()->id;
        $result = $this->shipmentStatusRepository->update($input, $id);
        return $this->sendResponse($result, 'Shipment status updated successfully');
    }

    // function delete
    public function destroy($id)
    {
        $result = $this->shipmentStatusRepository->delete($id);
        return $this->sendResponse($result, 'Shipment status delete successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

    public function createShipmentStatusLog(CreateShipmentStatusLogsAPIRequest $request)
    {
        $input = $request->all();
        $input['created_by']= $this->getCurrentUser()->id;
        $input['updated_by']= $this->getCurrentUser()->id;
        $result = $this->shipmentStatusService->createShipmentStatuslog($input);
        return $this->sendResponse($result, 'Shipment status log saved successfully');
    }

    public function updateShipmentStatusLog($id, UpdateShipmentStatusLogsAPIRequest $request)
    {
        $input = $request->all();
        $input['updated_by']= $this->getCurrentUser()->id;
        $result = $this->shipmentStatusLogsRepository->update($input, $id);
        return $this->sendResponse($result, 'Shipment status log updated successfully');
    }

    public function getShipmentStatusLogById($id)
    {
        $result = $this->shipmentStatusService->getShipmentStatusLog($id);
        return $this->sendResponse($result, 'Shipment status log updated successfully'); 
    }

    public function LoadShipmentStatus()
    {
        $result = $this->shipmentStatusService->LoadShipmentStatus();
        return  $result;
    }

    public function LoadShipmentStatusById($id)
    {
        $result = $this->shipmentStatusService->LoadShipmentStatusById($id);
        return $this->sendResponse($result, 'Shipment status log updated successfully');
    }

}
