<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAwbAirAPIRequest;
use App\Http\Requests\API\UpdateAwbAirAPIRequest;
use App\Models\AwbAir;
use App\Repositories\AwbAirRepository;
use App\Services\AwbAirService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class AwbAirAPIController extends AppBaseController
{
    /** @var  AwbAirRepository */
    private $awbAirRepository;

    /**
     * @var AwbAirService
     */
    private $awbAirService;

    public function __construct(AwbAirRepository $awbAirRepo,AwbAirService $awbAirService)
    {
        $this->awbAirRepository = $awbAirRepo;
        $this->awbAirService = $awbAirService;
    }

    public function pagination(Request $request)
    {
        $this->awbAirRepository->pushCriteria(new RequestCriteria($request));
        $this->awbAirRepository->pushCriteria(new LimitOffsetCriteria($request));
        $city = $this->awbAirRepository->paginate();
        return $this->sendResponse($city->toArray(), 'AwbAir retrieved successfully');
    }

    public function index(Request $request)
    {
        $result = $this->awbAirService->getListAwbAir();
        return $this->sendResponse($result, 'AwbAir retrieved successfully');
    }

    public function store(CreateAwbAirAPIRequest $request)
    {
        $input = $request->all();
        $awb_air = $this->awbAirService->checkCode($input['air_pre_code']);
        if($awb_air > 0){
            return $this->sendError('AwbAir Code existed');
        }
        $path = $request->file('file')->storeAs('airport_logo', $input['air_pre_code'].'.png', 'public');
        $result = $this->awbAirService->create($input);
        return $this->sendResponse($result, 'AwbAir created successfully');
    }

    public function update($id, UpdateAwbAirAPIRequest $request)
    {
        $input = $request->all();
        $awbAir = $this->awbAirRepository->findWithoutFail($id);
        $result = $this->awbAirRepository->update($input, $id);
        return $this->sendResponse($result, 'AwbAir updated successfully');
    }

    //update awb air
    public function edit($id, UpdateAwbAirAPIRequest $request)
    {
        $input = $request->all();
        // var_dump($request->file('file'));die;
        $awbAir = $this->awbAirRepository->findWithoutFail($id);
        if(empty($request->file('file'))){
            \File::move(resource_path('../public_html/storage/airport_logo/'.$awbAir->air_pre_code.'.png'),resource_path('../public_html/storage/airport_logo/'.$input['air_pre_code'].'.png'));
        }else{
            \File::delete(resource_path('../public_html/storage/airport_logo/'.$awbAir->air_pre_code.'.png'));
            $request->file('file')->storeAs('airport_logo', $input['air_pre_code'].'.png', 'public');
        }
        $result = $this->awbAirRepository->update($input, $id);
        return $this->sendResponse($result, 'AwbAir updated successfully');
    }

    public function destroy($id)
    {
        $awbAir = $this->awbAirRepository->findWithoutFail($id);

        if (empty($awbAir)) {
            return $this->sendError('AwbAir not found',400);
        }
       $result = $this->awbAirRepository->delete($id);
       return $this->sendResponse($result, 'AwbAir deleted successfully');
    }

}
