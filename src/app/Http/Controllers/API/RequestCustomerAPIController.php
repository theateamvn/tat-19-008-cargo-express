<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRequestCustomerAPIRequest;
use App\Http\Requests\API\UpdateRequestCustomerAPIRequest;
use App\Http\Requests\API\UploadCustomerIDRequest;
use App\Models\RequestCustomer;
use App\Repositories\RequestCustomerRepository;
use App\Services\RequestCustomerService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\RequestCustomerCriteria;
use Response;
use Stores;
use Storage;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class RequestCustomerAPIController extends AppBaseController
{
    /** @var  RequestCustomerRepository */
    private $requestCustomerRepository;

    /** @var RequestCustomerService */
    private $requestCustomerService;

    public function __construct(RequestCustomerRepository $requestCustomerRepo,RequestCustomerService $requestCustomerService)
    {
        $this->requestCustomerRepository = $requestCustomerRepo;
        $this->requestCustomerService = $requestCustomerService;
    }

    public function pagination(Request $request)
    {
        $this->requestCustomerRepository->pushCriteria(new RequestCriteria($request));
        $this->requestCustomerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->requestCustomerRepository->pushCriteria(new RequestCustomerCriteria($request));
        $requestCustomer = $this->requestCustomerRepository->paginate();
        return $this->sendResponse($requestCustomer->toArray(), 'RequestCustomer retrieved successfully');
    }

    public function index()
    {
        $requestCustomer = $this->requestCustomerService->getListRequestCustomer();
        return $this->sendResponse($requestCustomer, 'RequestCustomer retrieved successfully');
    }

    public function show()
    {
        $st = $this->requestCustomerService->getServiceType();
        return $this->sendResponse($st, 'Service type retrieved');
    }

    public function store(CreateRequestCustomerAPIRequest $request)
    {
        $input = $request->all();
        if(count($input)) {
            $token = $input["_token"];
            $check_csrf = $this->validCsrf($token, $request);
            if($check_csrf) {
                $path = $request->file('file')->store('request_customer', 'public');
                $input['id_number_url'] = $request->file('file')->hashName();
                $result = $this->requestCustomerService->create($input);
                return $this->sendResponse($result, 'RequestCustomer created successfully');
            } else {
                return $this->sendError('Not found T!');
            }
        } else {
            return $this->sendError('Not found');
        }
    }

    public function change_status($id, UpdateRequestCustomerAPIRequest $request)
    {
        $input = $request->all();
        $requestCustomer = $this->requestCustomerRepository->findWithoutFail($id);
        if (empty($requestCustomer)) {
            return $this->sendResponse('','RequestCustomer not found');
        }
        $result = $this->requestCustomerRepository->update($input, $id);
        return $this->sendResponse($result, 'RequestCustomer updated successfully');
    }

    public function update($id, UpdateRequestCustomerAPIRequest $request)
    {
        $input = $request->all();
        $requestCustomer = $this->requestCustomerRepository->findWithoutFail($id);
        if (empty($requestCustomer)) {
            return $this->sendResponse('','RequestCustomer not found');
        }
        \File::delete(resource_path('../public_html/storage/request_customer/'.$requestCustomer->id_number_url));
        $path = $request->file('file')->store('request_customer', 'public');
        $input['id_number_url'] = $request->file('file')->hashName();
        $result = $this->requestCustomerRepository->update($input, $id);
        return $this->sendResponse($result, 'RequestCustomer updated successfully');
    }

    public function edit($id, UpdateRequestCustomerAPIRequest $request)
    {
        $input = $request->all();
        $requestCustomer = $this->requestCustomerRepository->findWithoutFail($id);
        if (empty($requestCustomer)) {
            return $this->sendResponse('','RequestCustomer not found');
        }
        if(empty($request->file('file'))){

        }else{
           \File::delete(resource_path('../public_html/storage/request_customer/'.$requestCustomer->id_number_url));
            $path = $request->file('file')->store('request_customer', 'public');
            $input['id_number_url'] = $request->file('file')->hashName(); 
        }
        $result = $this->requestCustomerRepository->update($input, $id);
        return $this->sendResponse($result, 'RequestCustomer updated successfully');
    }

    public function destroy($id)
    {
        $requestCustomer = $this->requestCustomerRepository->findWithoutFail($id);

        if (empty($requestCustomer)) {
            return $this->sendResponse('','RequestCustomer not found');
        }
        $result = $this->requestCustomerRepository->delete($id);
        return $this->sendResponse($result, 'RequestCustomer deleted successfully');
    }

    public function getServiceType()
    {
        $st = $this->requestCustomerService->getServiceType();
        return $this->sendResponse($st, 'Service type retrieved');
    }

    public function test_csrf(CreateRequestCustomerAPIRequest $request)
    {
        $input = $request->all();
        if(count($input)) {
            $token = $input["_token"];
            $check_csrf = $this->validCsrf($token, $request);
            if($check_csrf) {
                echo 'đúng';
            } else {
                echo 'Sai';
            }
            return $this->sendResponse("", 'TestCsrf created successfully');
        } else {
            return $this->sendResponse('','Not found');
        }
    }

    protected function validCsrf($token, $request)
    {
        $isCsrf = false;
        $isXsrf = false;
        if($csrf = $token) {
            $isCsrf = hash_equals(
                $token, (string) $request->header('X-CSRF-TOKEN')
            );
            if($request->hasHeader('X-XSRF-TOKEN')) {
                $isXsrf = $this->encrypter->decrypt($request->header('X-XSRF-TOKEN')) === $csrf;
            }
        }
        return $isCsrf || $isXsrf;
    }

}
