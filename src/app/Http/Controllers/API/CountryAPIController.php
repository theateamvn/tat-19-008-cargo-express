<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCountryAPIRequest;
use App\Http\Requests\API\UpdateCountryAPIRequest;
use App\Models\Country;
use App\Repositories\CountryRepository;
use App\Services\CountryService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CountryAPIController extends AppBaseController
{
    /** @var  CountryRepository */
    private $countryRepository;

    /**
     * @var CountryService
     */
    private $countryService;

    public function __construct(CountryRepository $countryRepo, CountryService $countryService)
    {
        $this->countryRepository = $countryRepo;
        $this->countryService = $countryService;
    }

    public function index()
    {
        $country = $this->countryService->getListCountry();
        return $this->sendResponse($country, 'Country retrieved successfully');
    }

    public function store(CreateCityAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->countryService->create($input);
        return $this->sendResponse($result, 'Country created successfully');
    }

    public function update($id, UpdateCityAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->countryRepository->update($input, $id);
        return $this->sendResponse($result, 'Country updated successfully');
    }

    public function destroy($id)
    {
        $country = $this->countryRepository->findWithoutFail($id);

        if (empty($country)) {
            return $this->sendResponse('Country not found');
        }
       $result = $this->countryRepository->delete($id);
       return $this->sendResponse($result, 'Country deleted successfully');
    }

}
