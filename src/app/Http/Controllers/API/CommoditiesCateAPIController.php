<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateCommoditiesCateAPIRequest;
use App\Http\Requests\API\UpdateCommoditiesCateAPIRequest;
use App\Criteria\ListCommoditiesCateCriteria;
use App\Models\CommoditiesCate;
use App\Repositories\CommoditiesCateRepository;
use App\Services\CommoditiesCateService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CommoditiesCateAPIController extends AppBaseController
{
    private $commoditiesCateRepository;

    private $commoditiesCateService;

    public function __construct(CommoditiesCateRepository $commoditiesCateRepo, CommoditiesCateService $commoditiesCateService)
    {
        $this->commoditiesCateRepository = $commoditiesCateRepo;
        $this->commoditiesCateService = $commoditiesCateService;
    }

    public function pagination(Request $request)
    {
        $this->commoditiesCateRepository->pushCriteria(new ListCommoditiesCateCriteria($request));
        $this->commoditiesCateRepository->pushCriteria(new RequestCriteria($request));
        $this->commoditiesCateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commoditiesCate = $this->commoditiesCateRepository->paginate();
        return $this->sendResponse($commoditiesCate->toArray(), 'Commodities Cate retrieved successfully');
    }
    public function store(CreateCommoditiesCateAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['ip'] = $request->ip();
        $input['updated_by'] = $userId;
        $commoditiesCate = $this->commoditiesCateService->create($input);
        return $this->sendResponse($commoditiesCate->toArray(), 'Commodities cate saved successfully');
    }
    // function update
    public function update($id, UpdateCommoditiesCateAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $recipient = $this->commoditiesCateRepository->findWithoutFail($id);

        if (empty($recipient)) {
            return $this->sendError('customer not found');
        }

        $input['updated_by'] = $userId;
        $commoditiesCate = $this->commoditiesCateRepository->update($input, $id);

        return $this->sendResponse($commoditiesCate->toArray(), 'Commodities cate updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $commoditiesCate = $this->commoditiesCateRepository->findWithoutFail($id);

        if (empty($commoditiesCate)) {
            return $this->sendResponse('Commodities Cate not found');
        }
        $commoditiesCate->delete();
        return $this->sendResponse($id, 'Commodities Cate deleted successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

}
