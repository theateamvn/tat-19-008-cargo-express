<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Services\StatisticService;

class StatisticAPIController extends AppBaseController
{
    private $statisticService;

    /**
     * StatisticController constructor.
     * @param $statisticService
     */
    public function __construct(StatisticService $statisticService)
    {
        $this->statisticService = $statisticService;
    }

    public function index()
    {
        return $this->sendResponse($this->statisticService->getOverviewApp(),'Statistic information retrieved successfully');
    }

    public function user_statistic(){
        return $this->sendResponse($this->statisticService->getUsersInformation(),'Users statistic information retrieved successfully');
    }
}
