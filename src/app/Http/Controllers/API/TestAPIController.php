<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Ptondereau\LaravelUpsApi\Facades;
use Response;
use Stores;
use SoapClient;
use SoapFault;
use Dompdf\Dompdf;
use App\Http\Controllers;

/**
 * Class TestAPIController
 * @package App\Http\Controllers\API
 */
///var/www/public/tat-19-008-cargo-express/src/public_html/library/
class TestAPIController extends AppBaseController
{

    private $path_to_wsdl;

    public function __construct()
    {
        require_once('/var/www/public/tat-19-008-cargo-express/src/public_html/library/fedex-common.php5');
        $this->path_to_wsdl = "/var/www/public/tat-19-008-cargo-express/src/public_html/library/ShipService_v25.wsdl";
    }

    public function test_shipment()
    {
        echo phpinfo();die;
        require_once(base_path("public_html/library/fedex-common.php5"));
        $path_to_wsdl = base_path("public_html/library/ShipService_v25.wsdl");
        ini_set("soap.wsdl_cache_enabled", "0");
        $opts = array(
            'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
        );
        $client = new SoapClient($path_to_wsdl, array('trace' => 1, 'stream_context' => stream_context_create($opts)));

        $request['WebAuthenticationDetail']['UserCredential'] = array(
            'Key' => 'Avuqbbo9EzbruBj5',
            'Password' => 'xFose1vsrIQhTnEgLGg1lrKDe '
        );
        $request['ClientDetail'] = array(
            'AccountNumber' => '510087380',
            'MeterNumber' => '114062969'
        );
        $request['TransactionDetail']['CustomerTransactionId'] = '*** Ground Domestic Shipping Request using PHP ***';
        $request['Version'] = array(
            'ServiceId' => 'ship',
            'Major' => '25',
            'Intermediate' => '0',
            'Minor' => '0'
        );
        $request['RequestedShipment']['ShipTimestamp']  =  date('c');
        $request['RequestedShipment']['DropoffType']    = 'REGULAR_PICKUP';
        $request['RequestedShipment']['ServiceType']    = 'FIRST_OVERNIGHT';
        $request['RequestedShipment']['PackagingType']  = 'YOUR_PACKAGING';
        $request['RequestedShipment']['Shipper']['Contact'] = array(
            'PersonName' => 'Sender Name',
            'CompanyName' => 'Sender Company Name',
            'PhoneNumber' => '1234567890',
            'EMailAddress' => 'hanhphucmongmanh496@gmail.com'
        );
        $request['RequestedShipment']['Shipper']['Address'] = array(
            'StreetLines' => array('Address Line 1'),
            'City' => 'Newyork',
            'StateOrProvinceCode' => 'NY',
            'PostalCode' => '10007',
            'CountryCode' => 'US'
        );
        $request['RequestedShipment']['Recipient']['Contact'] = array(
            'PersonName' => 'Recipient Name',
            'CompanyName' => 'Recipient Company Name',
            'PhoneNumber' => '1234567890'
        );
        $request['RequestedShipment']['Recipient']['Address'] = array(
            'StreetLines' => array('Address Line 1'),
            'City' => 'NewOrleans',
            'StateOrProvinceCode' => 'LA',
            'PostalCode' => '70112',
            'CountryCode' => 'US',
            'Residential' => true
        );
        $request['RequestedShipment']['ShippingChargesPayment']['PaymentType'] =  'SENDER';
        $request['RequestedShipment']['ShippingChargesPayment']['Payor'] = array(
            'ResponsibleParty' => array(
                'AccountNumber' => '510087380',
                'Contact' => null,
                'Address' => array(
                    'CountryCode' => 'US'
                )
            )
        );
        $request['RequestedShipment']['LabelSpecification'] = array(
            'LabelFormatType' => 'COMMON2D',
            'ImageType' => 'PNG',
            'LabelStockType' => 'PAPER_7X4.75'
        );
        $request['RequestedShipment']['RateRequestTypes'] = 'LIST';
        $request['RequestedShipment']['PackageCount'] = 1;
        $request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
        $request['RequestedShipment']['RequestedPackageLineItems'] = array(
            '0' => array(
                'SequenceNumber' => 1,
                'GroupPackageCount' => 1,
                'Weight' => array(
                    'Value' => 50.0,
                    'Units' => 'LB'
                ),
                'Dimensions' => array(
                    'Length' => 12,
                    'Width' => 12,
                    'Height' => 12,
                    'Units' => 'IN'
                ),
                'CustomerReferences' => array(
                    '0' => array(
                        'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
                        'Value' => '510087380'
                    )
                )
            )
        );

        try {

            if (setEndpoint('changeEndpoint')) {
                $newLocation = $client->__setLocation('https://wsbeta.fedex.com/web-services');
            }
            $response = $client->processShipment($request);
            if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
                echo '<pre>';
                print_r($response);
                echo '<pre>';
                foreach ($response as $key => $value) {
                    $request[$key] = $value;
                }
                $response_1 = $client->validateShipment($request);
                echo '<pre>';
                print_r($response_1);
                echo '<pre>';
            } else {
                printError($client, $response);
            }

            writeToLog($client);
        } catch (SoapFault $exception) {
            printFault($exception, $client);
        }
    }

    public function form_pdf()
    {
        $html  = '';
        $html .= '<link type="text/css" href="'.base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css').'" rel="stylesheet" />';
        $html .= '<link type="text/css" href="'.base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.css').'" rel="stylesheet" />';
        // $html .= '<table class="table">';
        // $html .= '<tr>';
        // $html .= '<th>Package</th>';
        // $html .= '<th>Weight</th>';
        // $html .= '<th>Height</th>';
        // $html .= '<th>Width</th>';
        // $html .= '<th>Length</th>';
        // $html .= '<th>Description</th>';
        // $html .= '</tr>';
        // $html .= '<tr>';
        // $html .= '<td>Package 1</td>';
        // $html .= '<td>12</td>';
        // $html .= '<td>4</td>';
        // $html .= '<td>5</td>';
        // $html .= '<td>6</td>';
        // $html .= '<td></td>';
        // $html .= '</tr>';
        // $html .= '</table>';
        
        $html .= '<div class="container">';
        $html .= '<h2 align="center">Package 1</h2>';
        $html .= '<div class="row">';
        $html .= '<div class="col-md-3">Weight : 12 LBS</div>';
        $html .= '<div class="col-md-3">Height : 12 IN</div>';
        $html .= '<div class="col-md-3">Width : 12 IN</div>';
        $html .= '<div class="col-md-3">Length : 12 IN</div>';
        $html .= '<div class="col-md-12">Description : aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</div>';
        $html .= '</div>';
        $html .= '</div>';

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->set_base_path(base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css'));
        $dompdf->set_base_path(base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.css'));
        $dompdf->render();
        $dompdf->stream();
    }
}
