<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateShipmentsAPIRequest;
use App\Http\Requests\API\UpdateShipmnetsAPIRequest;
use App\Criteria\ListShipmentsCriteria;
use App\Models\Shipments;
use App\Repositories\ShipmentsRepository;
use App\Services\ShipmentsService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class ShipmentsAPIController extends AppBaseController
{
    private $shipmentsRepository;

    /**
     * @var UserService
     */
    private $shipmentsService;

    public function __construct(ShipmentsRepository $shipmentsRepo, ShipmentsService $shipmentsService)
    {
        $this->shipmentsRepository = $shipmentsRepo;
        $this->shipmentsService = $shipmentsService;
    }

    public function pagination(Request $request)
    {
        $this->shipmentsRepository->pushCriteria(new ListShipmentsCriteria($request));
        $this->shipmentsRepository->pushCriteria(new RequestCriteria($request));
        $this->shipmentsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shipmnets = $this->shipmentsRepository->paginate();
        return $this->sendResponse($shipmnets->toArray(), 'Shipments retrieved successfully');
    }
    public function store(CreateShipmentsAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $shimments['customer_id']       = $input['sender']['customer_id'];
        $shimments['sender_id']         = $input['sender']['id'];
        $shimments['recipient_id']      = $input['recipient']['id'];
        $shimments['info_service_type']      = $input['serviceType']['id']['value'];
        $shimments['info_service_item']      = $input['serviceType']['item'];
        $shimments['payment_type']              = $input['paymentPaids']['paymentPaid']['value'];
        $shimments['payment_type_item']         = $input['paymentPaids']['paymentPaidItem'];
        $shimments['payment_type_paid_type']    = $input['paymentPaids']['paymentPaidType'];
        $shimments['created_by'] = $userId;
        $shimments['updated_by'] = $userId;
        $shimments['ip'] = $request->ip();
        $shipment_ = $this->shipmentsService->create($shimments);
        $id = $shipment_->id;
        //update shipment code
        $id_ = $id;
        if($id_ < 1000){
            $id_ = sprintf("%04s", $id_);
        }
        $cus_detail = $this->shipmentsService->getCustomerDetail($input['sender']['customer_id']);
        $code = $cus_detail['customer_code'].'-'.$id_;
        $input_ = array( 'shipment_code' => $code);
        $this->shipmentsRepository->update($input_, $id);

        // shipment fee
        $shipmentFee = $input['shipmentRate'];
        $shipment_fee = $this->shipmentsService->saveShipmentFee($id, $shipmentFee);
        // package create
        $packages = $input['packages'];
        $package_create = $this->shipmentsService->savePackages($id, $packages);
        // commo create
        $commos = $input['commodities'];
        $commos_create = $this->shipmentsService->saveCommos($id, $commos);
        $result = [
            'shipment_code' => $code,
            'packageCount' => (count($input['packages'])+1)
        ];
        return $this->sendResponse($result, 'Shipments saved successfully');
    }
    // function update
    public function update($id, UpdateShipmnetsAPIRequest $request)
    {
        /*
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $recipient = $this->recipientRepository->findWithoutFail($id);
        if (empty($recipient)) {
            return $this->sendError('customer not found');
        }
        $input['updated_by'] = $userId;
        $recipient = $this->recipientRepository->update($input, $id);
        return $this->sendResponse($recipient->toArray(), 'User updated successfully');*/
        $shipment = $this->shipmentsRepository->findWithoutFail($id);
        if (empty($shipment)) {
            return $this->sendError('shipment not found');
        }
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $shipment_detail                        = $input['shipment_detail']; 
        $shimments['customer_id']               = $input['sender']['customer_id'];
        $shimments['sender_id']                 = $input['sender']['id'];
        $shimments['recipient_id']              = $input['recipient']['id'];
        $shimments['info_service_type']         = $input['serviceType']['id'];
        $shimments['info_service_item']         = $input['serviceType']['item'];
        if($input['paymentPaids']['paymentPaid'] == 1){
            $shimments['payment_type']              = $input['paymentPaids']['paymentPaid'];
            $shimments['payment_type_item']         = $input['paymentPaids']['paymentPaidItem'];
            $shimments['payment_type_paid_type']    = 0;
        } else {
            $shimments['payment_type']              = $input['paymentPaids']['paymentPaid'];
            $shimments['payment_type_item']         = $input['paymentPaids']['paymentPaidItem'];
            $shimments['payment_type_paid_type']    = $input['paymentPaids']['paymentPaidType'];
        }
        $shimments['created_by'] = $userId;
        $shimments['updated_by'] = $userId;
        $shimments['ip'] = $request->ip();
        // update shipment
        $this->shipmentsRepository->update($shimments, $id);
        /*
        $id = $shipment_->id;
        //update shipment code
        $id_ = $id;
        if($id_ < 1000){
            $id_ = sprintf("%04s", $id_);
        }
        $cus_detail = $this->shipmentsService->getCustomerDetail($input['sender']['customer_id']);
        $code = $cus_detail['customer_code'].'-'.$id_;
        $input_ = array( 'shipment_code' => $code);
        $this->shipmentsRepository->update($input_, $id);
        */
        // shipment fee
        $shipmentFee = $input['shipmentRate'];
        $shipment_fee = $this->shipmentsService->saveShipmentFee($id, $shipmentFee);
        // package create
        $packages = $input['packages'];
        $package_create = $this->shipmentsService->savePackages($id, $packages);
        // commo create
        $commos = $input['commodities'];
        $commos_create = $this->shipmentsService->saveCommos($id, $commos);
        $result = [
            'shipment_code' => $shipment_detail['shipment_code'],
            'packageCount' => (count($input['packages'])+1)
        ];
        return $this->sendResponse($result, 'Shipments updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $recipient = $this->shipmentsRepository->findWithoutFail($id);

        if (empty($recipient)) {
            return $this->sendResponse('Shipments not found');
        }
        //update shipment code
        $input_ = array( 'status' => -1);
        $this->shipmentsRepository->update($input_, $id);
        return $this->sendResponse($id, 'Shipments canceled successfully');
    }
    public function load_country()
    {
        $list = $this->recipientService->getCountryList();
        return $this->sendResponse($list, 'Countries list get successfully');
    }
    public function load_city($id)
    {
        $list = $this->recipientService->getCityList($id);
        return $this->sendResponse($list, 'Cities list get successfully');
    }


    // function shipmnets load_customer_detail
    public function load_customer()
    {
        $customer_list = $this->shipmentsService->getCustomerList();
        return $this->sendResponse($customer_list, 'Customer list get successfully');
    }
    public function load_customer_detail($id)
    {
        $customer = $this->shipmentsService->getCustomerDetail($id);
        return $this->sendResponse($customer, 'Customer detail get successfully');
    }
    public function load_customer_by_type($id)
    {
        $customer_list = $this->shipmentsService->getCustomerByTypeList($id);
        return $this->sendResponse($customer_list, 'Customer list get successfully');
    }
    public function load_sender()
    {
        $sender_list = $this->shipmentsService->getSenderList();
        return $this->sendResponse($sender_list, 'Customer list get successfully');
    }
    public function load_sender_customer($id)
    {
        $sender_customer_list = $this->shipmentsService->getSenderCustomerList($id);
        return $this->sendResponse($sender_customer_list, 'Sender By Customer list get successfully');
    }
    public function load_sender_detail($id)
    {
        $sender_list = $this->shipmentsService->getSenderDetail($id);
        return $this->sendResponse($sender_list, 'Customer detail get successfully');
    }
    public function load_recipient($id)
    {
        $recipient_list = $this->shipmentsService->getRecipientList($id);
        return $this->sendResponse($recipient_list, 'Recipient list get successfully');
    }
    public function load_recipient_by_sender($id)
    {
        $recipient_list = $this->shipmentsService->getRecipientBySenderList($id);
        return $this->sendResponse($recipient_list, 'Recipient list get successfully');
    }
    public function load_recipient_detail($id)
    {
        $recipient = $this->shipmentsService->getRecipientDetail($id);
        return $this->sendResponse($recipient, 'Recipient detail get successfully');
    }
    public function load_service_price($zoneId, $type)
    {
        $price = $this->shipmentsService->getServicePrice($zoneId, $type);
        return $this->sendResponse($price, 'Price detail get successfully');
    }
    public function load_service_type()
    {
        $service_type = $this->shipmentsService->getServiceTypes();
        return $this->sendResponse($service_type, 'service_type detail get successfully');
    }
    public function load_service_type_items($id)
    {
        $service_type_items = $this->shipmentsService->getServiceTypeItems($id);
        return $this->sendResponse($service_type_items, 'service_type_items detail get successfully');
    }
    public function package_price(Request $request)
    {
        $total_price = 0;
        $packages = $request->all();
        foreach ($packages as $key => $package) {
            $w = $package['w'];
            $dM = $package['dD'];
            $dH = $package['dW'];
            $dL = $package['dH'];
            $chargeable_weight = $dM*$dH*$dL/139;
            $real_w = $w;
            if($chargeable_weight > $w){
                $real_w = $chargeable_weight;
            }
            $price = $real_w*3.5;
            $total_price += $price;
        }
        return $this->sendResponse(round($total_price, 2), 'Price detail get successfully');
    }
    public function commo_price(Request $request)
    {
        $total_price = 0;
        $total_value = 0;
        $commos = $request->all();
        foreach ($commos as $key => $commo) {
            if(isset($commo['total_value'])){
                $total_value += $commo['total_value'];
            }
            if(isset($commo['surchage'])) {
                $total_price += $commo['surchage'];
            }
        }
        $price = [
            'total_price' =>round($total_price, 2),
            'total_value' => $total_value
        ];
        return $this->sendResponse($price, 'Price Commo detail get successfully');
    }
    /** bk */
    public function commo_price_item_bk(Request $request)
    {
        $commo = $request->all();
       // $commo_detail = $this->shipmentsService->getCommoDetail($commo['id']);
        $commo_detail = $this->shipmentsService->getCommoChargerDetail($commo['id']);
        // check type cal
        if($commo_detail->value != 0){
            $surcharge = $this->type1($commo, $commo_detail);
        } else {
            $surcharge = $this->type2($commo, $commo_detail);
        }
        return $this->sendResponse( $surcharge , 'Price Commo item get successfully');
    }
    /**
     * start commo calculator
     * 
     */
    public function commo_price_item(Request $request)
    {
        $commo = $request->all();
        // get charge by commo
        $commo_detail = $this->shipmentsService->getCommoChargerDetail($commo['id']);
        // check chager by conditions
        $base_on = $commo_detail->based_on;
        $surcharge = 0;
        if($base_on < 4){
            // no conditions: base on quanlity/weight/value
            $surcharge = $this->no_condition($commo, $commo_detail);
        } else if($base_on == 4) {
            // one condition
            $surcharge = $this->one_condition($commo, $commo_detail);
        } else if($base_on == 5) {
            // two condition
            $surcharge = $this->two_condition($commo, $commo_detail);
        }
        return $this->sendResponse( $surcharge , 'Price Commo item get successfully');
    }
    /**  */
    protected function no_condition($commo, $commo_detail) {
        // check baseon
        $base_on    = $commo_detail->based_on;
        $value_type = $commo_detail->value_type;
        $min_charge = $commo_detail->min_charge;
        $commo_value = 0;
        $real_surchage = 0;
        if($base_on == 2){
            $commo_value = $commo['total_value'];
        } else if($base_on == 3){
            $commo_value = $commo['weight'];
        } else {
            $commo_value = $commo['quanlity'];
        }
        
        if($value_type == '$'){
            $real_surchage = $commo_detail->value*$commo_value;
            $real_surchage = round($real_surchage,2);
        } else {
            $real_surchage = ($commo_detail->value*$commo_value/100);
            $real_surchage = round($real_surchage,2);
        }
        if($real_surchage < $min_charge)
        {
            $real_surchage = $min_charge;
        }
        return $real_surchage;
    }
    protected function one_condition($commo, $commo_detail) {
        $condition_q = $commo_detail->condition_q;
        $condition_v = $commo_detail->condition_v;
        $condition_w = $commo_detail->condition_w;
        $min_charge = $commo_detail->min_charge;
        $condition_result = false;
        $real_surchage = 0;
        $commo_value = 0;
        if($condition_q)
        {
            $type = $commo_detail->condition_q_type;
            $value1 = $commo['quanlity'];
            $value2 = $condition_q;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result = true;
            } else {
                $condition_result = false;
            }
        }
        if($condition_v)
        {
            $type = $commo_detail->condition_v_type;
            $value1 = $commo['total_value'];
            $value2 = $condition_v;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result = true;
            } else {
                $condition_result = false;
            }
        }
        if($condition_w)
        {
            $type = $commo_detail->condition_w_type;
            $value1 = $commo['weight'];
            $value2 = $condition_w;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result = true;
            } else {
                $condition_result = false;
            }
        }
        if($condition_result){
            $value_type = $commo_detail->value_correct_type;
            if($value_type == '$'){
                $real_surchage = $commo_detail->value_correct*$commo_value;
                $real_surchage = round($real_surchage,2);
            } else {
                $real_surchage = ($commo_detail->value_correct*$commo_value/100);
                $real_surchage = round($real_surchage,2);
            }
        }
        if($real_surchage < $min_charge)
        {
            $real_surchage = $min_charge;
        }
        return $real_surchage;
    }
    protected function two_condition($commo, $commo_detail) {
        $condition_q = $commo_detail->condition_q;
        $condition_v = $commo_detail->condition_v;
        $condition_w = $commo_detail->condition_w;
        $condition_q_2 = $commo_detail->condition_q_2;
        $condition_v_2 = $commo_detail->condition_v_2;
        $condition_w_2 = $commo_detail->condition_w_2;
        $min_charge = $commo_detail->min_charge;
        $condition_result = 0;
        $num_con = 0;
        $num_con_2 = 0;
        $condition_result_2 = 0;
        $real_surchage = 0;
        $commo_value = 0;
        if($condition_q)
        {
            $num_con++;
            $type = $commo_detail->condition_q_type;
            $value1 = $commo['quanlity'];
            $value2 = $condition_q;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result += 1;
            }
        }
        if($condition_v)
        {
            $num_con++;
            $type = $commo_detail->condition_v_type;
            $value1 = $commo['total_value'];
            $value2 = $condition_v;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result += 1;
            } 
        }
        if($condition_w)
        {
            $num_con++;
            $type = $commo_detail->condition_w_type;
            $value1 = $commo['weight'];
            $value2 = $condition_w;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result += 1;
            }
        }
        if($condition_q_2)
        {
            $num_con_2++;
            $type = $commo_detail->condition_q_type_2;
            $value1 = $commo['quanlity'];
            $value2 = $condition_q_2;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result_2 += 1;
            }
        }
        if($condition_v_2)
        {
            $num_con_2++;
            $type = $commo_detail->condition_v_type_2;
            $value1 = $commo['total_value'];
            $value2 = $condition_v_2;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result_2 += 1;
            }
        }
        if($condition_w_2)
        {
            $num_con_2++;
            $type = $commo_detail->condition_w_type_2;
            $value1 = $commo['weight'];
            $value2 = $condition_w_2;
            $con_ = $this->conditions($type, $value1, $value2);
            if($con_ == 2){
                $condition_result_2 += 1;
            } 
        }
        // check num con
        if($num_con > 1){
            if($condition_result == $num_con){
                $condition_result = 1;
            } else {
                $condition_result = 0;
            }
        }
        if($num_con_2 > 1){
            if($condition_result_2 == $num_con_2){
                $condition_result_2 = 1;
            } else {
                $condition_result_2 = 0;
            }
        }
        if($condition_result){
            $condition_base_on = $commo_detail->condition_base;
            if($condition_base_on == 2){
                $commo_value = $commo['total_value'];
            } else if($condition_base_on == 3){
                $commo_value = $commo['weight'];
            } else if($condition_base_on == 1){
                $commo_value = $commo['quanlity'];
            } else if($condition_base_on == 4){
                $commo_value = 1;
            }
            $value_type = $commo_detail->value_correct_type;
            if($value_type == '$'){
                $real_surchage = $commo_detail->value_correct*$commo_value;
                $real_surchage = round($real_surchage,2);
            } else {
                $real_surchage = ($commo_detail->value_correct*$commo_value/100);
                $real_surchage = round($real_surchage,2);
            }
        } else if($condition_result_2){
            $condition_base_on = $commo_detail->condition_base_2;
            if($condition_base_on == 2){
                $commo_value = $commo['total_value'];
            } else if($condition_base_on == 3){
                $commo_value = $commo['weight'];
            } else if($condition_base_on == 1){
                $commo_value = $commo['quanlity'];
            } else if($condition_base_on == 4){
                $commo_value = 1;
            }
            $value_type = $commo_detail->value_correct_type_2;
            if($value_type == '$'){
                $real_surchage = $commo_detail->value_correct_2*$commo_value;
                $real_surchage = round($real_surchage,2);
            } else {
                $real_surchage = ($commo_detail->value_correct_2*$commo_value/100);
                $real_surchage = round($real_surchage,2);
            }
        }
        if($real_surchage < $min_charge)
        {
            $real_surchage = $min_charge;
        }
        return $real_surchage;
    }
    public function conditions($type, $value1, $value2){
        /* echo $value1;
        echo $type;
        echo $value2;*/
        /**
         * result: 1-false, 2-true, 3-other
         */
        $result = 1;
        if($type == '>'){
            if($value1 > $value2) {
                $result = 2;
            }
        }
        if($type == '<'){
            if($value1 < $value2) {
                $result = 2;
            } 
        }
        if($type == '>='){
            if($value1 >= $value2) {
                $result = 2;
            }
        }
        if($type == '<='){
            if($value1 <= $value2) {
                $result = 2;
            }
        }
        return $result;
    }
    /** End Calculator */
    /**
     * start commo item cal type
     * base_on_type: 1: $, 2:%
     */
        protected function type1($commo, $commo_detail)
        {
            // call real surcharge
            $real_surchage = 0;
            $min_charge = $commo_detail->min_charge;
            $based_on_type = $commo_detail->value_type;
            if($based_on_type == '$'){
                $real_surchage = $commo_detail->value*$commo['quanlity'];
                $real_surchage = round($real_surchage,2);
                if($real_surchage < $min_charge){
                    $real_surchage = $min_charge;
                }
            } else {
                $real_surchage = ($commo_detail->value*$commo['total_value']/100);
                $real_surchage = round($real_surchage,2);
                if($real_surchage < $min_charge){
                    $real_surchage = $min_charge*$commo['quanlity'];
                } else {
                    $real_surchage = $real_surchage*$commo['quanlity'];
                }
            }
            return $real_surchage;
        }
        protected function type2($commo, $commo_detail)
        {
            // check contitions
            $based_on_type = '$';
            $based_on_type_w = false;
            $condition = true;
            $input_w = $commo['weight'];
            if($commo_detail->condition_q){
                $c_q = $this->conditions($commo_detail->condition_q_type, $commo['quanlity'], $commo_detail->condition_q);
            }
            if($commo_detail->condition_v){
                $c_v = $this->conditions($commo_detail->condition_v_type, $commo['total_value'], $commo_detail->condition_v);
            }
            if($commo_detail->condition_w){
                $c_w = $this->conditions($commo_detail->condition_w_type, $commo['weight'], $commo_detail->condition_w);
            }
            /**
             * based_on:
             * 1 => Quanlity
             * 2 => Value
             * 3 => Weight
             * $condition: 1 => false, 2=>true, 3->other
             */
            if($commo_detail->based_on == 2){

                // if(isset($c_v) && $c_v){
                //     if(isset($c_q) && $c_q){
                //         $condition = true;
                //     } else {
                //         $condition = false;
                //     }
                // } else {
                //     $condition = false;
                // }
                
                // check $condition
                if(isset($c_v) && $c_v){
                    if(isset($input_w) && $input_w){
                        $based_on_type_w = true;
                    }
                    if($c_v == 3){
                        $condition = 3;
                    } else if($c_v == 2){
                        $condition = 2;
                    } else {
                        // check if have quanlity
                        $condition = $c_v;
                        if(isset($c_q) && $c_q){
                            $condition = 1;
                        }
                        if(isset($c_w) && $c_w)
                        {
                            if($c_w == 1 || $c_w == 3){
                                $condition = 3;
                            } else {
                                $condition = 2;
                            }
                        }
                    }
                } else {
                    $condition = 1;
                }
            }
            if($commo_detail->based_on == 1){
                if(isset($c_q) && $c_q == 'none'){
                    $condition = 'none';
                }else if(isset($c_q) && $c_q){
                    if(isset($c_v) && $c_v){
                        $condition = 2;
                    } else {
                        $condition = 1;
                    }
                } else {
                    $condition = 1;
                }
            }
            if($commo_detail->based_on == 3){
                $based_on_type_w = 2;
                
                if(isset($c_w) && $c_w == 'none'){
                    $condition = 'none';
                }else if(isset($c_w) && $c_w){
                    if(isset($c_v) && $c_v){
                        $condition = 2;
                    } else {
                        $condition = 1;
                    }
                } else {
                    $condition = 1;
                }
            }
            if($condition == 3){
                $value = 0;
                $based_on_type = $commo_detail->value_correct_type;
            }else if($condition == 2){
                $value = $commo_detail->value_correct;
                $based_on_type = $commo_detail->value_correct_type;
            } else {
                $value = $commo_detail->value_uncorrect;
                $based_on_type = $commo_detail->value_uncorrect_type;
            }
            // call real surcharge
            $real_surchage = 0;
            $min_charge = $commo_detail->min_charge;
            if($based_on_type_w){
                $real_surchage = $value*$commo['weight'];
                if($real_surchage < $min_charge){
                    $real_surchage = $min_charge;
                }
            } else {
                if($based_on_type == '$'){
                    $real_surchage = $value*$commo['quanlity'];
                } else if($based_on_type == '%' && $commo_detail->based_on == 3){
                    $real_surchage = ($value*$commo['weight']/100)*$commo['quanlity'];
                } else {
                    $real_surchage = ($value*$commo['total_value']/100)*$commo['quanlity'];
                }
                $real_surchage = round($real_surchage,2);
                if($real_surchage < $min_charge){
                    $real_surchage = $min_charge;
                }
            }
            return $real_surchage;
        }
        public function conditions_bk($type, $value1, $value2){
            /* echo $value1;
            echo $type;
            echo $value2;*/
            /**
             * result: 1-false, 2-true, 3-other
             */
            $result = 1;
            if($type == '>'){
                if($value1 > $value2) {
                    $result = 2;
                } else if($value1 == $value2){
                    $result = 3;
                }
            }
            if($type == '<'){
                if($value1 < $value2) {
                    $result = 2;
                } else if($value1 == $value2){
                    $result = 3;
                }
            }
            if($type == '>='){
                if($value1 >= $value2) {
                    $result = 2;
                }
            }
            if($type == '<='){
                if($value1 <= $value2) {
                    $result = 2;
                }
            }
            return $result;
        }
    /**
      * end commo item cal type
      */
    public function load_commo(){
        $list_commo = $this->shipmentsService->getListCommo();
        return $this->sendResponse($list_commo, 'Commo list get successfully');
    }
    public function load_commo_cate($id){
        if($id == -1)
        {
            $list_commo = $this->shipmentsService->getListCommo();
        } else {
            $list_commo = $this->shipmentsService->getListCommoCate($id);
        }
        return $this->sendResponse($list_commo, 'Commo list by cate get successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
    /**
     * edit Shipment
     */
    public function shipment_detail($id)
    {
        $shipment = $this->shipmentsService->getShipmentDetail($id);
        return $this->sendResponse($shipment, 'Recipient detail get successfully');
    }
    public function shipment_packages($id)
    {
        $result = array();
        $packages = $this->shipmentsService->getShipmentPackages($id);
        $commos = $this->shipmentsService->getShipmentCommos($id);
        $result = [
            'packages' => $packages,
            'commos'   => $commos
        ];
        return $this->sendResponse($result, 'Recipient detail get successfully');
    }
    public function shipment_rate($id)
    {
        $result = array();
        $rates = $this->shipmentsService->getShipmentRates($id);
        return $this->sendResponse($rates, 'Recipient rates get successfully');
    }
    // function change status
    public function shipment_change_status(Request $request)
    {
        $shipment = $request->all();
        $id = $shipment['shipment_id'];
        $status = $shipment['status'];
        $shipment = $this->shipmentsRepository->findWithoutFail($id);

        if (empty($shipment)) {
            return $this->sendResponse('Shipments not found');
        }
        //update shipment code
        $input_ = array( 'status' => $status);
        $this->shipmentsRepository->update($input_, $id);
        return $this->sendResponse($id, 'Shipments status change successfully');
    }
}
