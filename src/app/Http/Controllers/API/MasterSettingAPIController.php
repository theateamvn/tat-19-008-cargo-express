<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSettingAPIRequest;
use App\Http\Requests\API\UpdateMasterSettingAPIRequest;
use App\Models\MasterSetting;
use App\Repositories\MasterSettingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MasterSettingController
 * @package App\Http\Controllers\API
 */
class MasterSettingAPIController extends AppBaseController
{
    /** @var  MasterSettingRepository */
    private $masterSettingRepository;

    public function __construct(MasterSettingRepository $masterSettingRepo)
    {
        $this->masterSettingRepository = $masterSettingRepo;
    }

    /**
     * Display a listing of the MasterSetting.
     * GET|HEAD /masterSettings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->masterSettingRepository->pushCriteria(new RequestCriteria($request));
        $this->masterSettingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSettings = $this->masterSettingRepository->all();

        return $this->sendResponse($masterSettings->toArray(), 'Master Settings retrieved successfully');
    }

    /**
     * Store a newly created MasterSetting in storage.
     * POST /masterSettings
     *
     * @param CreateMasterSettingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMasterSettingAPIRequest $request)
    {
        $input = $request->all();

        $masterSettings = $this->masterSettingRepository->create($input);

        return $this->sendResponse($masterSettings->toArray(), 'Master Setting saved successfully');
    }

    /**
     * Display the specified MasterSetting.
     * GET|HEAD /masterSettings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MasterSetting $masterSetting */
        $masterSetting = $this->masterSettingRepository->findWithoutFail($id);

        if (empty($masterSetting)) {
            return $this->sendError('Master Setting not found');
        }

        return $this->sendResponse($masterSetting->toArray(), 'Master Setting retrieved successfully');
    }

    /**
     * Update the specified MasterSetting in storage.
     * PUT/PATCH /masterSettings/{id}
     *
     * @param  int $id
     * @param UpdateMasterSettingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMasterSettingAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSetting $masterSetting */
        $masterSetting = $this->masterSettingRepository->findWithoutFail($id);

        if (empty($masterSetting)) {
            return $this->sendError('Master Setting not found');
        }

        $masterSetting = $this->masterSettingRepository->update($input, $id);

        return $this->sendResponse($masterSetting->toArray(), 'MasterSetting updated successfully');
    }

    /**
     * Remove the specified MasterSetting from storage.
     * DELETE /masterSettings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MasterSetting $masterSetting */
        $masterSetting = $this->masterSettingRepository->findWithoutFail($id);

        if (empty($masterSetting)) {
            return $this->sendError('Master Setting not found');
        }

        $masterSetting->delete();

        return $this->sendResponse($id, 'Master Setting deleted successfully');
    }

    public function getByKey($key)
    {
        $masterSetting = $this->masterSettingRepository->findWhere(['key' => $key])->first();
        if (!$masterSetting) {
            $masterSetting = MasterSetting::create([
                'key' => $key,
                'value' => ''
            ]);
        }
        return $this->sendResponse($masterSetting, 'Master Setting retrieved successfully');
    }
}
