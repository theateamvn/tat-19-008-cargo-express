<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByUserStoreCriteria;
use App\Facades\Stores;
use App\Http\Requests\API\AddContactBlastTemplateRequest;
use App\Http\Requests\API\CreateBlastTemplateAPIRequest;
use App\Http\Requests\API\UpdateBlastTemplateAPIRequest;
use App\Http\Requests\API\UploadPictureRequest;
use App\Models\BlastTemplate;
use App\Notifications\SendBlast;
use App\Repositories\BlastTemplateRepository;
use App\Services\BlastSmsService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;

/**
 * Class BlastTemplateController
 * @package App\Http\Controllers\API
 */
class BlastTemplateAPIController extends AppBaseController
{
    /** @var  BlastTemplateRepository */
    private $blastTemplateRepository;

    /** @var BlastSmsService  */
    private $blastSmsService;

    public function __construct(BlastTemplateRepository $blastTemplateRepo, BlastSmsService $blastSmsService)
    {
        $this->blastTemplateRepository = $blastTemplateRepo;
        $this->blastSmsService = $blastSmsService;
    }

    /**
     * Display a listing of the BlastTemplate.
     * GET|HEAD /blastTemplates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->blastTemplateRepository->pushCriteria(new RequestCriteria($request));
        $this->blastTemplateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $blastTemplates = $this->blastTemplateRepository->paginate(10);

        return $this->sendResponse($blastTemplates->toArray(), 'Blast Templates retrieved successfully');
    }

    /**
     * Store a newly created BlastTemplate in storage.
     * POST /blastTemplates
     *
     * @param CreateBlastTemplateAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBlastTemplateAPIRequest $request)
    {
        $input = $request->all();

        $blastTemplates = $this->blastTemplateRepository->create($input);

        return $this->sendResponse($blastTemplates->toArray(), 'Blast Template saved successfully');
    }

    /**
     * Display the specified BlastTemplate.
     * GET|HEAD /blastTemplates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BlastTemplate $blastTemplate */
        $blastTemplate = $this->blastTemplateRepository->findWithoutFail($id);

        if (empty($blastTemplate)) {
            return $this->sendError('Blast Template not found');
        }

        return $this->sendResponse($blastTemplate->toArray(), 'Blast Template retrieved successfully');
    }

    /**
     * Update the specified BlastTemplate in storage.
     * PUT/PATCH /blastTemplates/{id}
     *
     * @param  int $id
     * @param UpdateBlastTemplateAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlastTemplateAPIRequest $request)
    {
        $input = $request->all();

        /** @var BlastTemplate $blastTemplate */
        $blastTemplate = $this->blastTemplateRepository->findWithoutFail($id);

        if (empty($blastTemplate)) {
            return $this->sendError('Blast Template not found');
        }

        $blastTemplate = $this->blastTemplateRepository->update($input, $id);

        return $this->sendResponse($blastTemplate->toArray(), 'BlastTemplate updated successfully');
    }

    /**
     * Remove the specified BlastTemplate from storage.
     * DELETE /blastTemplates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BlastTemplate $blastTemplate */
        $blastTemplate = $this->blastTemplateRepository->findWithoutFail($id);

        if (empty($blastTemplate)) {
            return $this->sendError('Blast Template not found');
        }

        $blastTemplate->delete();

        return $this->sendResponse($id, 'Blast Template deleted successfully');
    }

    /**
     * @param UploadPictureRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadPicture(UploadPictureRequest $request)
    {
        $path = $request->file('file')->store('public' . DIRECTORY_SEPARATOR . 'mms');
        $url = Storage::url($path);
        return $this->sendResponse($url, 'Upload picture successfully');
    }

    public function byUser(Request $request)
    {
        $this->blastTemplateRepository->pushCriteria(new RequestCriteria($request));
        $this->blastTemplateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->blastTemplateRepository->pushCriteria(new ByUserStoreCriteria());
        /** @var BlastTemplate $blastTemplate */
        $blastTemplates = $this->blastTemplateRepository->paginate(10);

        return $this->sendResponse($blastTemplates->toArray(), 'Blast Template retrieved successfully');
    }

    public function sendBlast($id)
    {
        /** @var BlastTemplate $blastTemplate */
        $blastTemplate = $this->blastTemplateRepository->findWithoutFail($id);

        if (empty($blastTemplate)) {
            return $this->sendError('Blast Template not found');
        }

        \Notification::send($blastTemplate, new SendBlast());

        return $this->sendResponse($blastTemplate, 'Blast template sent');
    }

    public function sendAll($id){
        /** @var BlastTemplate $blastTemplate */
        $blastTemplate = $this->blastTemplateRepository->findWithoutFail($id);

        if (empty($blastTemplate)) {
            return $this->sendError('Blast Template not found');
        }

        $this->blastSmsService->sendAll($blastTemplate);
        return $this->sendResponse($blastTemplate,"Blast template sent");
    }

    public function addContacts($id, AddContactBlastTemplateRequest $request)
    {
        /** @var BlastTemplate $blastTemplate */
        $blastTemplate = $this->blastTemplateRepository->findWithoutFail($id);

        if (empty($blastTemplate)) {
            return $this->sendError('Blast Template not found');
        }

        $blastTemplate->contacts()->sync($request->get('contacts'));

        return $this->sendResponse($blastTemplate, 'Blast Template add contacts successfully');
    }

    public function syncContacts(){
        $count = $this->blastSmsService->synchronizeClientContactByUser();
        return $this->sendResponse($count,"$count contacts list synchronized");
    }
}
