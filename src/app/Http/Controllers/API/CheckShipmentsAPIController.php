<?php

namespace App\Http\Controllers\API;
use App\Criteria\ListCheckShipmentsCriteria;
use App\Criteria\ListCheckExportListCriteria;
use App\Criteria\ListCheckExportListNewCriteria;
use App\Criteria\ListCheckExportListByCodeTExpressCriteria;
use App\Criteria\ListCheckExportListByCustomerCriteria;
use App\Models\Shipments;
use App\Repositories\ShipmentsRepository;
use App\Repositories\CheckExportListRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Review;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CheckShipmentsAPIController extends AppBaseController
{
    private $shipmentsRepository;
    private $checkExportListRepository;

    /**
     * @var UserService
     */

    public function __construct(ShipmentsRepository $shipmentsRepo, CheckExportListRepository $checkExportListRepo)
    {
        $this->shipmentsRepository = $shipmentsRepo;
        $this->checkExportListRepository = $checkExportListRepo;
    }

    public function pagination(Request $request)
    {
        $this->shipmentsRepository->pushCriteria(new ListCheckShipmentsCriteria($request));
        $this->shipmentsRepository->pushCriteria(new RequestCriteria($request));
        $this->shipmentsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shipmnets = $this->shipmentsRepository->paginate();
        $result = array();
        $result['total']= $shipmnets->total();
        $result['per_page']= $shipmnets->perPage();
        $result['current_page']= $shipmnets->currentPage();
        $result['last_page']= $shipmnets->lastPage();
        foreach ($shipmnets as $key => $value) {
            $recipient_phone="";
            $sender_phone="";
            for($i=0; $i < (strlen($value['recipient_phone'])-3) ; $i++ ){
                $recipient_phone =$recipient_phone . '*';
            }
            for($i=0; $i < (strlen($value['sender_phone'])-3); $i++ ){
                $sender_phone = $sender_phone . '*';
            }
            $recipient_phone= $recipient_phone . substr($value['recipient_phone'],strlen($value['recipient_phone'])-3);
            $sender_phone=$sender_phone . substr($value['sender_phone'],strlen($value['sender_phone'])-3);
            $result['data'][$key] = $value;
            $result['data'][$key]['recipient_phone'] = $recipient_phone;
            $result['data'][$key]['sender_phone'] = $sender_phone;
            
        }
        return $this->sendResponse($result, 'Shipments retrieved successfully');
    }

    // export list check
    public function check_export_list_pagination(Request $request)
    {
        $this->checkExportListRepository->pushCriteria(new ListCheckExportListCriteria($request));
        $this->checkExportListRepository->pushCriteria(new RequestCriteria($request));
        $this->checkExportListRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shipmnets = $this->checkExportListRepository->paginate();
        $result = array();
        $result['total']= $shipmnets->total();
        $result['per_page']= $shipmnets->perPage();
        $result['current_page']= $shipmnets->currentPage();
        $result['last_page']= $shipmnets->lastPage();
        foreach ($shipmnets as $key => $value) {
            $result['data'][$key] = $value;
        }
        return $this->sendResponse($result, 'Shipments Export List retrieved successfully');
    }
    // export list check new
    public function check_export_list_pagination_new(Request $request)
    {
        $this->checkExportListRepository->pushCriteria(new ListCheckExportListNewCriteria($request));
        $this->checkExportListRepository->pushCriteria(new RequestCriteria($request));
        $this->checkExportListRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shipmnets = $this->checkExportListRepository->paginate();
        $result = array();
        $result['total']= $shipmnets->total();
        $result['per_page']= $shipmnets->perPage();
        $result['current_page']= $shipmnets->currentPage();
        $result['last_page']= $shipmnets->lastPage();
        foreach ($shipmnets as $key => $value) {
            $result['data'][$key] = $value;
        }
        return $this->sendResponse($result, 'Shipments Export List retrieved successfully');
    }

    public function check_export_list_filter(Request $request)
    {
        $input = $request->all();
        if(!isset($input['customer_code']) && !isset($input['code_T_Express'])){
            return $this->sendResponse('', 'Shipments Export List retrieved successfully');
        }
        if(isset($input['customer_code'])&& !isset($input['code_T_Express'])){
            $this->checkExportListRepository->pushCriteria(new ListCheckExportListByCustomerCriteria($request));
        }
        if(isset($input['code_T_Express']) && !isset($input['customer_code'])){
            $this->checkExportListRepository->pushCriteria(new ListCheckExportListByCodeTExpressCriteria($request));
        }
        if(isset($input['customer_code']) && isset($input['code_T_Express'])){
            $this->checkExportListRepository->pushCriteria(new ListCheckExportListByCustomerCriteria($request));
        }
        $this->checkExportListRepository->pushCriteria(new RequestCriteria($request));
        $this->checkExportListRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shipmnets = $this->checkExportListRepository->paginate();
        $result = array();
        $result['total']= $shipmnets->total();
        $result['per_page']= $shipmnets->perPage();
        $result['current_page']= $shipmnets->currentPage();
        $result['last_page']= $shipmnets->lastPage();
        foreach ($shipmnets as $key => $value) {
            $result['data'][$key] = $value;
        }
        return $this->sendResponse($result, 'Shipments Export List retrieved successfully');
    }
    
}
