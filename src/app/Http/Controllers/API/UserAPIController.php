<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreAPIRequest;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateStoreAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Requests\API\UpdatePasswordAPIRequest;
use App\Http\Requests\API\CreateStorePackageAPIRequest;
use App\Models\Store;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Repositories\UserPackageRepository;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\UserListByRoleCriteria;
use App\Criteria\UserPackageListByUserIdCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    private $userPackageRepository;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserRepository $userRepo, UserService $userService, UserPackageRepository $userPackageRepo)
    {
        $this->userPackageRepository = $userPackageRepo;
        $this->userRepository = $userRepo;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new UserListByRoleCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
        //$users = $this->userRepository->all();
        $users = $this->userRepository->with(['storeUsers','storeUsers.reviewProviders'], ['reviewLink','storeUsers.reviewProviders'])->all();

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
       // var_dump($user);die;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['updated_by'] = $userId;
        $users = $this->userService->createNewUserWithoutPayment($input);

        return $this->sendResponse($users->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param  int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        if(isset($input['password']) && $input['password'] != '') {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input['password'] = $user->password;
        }
        $input['updated_by'] = $userId;
        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendResponse($id, 'User deleted successfully');
    }

    public function stores(){
        $stores = Auth::user()->storeUsers;
        return $this->sendResponse($stores?$stores:[],'List stores retrieved successfully');
    }

    public function createStore(CreateStoreAPIRequest $request){
        $store = Store::create($request->all());
        Auth::user()->storeUsers()->save($store);
        return $this->sendResponse($store,'Create store successfully');
    }

    public function updateStore(UpdateStoreAPIRequest $request){
        $store = Stores::getCurrentStore();
        $store->update($request->all());
        return $this->sendResponse($store,'Update store successfully');
    }

    public function updatePassword(UpdatePasswordAPIRequest $request){
        $user = Auth::user();
        $input = $request->all();
        if (Hash::check($input['current_password'], $user->getAuthPassword())) {
            $user->password = Hash::make($input['password']);
            $user->save();
        } else {
          return $this->sendError('Current password is not correct');
        }
        return $this->sendResponse($user->toArray(),'Update password successfully');
    }
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
    /**
    ** Store Package
    **/
    public function listUserPackage(Request $request)
    {
        $this->userPackageRepository->resetCriteria();
        $this->userPackageRepository->pushCriteria(new RequestCriteria($request));
        //$this->userPackageRepository->resetCriteria();
        $this->userPackageRepository->pushCriteria(new UserPackageListByUserIdCriteria($request));
        $this->userPackageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $usePackages = $this->userPackageRepository->paginate();
        return $this->sendResponse($usePackages, 'User Packages retrieved successfully');
    }
    public function createStorePackage(CreateStorePackageAPIRequest $request){
        $input = $request->all();
        $result = $this->userService->createStorePackage($input);
        return $this->sendResponse($result, 'Create store package successfully');
    }
    public function deleteStorePackage($id)
    {
        /** @var User $user */
        $userPackage = $this->userPackageRepository->findWithoutFail($id);

        if (empty($userPackage)) {
            return $this->sendError('User not found');
        }

        $userPackage->delete();

        return $this->sendResponse($id, 'User Package deleted successfully');
    }

    public function getUserByAgent($idAgent)
    {
        $result = $this->userService->getUserTotalByAgent($idAgent);
        return $this->sendResponse($result, 'Get total user by agent');
    }

    public function user_statistic(){
        //echo 'f';die;
        return $this->sendResponse($this->_statisticService->getUsersInformation(),'Users statistic information retrieved successfully');
    }
}
