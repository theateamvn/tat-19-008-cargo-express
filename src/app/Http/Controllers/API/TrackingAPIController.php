<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTrackingAPIRequest;
use App\Http\Requests\API\UpdateTrackingAPIRequest;
use App\Repositories\TrackingRepository;
use App\Repositories\TrackingConsolidatedRepository;
use App\Services\TrackingService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\TrackingCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class TrackingAPIController extends AppBaseController
{
    /** @var  TrackingRepository */
    private $trackingRepository;

    /** @var  TrackingConsolidatedRepository */
    private $trackingConsolidatedRepository;
    
    /**
     * @var TrackingService
     */
    private $trackingService;

    public function __construct(TrackingRepository $trackingRepo,TrackingService $trackingService, TrackingConsolidatedRepository $trackingConsolidatedRepo)
    {
        $this->trackingRepository = $trackingRepo;
        $this->trackingService = $trackingService;
        $this->trackingConsolidatedRepository = $trackingConsolidatedRepo;
    }

    public function pagination(Request $request)
    {
        $this->trackingRepository->pushCriteria(new RequestCriteria($request));
        $this->trackingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->trackingRepository->pushCriteria(new TrackingCriteria($request));
        $tracking = $this->trackingRepository->paginate();
        return $this->sendResponse($tracking->toArray(), 'Tracking retrieved successfully');
    }

    public function getListConsolidated(Request $request)
    {
        $input = $request->all();
        $result = $this->trackingService->getListConsolidated($input);
        return $this->sendResponse($result, 'Tracking consolidated retrieved successfully');
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $customer_code = $input['customer_code'];
        $tracking = $this->trackingService->getListTracking($customer_code);
        return $this->sendResponse($tracking, 'Tracking retrieved successfully');
    }

    public function store(CreateTrackingAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $listItem = $input['listItem'];
        for ($i = 0; $i < count($listItem); $i++) {
            $listItem[$i]['created_by']    = $userId;
            $listItem[$i]['ip']            = $request->ip();
        }
        $result = $this->trackingService->create($listItem);
        return $this->sendResponse($result, 'Tracking created successfully');
    }

    public function update($id, UpdateTrackingAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['updated_by']    = $userId;
        $input['ip']            = $request->ip();
        $result = $this->trackingRepository->update($input,$id);
        return $this->sendResponse($result, 'Tracking updated successfully');
    }

    public function destroy($id)
    {
        $tracking = $this->trackingRepository->findWithoutFail($id);

        if (empty($tracking)) {
            return $this->sendResponse('','Tracking not found');
        }
        $status = $tracking->status;
        if($status == 1){
            return $this->sendError('Tracking used.',400);
        }
        $result = $this->trackingRepository->delete($id);
        return $this->sendResponse($result, 'Tracking deleted successfully');
    }

    public function createConsolidatedTracking(UpdateTrackingAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $listItem = $input['listItem'];
        for ($i=0; $i < count($listItem); $i++) { 
            $list_tracking_id = explode(';',$listItem[$i]['group_tracking']);
            for ($y=0; $y < count($list_tracking_id)-1; $y++) { 
                $this->trackingRepository->update(array(
                    'status'=>1,
                    'updated_by'=>$userId,
                    'ip'=> $request->ip()),$list_tracking_id[$y]);
            }
            $listItem[$i]['created_by']    = $userId;
            $listItem[$i]['ip']            = $request->ip();
            $listItem[$i]['status']        = 1;
        }
        $this->trackingService->createConsolidated($listItem);
        return $this->sendResponse($listItem, 'Tracking updated successfully');
    }

    public function updateConsolidatedTracking(UpdateTrackingAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $consolidated = $input['consolidated'];
        $consolidated['updated_by'] = $userId;
        $list_tracking_id = explode(';',$consolidated['group_tracking']);
        $list_tracking_rermove = $input['list_tracking_rermove'];
        for ($y=0; $y < count($list_tracking_rermove); $y++) { 
            $this->trackingRepository->update(array(
                'status'=> $list_tracking_rermove[$y]['status'],
                'updated_by'=>$userId,
                'ip'=> $request->ip()),$list_tracking_rermove[$y]['id']);
        }
        for ($y=0; $y < count($list_tracking_id)-1; $y++) { 
            $this->trackingRepository->update(array(
                'status'=>1,
                'updated_by'=>$userId,
                'ip'=> $request->ip()),$list_tracking_id[$y]);
        }
        $result = $this->trackingConsolidatedRepository->update($consolidated,$consolidated['id']);
        return $this->sendResponse($result, 'Tracking consolidated updated successfully');
    }

    public function checkTrackingCode($tracking_code, $customer_code)
    {
        $result = $this->trackingService->checkTrackingCode($tracking_code, $customer_code);
        return $this->sendResponse($result, 'Check tracking code successfully');
    }
    
    public function getConsolidatedByCustomer($customer_code)
    {
        $result = $this->trackingService->getConsolidatedByCustomer($customer_code);
        return $this->sendResponse($result, 'Tracking consolidated retrieved successfully');
    }

    public function getListTrackingByCustomer(Request $request)
    {
        $input = $request->all();
        $list_tracking_id = explode(";",$input['group_tracking']);
        $customer_code = $input['customer_code'];
        $result = $this->trackingService->getListTrackingByCustomer($customer_code, $list_tracking_id);
        return $this->sendResponse($result, 'Tracking list retrieved successfully');
    }

    public function getCarrier($tracking_number)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.trackingmore.com/v2/carriers/detect",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                
                "cache-control: no-cache",
                // 'Trackingmore-Api-Key:60e1b250-0bdf-4b44-a052-bbd84b1742ce'
                'Trackingmore-Api-Key:69985761-e168-4c55-b9f8-47f79bd6cbac'
            ),
            CURLOPT_POSTFIELDS => '{"tracking_number":"'.$tracking_number.'"}',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        if(isset($response['data'][0])){
            $result = $response['data'][0];
        } else {
            $result = array('name' => 'Other','code' => 'other');
        }
        $err = curl_error($curl);
        curl_close($curl);
        return $this->sendResponse($result,'Carrier retrieved success');
    }
    public function getListConsolidatedById($id)
    {
        $result = $this->trackingService->getListConsolidatedById($id);
        return $this->sendResponse($result, 'Get consolidated successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

}
