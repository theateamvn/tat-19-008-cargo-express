<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateRecipientAPIRequest;
use App\Http\Requests\API\UpdateRecipientAPIRequest;
use App\Criteria\ListRecipientCriteria;
use App\Models\Recipient;
use App\Repositories\RecipientRepository;
use App\Services\RecipientService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class RecipientAPIController extends AppBaseController
{
    private $recipientRepository;

    /**
     * @var UserService
     */
    private $recipientService;

    public function __construct(RecipientRepository $recipientRepo, RecipientService $recipientService)
    {
        $this->recipientRepository = $recipientRepo;
        $this->recipientService = $recipientService;
    }

    public function pagination(Request $request)
    {
        $this->recipientRepository->pushCriteria(new ListRecipientCriteria($request));
        $this->recipientRepository->pushCriteria(new RequestCriteria($request));
        $this->recipientRepository->pushCriteria(new LimitOffsetCriteria($request));
        $recipients = $this->recipientRepository->paginate();
        return $this->sendResponse($recipients->toArray(), 'Recipient retrieved successfully');
    }

    public function store(CreateRecipientAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['ip'] = $request->ip();
        $input['updated_by'] = $userId;
        $recipient = $this->recipientService->create($input);
        /*$input_ = array( 'contact_code' => $input['customer_code'].'-'.$recipient['id']);
        $recipient_update = $this->recipientRepository->update($input_, $recipient['id']);*/
        return $this->sendResponse($recipient->toArray(), 'Recipient saved successfully');
    }
    // function update
    public function update($id, UpdateRecipientAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $recipient = $this->recipientRepository->findWithoutFail($id);

        if (empty($recipient)) {
            return $this->sendError('customer not found');
        }

        $input['updated_by'] = $userId;
        $recipient = $this->recipientRepository->update($input, $id);

        return $this->sendResponse($recipient->toArray(), 'User updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $recipient = $this->recipientRepository->findWithoutFail($id);

        if (empty($recipient)) {
            return $this->sendResponse('Recipient not found');
        }
        $recipient->delete();
        return $this->sendResponse($id, 'Recipient deleted successfully');
    }
    public function load_customer()
    {
        $customer_list = $this->recipientService->getCustomerList();
        return $this->sendResponse($customer_list, 'Customer list get successfully');
    }
    public function load_country()
    {
        $list = $this->recipientService->getCountryList();
        return $this->sendResponse($list, 'Countries list get successfully');
    }
    public function load_city($id)
    {
        $list = $this->recipientService->getCityList($id);
        return $this->sendResponse($list, 'Cities list get successfully');
    }

    public function loadRecipientByCustomer($id)
    {
        $list = $this->recipientService->loadRecipientByCustomer($id);
        return $this->sendResponse($list, 'Recipient list get successfully');
    }

    public function createByCustomer(CreateRecipientAPIRequest $request)
    {
        $input = $request->all();
        if(count($input)) {
            $token = $input["_token"];
            $check_csrf = $this->validCsrf($token, $request);
            if($check_csrf) {
                $input['created_by'] = "-1";
                $input['ip'] = $request->ip();
                $input['updated_by'] = "-1";
                $recipient = $this->recipientService->create($input);
                return $this->sendResponse($recipient->toArray(), 'Recipient saved successfully');
            } else {
                return $this->sendError('Not found T!');
            }
        } else {
            return $this->sendError('Not found');
        }
    }

    public function updateByCustomer($id, UpdateRecipientAPIRequest $request)
    {
        $userId = '-1';
        $input = $request->all();
        if(count($input)) {
            $token = $input["_token"];
            $check_csrf = $this->validCsrf($token, $request);
            if($check_csrf) {
                $recipient = $this->recipientRepository->findWithoutFail($id);

                if (empty($recipient)) {
                    return $this->sendError('customer not found');
                }

                $input['updated_by'] = $userId;
                $recipient = $this->recipientRepository->update($input, $id);

                return $this->sendResponse($recipient->toArray(), 'User updated successfully');
            } else {
                return $this->sendError('Not found T!');
            }
        } else {
            return $this->sendError('Not found');
        }
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

    protected function validCsrf($token, $request)
    {
        $isCsrf = false;
        $isXsrf = false;
        if($csrf = $token) {
            $isCsrf = hash_equals(
                $token, (string) $request->header('X-CSRF-TOKEN')
            );
            if($request->hasHeader('X-XSRF-TOKEN')) {
                $isXsrf = $this->encrypter->decrypt($request->header('X-XSRF-TOKEN')) === $csrf;
            }
        }
        return $isCsrf || $isXsrf;
    }
}
