<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreUserAPIRequest;
use App\Http\Requests\API\UpdateStoreUserAPIRequest;
use App\Models\StoreUser;
use App\Repositories\StoreUserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StoreUserController
 * @package App\Http\Controllers\API
 */

class StoreUserAPIController extends AppBaseController
{
    /** @var  StoreUserRepository */
    private $storeUserRepository;

    public function __construct(StoreUserRepository $storeUserRepo)
    {
        $this->storeUserRepository = $storeUserRepo;
    }

    /**
     * Display a listing of the StoreUser.
     * GET|HEAD /storeUsers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeUserRepository->pushCriteria(new RequestCriteria($request));
        $this->storeUserRepository->pushCriteria(new LimitOffsetCriteria($request));
        $storeUsers = $this->storeUserRepository->all();

        return $this->sendResponse($storeUsers->toArray(), 'Store Users retrieved successfully');
    }

    /**
     * Store a newly created StoreUser in storage.
     * POST /storeUsers
     *
     * @param CreateStoreUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreUserAPIRequest $request)
    {
        $input = $request->all();

        $storeUsers = $this->storeUserRepository->create($input);

        return $this->sendResponse($storeUsers->toArray(), 'Store User saved successfully');
    }

    /**
     * Display the specified StoreUser.
     * GET|HEAD /storeUsers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StoreUser $storeUser */
        $storeUser = $this->storeUserRepository->findWithoutFail($id);

        if (empty($storeUser)) {
            return $this->sendError('Store User not found');
        }

        return $this->sendResponse($storeUser->toArray(), 'Store User retrieved successfully');
    }

    /**
     * Update the specified StoreUser in storage.
     * PUT/PATCH /storeUsers/{id}
     *
     * @param  int $id
     * @param UpdateStoreUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var StoreUser $storeUser */
        $storeUser = $this->storeUserRepository->findWithoutFail($id);

        if (empty($storeUser)) {
            return $this->sendError('Store User not found');
        }

        $storeUser = $this->storeUserRepository->update($input, $id);

        return $this->sendResponse($storeUser->toArray(), 'StoreUser updated successfully');
    }

    /**
     * Remove the specified StoreUser from storage.
     * DELETE /storeUsers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StoreUser $storeUser */
        $storeUser = $this->storeUserRepository->findWithoutFail($id);

        if (empty($storeUser)) {
            return $this->sendError('Store User not found');
        }

        $storeUser->delete();

        return $this->sendResponse($id, 'Store User deleted successfully');
    }
}
