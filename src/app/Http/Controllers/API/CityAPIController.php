<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCityAPIRequest;
use App\Http\Requests\API\UpdateCityAPIRequest;
use App\Http\Requests\API\CreateDistrictAPIRequest;
use App\Http\Requests\API\UpdateDistrictAPIRequest;
use App\Models\City;
use App\Models\District;
use App\Repositories\CityRepository;
use App\Repositories\DistrictRepository;
use App\Services\CityService;
use App\Services\DistrictService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\CitiesCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CityAPIController extends AppBaseController
{
    /** @var  CityRepository */
    private $cityRepository;
    /** @var  DistrictRepository */
    private $districtRepository;

    /**
     * @var CityService
     */
    private $cityService;
    /**
     * @var DistrictService
     */
    private $districtService;

    public function __construct(CityRepository $cityRepo, DistrictRepository $districtRepo,CityService $cityService, DistrictService $districtService)
    {
        $this->cityRepository = $cityRepo;
        $this->districtRepository = $districtRepo;
        $this->cityService = $cityService;
        $this->districtService = $districtService;
    }

    public function pagination(Request $request)
    {
        $this->cityRepository->pushCriteria(new RequestCriteria($request));
        $this->cityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $city = $this->cityRepository->paginate();
        return $this->sendResponse($city->toArray(), 'City retrieved successfully');
    }

    public function index()
    {
        $cities = $this->cityService->getListCities();
        return $this->sendResponse($cities, 'City retrieved successfully');
    }

    public function store(CreateCityAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->cityService->create($input);
        return $this->sendResponse($result, 'City created successfully');
    }

    public function update($id, UpdateCityAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->cityRepository->update($input, $id);
        return $this->sendResponse($result, 'City updated successfully');
    }

    public function destroy($id)
    {
        $city = $this->cityRepository->findWithoutFail($id);

        if (empty($city)) {
            return $this->sendResponse('City not found');
        }
       $result = $this->cityRepository->delete($id);
       return $this->sendResponse($result, 'City deleted successfully');
    }

    public function getDistrictListById($id)
    {
        $result = $this->districtService->getDistrictListById($id);
        return $this->sendResponse($result, 'District retrieved successfully'); 
    }

}
