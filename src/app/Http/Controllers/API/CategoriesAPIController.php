<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByUserStoreCriteria;
use App\Http\Controllers\AppBaseController;
use App\Repositories\CategoriesRepository;
use App\Criteria\CategoriesCriteria;
use App\Services\CategoriesService;
use App\Http\Requests\API\CreateCategoriesAPIRequest;
use App\Http\Requests\API\UpdateCategoriesAPIRequest;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;


class CategoriesAPIController extends AppBaseController
{

    private $CategoriesRepository;

    private $CategoriesService;

    public function __construct(CategoriesRepository $categoriesRepo, CategoriesService $CategoriesService)
    {
        $this->CategoriesRepository = $categoriesRepo;
        $this->CategoriesService = $CategoriesService;
    }

    public function index(Request $request)
    {
        $this->CategoriesRepository->pushCriteria(new RequestCriteria($request));
        $this->CategoriesRepository->pushCriteria(new CategoriesCriteria($request));
        $this->CategoriesRepository->pushCriteria(new LimitOffsetCriteria($request));
        //$this->CategoriesRepository->pushCriteria(new ByUserStoreCriteria());
        $reviews = $this->CategoriesRepository->all();

        return $this->sendResponse($reviews->toArray(), 'Categories retrieved successfully');
    }

    public function store(CreateCategoriesAPIRequest $request)
    {
        $input = $request->all();

        $reviews = $this->CategoriesRepository->create($input);

        return $this->sendResponse($reviews->toArray(), 'Categories saved successfully');
    }

    public function show($id)
    {
        $review = $this->CategoriesRepository->findWithoutFail($id);

        if (empty($review)) {
            return $this->sendError('Categories not found');
        }

        return $this->sendResponse($review->toArray(), 'Categories retrieved successfully');
    }

    public function update($id, UpdateCategoriesAPIRequest $request)
    {
        $input = $request->all();

        $review = $this->CategoriesRepository->findWithoutFail($id);

        if (empty($review)) {
            return $this->sendError('Categories not found');
        }

        $review = $this->CategoriesRepository->update($input, $id);

        return $this->sendResponse($review->toArray(), 'Categories updated successfully');
    }

    public function destroy($id)
    {
        $review = $this->CategoriesRepository->findWithoutFail($id);

        if (empty($review)) {
            return $this->sendError('Categories not found');
        }

        $review->delete();

        return $this->sendResponse($id, 'Categories deleted successfully');
    }

    public function all()
    {

    }

    public function get(){

    }
}
