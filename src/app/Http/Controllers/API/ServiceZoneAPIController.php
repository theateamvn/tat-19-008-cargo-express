<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateServiceZoneAPIRequest;
use App\Http\Requests\API\UpdateServiceZoneAPIRequest;
use App\Models\ServiceZone;
use App\Repositories\ServiceZoneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class ServiceZoneAPIController extends AppBaseController
{
    /** @var  ServiceZoneRepository */
    private $serviceZoneRepository;

    public function __construct(ServiceZoneRepository $serviceZoneRepo)
    {
        $this->serviceZoneRepository = $serviceZoneRepo;
    }

    public function pagination(Request $request)
    {
        $this->serviceZoneRepository->pushCriteria(new RequestCriteria($request));
        $this->serviceZoneRepository->pushCriteria(new LimitOffsetCriteria($request));
        $serviceZone = $this->serviceZoneRepository->paginate();
        return $this->sendResponse($serviceZone->toArray(), 'ServiceZone retrieved successfully');
    }

    public function store(CreateCityAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->cityService->create($input);
        return $this->sendResponse($result, 'ServiceZone created successfully');
    }

    public function update($id, UpdateCityAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->serviceZoneRepository->update($input, $id);
        return $this->sendResponse($result, 'ServiceZone updated successfully');
    }

    public function destroy($id)
    {
        $city = $this->serviceZoneRepository->findWithoutFail($id);

        if (empty($city)) {
            return $this->sendResponse('ServiceZone not found');
        }
       $result = $this->serviceZoneRepository->delete($id);
       return $this->sendResponse($result, 'ServiceZone deleted successfully');
    }

}
