<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateSenderAPIRequest;
use App\Http\Requests\API\UpdateSenderAPIRequest;
use App\Criteria\ListSenderCriteria;
use App\Models\Sender;
use App\Repositories\SenderRepository;
use App\Services\SenderService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class SenderAPIController extends AppBaseController
{
    /** @var  SenderRepository */
    private $senderRepository;

    /**
     * @var UserService
     */
    private $senderService;

    public function __construct(SenderRepository $senderRepo, SenderService $senderService)
    {
        $this->senderRepository = $senderRepo;
        $this->senderService = $senderService;
    }

    public function pagination(Request $request)
    {
        $this->senderRepository->pushCriteria(new ListSenderCriteria($request));
        $this->senderRepository->pushCriteria(new RequestCriteria($request));
        $this->senderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $senders = $this->senderRepository->paginate();
        return $this->sendResponse($senders->toArray(), 'Sender retrieved successfully');
    }

    public function store(CreateSenderAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['ip'] = $request->ip();
        $input['updated_by'] = $userId;
        $sender = $this->senderService->create($input);
        /*$input_ = array( 'contact_code' => $input['customer_code'].'-'.$sender['id']);
        $sender_update = $this->senderRepository->update($input_, $sender['id']);*/
        return $this->sendResponse($sender->toArray(), 'Sender saved successfully');
    }
    // function update
    public function update($id, UpdateSenderAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $sender = $this->senderRepository->findWithoutFail($id);

        if (empty($sender)) {
            return $this->sendError('customer not found');
        }

        $input['updated_by'] = $userId;
        $sender = $this->senderRepository->update($input, $id);

        return $this->sendResponse($sender->toArray(), 'User updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $sender = $this->senderRepository->findWithoutFail($id);

        if (empty($sender)) {
            return $this->sendError('Sender not found');
        }
        $sender->delete();
        return $this->sendResponse($id, 'Sender deleted successfully');
    }
    public function load_customer()
    {
        $customer_list = $this->senderService->getCustomerList();
        return $this->sendResponse($customer_list, 'Customer list get successfully');
    }
    public function load_country()
    {
        $list = $this->senderService->getCountryList();
        return $this->sendResponse($list, 'Countries list get successfully');
    }
    public function load_city($id)
    {
        $list = $this->senderService->getCityList($id);
        return $this->sendResponse($list, 'Cities list get successfully');
    }
    public function load_district($id)
    {
        $list = $this->senderService->getDistrictList($id);
        return $this->sendResponse($list, 'District list get successfully');
    }
    public function load_city_()
    {
        $id = -1;
        $list = $this->senderService->getCityList($id);
        return $this->sendResponse($list, 'Cities list get successfully');
    }
    public function load_city_detail($id)
    {
        $list = $this->senderService->getCityDetail($id);
        return $this->sendResponse($list, 'Cities detal get successfully');
    }



    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

}
