<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Http\Requests\API\UpdateCustomerAPIRequest;
use App\Http\Requests\API\UploadCustomerIDRequest;
use App\Models\Customer;
use App\Repositories\CustomerRepository;
use App\Services\CustomerService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\CustomerListByTypeCriteria;
use Response;
use Storage;
use App\CustomClass\SimpleImage;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CustomerAPIController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    /**
     * @var CustomerService
     */
    private $customerService;

    public function __construct(CustomerRepository $customerRepo, CustomerService $customerService)
    {
        $this->customerRepository = $customerRepo;
        $this->customerService = $customerService;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->customerRepository->pushCriteria(new CustomerListByTypeCriteria());
        $this->customerRepository->pushCriteria(new RequestCriteria($request));
        $this->customerRepository->pushCriteria(new LimitOffsetCriteria($request));

        return $this->sendResponse($customer->toArray(), 'Customers retrieved successfully');
    }
    public function pagination(Request $request)
    {
        $this->customerRepository->pushCriteria(new CustomerListByTypeCriteria());
        $this->customerRepository->pushCriteria(new RequestCriteria($request));
        $this->customerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $customers = $this->customerRepository->paginate($perPage = $request['per_page']);
        return $this->sendResponse($customers->toArray(), 'Customer retrieved successfully');
    }
    // function create new
    public function store(CreateCustomerAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['ip'] = $request->ip();
        $input['updated_by'] = $userId;
        $customer = $this->customerService->create($input);
        // create customer code
        /* if($input['type_customer'] == 1 && !$input['customer_code']){
           // if($input['type_customer'] == 1 && !$input['type_customer']){
            $code = $this->generateCustomerCodeRemote();
        }
        if($input['type_customer'] == 2 && !$input['customer_code']){
            $code = $this->generateCustomerCodeAgent();
        }
        if($input['type_customer'] == 0 && !$input['customer_code']){
            $type = "W";
            $number = mt_rand(100, 999);
            $code = $type.$customer['id'].$number;
        }
        $input_ = array( 'customer_code' => $code);
        $this->customerRepository->update($input_, $customer['id']);*/
        return $this->sendResponse($customer->toArray(), 'Customer saved successfully');
    }
    public function showCustomerCode($type_customer){
        // create customer code
        $code = "";
        if($type_customer == 1){
            $code = $this->generateCustomerCodeRemoteAuto();
        }
        if($type_customer == 2){
            $code = $this->generateCustomerCodeAgentAuto();
        }
        if($type_customer == 0){
            $code = $this->generateCustomerCodeWAuto();
        }
        return $this->sendResponse($code, 'Customer code successfully');
    }
    private static function customerCodeExists($code) {
        return Customer::whereCustomerCode($code)->exists();
    }
    public function generateCustomerCodeRemoteAuto() {
        // get total customer by customer type
        $number = $this->customerService->getTotalCustomerByType(1);
        $number = $number + 1;
        if($number < 100){
            $number = sprintf("%03s", $number);
        }
        $seed = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
        $seed = str_shuffle($seed);
        $code = substr($seed,0,1).$number;
        if (static::customerCodeExists($code)) {
            return static::generateCustomerCodeNumber($code);
        }
        return $code;
    }
    private static function generateCustomerCodeRemote() {
        $number = mt_rand(100, 999);
        $seed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $seed = str_shuffle($seed);
        $code = substr($seed,0,1).$number;
        if (static::customerCodeExists($code)) {
            return static::generateCustomerCodeNumber($code);
        }
        return $code;
    }
    public function generateCustomerCodeAgentAuto() {
        // get total customer by customer type
        $number = $this->customerService->getTotalCustomerByType(2);
        $number = $number + 1;
        if($number < 100){
            $number = sprintf("%03s", $number);
        }
        $seed = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
        $seed = str_shuffle($seed);
        $code = substr($seed,0,2).$number;
        if (static::customerCodeExists($code)) {
            return static::generateCustomerCodeAgent($code);
        }
        return $code;
    }
    private static function generateCustomerCodeAgent() {
        $seed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $seed = str_shuffle($seed);
        $code = substr($seed,0,2);
        if (static::customerCodeExists($code)) {
            return static::generateCustomerCodeAgent($code);
        }
        return $code;
    }
    public function generateCustomerCodeWAuto() {
        $type = "W";
        // get total customer by customer type
        $number = $this->customerService->getTotalCustomerByType(0);
        $number = $number + 1;
        if($number < 100){
            $number = sprintf("%03s", $number);
        }
        $code   = $type.$number;
        if (static::customerCodeExists($code)) {
            return static::generateCustomerCodeW($code);
        }
        return $code;
    }
    private static function generateCustomerCodeW() {
        $type = "W";
        $number = mt_rand(0, 9999);
        if($number < 100){
            $number = sprintf("%03s", $number);
        }
        $code = $type.$number;
        if (static::customerCodeExists($code)) {
            return static::generateCustomerCodeW($code);
        }
        return $code;
    }
    // function update
    public function update($id, UpdateCustomerAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            return $this->sendError('customer not found');
        }
        // $cus_sender = $this->customerService->customerCreatedSender($id, $input);
        // if($cus_sender){
        //     return $this->sendError('Customer created sender or reciept');
        // }

        $input['updated_by'] = $userId;
        $customer = $this->customerRepository->update($input, $id);

        return $this->sendResponse($customer->toArray(), 'User updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer->delete();
        return $this->sendResponse($id, 'Customer deleted successfully');
    }
    public function showCitiesZipcode($zipcode){
        $cities_detail = $this->customerService->getCityByZipcode($zipcode);
        return $this->sendResponse($cities_detail, 'Customer deleted successfully');
    }
    public function customerUploadId(UploadCustomerIDRequest $request)
    {
        $id = $request->get('id');
        /** @var Store $store */
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            return $this->sendError('Store not found');
        }

        $path = $request->file('file')->store('customer_id', 'public');
        $customer->customer_upload_id = Storage::url($path);
        
        $customer->save();
        return $this->sendResponse($customer, 'Upload redirect background successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

    public function getCustomerByCode($code)
    {
        $customer = $this->customerService->getCustomerByCode($code);
        //var_dump($customer);
        if(count($customer) <= 0 ){
            return $this->sendError("Customer not found",400);
        }
        return $this->sendResponse($customer, 'Customer retrieved successfully');
    }
    public function getCustomerByPhone_bk($code)
    {
        $customer = $this->customerService->getCustomerByPhone($code);
        if(empty($customer)){
            return $this->sendError("Customer not found",400);
        }
        return $this->sendResponse($customer, 'Customer retrieved successfully');
    }
    public function getCustomerByPhone($code)
    {
        $customer = $this->customerService->getCustomerByPhone($code);
        if(empty($customer)){
            return $this->sendError("Customer not found",400);
        }
        return $this->sendResponse($customer, 'Customer retrieved successfully');
    }
    public function customerLogin(Request $request)
    {
        $input = $request->all();
        $user = $input['user'];
        $customer = $this->customerService->customerLogin($user);
        if(count($customer) < 1 ){
            return $this->sendError('Customer not found. Please try again.',400);
        }
        return $this->sendResponse($customer, 'Login retrived');
    }

    public function check_customer_code($code)
    {
        if (static::customerCodeExists($code)) {
            return $this->sendError('Customer Code is Existed! Please try other code.');
        }
        return $this->sendResponse($code, 'RequestCustomer deleted successfully');
    }

}
