<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRequestLabelPackageAPIRequest;
use App\Http\Requests\API\UpdateRequestLabelPackageAPIRequest;
use App\Models\RequestLabelPackage;
use App\Repositories\RequestLabelPackageRepository;
use App\Services\RequestLabelPackageService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\RequestLabelPackageCriteria;
// use Ptondereau\LaravelUpsApi\Facades;
//use Exception;

use Response;
use Stores;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class RequestLabelPackageAPIController extends AppBaseController
{
    /** @var  RequestLabelPackageRepository */
    private $requestLabelpackageRepository;

    /** @var RequestLabelPackageService */
    private $requestLabelpackageService;


    public function __construct(RequestLabelPackageRepository $requestLabelPackageRepo, RequestLabelPackageService $requestLabelPackageService)
    {
        $this->requestLabelpackageRepository = $requestLabelPackageRepo;
        $this->requestLabelpackageService = $requestLabelPackageService;
    }

    public function pagination(Request $request)
    {
        $this->requestLabelpackageRepository->pushCriteria(new RequestLabelPackageCriteria($request));
        $this->requestLabelpackageRepository->pushCriteria(new RequestCriteria($request));
        $this->requestLabelpackageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $RequestLabel = $this->requestLabelpackageRepository->paginate();
        return $this->sendResponse($RequestLabel->toArray(), 'RequestLabel package retrieved successfully');
    }

    public function index()
    {
        return $this->sendResponse('', 'RequestLabel package retrieved successfully');
    }

    public function store(CreateRequestLabelPackageAPIRequest $request)
    {
        $input = $request->all();
        foreach ($input as $value) {
            $this->requestLabelpackageService->create($value);
        }
        return $this->sendResponse($input, 'RequestLabel package created successfully');
    }

    public function update($id, UpdateRequestLabelPackageAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->requestLabelpackageRepository->update($input, $id);
        return $this->sendResponse($result, 'RequestLabel package updated successfully');
    }

    public function destroy($id)
    {
        $RequestLabel = $this->requestLabelpackageRepository->findWithoutFail($id);

        if (empty($RequestLabel)) {
            return $this->sendResponse('', 'RequestLabel package not found');
        }
        $result = $this->requestLabelpackageRepository->delete($id);
        return $this->sendResponse($result, 'RequestLabel package deleted successfully');
    }

    public function rateUPS(CreateRequestLabelPackageAPIRequest $request)
    {
        $packages = $request->all();
        $list_rate = array();
        $rate = new \Ups\RateTimeInTransit('AD7A19F242131A95', 'texpresscargo', 'Chuv!tcon511312');
        $shipment = new \Ups\Entity\Shipment();
        foreach ($packages as $value) {
            $state = isset($value['state']) ?  $value['state'] : '';
            $city = isset($value['city']) ?  $value['city'] : '';
            $description = isset($value['description']) ?  $value['description'] : '';
            $saved = isset($value['saved']) ? $value['saved'] : '';
            $zipcode = $value['zipcode'];
            $weight = $value['weight'];
            $height = $value['height'];
            $width = $value['width'];
            $length = $value['length']; 
            $weight_unit = $value['weight_unit'];
            $send_date = "";
            $date = explode("-", $value['send_date']);
            foreach ($date as $d) {
                $send_date .= $d;
            }
            $service_option = $value['service_option'];
            // check save or get rate
            if (!isset($packages[0]['saved'])) {
                // get thông tin của người gửi
                $address = "test 123";
                $sender_address = "abc st";
                $sender_city = "GRAND RAPIDS";
                $sender_province = "MI";
                $sender_name = "Nguyễn ánh";
                $sender_email = "";
                $sender_phone = "6162024320";
                $sender_description = "Bao gồm 8 túi quà!";
                $return = true;
                $return_id = '123';
                $order_id = '123';
                $zipcode = '49548';
                // Set shipper
                $shipper = $shipment->getShipper();
                //$shipper->setShipperNumber('123');
                $shipper->setName('SAIGON TRAVEL+CARGO SERVICES');
                $shipper->setAttentionName('NV-THONG VU');
                $shipperAddress = $shipper->getAddress();
                $shipperAddress->setAddressLine1('4219 DIVISION AVE');
                $shipperAddress->setPostalCode('49548');
                $shipperAddress->setCity('GRAND RAPIDS');
                $shipperAddress->setStateProvinceCode('MI'); // required in US
                $shipperAddress->setCountryCode('US');
                $shipper->setAddress($shipperAddress);
                $shipper->setEmailAddress('tungtnguyen@outlook.com'); 
                $shipper->setPhoneNumber('6162024320');
                $shipment->setShipper($shipper);
                // To address
                $address = new \Ups\Entity\Address();
                $address->setAddressLine1('14936 Dillow St');
                $address->setPostalCode('92683');
                $address->setCity('WESTMINSTER');
                $address->setStateProvinceCode('CA');  // Required in US
                $address->setCountryCode('US');
                $shipTo = new \Ups\Entity\ShipTo();
                $shipTo->setAddress($address);
                $shipTo->setCompanyName('T Express Cargo');
                $shipTo->setAttentionName('Tung Nguyen');
                $shipTo->setEmailAddress('tungtnguyen@outlook.com'); 
                $shipTo->setPhoneNumber('6265866044');
                $shipment->setShipTo($shipTo);
                // From address
                $address = new \Ups\Entity\Address();
                $address->setAddressLine1($sender_address);
                $address->setPostalCode($zipcode);
                $address->setCity($sender_city);
                $address->setStateProvinceCode($sender_province);  
                $address->setCountryCode('US');
                $shipFrom = new \Ups\Entity\ShipFrom();
                $shipFrom->setAddress($address);
                $shipFrom->setName($sender_name);
                $shipFrom->setAttentionName($shipFrom->getName());
                $shipFrom->setCompanyName($shipFrom->getName());
                $shipFrom->setEmailAddress($sender_email);
                $shipFrom->setPhoneNumber($sender_phone);
                $shipment->setShipFrom($shipFrom);
                // Sold to
                
                $address = new \Ups\Entity\Address();
                $address->setAddressLine1('14936 Dillow St');
                $address->setPostalCode('92683');
                $address->setCity('WESTMINSTER');
                $address->setCountryCode('US');
                $address->setStateProvinceCode('CA');
                $soldTo = new \Ups\Entity\SoldTo;
                $soldTo->setAddress($address);
                $soldTo->setAttentionName('T Express Cargo');
                $soldTo->setCompanyName($soldTo->getAttentionName());
                $soldTo->setEmailAddress('tungtnguyen@outlook.com');
                $soldTo->setPhoneNumber('6265866044');
                $shipment->setSoldTo($soldTo);
                // Set service
                $service = new \Ups\Entity\Service;
                $service->setCode($service_option);
                $service->setDescription($service->getName());
                $shipment->setService($service);
                // Mark as a return (if return)
                if ($return) {
                    $returnService = new \Ups\Entity\ReturnService;
                    $returnService->setCode(\Ups\Entity\ReturnService::PRINT_RETURN_LABEL_PRL);
                    $shipment->setReturnService($returnService);
                }
                // Set description
                $shipment->setDescription($sender_description);
                // Add package
                $package = new \Ups\Entity\Package();
                $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
                $package->getPackageWeight()->setWeight($weight);
                $weightUnit = new \Ups\Entity\UnitOfMeasurement;
                if ($weight_unit == 'LBS') {
                    $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
                } else {
                    $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_KGS);
                }
                $package->getPackageWeight()->setUnitOfMeasurement($weightUnit);
                // Set Package Service Options
                $packageServiceOptions = new \Ups\Entity\PackageServiceOptions();
                $packageServiceOptions->setShipperReleaseIndicator(true);
                $package->setPackageServiceOptions($packageServiceOptions);

                // Set dimensions
                $dimensions = new \Ups\Entity\Dimensions();
                $dimensions->setHeight($height);
                $dimensions->setWidth($width);
                $dimensions->setLength($length);
                $unit = new \Ups\Entity\UnitOfMeasurement;
                if ($weight_unit == 'LBS') {
                    $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);
                } else {
                    $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_CM);
                }
                $dimensions->setUnitOfMeasurement($unit);
                $package->setDimensions($dimensions);
                // Add descriptions because it is a package
                $package->setDescription($sender_description);
                // Add this package
                $shipment->addPackage($package);
                
                // Set Reference Number
                $referenceNumber = new \Ups\Entity\ReferenceNumber;
                if ($return) {
                    $referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_RETURN_AUTHORIZATION_NUMBER);
                    $referenceNumber->setValue($return_id);
                } else {
                    $referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_INVOICE_NUMBER);
                    $referenceNumber->setValue($order_id);
                }
                $shipment->setReferenceNumber($referenceNumber);
                
                // Set payment information
                $shipment->setPaymentInformation(new \Ups\Entity\PaymentInformation('prepaid', (object)array('AccountNumber' => '87FR61')));
                // Ask for negotiated rates (optional)
                $rateInformation = new \Ups\Entity\RateInformation;
                $rateInformation->setNegotiatedRatesIndicator(1);
                $shipment->setRateInformation($rateInformation);
                //var_dump($rate->getRate($shipment)->RatedShipment[0]->RatedPackage[0]->TotalCharges->MonetaryValue);die;
                $api = new \Ups\Shipping('AD7A19F242131A95', 'texpresscargo', 'Chuv!tcon511312'); 
                //var_dump($rate->getRate($shipment)->RatedShipment[0]->RatedPackage[0]->TotalCharges->MonetaryValue);die;
                $confirm = $api->confirm(\Ups\Shipping::REQ_NONVALIDATE, $shipment);
                
                print_r($confirm);die;// Confirm holds the digest you need to accept the result
                
                if ($confirm) {
                    $accept = $api->accept($confirm->ShipmentDigest);
                    var_dump($accept); // Accept holds the label and additional information
                }
                //$value['shippingRate'] = $rate->getRateTimeInTransit($shipment)->RatedShipment[0]->RatedPackage[0]->TotalCharges->MonetaryValue;
                
                
            } else {
                try {
                    $shipperAddress = $shipment->getShipper()->getAddress();
                    $shipperAddress->setPostalCode($zipcode);

                    $address = new \Ups\Entity\Address();
                    $address->setPostalCode($zipcode);
                    $shipFrom = new \Ups\Entity\ShipFrom();
                    $shipFrom->setAddress($address);

                    $shipment->setShipFrom($shipFrom);

                    $shipTo = $shipment->getShipTo();
                    $shipTo->setCompanyName('T Express Cargo');
                    $shipToAddress = $shipTo->getAddress();
                    $shipToAddress->setPostalCode('92683');

                    $package = new \Ups\Entity\Package();
                    $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
                    $package->getPackageWeight()->setWeight($weight);

                    // if you need this (depends of the shipper country)
                    $weightUnit = new \Ups\Entity\UnitOfMeasurement;
                    $unit = new \Ups\Entity\UnitOfMeasurement;
                    if ($weight_unit == 'LBS') {
                        $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
                        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);
                    } else {
                        $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_KGS);
                        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_CM);
                    }
                    $package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

                    $dimensions = new \Ups\Entity\Dimensions();
                    $dimensions->setHeight($height);
                    $dimensions->setWidth($width);
                    $dimensions->setLength($length);

                    $dimensions->setUnitOfMeasurement($unit);
                    $package->setDimensions($dimensions);

                    $service = new \Ups\Entity\Service;
                    $service->setCode($service_option);
                    $service->setDescription($service->getName());
                    $shipment->setService($service);

                    $deliveryTimeInformation = new \Ups\Entity\DeliveryTimeInformation();
                    $deliveryTimeInformation->setPackageBillType(\Ups\Entity\DeliveryTimeInformation::PBT_NON_DOCUMENT);

                    $pickup = new \Ups\Entity\Pickup();
                    $pickup->setDate($send_date);
                    //$pickup->setTime("160000");
                    $deliveryTimeInformation->setPickup($pickup);
                    $shipment->setDeliveryTimeInformation($deliveryTimeInformation);

                    $shipment->addPackage($package);
                    var_dump($rate->getRateTimeInTransit($shipment));die;
                    $value['shippingRate'] = $rate->getRateTimeInTransit($shipment)->RatedShipment[0]->RatedPackage[0]->TotalCharges->MonetaryValue;
                    array_push($list_rate, $value);
                } catch (Exception $e) {
                    var_dump($e);
                }
            } // end else
        }
        return $this->sendResponse($list_rate, 'Rate package retrived successfully');;
    }

    public function rateUPS_edit(CreateRequestLabelPackageAPIRequest $request)
    {
        $packages = $request->all();
        $rate = new \Ups\Rate('0D783B38369B60D0', 'texpresscargo1', 'Chuv!tcon511312');
        $shipment = new \Ups\Entity\Shipment();
        $zipcode = $packages['zipcode'];
        $weight = $packages['weight'];
        $height = $packages['height'];
        $width = $packages['width'];
        $length = $packages['length'];
        $weight_unit = $packages['weight_unit'];
        $service_option = $packages['service_option'];
        try {
            $shipperAddress = $shipment->getShipper()->getAddress();
            $shipperAddress->setPostalCode($zipcode);

            $address = new \Ups\Entity\Address();
            $address->setPostalCode($zipcode);
            $shipFrom = new \Ups\Entity\ShipFrom();
            $shipFrom->setAddress($address);

            $shipment->setShipFrom($shipFrom);

            $shipTo = $shipment->getShipTo();
            $shipTo->setCompanyName('Test Ship To');
            $shipToAddress = $shipTo->getAddress();
            $shipToAddress->setPostalCode('92683');

            $package = new \Ups\Entity\Package();
            $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
            $package->getPackageWeight()->setWeight($weight);

            // if you need this (depends of the shipper country)
            $weightUnit = new \Ups\Entity\UnitOfMeasurement;
            $unit = new \Ups\Entity\UnitOfMeasurement;
            if ($weight_unit == 'LBS') {
                $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
                $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);
            } else {
                $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_KGS);
                $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_CM);
            }
            $package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

            $dimensions = new \Ups\Entity\Dimensions();
            $dimensions->setHeight($height);
            $dimensions->setWidth($width);
            $dimensions->setLength($length);

            $dimensions->setUnitOfMeasurement($unit);
            $package->setDimensions($dimensions);

            $service = new \Ups\Entity\Service;
            $service->setCode($service_option);
            $service->setDescription($service->getName());
            $shipment->setService($service);

            $shipment->addPackage($package);
            $packages['shippingRate'] = $rate->getRate($shipment)->RatedShipment[0]->RatedPackage[0]->TotalCharges->MonetaryValue;
        } catch (Exception $e) {
            var_dump($e);
        }
        return $this->sendResponse($packages, 'Rate package retrived successfully');;
    }
}
