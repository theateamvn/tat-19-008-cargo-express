<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateExportListAPIRequest;
use App\Http\Requests\API\UpdateExportListAPIRequest;
use App\Http\Requests\API\InviteListImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Criteria\ListExportListCriteria;
use App\Criteria\ListExportListImportCriteria;
use App\Models\ExportList;
use App\Repositories\ExportListRepository;
use App\Repositories\ExportListImportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Services\ExportListService;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class ExportListAPIController extends AppBaseController
{
    private $exportListRepository;
    private $exportListImportRepository;
    private $exportListService;

    public function __construct(ExportListRepository $exportListRepo, ExportListService $exportListService, ExportListImportRepository $exportListImportRepo)
    {
        $this->exportListService            = $exportListService;
        $this->exportListRepository         = $exportListRepo;
        $this->exportListImportRepository   = $exportListImportRepo;
    }

    // START export list 
    public function pagination(Request $request)
    {
        $this->exportListRepository->pushCriteria(new ListExportListCriteria($request));
        $this->exportListRepository->pushCriteria(new RequestCriteria($request));
        $this->exportListRepository->pushCriteria(new LimitOffsetCriteria($request));
        $exportList = $this->exportListRepository->paginate();
        return $this->sendResponse($exportList->toArray(), 'Export List retrieved successfully');
    }
    public function pagination_list(Request $request)
    {
        $this->exportListImportRepository->pushCriteria(new ListExportListImportCriteria($request));
        $this->exportListImportRepository->pushCriteria(new RequestCriteria($request));
        $this->exportListImportRepository->pushCriteria(new LimitOffsetCriteria($request));
        $exportList = $this->exportListImportRepository->paginate();
        return $this->sendResponse($exportList->toArray(), 'Export List retrieved successfully');
    }
    public function load_shipments()
    {
        $shipment_list = $this->exportListService->getShipmentList();
        return $this->sendResponse($shipment_list, 'Shipment list get successfully');
    }
    public function load_air()
    {
        $air_list = $this->exportListService->getAirList();
        return $this->sendResponse($air_list, 'Air list get successfully');
    }
    public function load_shipment_detail($id)
    {
        $shipment = $this->exportListService->getShipmentDetail($id);
        return $this->sendResponse($shipment, 'Shipment detail get successfully');
    }
    public function load_shipment_detail_code($code)
    {
        $shipment = $this->exportListService->getShipmentDetailCode($code);
        return $this->sendResponse($shipment, 'Shipment detail get successfully');
    }
    public function load_shipment_detail_code_scan($code, $exportlist_id = 0)
    {
        $shipment_check = $this->exportListService->getShipmentDetailCodeScan($code, $exportlist_id);
        return $this->sendResponse($shipment_check, 'Shipment detail get successfully');
    }
    public function store(CreateExportListAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $listBox                = $input['listBox']; 
        $listItemBox            = $input['listItemBox']; 
        $summaryW               = $input['summaryW']; 
        $summaryP               = $input['summaryP']; 
        $airport                = $input['airport']; 
        $awb_number               = $input['awb_number']; 
        $departure               = $input['departure']; 
        $destination               = $input['destination']; 
        $export_list_name       = $input['export_list_name'];
        $code_T_Express         = $input['code_T_Express'];
       // $awb                    = $input['awb']; 

        // set save export list
        $exportList['shipment_barcode_airport']         = $airport;
        $exportList['shipment_barcode_awb']             = $awb_number;
        $exportList['shipment_barcode_departure']       = $departure;
        $exportList['shipment_barcode_destination']     = $destination;
        $exportList['code_T_Express']     = $code_T_Express;
        $exportList['export_list_name']     = $export_list_name;
        $exportList['total_weight']     = $summaryW;
        $exportList['total_packages']   = $summaryP;
        $exportList['total_box']   = count($listBox) - 1;
        $exportList['updated_by'] = $userId;
        $exportList['ip'] = $request->ip();
        $exportList_ = $this->exportListService->create($exportList);
        $id = $exportList_->id;
        if(isset($awb)){
            // save awb
           // $this->exportListService->create_awb($awb, $id);
        }
        // exportlist fee
        $exportListItem = $this->exportListService->saveExportListItem($id, $listBox, $listItemBox);
        $result = [
            'id' => $id
        ];
        return $this->sendResponse($result, 'Export list saved successfully');
    }
    public function update($id, UpdateExportListAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        /** @var User $user */
        $export = $this->exportListRepository->findWithoutFail($id);

        if (empty($export)) {
            return $this->sendError('Export List not found');
        }
        //$input['updated_by'] = $userId;

        $userId = $this->getCurrentUser()->id;
        //$input = $request->all();
        $listBox                = $input['listBox']; 
        $listItemBox            = $input['listItemBox']; 
        $summaryW               = $input['summaryW']; 
        $summaryP               = $input['summaryP']; 
        $airport                = $input['airport']; 
        $awb_number             = $input['awb_number']; 
        $departure              = $input['departure']; 
        $destination            = $input['destination']; 
        $export_list_name       = $input['export_list_name'];
        $code_T_Express         = $input['code_T_Express'];
       // $awb                    = $input['awb']; 

        // set save export list
        $exportList['shipment_barcode_airport']         = $airport;
        $exportList['shipment_barcode_awb']             = $awb_number;
        $exportList['shipment_barcode_departure']       = $departure;
        $exportList['shipment_barcode_destination']     = $destination;
        $exportList['code_T_Express']     = $code_T_Express;
        $exportList['export_list_name']     = $export_list_name;
        $exportList['total_weight']     = $summaryW;
        $exportList['total_packages']   = $summaryP;
        $exportList['total_box']   = count($listBox) - 1;
        $exportList['updated_by'] = $userId;
        $exportList['ip'] = $request->ip();
        $this->exportListRepository->update($exportList,$id);
        $export_list_id = $id;
        if(isset($awb)){
            // save awb
           // $this->exportListService->create_awb($awb, $id);
        }
        // exportlist fee
        $exportListItem = $this->exportListService->saveExportListItem($id, $listBox, $listItemBox,$export_list_id);
        $result = [
            'id' => $id
        ];
        
        //$exportList = $this->exportListRepository->update($input, $id);

        return $this->sendResponse($result, 'Export List updated successfully');
    }

    public function changeStatus($id, Request $request)
    {
        $input = $request->all();
        $exportList = $this->exportListRepository->findWithoutFail($id);
        if (empty($exportList)) {
            return $this->sendError('Export List not found',400);
        }
        $this->exportListRepository->update($input, $id);
        return $this->sendResponse($id, 'Export List updated successfully');
    }

    public function destroy($id)
    {
        $exportList = $this->exportListRepository->findWithoutFail($id);

        if (empty($exportList)) {
            return $this->sendResponse('','Export List not found');
        }
        //delete export list
        $this->exportListRepository->delete($id);
        //delete export list item
        $this->exportListService->delExportListItem($id);
        return $this->sendResponse($id, 'Export List canceled successfully');
    }
    public function load_exportlist_edit($id)
    {
        $edit = $this->exportListService->getExportListDetail($id);
        return $this->sendResponse($edit, 'export list detail get successfully');
    }

    public function getExportEdit($id)
    {
        $edit = $this->exportListService->getExportEdit($id);
        return $this->sendResponse($edit, 'export list detail get successfully');
    }

    // import and add new
    public function import_add(Request $request)
    {
        $exportList_ = $this->exportListService->create_import($request->all());
        return $this->sendResponse($exportList_, 'Export List updated successfully');
    }
    public function import_upload(InviteListImportRequest $request)
    {
        $path = $request->file('file')->store('imports');
        $exportList = Excel::load(storage_path('app' . DIRECTORY_SEPARATOR . $path), function ($reader) {
        })->get();
        $result = $this->exportListService->create_import_upload($exportList->toArray());
        if ($result) {
            return $this->sendResponse($result, 'Import invites successfully');
        }
        return $this->sendError($result, 'Import invites fail');
    }
    public function import_edit(Request $request)
    {
        $exportList_ = $this->exportListService->edit_import($request->all());
        return $this->sendResponse($exportList_, 'Export List updated successfully');
    }
    public function import_delete($id)
    {
        $exportList = $this->exportListImportRepository->findWithoutFail($id);

        if (empty($exportList)) {
            return $this->sendResponse('Export List not found');
        }
        $exportList->delete();
        return $this->sendResponse($id, 'Export List delete successfully');
    }
    // END import and add new

    // END export list
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
