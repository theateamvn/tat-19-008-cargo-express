<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 11/02/2017
 * Time: 4:25 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\SendSupportRequest;
use App\Mail\SendSupportTicket;
use DB;
class CommonAPIController extends AppBaseController
{
    public function supportAndRequest(SendSupportRequest $request)
    {
        \Mail::to(config('pushmeup.setting.support_mail'))->send(new SendSupportTicket($request));
        return $this->sendResponse(null,'Your ticket has been sent');
    }
    public function infoSupport()
    {
        $infoPhone = $this->getConfigMasterSetting('support_phone')[0]->value;
        $infoEmail = $this->getConfigMasterSetting('support_email')[0]->value;
        $infoWeb = $this->getConfigMasterSetting('support_web')[0]->value;
        $info = array(
                    'phone' => $infoPhone,
                    'email' => $infoEmail,
                    'website' => $infoWeb
                );
        return $this->sendResponse($info, 'Info config retrieved successfully');
    }
    public function getConfigMasterSetting($key) {
        return DB::table('master_settings')->where('key', $key)->get();
    }
}