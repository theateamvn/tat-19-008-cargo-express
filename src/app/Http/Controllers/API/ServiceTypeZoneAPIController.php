<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateServiceTypeZoneAPIRequest;
use App\Http\Requests\API\UpdateServiceTypeZoneAPIRequest;
use App\Http\Requests\API\CreateServiceTypeAPIRequest;
use App\Http\Requests\API\UpdateServiceTypeAPIRequest;
use App\Models\ServiceTypeZone;
use App\Repositories\ServiceTypeZoneRepository;
use App\Repositories\ServiceTypeRepository;
use App\Services\ServiceTypeZoneService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Stores;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class ServiceTypeZoneAPIController extends AppBaseController
{
    /** @var  ServiceTypeZoneRepository */
    private $serviceTypeZoneRepository;

    private $serviceTypeRepository;

    /**
     * @var UserService
     */
    private $serviceTypeZoneService;

    public function __construct(ServiceTypeZoneRepository $serviceTypeZoneRepo, ServiceTypeZoneService $serviceTypeZoneService, ServiceTypeRepository $serviceTypeRepository)
    {
        $this->serviceTypeZoneRepository = $serviceTypeZoneRepo;
        $this->serviceTypeZoneService = $serviceTypeZoneService;
        $this->serviceTypeRepository = $serviceTypeRepository;

    }

    public function pagination(Request $request)
    {
        $this->serviceTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->serviceTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $serviceTypeZone = $this->serviceTypeRepository->paginate();
        return $this->sendResponse($serviceTypeZone->toArray(), 'service Type Zone retrieved successfully');
    }

    public function getListServiceTypeZone($id)
    {
        $result = $this->serviceTypeZoneService->getListServiceTypeZone($id);
        return $this->sendResponse($result, 'Get list service type zone successfully'); 
    }

    public function store(CreateServiceTypeZoneAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->serviceTypeZoneService->create($input);
        return $this->sendResponse($result, 'Service type zone created successfully');
    }

    public function update($id, UpdateServiceTypeZoneAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->serviceTypeZoneRepository->update($input, $id);
        return $this->sendResponse($result, 'Service type zone updated successfully');
    }

    public function destroy($id)
    {
        $serviceTypeZone = $this->serviceTypeZoneRepository->findWithoutFail($id);

        if (empty($serviceTypeZone)) {
            return $this->sendResponse('Service type zone not found');
        }
       $result = $this->serviceTypeZoneRepository->delete($id);
       return $this->sendResponse($result, 'Service type zone deleted successfully');
    }

    public function getListService()
    {
        $result = $this->serviceTypeZoneService->getListService();
        return $this->sendResponse($result, 'Get list service type zone successfully'); 
    }

    public function getListZone()
    {
        $result = $this->serviceTypeZoneService->getListZone();
        return $this->sendResponse($result, 'Get list service zone successfully'); 
    }

    public function createTypeZone(CreateServiceTypeAPIRequest $request)
    {
        $input = $request->all();
        $result = $this->serviceTypeZoneService->createServiceZone($input);
        return $this->sendResponse($result, 'Get list service created successfully'); 
    }

    public function deleteTypeZone($id)
    {
        $serviceType = $this->serviceTypeRepository->findWithoutFail($id);

        if (empty($serviceType)) {
            return $this->sendResponse('Service type not found');
        }
        $typeZone= array('status' => 0);
        $result = $this->serviceTypeRepository->update($typeZone, $id);
        return $this->sendResponse($result, 'Service type zone deleted successfully'); 
    }

}
