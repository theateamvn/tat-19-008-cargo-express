<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateActivityLogAPIRequest;
use App\Http\Requests\API\UpdateActivityLogAPIRequest;
use App\Models\ActivityLog;
use App\Repositories\ActivityLogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ActivityLogController
 * @package App\Http\Controllers\API
 */

class ActivityLogAPIController extends AppBaseController
{
    /** @var  ActivityLogRepository */
    private $activityLogRepository;

    public function __construct(ActivityLogRepository $activityLogRepo)
    {
        $this->activityLogRepository = $activityLogRepo;
    }

    /**
     * Display a listing of the ActivityLog.
     * GET|HEAD /activityLogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->activityLogRepository->pushCriteria(new RequestCriteria($request));
        $this->activityLogRepository->pushCriteria(new LimitOffsetCriteria($request));
        $activityLogs = $this->activityLogRepository->with('user')->paginate();
        return $this->sendResponse($activityLogs->toArray(), 'Activity Logs retrieved successfully');
    }

    /**
     * Store a newly created ActivityLog in storage.
     * POST /activityLogs
     *
     * @param CreateActivityLogAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityLogAPIRequest $request)
    {
        $input = $request->all();

        $activityLogs = $this->activityLogRepository->create($input);

        return $this->sendResponse($activityLogs->toArray(), 'Activity Log saved successfully');
    }

    /**
     * Display the specified ActivityLog.
     * GET|HEAD /activityLogs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ActivityLog $activityLog */
        $activityLog = $this->activityLogRepository->findWithoutFail($id);

        if (empty($activityLog)) {
            return $this->sendError('Activity Log not found');
        }

        return $this->sendResponse($activityLog->toArray(), 'Activity Log retrieved successfully');
    }

    /**
     * Update the specified ActivityLog in storage.
     * PUT/PATCH /activityLogs/{id}
     *
     * @param  int $id
     * @param UpdateActivityLogAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityLogAPIRequest $request)
    {
        $input = $request->all();

        /** @var ActivityLog $activityLog */
        $activityLog = $this->activityLogRepository->findWithoutFail($id);

        if (empty($activityLog)) {
            return $this->sendError('Activity Log not found');
        }

        $activityLog = $this->activityLogRepository->update($input, $id);

        return $this->sendResponse($activityLog->toArray(), 'ActivityLog updated successfully');
    }

    /**
     * Remove the specified ActivityLog from storage.
     * DELETE /activityLogs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ActivityLog $activityLog */
        $activityLog = $this->activityLogRepository->findWithoutFail($id);

        if (empty($activityLog)) {
            return $this->sendError('Activity Log not found');
        }

        $activityLog->delete();

        return $this->sendResponse($id, 'Activity Log deleted successfully');
    }
}
