<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByUserStoreCriteria;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateInviteAPIRequest;
use App\Http\Requests\API\InviteListImportRequest;
use App\Http\Requests\API\SendInviteRequest;
use App\Http\Requests\API\UpdateInviteAPIRequest;
use App\Models\Invite;
use App\Repositories\InviteRepository;
use App\Services\InviteService;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Maatwebsite\Excel\Facades\Excel;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Auth;
use Response;

/**
 * Class InviteController
 * @package App\Http\Controllers\API
 */
class InviteAPIController extends AppBaseController
{
    /** @var  InviteRepository */
    private $inviteRepository;

    private $inviteService;

    public function __construct(InviteRepository $inviteRepo, InviteService $inviteService)
    {
        $this->inviteRepository = $inviteRepo;
        $this->inviteService = $inviteService;
    }

    /**
     * Display a listing of the Invite.
     * GET|HEAD /invites
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inviteRepository->pushCriteria(new RequestCriteria($request));
        $this->inviteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $invites = $this->inviteRepository->all();

        return $this->sendResponse($invites->toArray(), 'Invites retrieved successfully');
    }

    /**
     * Store a newly created Invite in storage.
     * POST /invites
     *
     * @param CreateInviteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInviteAPIRequest $request)
    {
        $input = $request->all();
        // the a team edited
        // check exist data
        $checkExxist = $this->inviteService->checkExistInvite($input['store_id'], $input['phone'], $input['email']);
        if($checkExxist == 1 && $input['phone'] != '')
        {
            $getDetailInvite = $this->inviteService->getDetailByStoreIdAndPhone($input['store_id'], $input['phone']);
            if($input['email'] && $getDetailInvite[0]->id)
            {
                $userId = $this->getCurrentUser()->id;
                $input['updated_by'] = $userId;
                $invites = $this->inviteRepository->update($input, $getDetailInvite[0]->id);
                return $this->sendResponse($invites->toArray(), 'Invite saved successfully');
            }
            $data['data']    = $getDetailInvite;
            return $this->sendResponse($data['data'][0], 'Invite get successfully');
        } else if($checkExxist == 2 && $input['email'] != '') {
            $getDetailInvite = $this->inviteService->getDetailByStoreIdAndEmail($input['store_id'], $input['email']);
            if($input['phone'] && $getDetailInvite[0]->id)
            {
                $userId = $this->getCurrentUser()->id;
                $input['updated_by'] = $userId;
                $invites = $this->inviteRepository->update($input, $getDetailInvite[0]->id);
                return $this->sendResponse($invites->toArray(), 'Invite saved successfully');
            }
            $data['data']    = $getDetailInvite;
            return $this->sendResponse($data['data'][0], 'Invite get successfully');
        } else {
            $invites = $this->inviteRepository->create($input);
            return $this->sendResponse($invites->toArray(), 'Invite saved successfully');
        }
    }

    /**
     * Display the specified Invite.
     * GET|HEAD /invites/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Invite $invite */
        $invite = $this->inviteRepository->findWithoutFail($id);

        if (empty($invite)) {
            return $this->sendError('Invite not found');
        }

        return $this->sendResponse($invite->toArray(), 'Invite retrieved successfully');
    }

    /**
     * Update the specified Invite in storage.
     * PUT/PATCH /invites/{id}
     *
     * @param  int $id
     * @param UpdateInviteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInviteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Invite $invite */
        $invite = $this->inviteRepository->findWithoutFail($id);

        if (empty($invite)) {
            return $this->sendError('Invite not found');
        }

        $invite = $this->inviteRepository->update($input, $id);

        return $this->sendResponse($invite->toArray(), 'Invite updated successfully');
    }

    /**
     * Remove the specified Invite from storage.
     * DELETE /invites/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Invite $invite */
        $invite = $this->inviteRepository->findWithoutFail($id);

        if (empty($invite)) {
            return $this->sendError('Invite not found');
        }

        $invite->delete();

        return $this->sendResponse($id, 'Invite deleted successfully');
    }

    public function pagination(Request $request)
    {
        $this->inviteRepository->pushCriteria(new ByUserStoreCriteria());
        $this->inviteRepository->pushCriteria(new RequestCriteria($request));
        $this->inviteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $invites = $this->inviteRepository->paginate();

        return $this->sendResponse($invites->toArray(), 'Invites retrieved successfully');
    }

    public function import(InviteListImportRequest $request)
    {
        $path = $request->file('file')->store('imports');
        $invites = Excel::load(storage_path('app' . DIRECTORY_SEPARATOR . $path), function ($reader) {
        })->get();
        $result = $this->inviteService->importInvites($invites->toArray());
        if ($result) {
            return $this->sendResponse($result, 'Import invites successfully');
        }
        return $this->sendError($result, 'Import invites fail');
    }

    public function send(SendInviteRequest $request)
    {
        set_time_limit(0);
        // check valid send sms/email
        $checkSMS = false;
        $listVia = $request->get('via');
        if(in_array('sms', $listVia) || in_array('mms', $listVia)){
            $checkSMS = true;
        }
        if($checkSMS){
            $cus_id = $request->get('list')[0];
            $store_id = $this->inviteService->getStoreIdByCustomerId($cus_id)[0]->store_id;
            $totalSent = 0;
            $countListInvites = count($request->get('list'));
            $totalSent = $this->inviteService->getTotalSentTypeCount($store_id,'phone_sent');
            $getPackageSms = $this->inviteService->getTotalPackages($store_id);
            $restMessage = $getPackageSms[0]->count_phone - $totalSent;
            if($countListInvites > $restMessage ) {
                return $this->sendError('Number of messages not enough to use');
            }
        }

        $this->inviteService->sendListInvites($request->get('list'), $request->get('via'));
        return $this->sendResponse(null, 'Send invites successfully');
    }

    public function sendAll(SendInviteRequest $request)
    {
        $this->inviteService->sendAll($request->get('via'));
        return $this->sendResponse(null, 'Send invites successfully');
    }

    public function getCustomerInformation($startDate = null, $endDate = null)
    {
        if($startDate){
            $result = $this->inviteService->getCustomerInformation($startDate);
        }else if($startDate && $endDate){
            $result = $this->inviteService->getCustomerInformation($startDate,$endDate);
        }else{
            $result = $this->inviteService->getCustomerInformation();
        }

        return $this->sendResponse($result, "Customer information retrieved successfully");
    }
    public function getCustomerInviteInformation($startDate = null, $endDate = null)
    {
        $result = $this->inviteService->getCustomerInviteInformation($startDate,$endDate);
        return $this->sendResponse($result, "Customer Invite information retrieved successfully");
    }
    public function click_review_link($id = 0){
        return $this->sendResponse($this->inviteService->getReviewLinkByCustomer($id),'Review Link clicked information retrieved successfully');
    }
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
