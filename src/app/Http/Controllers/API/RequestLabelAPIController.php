<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRequestLabelAPIRequest;
use App\Http\Requests\API\UpdateRequestLabelAPIRequest;
use App\Criteria\ListRequestLabelCriteria;
use App\Models\RequestLabel;
use App\Repositories\RequestLabelRepository;
use App\Repositories\RequestLabelPackageRepository;
use App\Services\RequestLabelService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Notifications\SendReviewInvitation;

use SoapClient;
use SoapFault;
use Response;
use Stores;
use SimpleXMLElement;
use Dompdf\Dompdf;
use Dompdf\Options;
use Exception;
use Notification;
use DateTime;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class RequestLabelAPIController extends AppBaseController
{
    /** @var  RequestLabelRepository */
    private $requestLabelRepository;

    /** @var  RequestLabelPackageRepository */
    private $requestLabelPackageRepository;

    /** @var RequestLabelService */
    private $requestLabelService;


    public function __construct(RequestLabelRepository $requestLabelRepo, RequestLabelService $requestLabelService, RequestLabelPackageRepository $requestLabelPackageRepo)
    {
        $this->requestLabelRepository = $requestLabelRepo;
        $this->requestLabelService = $requestLabelService;
        $this->requestLabelPackageRepository = $requestLabelPackageRepo;
    }

    public function pagination(Request $request)
    {
        $this->requestLabelRepository->pushCriteria(new ListRequestLabelCriteria($request));
        $this->requestLabelRepository->pushCriteria(new RequestCriteria($request));
        $this->requestLabelRepository->pushCriteria(new LimitOffsetCriteria($request));
        $RequestLabel = $this->requestLabelRepository->paginate();
        return $this->sendResponse($RequestLabel, 'RequestLabel retrieved successfully');
    }

    public function pagination_bk(Request $request)
    {
        $RequestLabel = $this->requestLabelService->list_request_label();
        return $this->sendResponse($RequestLabel, 'RequestLabel retrieved successfully');
    }

    public function index()
    {
        $RequestLabel = $this->requestLabelService->list_request_label();
        return $this->sendResponse($RequestLabel, 'RequestLabel retrieved successfully');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        if(count($input)) {
            $token = $input["_token"];
            $check_csrf = $this->validCsrf($token, $request);
            if($check_csrf) {
                $result = $this->requestLabelService->create($input);
                return $this->sendResponse($result, 'RequestLabel created successfully');
            } else {
                return $this->sendError('Not found TR!');
            }
        } else {
            return $this->sendError('Not found Data');
        }
    }

    public function update($id, UpdateRequestLabelAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['updated_by'] = $userId;
        $result = $this->requestLabelRepository->update($input, $id);
        return $this->sendResponse($result, 'RequestLabel updated successfully');
    }

    public function destroy($id)
    {
        $RequestLabel = $this->requestLabelRepository->findWithoutFail($id);

        if (empty($RequestLabel)) {
            return $this->sendResponse('', 'RequestLabel not found');
        }
        $result = $this->requestLabelRepository->delete($id);
        return $this->sendResponse($result, 'RequestLabel deleted successfully');
    }

    public function listPackageById($id)
    {
        $result = $this->requestLabelService->listPackageById($id);
        return $this->sendResponse($result, 'List Package retrieved successfully');
    }

    public function confirm_request_label($id)
    {
        // check request label 
        $result = array();
        $checkRequestLabel = $this->requestLabelService->checkRequestLabel($id);
        if (!$checkRequestLabel) {
            return $this->sendError('RequestLabel is not pending status!');
        }
        // get request label infor and packages
        $requestLabelInfor          = $this->requestLabelService->getRequestLabelInfor($id);
        $requestLabelPackageInfor   = $this->requestLabelService->getRequestLabelPackageInfor($id);
        $delivery_company = $requestLabelInfor->delivery_company; // 1: ups, 2: fedex
        if ($delivery_company == 1) {
            $this->UPS_Confirm($requestLabelInfor, $requestLabelPackageInfor);
        } else {
            $this->Fedex_Confirm($requestLabelInfor, $requestLabelPackageInfor);
        }
        return $this->sendResponse($result, 'Confirm shipment successfully');
    }
    public function UPS_Confirm($requestLabelInfor, $requestLabelPackageInfor)
    {
        // set infor for shipment
        $shipment = new \Ups\Entity\Shipment();
        try {
            $sender_name        = $this->vn_to_str($requestLabelInfor->sender_name);
            $receiver_name        = $this->vn_to_str($requestLabelInfor->receiver_name);
            $sender_phone       = $requestLabelInfor->phone;
            $customer_code      = $requestLabelInfor->customer_code;
            $address_sender     = $requestLabelInfor->address;
            $receiver_address   = $requestLabelInfor->receiver_address;
            $zipcode_sender     = $requestLabelInfor->zipcode;
            $state_sender       = $requestLabelInfor->state;
            $city_sender        = $requestLabelInfor->city;
            $send_date          = $requestLabelInfor->send_date;
            $service_option     = $requestLabelInfor->service_option;
            $ref                = $customer_code.'-'.$receiver_name;
            // configuration UPS API
            $access = "AD7A19F242131A95";
            $userid = "texpresscargo";
            //$passwd = "Chuv!tcon511312";
            $passwd = "TExpress!4936";
            $endpointurl = "https://onlinetools.ups.com/ups.app/xml/ShipConfirm";
            $outputFileName = "XOLTResult.xml";
            // Create AccessRequest XMl
            $accessRequestXML = new SimpleXMLElement("<AccessRequest></AccessRequest>");
            $accessRequestXML->addChild("AccessLicenseNumber", $access);
            $accessRequestXML->addChild("UserId", $userid);
            $accessRequestXML->addChild("Password", $passwd);

            /** START confirm shipment */
            $shipmentConfirmRequestXML = new SimpleXMLElement("<ShipmentConfirmRequest ></ShipmentConfirmRequest>");
            $request = $shipmentConfirmRequestXML->addChild('Request');
            $request->addChild("RequestAction", "ShipConfirm");
            $request->addChild("RequestOption", "nonvalidate");

            // label
            $labelSpecification = $shipmentConfirmRequestXML->addChild('LabelSpecification');
            $labelSpecification->addChild("HTTPUserAgent", "");
            $labelPrintMethod = $labelSpecification->addChild('LabelPrintMethod');
            $labelPrintMethod->addChild("Code", "GIF");
            $labelPrintMethod->addChild("Description", "");
            $labelImageFormat = $labelSpecification->addChild('LabelImageFormat');
            $labelImageFormat->addChild("Code", "GIF");
            $labelImageFormat->addChild("Description", "");

            $shipment = $shipmentConfirmRequestXML->addChild('Shipment');
            $shipment->addChild("Description", "");
            $rateInformation = $shipment->addChild('RateInformation');
            $rateInformation->addChild("NegotiatedRatesIndicator", "");

            // shipper
            /*$shipper = $shipment->addChild('Shipper');
            $shipper->addChild("Name", "T Express Cargo");
            $shipper->addChild("PhoneNumber", "6265866044");
            $shipper->addChild("TaxIdentificationNumber", "");
            $shipper->addChild("ShipperNumber", "87FR61");
            $shipperAddress = $shipper->addChild('Address');
            $shipperAddress->addChild("AddressLine1", "14936 DILLOW ST");
            $shipperAddress->addChild("City", "WESTMINSTER");
            $shipperAddress->addChild("StateProvinceCode", "CA");
            $shipperAddress->addChild("PostalCode", "926835911");
            $shipperAddress->addChild("CountryCode", "US");*/
            $shipper = $shipment->addChild('Shipper');
            $shipper->addChild("Name", $sender_name);
            $shipper->addChild("PhoneNumber", $sender_phone);
            $shipper->addChild("TaxIdentificationNumber", "");
            $shipper->addChild("ShipperNumber", "87FR61");
            $shipperAddress = $shipper->addChild('Address');
            $shipperAddress->addChild("AddressLine1", $address_sender);
            $shipperAddress->addChild("City", $city_sender);
            $shipperAddress->addChild("StateProvinceCode", $state_sender);
            $shipperAddress->addChild("PostalCode", $zipcode_sender);
            $shipperAddress->addChild("CountryCode", "US");
            // ship to
            $shipTo = $shipment->addChild('ShipTo');
            $shipTo->addChild("CompanyName", "T Express Cargo TEST");
            $shipTo->addChild("AttentionName", $receiver_name);
            $shipTo->addChild("PhoneNumber", "7147909999");
            $shipToAddress = $shipTo->addChild('Address');
            $shipToAddress->addChild("AddressLine1", $receiver_address);
            $shipToAddress->addChild("City", "WESTMINSTER");
            $shipToAddress->addChild("StateProvinceCode", "CA");
            $shipToAddress->addChild("PostalCode", "92683");
            $shipToAddress->addChild("CountryCode", "US");
            // ship from
            $shipFrom = $shipment->addChild('ShipFrom');
            $shipFrom->addChild("CompanyName", $sender_name);
            $shipFrom->addChild("AttentionName", $sender_name);
            $shipFrom->addChild("PhoneNumber", $sender_phone);
            $shipFrom->addChild("TaxIdentificationNumber", "");
            $shipFromAddress = $shipFrom->addChild('Address');
            $shipFromAddress->addChild("AddressLine1", $address_sender);
            $shipFromAddress->addChild("City", $city_sender);
            $shipFromAddress->addChild("StateProvinceCode", $state_sender);
            $shipFromAddress->addChild("PostalCode", $zipcode_sender);
            $shipFromAddress->addChild("CountryCode", "US");

            $paymentInformation = $shipment->addChild('PaymentInformation');
            $prepaid = $paymentInformation->addChild('Prepaid');
            $billShipper = $prepaid->addChild('BillShipper');
            $billShipper->addChild("AccountNumber", "87FR61");
            $service = $shipment->addChild('Service');
            $service->addChild("Code", $service_option);
            // packages
            foreach ($requestLabelPackageInfor as $package_item) {
                $package = $shipment->addChild('Package');
                $package->addChild("Description", $package_item->description);
                $packagingType = $package->addChild('PackagingType');
                $packagingType->addChild("Code", "02");
                $packagingType->addChild("Description", "UPS Package");
                $packageWeight = $package->addChild('PackageWeight');
                $packageWeight->addChild("Weight", (intval($package_item->weight) * 0.7));
                //$packageWeight->addChild ( 'UnitOfMeasurement' );
                $unitOfMeasurement = $packageWeight->addChild('UnitOfMeasurement');
                $unitOfMeasurement->addChild("Code", "LBS");
                // REF
                $ref_shipment_ = $package->addChild('ReferenceNumber');
                $ref_shipment_->addChild("Code", "US");
                $ref_shipment_->addChild("Value", $ref);
            }
            // action 
            $requestXML = $accessRequestXML->asXML() . $shipmentConfirmRequestXML->asXML();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $endpointurl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
            $response = curl_exec($ch);
            curl_close($ch);
            if ($response == false) {
                //throw new Exception ( "Bad data." );
                return $this->sendError("Bad data.");
            } else {
                // save request and response to file
                $fw = fopen($outputFileName, 'w');
                fwrite($fw, "Request: \n" . $requestXML . "\n");
                fwrite($fw, "Response: \n" . $response . "\n");
                fclose($fw);

                // get response status
                $resp = new SimpleXMLElement($response); //var_dump($resp);die;
                $shipmentDigest = (string) $resp->ShipmentDigest;
                // update ShipmentIdentificationNumber 
                $this->requestLabelService->updateRequestLabel($requestLabelInfor->id, $resp->ShipmentIdentificationNumber);
                $this->shipmment_accept($shipmentDigest, $requestLabelInfor->id, $requestLabelPackageInfor);
                // echo $resp->Response->ResponseStatusDescription . "\n";
                $result = [
                    'ShipmentIdentificationNumber' => $resp->ShipmentIdentificationNumber
                ];
                return $result;
            }
            Header('Content-type: text/xml');
            /** END confirm shipment */
        } catch (Exception $e) {
            var_dump($e);
            return $this->sendResponse($e, 'Confirm shipment error!');
        }
    }
    public function shipmment_accept($shipmentDigest, $id, $requestLabelPackageInfor)
    {
        // Configuration
        $accessLicenseNumber = "AD7A19F242131A95";
        $userId = "texpresscargo";
        $password = "TExpress!4936";

        $endpointurl = 'https://onlinetools.ups.com/ups.app/xml/ShipAccept';
        $outputFileName = "XOLTResult.xml";

        try {

            // Create AccessRequest XMl
            $accessRequestXML = new SimpleXMLElement("<AccessRequest></AccessRequest>");
            $accessRequestXML->addChild("AccessLicenseNumber", $accessLicenseNumber);
            $accessRequestXML->addChild("UserId", $userId);
            $accessRequestXML->addChild("Password", $password);

            // Create ShipmentAcceptRequest XMl
            $shipmentAcceptRequestXML = new SimpleXMLElement("<ShipmentAcceptRequest ></ShipmentAcceptRequest >");
            $request = $shipmentAcceptRequestXML->addChild('Request');
            $request->addChild("RequestAction", "01");

            $shipmentAcceptRequestXML->addChild("ShipmentDigest", $shipmentDigest);

            $requestXML = $accessRequestXML->asXML() . $shipmentAcceptRequestXML->asXML();

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $endpointurl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
            $response = curl_exec($ch);
            curl_close($ch);

            if ($response == false) {
                throw new Exception("Bad data.");
            } else {
                // save request and response to file
                $fw = fopen($outputFileName, 'w');
                fwrite($fw, "Request: \n" . $requestXML . "\n");
                fwrite($fw, "Response: \n" . $response . "\n");
                fclose($fw);

                // get response status
                $resp = new SimpleXMLElement($response);
                $package_result = $resp->ShipmentResults->PackageResults; //var_dump($package_result);
                $tracking_number_arr = "";
                foreach ($package_result as $k=>$package_item) {
                    $tracking_number = (string) $package_item->TrackingNumber;
                    $tracking_number_arr .= $tracking_number . ";";
                    $image = (string) $package_item->LabelImage->GraphicImage;
                    // save image label
                    $label_file =  'storage/tracking_number/ups/' . $tracking_number . ".gif";
                    $base64_string = $image;
                    $ifp = fopen($label_file, 'wb');
                    fwrite($ifp, base64_decode($base64_string));
                    fclose($ifp);
                    $this->gifToPdf($label_file,$tracking_number);
                    $this->createLabelUpsPDF($requestLabelPackageInfor, $tracking_number, $k);
                }
                // update status finish accept
                $this->requestLabelService->updateRequestLabelAccept($id, $tracking_number_arr);
                //echo $resp->Response->ResponseStatusDescription . "\n";
            }

            Header('Content-type: text/xml');
        } catch (Exception $ex) {
            echo $ex;
        }
    }
    public function Fedex_Confirm($requestLabelInfor, $requestLabelPackageInfor)
    {
        require_once(base_path("public_html/library/fedex-common.php5"));
        $path_to_wsdl = base_path("public_html/library/ShipService_v25.wsdl");
        define('SHIP_LABEL', base_path("public_html/library/shipgroundlabel.pdf"));
        define('SHIP_CODLABEL', base_path("public_html/library/shipgroundlabel.pdf"));
        ini_set("soap.wsdl_cache_enabled", "0");
        $opts = array(
            'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
        );

        $sender_name        = $this->vn_to_str($requestLabelInfor->sender_name);
        $receiver_name        = $this->vn_to_str($requestLabelInfor->receiver_name);
        $sender_phone       = $requestLabelInfor->phone;
        $customer_code       = $requestLabelInfor->customer_code;
        $address_sender     = $requestLabelInfor->address;
        $receiver_address     = $requestLabelInfor->receiver_address;
        $zipcode_sender     = $requestLabelInfor->zipcode;
        $state_sender       = $requestLabelInfor->state;
        $city_sender        = $requestLabelInfor->city;
        $send_date          = $requestLabelInfor->send_date;
        $service_option     = $requestLabelInfor->service_option;
        $ref                = $customer_code.'-'.$receiver_name;
        $client = new SoapClient($path_to_wsdl, array('trace' => 1, 'stream_context' => stream_context_create($opts)));
        $tracking_number_arr = "";
        $masterTrackingId = "";
        $tracking_number = "";
        try {
            if (setEndpoint('changeEndpoint')) {
                $newLocation = $client->__setLocation('https://wsbeta.fedex.com:443/web-services');
            }
            //$request['RequestedShipment']['PackageCount'] = count($requestLabelPackageInfor);
            foreach ($requestLabelPackageInfor as $k => $package_item) {
                $request['WebAuthenticationDetail'] = array(
                    'UserCredential' => array(
                        'Key' => 'DckDbzMFNJJKK2zn',
                        'Password' => 'ESfPvYFBUshbAxn4uunMJaX6i'
                    )
                );
                $request['ClientDetail'] = array(
                    'AccountNumber' => '510087240',
                    'MeterNumber' => '114061106'
                );
                $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Express Domestic Shipping Request using PHP ***');
                $request['Version'] = array(
                    'ServiceId' => 'ship',
                    'Major' => '25',
                    'Intermediate' => '0',
                    'Minor' => '0'
                );
                $request['RequestedShipment'] = array(
                    //'ShipTimestamp' => date('c'),
                    'ShipTimestamp' => date("c", strtotime($send_date . ' 09:00:00')),
                    'DropoffType' => 'REGULAR_PICKUP',
                    'ServiceType' => $service_option,
                    'PackagingType' => 'YOUR_PACKAGING',
                    'Shipper' => array(
                        'Contact' => array(
                            'PersonName' => $sender_name,
                            'PhoneNumber' => $sender_phone,
                        ),
                        'Address' => array(
                            'StreetLines' => $address_sender,
                            'City' => $city_sender,
                            'StateOrProvinceCode' => $state_sender,
                            'PostalCode' => $zipcode_sender,
                            'CountryCode' => 'US'
                        )
                    ),
                    'Recipient' => array(
                        'Contact' => array(
                            //'PersonName' => 'Tung Nguyen',
                            'PersonName' => $receiver_name,
                            //'CompanyName' => 'T Express Cargo TEST',
                            'PhoneNumber' => '7147909999'
                        ),
                        'Address' => array(
                            //'StreetLines' => array('14936 Dillow St'),
                            'StreetLines' => $receiver_address,
                            'City' => 'WESTMINSTER',
                            'StateOrProvinceCode' => 'CA',
                            'PostalCode' => '92683',
                            'CountryCode' => 'US',
                            'Residential' => false
                        )
                    ),
                    'ShippingChargesPayment' => array(
                        'PaymentType' => 'SENDER',
                        'Payor' => array(
                            'ResponsibleParty' => array(
                                'AccountNumber' => '510087240',
                                'Contact' => null,
                                'Address' => array(
                                    'CountryCode' => 'US'
                                )
                            )
                        )
                    ),
                    'LabelSpecification' => array(
                        'LabelFormatType' => 'COMMON2D',
                        'ImageType' => 'PDF',
                        'LabelStockType' => 'PAPER_7X4.75'
                    ),
                    'PackageCount' => count($requestLabelPackageInfor),
                    'RequestedPackageLineItems' => array(
                        '0' => array(
                            'CustomerReferences' => array(
                                '0' => array(
                                    'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
                                    'Value' => $ref
                                )
                            )
                        )
                    )
                );
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['SequenceNumber'] = $k + 1;
                //$request['RequestedShipment']['RequestedPackageLineItems'][0]['GroundPackageCount'] = 1;
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['GroupPackageCount'] = 1;
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['Weight']['Value'] = intval($package_item->weight) * 0.7;
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['Weight']['Units'] = 'LB';
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['Dimensions']['Length'] = $package_item->length;
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['Dimensions']['Width'] = $package_item->width;
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['Dimensions']['Height'] = $package_item->height;
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['Dimensions']['Units'] = 'IN';
                $request['RequestedShipment']['RequestedPackageLineItems'][0]['Description'] = $package_item->description;
                if ($masterTrackingId) {
                    $request['RequestedShipment']['MasterTrackingId'] = $masterTrackingId;
                }

                $response = $client->processShipment($request);
                if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
                    if (!$masterTrackingId) {
                        $masterTrackingId = $response->CompletedShipmentDetail->MasterTrackingId;
                    }
                    $tracking_number = $response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber;
                    $tracking_number_arr .= $tracking_number . '_' . ($k + 1) . ";";
                    $fp = fopen('storage/tracking_number/fedex/' . $tracking_number . '_' . ($k + 1) . ".pdf", 'wb');
                    fwrite($fp, $response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image);
                    fclose($fp);
                } else {
                    printError($client, $response);
                    throw new Exception('Bad data.');
                }
            }
            $request_label_id = $requestLabelInfor->id;
            if($tracking_number) {
                $this->createLabelPDF($requestLabelPackageInfor, $tracking_number);
            }
            $this->requestLabelService->updateRequestLabelAccept($request_label_id, $tracking_number_arr);
        } catch (SoapFault $exception) {
            printFault($exception, $client);
        }
        $this->sendResponse('success', 'updated success');
    }

    public function Fedex_validate($requestLabelInfor, $requestLabelPackageInfor)
    {
        require_once('/var/www/public/tat-19-008-cargo-express/src/public_html/library/fedex-common.php5');
        $path_to_wsdl = "/var/www/public/tat-19-008-cargo-express/src/public_html/library/ShipService_v25.wsdl";
        ini_set("soap.wsdl_cache_enabled", "0");
        $opts = array(
            'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
        );
        $client = new SoapClient($path_to_wsdl, array('trace' => 1, 'stream_context' => stream_context_create($opts)));

        $request['WebAuthenticationDetail']['UserCredential'] = array(
            'Key' => 'Avuqbbo9EzbruBj5',
            'Password' => 'xFose1vsrIQhTnEgLGg1lrKDe '
        );
        $request['ClientDetail'] = array(
            'AccountNumber' => '510087380',
            'MeterNumber' => '114062969'
        );
        $request['TransactionDetail']['CustomerTransactionId'] = '*** Ground Domestic Shipping Request using PHP ***';
        $request['Version'] = array(
            'ServiceId' => 'ship',
            'Major' => '25',
            'Intermediate' => '0',
            'Minor' => '0'
        );
        print_r(date('c'));
        $request['RequestedShipment']['ShipTimestamp']  =  date('c');
        $request['RequestedShipment']['DropoffType']    = 'REGULAR_PICKUP';
        $request['RequestedShipment']['ServiceType']    = 'FIRST_OVERNIGHT';
        $request['RequestedShipment']['PackagingType']  = 'YOUR_PACKAGING';
        $request['RequestedShipment']['Shipper']['Contact'] = array(
            'PersonName' => 'Sender Name',
            'CompanyName' => 'Sender Company Name',
            'PhoneNumber' => '1234567890',
            'EMailAddress' => 'hanhphucmongmanh496@gmail.com'
        );
        $request['RequestedShipment']['Shipper']['Address'] = array(
            'StreetLines' => array('Address Line 1'),
            'City' => 'Newyork',
            'StateOrProvinceCode' => 'NY',
            'PostalCode' => '10007',
            'CountryCode' => 'US'
        );
        $request['RequestedShipment']['Recipient']['Contact'] = array(
            'PersonName' => 'Recipient Name',
            'CompanyName' => 'Recipient Company Name',
            'PhoneNumber' => '1234567890'
        );
        $request['RequestedShipment']['Recipient']['Address'] = array(
            'StreetLines' => array('Address Line 1'),
            'City' => 'NewOrleans',
            'StateOrProvinceCode' => 'LA',
            'PostalCode' => '70112',
            'CountryCode' => 'US',
            'Residential' => true
        );
        $request['RequestedShipment']['ShippingChargesPayment']['PaymentType'] =  'SENDER';
        $request['RequestedShipment']['ShippingChargesPayment']['Payor'] = array(
            'ResponsibleParty' => array(
                'AccountNumber' => '510087380',
                'Contact' => null,
                'Address' => array(
                    'CountryCode' => 'US'
                )
            )
        );
        $request['RequestedShipment']['LabelSpecification'] = array(
            'LabelFormatType' => 'COMMON2D',
            'ImageType' => 'PNG',
            'LabelStockType' => 'PAPER_7X4.75'
        );
        $request['RequestedShipment']['RateRequestTypes'] = 'LIST';
        $request['RequestedShipment']['PackageCount'] = 1;
        $request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
        $request['RequestedShipment']['RequestedPackageLineItems'] = array(
            '0' => array(
                'SequenceNumber' => 1,
                'GroupPackageCount' => 1,
                'Weight' => array(
                    'Value' => 50.0,
                    'Units' => 'LB'
                ),
                'Dimensions' => array(
                    'Length' => 12,
                    'Width' => 12,
                    'Height' => 12,
                    'Units' => 'IN'
                ),
                'CustomerReferences' => array(
                    '0' => array(
                        'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
                        'Value' => 'GR4567892'
                    )
                )
            )
        );

        try {

            if (setEndpoint('changeEndpoint')) {
                $newLocation = $client->__setLocation('https://wsbeta.fedex.com/web-services');
            }
            $response = $client->validateShipment($request);
            if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
                echo '<pre>';
                print_r($response);
                echo '<pre>';
                printSuccess($client, $response);

                $fp = fopen(SHIP_CODLABEL, 'wb');
                fwrite($fp, $response->CompletedShipmentDetail->CompletedPackageDetails->CodReturnDetail->Label->Parts->Image);
                fclose($fp);
                echo '<a href="./' . SHIP_CODLABEL . '">' . SHIP_CODLABEL . '</a> was generated.' . Newline;
                $fp = fopen(SHIP_LABEL, 'wb');
                fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
                fclose($fp);
                echo '<a href="./' . SHIP_LABEL . '">' . SHIP_LABEL . '</a> was generated.';
            } else {
                printError($client, $response);
            }

            writeToLog($client);
        } catch (SoapFault $exception) {
            printFault($exception, $client);
        }
    }

    public function createLabelPDF($packages, $file_name)
    {
        $index = 1;
        $html = '';
        foreach ($packages as $package) {
            $html .= '<link type="text/css" href="' . base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css') . '" rel="stylesheet" />';
            $html .= '<table class="table">';
            $html .= '<tr>';
            $html .= '<th>Package</th>';
            $html .= '<th>Weight</th>';
            $html .= '<th>Height</th>';
            $html .= '<th>Width</th>';
            $html .= '<th>Length</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td>Package ' . $index . '</td>';
            $html .= '<td>' . $package->weight . '</td>';
            $html .= '<td>' . $package->height . '</td>';
            $html .= '<td>' . $package->width . '</td>';
            $html .= '<td>' . $package->length . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td colspan="5">Note</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td colspan="5">' . $package->description . '</td>';
            $html .= '</tr>';
            $html .= '</table>';
            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->set_base_path(base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css'));
            $dompdf->render();
            $output = $dompdf->output();
            $fp = fopen('storage/tracking_number/fedex/' . $file_name .'_'.$index .'_info' . ".pdf", 'wb');
            fwrite($fp, $dompdf->output());
            fclose($fp);
            $index += 1;
            if($index > 1){
                $html = "";
            }
        }
        return true;
    }

    public function createLabelUpsPDF($packages, $file_name, $k)
    {
        $index = 1;
        $html = '';
        foreach ($packages as $k_pack => $package) {
            if($k_pack == $k) {
                $html .= '<link type="text/css" href="' . base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css') . '" rel="stylesheet" />';
                $html .= '<table class="table">';
                $html .= '<tr>';
                $html .= '<th>Package</th>';
                $html .= '<th>Weight</th>';
                $html .= '<th>Height</th>';
                $html .= '<th>Width</th>';
                $html .= '<th>Length</th>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Package ' . $index . '</td>';
                $html .= '<td>' . $package->weight . '</td>';
                $html .= '<td>' . $package->height . '</td>';
                $html .= '<td>' . $package->width . '</td>';
                $html .= '<td>' . $package->length . '</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td colspan="5">Note</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td colspan="5">' . $package->description . '</td>';
                $html .= '</tr>';
                $html .= '</table>';
                $dompdf = new Dompdf();
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->set_base_path(base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css'));
                $dompdf->render();
                $output = $dompdf->output();
                $fp = fopen('storage/tracking_number/ups/' . $file_name .'_'.$index .'_info' . ".pdf", 'wb');
                fwrite($fp, $dompdf->output());
                fclose($fp);
                $index += 1;
                if($index > 1){
                    $html = "";
                }
            }
        }
        return true;
    }

    public function request_label_send_email($id)
    {
        // check request label 
        $result = array();
        $checkRequestLabel = $this->requestLabelService->checkRequestLabelSendEmail($id);
        if (!$checkRequestLabel) {
            return $this->sendError('RequestLabel is not confirm status!');
        }
        // get request label infor and packages
        $requestLabelInfor          = $this->requestLabelService->getRequestLabelInfor($id, 2);
        $tracking_number_list = explode(";", $requestLabelInfor->tracking_number);
        $tracking_number = "";
        $tracking_number_info = "";
        foreach ($tracking_number_list as $k=>$item) {
            if ($item) {
                if ($requestLabelInfor->delivery_company == 1) {
                    $url = url('/storage/tracking_number/ups/') . '/' . $item . '.pdf';
                    $url_info =  url('/storage/tracking_number/ups/') . '/' . $item .'_'.($k+1). '_info.pdf';
                    $tracking_number        .=  '<a href="' . $url . '" target="_blank">' . $item . ' </a><br/>';
                    $tracking_number_info   .=  '<a href="' . $url_info . '" target="_blank">' . $item .'_'.($k+1). '_info </a> <br/>';
                } else {
                    $url = url('/storage/tracking_number/fedex/') . '/' . $item . '.pdf';
                    $url_info = url('/storage/tracking_number/fedex/') . '/' . $item . '_info.pdf';
                    $tracking_number        .= '<a href="' . $url . '" target="_blank">' . $item . ' </a><br/>';
                    $tracking_number_info   .= '<a href="' . $url_info . '" target="_blank">' . $item . '_info </a> <br/>';
                }
            }
        }
        $via_email = ['email'];
        $emailDetail = [
            'type_email'            => 'email_request',
            'id' => $requestLabelInfor->id,
            'store_id'              => 1,
            'name'                  => $requestLabelInfor->sender_name,
            'email'                 => $requestLabelInfor->email,
            'tracking_label'        => $tracking_number,
            'tracking_label_info'   => $tracking_number_info
        ];
        Notification::send((object) $emailDetail, new SendReviewInvitation($via_email));
        return $this->sendResponse($emailDetail, 'Send email successfully!');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

    public function rate_ups($id, Request $request)
    {
        $input = $request->all();
        $requestLabelInfor          = $this->requestLabelService->getRequestLabelInfor($id);
        if (count($input)) {
            $requestLabelPackageInfor[0] = (object)$input;
        } else {
            $requestLabelPackageInfor   = $this->requestLabelService->getRequestLabelPackageInfor($id);
        }
        // set infor for shipment
        $shipment = new \Ups\Entity\Shipment();
        $sender_name        = $requestLabelInfor->sender_name;
        $sender_phone       = "0467673878";
        $address_sender     = $requestLabelInfor->address;
        $zipcode_sender     = $requestLabelInfor->zipcode;
        $state_sender       = $requestLabelInfor->state;
        $city_sender        = $requestLabelInfor->city;
        $send_date          = $requestLabelInfor->send_date;
        $service_option     = $requestLabelInfor->service_option;
        $package_desc       = "TEST";
        try {
            // configuration UPS API
            $access = "AD7A19F242131A95";
            $userid = "texpresscargo";
            $passwd = "TExpress!4936";
            $endpointurl = "https://onlinetools.ups.com/ups.app/xml/Rate";
            $outputFileName = "XOLTResult.xml";
            // Create AccessRequest XMl
            $accessRequestXML = new SimpleXMLElement("<AccessRequest></AccessRequest>");
            $accessRequestXML->addChild("AccessLicenseNumber", $access);
            $accessRequestXML->addChild("UserId", $userid);
            $accessRequestXML->addChild("Password", $passwd);

            /** START confirm shipment */
            $RatingServiceSelectionRequest = new SimpleXMLElement("<RatingServiceSelectionRequest></RatingServiceSelectionRequest>");
            $request = $RatingServiceSelectionRequest->addChild('Request');
            $request->addChild("RequestAction", "Rate");
            $request->addChild("RequestOption", "Rate");

            // $pickupType =  $RatingServiceSelectionRequest->addChild('PickupType');
            // $pickupType->addChild('Code',$requestLabelInfor->service_option);
            // $pickupType->addChild('Description','');


            $shipment = $RatingServiceSelectionRequest->addChild('Shipment');
            $shipment->addChild("Description", "");
            $rateInformation = $shipment->addChild('RateInformation');
            $rateInformation->addChild("NegotiatedRatesIndicator", "");

            // shipper
            $shipper = $shipment->addChild('Shipper');
            $shipper->addChild("Name", "T Express Cargo");
            $shipper->addChild("PhoneNumber", "6265866044");
            $shipper->addChild("TaxIdentificationNumber", "");
            $shipper->addChild("ShipperNumber", "87FR61");
            $shipperAddress = $shipper->addChild('Address');
            $shipperAddress->addChild("AddressLine1", "14936 DILLOW ST");
            $shipperAddress->addChild("City", "WESTMINSTER");
            $shipperAddress->addChild("StateProvinceCode", "CA");
            $shipperAddress->addChild("PostalCode", "926835911");
            $shipperAddress->addChild("CountryCode", "US");
            // ship to
            $shipTo = $shipment->addChild('ShipTo');
            $shipTo->addChild("CompanyName", "T Express Cargo TEST");
            $shipTo->addChild("AttentionName", "Tung Nguyen");
            $shipTo->addChild("PhoneNumber", "6265866044");
            $shipToAddress = $shipTo->addChild('Address');
            $shipToAddress->addChild("AddressLine1", "14936 Dillow St");
            $shipToAddress->addChild("City", "WESTMINSTER");
            $shipToAddress->addChild("StateProvinceCode", "CA");
            $shipToAddress->addChild("PostalCode", "92683");
            $shipToAddress->addChild("CountryCode", "US");
            // ship from
            $shipFrom = $shipment->addChild('ShipFrom');
            $shipFrom->addChild("CompanyName", $sender_name);
            $shipFrom->addChild("AttentionName", $sender_name);
            $shipFrom->addChild("PhoneNumber", $sender_phone);
            $shipFrom->addChild("TaxIdentificationNumber", "");
            $shipFromAddress = $shipFrom->addChild('Address');
            $shipFromAddress->addChild("AddressLine1", $address_sender);
            $shipFromAddress->addChild("City", $city_sender);
            $shipFromAddress->addChild("StateProvinceCode", $state_sender);
            $shipFromAddress->addChild("PostalCode", $zipcode_sender);
            $shipFromAddress->addChild("CountryCode", "US");

            $service = $shipment->addChild('Service');
            $service->addChild("Code", $service_option);
            // REF
            $ref_shipment = $shipment->addChild('Reference');
            $ref_num = $ref_shipment->addChild('Number');
            $ref_num->addChild("Value", $ref);

            // packages
            foreach ($requestLabelPackageInfor as $package_item) {
                $package = $shipment->addChild('Package');
                $package->addChild("Description", $package_desc);
                $packagingType = $package->addChild('PackagingType');
                $packagingType->addChild("Code", "02");
                $packagingType->addChild("Description", "UPS Package");
                $packageWeight = $package->addChild('PackageWeight');
                $packageWeight->addChild("Weight", (intval($package_item->weight) * 0.7));
                //$packageWeight->addChild ( 'UnitOfMeasurement' );
                $unitOfMeasurement = $packageWeight->addChild('UnitOfMeasurement');
                $unitOfMeasurement->addChild("Code", "LBS");
            }
            // action 
            $requestXML = $accessRequestXML->asXML() . $RatingServiceSelectionRequest->asXML();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $endpointurl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
            $response = curl_exec($ch);
            curl_close($ch);
            if ($response == false) {
                return $this->sendError("Bad data.");
            } else {
                $resp = new SimpleXMLElement($response);
                $shippingRate = strval($resp->RatedShipment[0]->TotalCharges->MonetaryValue);
                return array('request_package_id' => $requestLabelPackageInfor[0]->id, 'shippingRate' => $shippingRate, 'package' => $requestLabelPackageInfor);
            }
            Header('Content-type: text/xml');
            /** END confirm shipment */
        } catch (Exception $e) {
            var_dump($e);
            return $this->sendResponse($e, 'Rate error!');
        }
    }

    public function rate_fedex($id, Request $request)
    {
        $input = $request->all();
        $requestLabelInfor          = $this->requestLabelService->getRequestLabelInfor($id);
        if (count($input)) {
            $requestLabelPackageInfor[0] = (object)$input;
        } else {
            $requestLabelPackageInfor   = $this->requestLabelService->getRequestLabelPackageInfor($id);
        }
        // set infor for shipment
        $sender_name        = $requestLabelInfor->sender_name;
        $sender_phone       = "0467673878";
        $address_sender     = $requestLabelInfor->address;
        $zipcode_sender     = $requestLabelInfor->zipcode;
        $state_sender       = $requestLabelInfor->state;
        $city_sender        = $requestLabelInfor->city;
        $send_date          = $requestLabelInfor->send_date;
        $service_option     = 'GROUND_HOME_DELIVERY';//$requestLabelInfor->service_option;
        $dateformat = DateTime::createFromFormat('Y-m-d', $send_date);
        $date = $dateformat->format('c');

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://fedex.com/ws/rate/v13">';
        $xml .= '<SOAP-ENV:Body>';
        $xml .= '<ns1:RateRequest>';
        $xml .= '<ns1:WebAuthenticationDetail>';
        $xml .= '<ns1:UserCredential>';
        $xml .= '<ns1:Key>DckDbzMFNJJKK2zn</ns1:Key>';   // Use your Fedex Test Key Here
        $xml .= '<ns1:Password>ESfPvYFBUshbAxn4uunMJaX6i</ns1:Password>';  //// Use your Fedex Test Password Here
        $xml .= '</ns1:UserCredential>';
        $xml .= '</ns1:WebAuthenticationDetail>';
        $xml .= '<ns1:ClientDetail>';
        $xml .= '<ns1:AccountNumber>510087240</ns1:AccountNumber>';
        $xml .= '<ns1:MeterNumber>114061106</ns1:MeterNumber>';
        $xml .= '</ns1:ClientDetail>';
        $xml .= '<ns1:TransactionDetail>';
        $xml .= '<ns1:CustomerTransactionId> *** Rate Request v24 using PHP ***</ns1:CustomerTransactionId>';
        $xml .= '</ns1:TransactionDetail>';
        $xml .= '<ns1:Version>';
        $xml .= '<ns1:ServiceId>crs</ns1:ServiceId>';
        $xml .= '<ns1:Major>13</ns1:Major>';
        $xml .= '<ns1:Intermediate>0</ns1:Intermediate>';
        $xml .= '<ns1:Minor>0</ns1:Minor>';
        $xml .= '</ns1:Version>';
        $xml .= '<ns1:ReturnTransitAndCommit>true</ns1:ReturnTransitAndCommit>';
        $xml .= '<ns1:RequestedShipment>';
        $xml .= '<ns1:DropoffType>REGULAR_PICKUP</ns1:DropoffType>';
        $xml .= '<ns1:ServiceType>' . $service_option . '</ns1:ServiceType>';
        $xml .= '<ns1:PackagingType>YOUR_PACKAGING</ns1:PackagingType>';
        $xml .= '<ns1:TotalInsuredValue>';
        $xml .= '<ns1:Currency>USD</ns1:Currency>';
        $xml .= '</ns1:TotalInsuredValue>';
        $xml .= '<ns1:Shipper>';
        $xml .= '<ns1:Contact>';
        $xml .= '<ns1:PersonName>' . $sender_name . '</ns1:PersonName>';
        $xml .= '<ns1:CompanyName>Sender Company</ns1:CompanyName>';
        $xml .= '<ns1:PhoneNumber>' . $sender_phone . '</ns1:PhoneNumber>';
        $xml .= '</ns1:Contact>';
        $xml .= '<ns1:Address>';
        $xml .= '<ns1:StreetLines>' . $address_sender . '</ns1:StreetLines>';
        $xml .= '<ns1:City>' . $city_sender . '</ns1:City>';
        $xml .= '<ns1:StateOrProvinceCode>' . $state_sender . '</ns1:StateOrProvinceCode>';
        $xml .= '<ns1:PostalCode>' . $zipcode_sender . '</ns1:PostalCode>';
        $xml .= '<ns1:CountryCode>US</ns1:CountryCode>';
        $xml .= '</ns1:Address>';
        $xml .= '</ns1:Shipper>';
        $xml .= '<ns1:Recipient>';
        $xml .= '<ns1:Contact>';
        $xml .= '<ns1:PersonName>Tung Nguyen</ns1:PersonName>';
        $xml .= '<ns1:CompanyName>T Express Cargo TEST</ns1:CompanyName>';
        $xml .= '<ns1:PhoneNumber>6265866044</ns1:PhoneNumber>';
        $xml .= '</ns1:Contact>';
        $xml .= '<ns1:Address>';
        $xml .= '<ns1:StreetLines>14936 Dillow St</ns1:StreetLines>';
        $xml .= '<ns1:City>WESTMINSTER</ns1:City>';
        $xml .= '<ns1:StateOrProvinceCode>CA</ns1:StateOrProvinceCode>';
        $xml .= '<ns1:PostalCode>92683</ns1:PostalCode>';
        $xml .= '<ns1:CountryCode>US</ns1:CountryCode>';
        $xml .= '<ns1:Residential>true</ns1:Residential>';
        $xml .= '</ns1:Address>';
        $xml .= '</ns1:Recipient>';
        $xml .= '<ns1:ShippingChargesPayment>';
        $xml .= '<ns1:PaymentType>SENDER</ns1:PaymentType>';
        $xml .= '<ns1:Payor>';
        $xml .= '<ns1:ResponsibleParty>';
        $xml .= '<ns1:AccountNumber>510087240</ns1:AccountNumber>';
        $xml .= '</ns1:ResponsibleParty>';
        $xml .= '</ns1:Payor>';
        $xml .= '</ns1:ShippingChargesPayment>';
        $xml .= '<ns1:PickupDetail>';
        $xml .= '<ns1:ReadyDateTime>' . $date . '</ns1:ReadyDateTime>';
        $xml .= '</ns1:PickupDetail>';
        $xml .= '<ns1:RateRequestTypes>LIST</ns1:RateRequestTypes>';
        $xml .= '<ns1:PackageCount>'.count($requestLabelPackageInfor).'</ns1:PackageCount>';
        foreach ($requestLabelPackageInfor as $package) {

            $xml .= '<ns1:RequestedPackageLineItems>';
            $xml .= '<ns1:SequenceNumber>1</ns1:SequenceNumber>';
            $xml .= '<ns1:GroupPackageCount>1</ns1:GroupPackageCount>';
            $xml .= '<ns1:Weight>';
            $xml .= '<ns1:Units>LB</ns1:Units>';
            $xml .= '<ns1:Value>' . (intval($package->weight) * 0.7) . '</ns1:Value>';
            $xml .= '</ns1:Weight>';
            $xml .= '<ns1:Dimensions>';
            $xml .= '<ns1:Length>' . $package->length . '</ns1:Length>';
            $xml .= '<ns1:Width>' . $package->width . '</ns1:Width>';
            $xml .= '<ns1:Height>' . $package->height . '</ns1:Height>';
            $xml .= '<ns1:Units>IN</ns1:Units>';
            $xml .= '</ns1:Dimensions>';
            $xml .= '</ns1:RequestedPackageLineItems>';
        }
        $xml .= '</ns1:RequestedShipment>';
        $xml .= '</ns1:RateRequest>';
        $xml .= '</SOAP-ENV:Body>';
        $xml .= '</SOAP-ENV:Envelope>';
        try{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://wsbeta.fedex.com:443/web-services');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $result_xml = curl_exec($ch);
        $result_xml = str_replace(array(':', '-'), '', $result_xml);
        $result = @simplexml_load_string($result_xml);
        if(strval($result->SOAPENVBody->RateReply->HighestSeverity) == "ERROR"){
            return false;
        }
        $shippingRate = strval($result->SOAPENVBody->RateReply->RateReplyDetails->RatedShipmentDetails->ShipmentRateDetail->TotalNetChargeWithDutiesAndTaxes->Amount);
        }catch(Exception $e){
            echo $e;
        }
        return array('request_package_id' => $requestLabelPackageInfor[0]->id, 'shippingRate' => $shippingRate, 'package' => $requestLabelPackageInfor);
    }

    public function submitRate($id)
    {
        $result = array();
        $RequestLabel = $this->requestLabelRepository->findWithoutFail($id);
        if (!$RequestLabel) {
            return $this->sendError('RequestLabel is not pending status!');
        }
        $requestLabelInfor          = $this->requestLabelService->getRequestLabelInfor($id);
        $delivery_company = $requestLabelInfor->delivery_company; // 1: ups, 2: fedex
        if ($delivery_company == 1) {
            $result = $this->rate_ups($id, new Request());
        } else {
            $result = $this->rate_fedex($id, new Request());
        }
        if(!$result){
            return $this->sendError('Created request label failed!!!',400);
        }
        $this->requestLabelPackageRepository->update(array('shippingRate' => $result['shippingRate']), $result['request_package_id']);
        return $this->sendResponse($result, 'Submit rate successfully');
    }
    function vn_to_str($str)
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }
        //$str = str_replace(' ','_',$str);

        return $str;
    }

    public function gifToPdf($label_file, $tracking_number)
    {
        $image = imagecreatefromgif(resource_path('../public_html/'.$label_file));
        $rotate = imagerotate($image, '-90', 0);
        imagejpeg($rotate, base_path('public_html/storage/tracking_number/ups/'.$tracking_number.'.jpg'));
        $html = '';
        $html .= '<link type="text/css" href="' . base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css') . '" rel="stylesheet" />';
        $html .= '<div style="text-align:center"><img style="width:70%;height:auto" src="'.base_path('public_html/storage/tracking_number/ups/'.$tracking_number.'.jpg').'" /></div>';
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->set_base_path(base_path('public_html/assets/global/plugins/bootstrap/css/bootstrap.min.css'));
        $dompdf->render();
        $fp = fopen('storage/tracking_number/ups/' . $tracking_number . ".pdf", 'wb');
        fwrite($fp, $dompdf->output());
        fclose($fp);
        \File::delete(resource_path('../public_html/storage/tracking_number/ups/'.$tracking_number.'.jpg'));
        return true;
    }

    protected function validCsrf($token, $request)
    {
        $isCsrf = false;
        $isXsrf = false;
        if($csrf = $token) {
            $isCsrf = hash_equals(
                $token, (string) $request->header('X-CSRF-TOKEN')
            );
            if($request->hasHeader('X-XSRF-TOKEN')) {
                $isXsrf = $this->encrypter->decrypt($request->header('X-XSRF-TOKEN')) === $csrf;
            }
        }
        return $isCsrf || $isXsrf;
    }
}
