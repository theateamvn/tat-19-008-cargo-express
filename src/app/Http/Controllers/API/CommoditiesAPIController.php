<?php

namespace App\Http\Controllers\API;
use App\Http\Requests\API\CreateCommoditiesAPIRequest;
use App\Http\Requests\API\UpdateCommoditiesAPIRequest;
use App\Criteria\ListCommoditiesCriteria;
use App\Models\Commodities;
use App\Repositories\CommoditiesRepository;
use App\Services\CommoditiesService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class CommoditiesAPIController extends AppBaseController
{
    private $commoditiesRepository;

    private $commoditiesService;

    public function __construct(CommoditiesRepository $commoditiesRepo, CommoditiesService $commoditiesService)
    {
        $this->commoditiesRepository = $commoditiesRepo;
        $this->commoditiesService = $commoditiesService;
    }

    public function pagination(Request $request)
    {
        $this->commoditiesRepository->pushCriteria(new ListCommoditiesCriteria($request));
        $this->commoditiesRepository->pushCriteria(new RequestCriteria($request));
        $this->commoditiesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commodities = $this->commoditiesRepository->paginate();
        return $this->sendResponse($commodities->toArray(), 'Commodities retrieved successfully');
    }
    public function store(CreateCommoditiesAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['ip'] = $request->ip();
        $input['updated_by'] = $userId;
        $commodities = $this->commoditiesService->create($input);
        return $this->sendResponse($commodities->toArray(), 'Commodities saved successfully');
    }
    // function update
    public function update($id, UpdateCommoditiesAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        $recipient = $this->commoditiesRepository->findWithoutFail($id);

        if (empty($recipient)) {
            return $this->sendError('Commodities not found');
        }

        $input['updated_by'] = $userId;
        $recipient = $this->commoditiesRepository->update($input, $id);

        return $this->sendResponse($recipient->toArray(), 'Commodities updated successfully');
    }
    // function delete
    public function destroy($id)
    {
        $commo = $this->commoditiesRepository->findWithoutFail($id);

        if (empty($commo)) {
            return $this->sendResponse('Commodities not found');
        }
        //update shipment code
        $input_ = array( 'status' => -1);
        $this->commoditiesRepository->update($input_, $id);
        return $this->sendResponse($id, 'Commodities deleted successfully');
    }
    public function load_customer()
    {
        $customer_list = $this->recipientService->getCustomerList();
        return $this->sendResponse($customer_list, 'Customer list get successfully');
    }
    public function load_cate()
    {
        $list = $this->commoditiesService->getCateList();
        return $this->sendResponse($list, 'Cate list list get successfully');
    }
    public function load_brand($id)
    {
        $list = $this->commoditiesService->getBrandList($id);
        return $this->sendResponse($list, 'Brand list get successfully');
    }
    public function load_charges($id)
    {
        $list = $this->commoditiesService->getChargesList($id);
        return $this->sendResponse($list, 'Charges list get successfully');
    }
    public function add_from_shipment(Request $request)
    {
        $input = array();
        $userId                     = $this->getCurrentUser()->id;
        $_input                      = $request->all();
        $input['created_by']        = $userId;
        $input['ip']                = $request->ip();
        $input['updated_by']        = $userId;
        $input['commodity_cate']    = isset($_input['cate']) ? $_input['cate'] : 0 ;
        $input['commodity_name']    = isset($_input['commodity_name_new']) ? $_input['commodity_name_new'] : 0;
        $input['commodity_brand']   = $_input['commodity_brand_new'];
        $commodities = $this->commoditiesService->create($input);
        return $this->sendResponse($commodities->toArray(), 'Commodities saved successfully');
    }


    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

}
