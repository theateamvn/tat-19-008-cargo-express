<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Store;
use App\Models\Review;
use App\Repositories\InviteRepository;
use App\Repositories\ReviewProviderRepository;
use App\Services\Bijective;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Request;
use DB;

class ApiGetReviews extends Controller
{

    public function get(Request $request){
      $word = $request->key_word;
      $key_word = '';
      if(!empty($request->key_word)){
        $key_word = $word;
      }
      if($key_word){
        $data = DB::table('reviews')
              ->leftJoin('stores', 'reviews.store_id', '=', 'stores.id')
              ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug')
              ->where('content', '<>', '')
              ->where('stores.name', 'like', '%' . $key_word . '%')
              ->orderBy(DB::raw('RAND()'))
              ->paginate(12);
        return $data;
      }
      else{
        $data = DB::table('reviews')
              ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug')
              ->leftJoin('stores', '.store_id', '=', 'stores.id')
              ->where('content', '<>', "''")
              ->orderBy(DB::raw('RAND()'))
              ->paginate(12);
        return $data;
      }

    }

    public function detail(Request $request){
      $data_response = array();
      $id = $request->id;
      $store_id = 0;
      $data_detail = DB::table('reviews')
            ->leftJoin('stores', 'reviews.store_id', '=', 'stores.id')
            ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug')
            ->where('reviews.id', '=', $id)->get();
            foreach ($data_detail as $key => $value) {
              $store_id = $value->store_id;
            }
      $data_response['detail'] = $data_detail;
      $data_related = DB::table('reviews')
            ->leftJoin('stores', 'reviews.store_id', '=', 'stores.id')
            ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug', 'stores.slug')
            ->where('reviews.id', '!=', $id)
            ->where('reviews.store_id', '=', $store_id)
            ->where('content', '<>', '')
            ->orderBy('reviews.id', 'desc')
            ->skip(10)->take(6)->get();
      $data_response['related'] = $data_related;
      // avg rating 
      $avgRating = DB::table('reviews')->where('store_id', $store_id)->avg('rating');
      $data_response['avgRating'] = round($avgRating, 1);
      // total review
      $data_response['totalReview'] = DB::table('reviews')->where('store_id', $store_id)->count();

      return $data_response;

    }
}
