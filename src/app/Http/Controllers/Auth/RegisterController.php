<?php

namespace App\Http\Controllers\Auth;

use App\Models\InviteMessage;
use App\Models\Setting;
use App\Models\Store;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Stripe\Plan;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/register-thanks';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, array_merge(\App\Models\User::$rules, Store::$rules));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User($data);
        $user->password = bcrypt($data['password']);
        $user->role = User::ROLE_USER;
        $user->save();
        return $user;
    }

    public function register(Request $request)
    {
        $input = $request->all();
        $this->validator($input)->validate();
        $user = $this->create($input);
        $store = $this->createStore($input);
        $this->subscriptionPackage($request, $user);
        $user->storeUsers()->attach($store->id);
        event(new Registered($user));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    private function createStore(array $data)
    {
        return Store::create($data);
    }

    /**
     * @param Request $request
     * @param User $user
     */
    private function subscriptionPackage($request, $user)
    {
        $stripeToken = $request->get('stripeToken');
        if ($stripeToken) {
            $package = $request->get('package');
            $user->newSubscription('main', $package)->create($stripeToken);
            $user->status = User::STATUS_ACTIVATED;
            $user->save();
        }
    }

    public function showRegistrationForm()
    {
//        $plans = Plan::all()['data'];
//        $plans = array_where($plans, function ($value, $key) {
//            return str_contains(strtolower($value['name']),'pushmeup');
//        });
//        usort($plans,function($a,$b){
//            if($a['amount']==$b['amount']) return 0;
//            return ($a['amount'] < $b['amount']) ? -1 : 1;
//        });
        $plans = config('services.stripe.packages');
        return view('auth.register', [
            'plans' => $plans
        ]);
    }


}
