<?php

namespace App\Http\Controllers\Auth;

use Activity;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if (!$this->hasActivatedAccount($request)) {
                return $this->sendUnactivatedResponse($request);
            }
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt([
                'username' => $request->get($this->username()),
                'password' => $request->get('password')
            ], $request->has('remember'))
            || $this->guard()->attempt([
                'email' => $request->get($this->username()),
                'password' => $request->get('password')
            ], $request->has('remember'));
    }

    private function hasActivatedAccount(Request $request)
    {
        $username = $request->get($this->username());
        $user = User::where('email', $username)->orWhere('username', $username)->first();

        return isset($user) && $user->status == User::STATUS_ACTIVATED;
    }

    public function logout(Request $request)
    {
        Activity::log("User logged out");
        $this->clearCurrentAuth($request);
        return redirect('/login');
    }

    private function sendUnactivatedResponse(Request $request)
    {
        $this->clearCurrentAuth($request);

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.pending'),
            ]);
    }

    protected function authenticated(Request $request, $user)
    {
        Activity::log('User logged in');
        if($user->role === User::ROLE_ADMIN){
           // $redirectPath = '/admin/dashboard';
            $redirectPath = '/admin/users';
        } else if($user->role === User::ROLE_AGENT) {
            $redirectPath = '/admin/dashboard-agent';
        } else {
            $redirectPath = '/dashboard';
        }
        return redirect($redirectPath);
    }

    /**
     * @param Request $request
     */
    protected function clearCurrentAuth(Request $request)
    {
//        Auth::guard('api')->logout();
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
    }
}
