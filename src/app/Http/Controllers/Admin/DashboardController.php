<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function accessUser($userId,$redirectTo = '/settings'){
        $user = User::findOrFail($userId);
        Auth::login($user);
        return redirect($redirectTo);
    }
}
