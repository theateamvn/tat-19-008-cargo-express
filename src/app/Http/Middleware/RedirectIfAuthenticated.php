<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   * @param  string|null $guard
   * @return mixed
   */
  public function handle($request, Closure $next, $guard = null)
  {
    if (Auth::guard($guard)->check()) {
      $role = Auth::guard($guard)->user()->role;
      if($role ===  \App\Models\User::ROLE_ADMIN){
            $redirectPath = '/admin/dashboard';
        } else if($role ===  \App\Models\User::ROLE_AGENT) {
            $redirectPath = '/admin/dashboard-agent';
        } else {
            $redirectPath = '/dashboard';
        }
      //$redirectPath = $role === \App\Models\User::ROLE_ADMIN || $role === \App\Models\User::ROLE_AGENT?'/admin/dashboard':'/dashboard';
      return redirect($redirectPath);
    }

    return $next($request);
  }
}
