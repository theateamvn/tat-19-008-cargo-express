<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->role == config('settings.roles.admin') || Auth::user()->role == config('settings.roles.agent'))) {
            return $next($request);
        }
        if($request->ajax()){
            throw new AccessDeniedHttpException();
        }
        return redirect()->route('login');
    }
}
