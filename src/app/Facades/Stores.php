<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/2/2016
 * Time: 9:07 AM
 */

namespace App\Facades;


use App\Services\StoreService;
use Illuminate\Support\Facades\Facade;

class Stores extends Facade
{
    protected static function getFacadeAccessor()
    {
        return StoreService::class;
    }

}