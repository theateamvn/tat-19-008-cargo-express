<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/12/2017
 * Time: 2:45 PM
 */

namespace App\Facades;


use App\Services\ProxyService;
use Illuminate\Support\Facades\Facade;

class Proxy extends Facade
{
    protected static function getFacadeAccessor(){
        return ProxyService::class;
    }
}