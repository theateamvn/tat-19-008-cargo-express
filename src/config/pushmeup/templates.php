<?php
/**
 * Created by PhpStorm.
 * User: Lai Vu
 * Date: 12/19/2016
 * Time: 2:01 PM
 */

return [
    'mms' => [
        'content' => "Hi {{full_name}},\nThank you for choosing {{business_name}}. Can you take 30 seconds and leave us a quick review? The link below makes it easy: {{review_link}}.",
        'mediaUrl' => '/assets/global/img/mms_image_1.jpg'
    ],
    'sms' => "Hi {{full_name}},\nThank you for choosing {{business_name}}. Can you take 30 seconds and leave us a quick review? The link below makes it easy: {{review_link}}.",
    'email' => '<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"><div style="background-color:#f5f5f5;width:100%;padding:20px;font-family:\'Open Sans\';"> <div class="wrapper" style="width: 700px;background-color:#ffffff;margin:0 auto"> <div class="header" style="background-color: #5C98BD;-webkit-border-top-left-radius: 1px;-webkit-border-top-right-radius: 2px;-moz-border-radius-topleft: 1px;-moz-border-radius-topright: 2px;border-top-left-radius: 10px;border-top-right-radius: 10px;padding: 15px;"> <p style="margin: auto 0;display: block;color: white;font-weight: lighter;font-size: 20px;">{{business_name}}</p></div><div class="content" style="padding: 10px 30px 20px 30px;"> <div class="content-wrapper"> <p style="color: #A1A1A1;">Dear <b style="color: #848484;text-decoration: none;">{{full_name}},</b></p><p style="color: #A1A1A1;">Thank you for choosing <b style="color: #848484;text-decoration: none;">{{business_name}}</b>, can you take 30 seconds and leave us a quick review?</p><p style="color: #A1A1A1;"> Our review link: <b style="color: #848484;text-decoration: none;"><a class="review-link" href="{{review_link}}" style="color: #848484;text-decoration: underline;">{{review_link}}</a></b> </p><br><p style="color: #A1A1A1;"> My contact information is listed below. We look forward to speaking with you soon! </p><p style="color: #A1A1A1;"> Thank you so much, </p><hr style="border: 0; height: 1px; background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); "> <p style="color: #A1A1A1;"> <b style="color: #848484;text-decoration: none;">{{business_name}}</b></p> <p style="color: #A1A1A1;">{{business_address}}</p></div></div></div><div class="ft" style="display: block;width: 100%;clearfix: both;;text-align:center"> <div class="footer-left" style="display: block;padding: 10px;"><a href="{{config(\'app.url\')}}" style="color: #848484;text-decoration: none;padding-top: 10px">www.PushMeUp.net | Push your brand UP today</a> </div></div></div>'
];
