<?php

return [
    /*
    |--------------------------------------------------------------------------
    | UPS Credentials
    |--------------------------------------------------------------------------
    |
    | This option specifies the UPS credentials for your account.
    | You can put it here but I strongly recommend to put thoses settings into your
    | .env & .env.example file.
    |
    */
    'access_key' => env('UPS_ACCESS_KEY', 'AD7A19F242131A95'),
    'user_id'    => env('UPS_USER_ID', 'texpresscargo'),
    'password'   => env('UPS_PASSWORD', 'Chuv!tcon511312'),
    'sandbox'    => env('UPS_SANDBOX', false), // Set it to false when your ready to use your app in production.
];
