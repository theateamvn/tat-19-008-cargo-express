<?php

return [

  'roles' => [
    'user' => 0,
    'admin' => 1,
    'agent' => 2
  ],

  'storeCategories' => [
    'Resort'        => 0,
    'Hotel'         => 1,
    'Restaurent'    => 2
  ]
];
