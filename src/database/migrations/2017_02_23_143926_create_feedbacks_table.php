<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores');
            $table->text('content');
            $table->timestamps();
        });

        Schema::table('settings',function(Blueprint $table){
           $table->string('email_nofify_feedback')->nullable();
            $table->string('sms_esms_brand_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
        Schema::table('settings',function (Blueprint $table) {
            $table->dropColumn('email_nofify_feedback');
            $table->dropColumn('sms_esms_brand_name');
        });
    }
}
