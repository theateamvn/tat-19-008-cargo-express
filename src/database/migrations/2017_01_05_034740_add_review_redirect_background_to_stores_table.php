<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReviewRedirectBackgroundToStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->string('review_redirect_background')->nullable();
        });

        Schema::create('master_redirect_background', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->string('url');
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_redirect_background');
        Schema::table('stores', function (Blueprint $table) {
            $table->dropColumn('review_redirect_background');
        });
    }
}
