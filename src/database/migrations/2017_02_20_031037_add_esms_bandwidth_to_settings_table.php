<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEsmsBandwidthToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('sms_gateway',45)->default('twilio');

            $table->string('sms_bandwidth_phone')->nullable();
            $table->string('sms_bandwidth_user_id')->nullable();
            $table->string('sms_bandwidth_api_token')->nullable();
            $table->string('sms_bandwidth_api_secret')->nullable();

            $table->string('sms_esms_api_token')->nullable();
            $table->string('sms_esms_api_secret')->nullable();
            $table->string('sms_esms_sms_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('sms_gateway');

            $table->dropColumn('sms_bandwidth_phone');
            $table->dropColumn('sms_bandwidth_user_id');
            $table->dropColumn('sms_bandwidth_api_secret');
            $table->dropColumn('sms_bandwidth_api_token');

            $table->dropColumn('sms_esms_api_token');
            $table->dropColumn('sms_esms_api_secret');
            $table->dropColumn('sms_esms_sms_type');
        });
    }
}
